package com.xiaomaoguai.fcp.service.impl;

import com.xiaomaoguai.fcp.dao.MatrixRequestLogRepository;
import com.xiaomaoguai.fcp.dto.MatrixRequestLog;
import com.xiaomaoguai.fcp.service.MatrixRequestLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @fileName: MatrixRequestLogServiceImpl.java
 * @author: WeiHui
 * @date: 2018/10/22 20:35
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Service
public class MatrixRequestLogServiceImpl implements MatrixRequestLogService {

	@Resource
	private MatrixRequestLogRepository matrixRequestLogRepository;

	@Override
	public List<MatrixRequestLog> findByThirdUserNo(String thirdUserNo) {
		return null;
	}

	@Override
	public List<MatrixRequestLog> findByString(String query) {
		return null;
	}

}
