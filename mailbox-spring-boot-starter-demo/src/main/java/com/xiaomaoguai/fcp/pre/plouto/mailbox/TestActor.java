package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.dh.mailbox2.wrapper.AbstractActor;
import com.dh.mailbox2.wrapper.MSG;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @fileName: TestActor.java
 * @author: WeiHui
 * @date: 2018/11/6 11:07
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Component
public class TestActor extends AbstractActor {

	@Override
	public void onReceive(MSG msg) {
		log.info("====>{}", msg.getData());
	}

}
