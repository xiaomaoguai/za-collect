package com.xiaomaoguai.fcp.fcp.dto.req;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @fileName: CardInfo.java
 * @author: WeiHui
 * @date: 2018/9/13 13:51
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class CardInfo {

	/**
	 * 发卡行名称
	 */
	private String bankName;

	/**
	 * 银行编码
	 */
	private String bankCode;

	/**
	 * 银行开户姓名
	 */
	@JSONField(serialize = false)
	private String userName;

	/**
	 * 银行开户人身份证号
	 */
	@JSONField(serialize = false)
	private String certNo;

	/**
	 * 银行账号
	 */
	@NotBlank(message = "银行账号不能为空")
	private String bankCardNo;

	/**
	 * 银行预留手机号
	 */
	@JSONField(serialize = false)
	private String cardPhone;

	/**
	 * 是否默认卡
	 * 0-否
	 * 1-是
	 */
	@NotNull(message = "默认卡标识不能为空")
	@Range(min = 0, max = 1, message = "默认卡标识，必须0-1")
	private Integer isDefault;

	/**
	 * 卡类型
	 * 1-借记
	 * 2-贷记
	 */
	private Integer cardType;

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
