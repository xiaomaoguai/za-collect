package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.response;

import io.netty.buffer.ByteBufOutputStream;
import io.netty.handler.codec.http.DefaultFullHttpResponse;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import java.io.IOException;

public class NettyHttpServletOutputStream extends ServletOutputStream {


	private ByteBufOutputStream out;

	private boolean flushed = false;

	public NettyHttpServletOutputStream(DefaultFullHttpResponse response) {
		this.out = new ByteBufOutputStream(response.content());
	}

	@Override
	public void write(int b) throws IOException {
		this.out.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		this.out.write(b);
	}

	@Override
	public void write(byte[] b, int offset, int len) throws IOException {
		this.out.write(b, offset, len);
	}

	@Override
	public void flush() throws IOException {
		this.flushed = true;
	}

	public void resetBuffer() {
		this.out.buffer().clear();
	}

	public boolean isFlushed() {
		return flushed;
	}

	public int getBufferSize() {
		return this.out.buffer().capacity();
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public void setWriteListener(WriteListener writeListener) {
		// TODO Auto-generated method stub

	}
}
