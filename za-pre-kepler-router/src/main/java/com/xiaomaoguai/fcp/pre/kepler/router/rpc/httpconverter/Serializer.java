package com.xiaomaoguai.fcp.pre.kepler.router.rpc.httpconverter;

import java.io.InputStream;

public interface Serializer {
	
	public byte[] writeObject(Object obj) ;
	
	public Object readObject(InputStream inputStream);
	
	public Object readObject(byte[] bytes);

}
