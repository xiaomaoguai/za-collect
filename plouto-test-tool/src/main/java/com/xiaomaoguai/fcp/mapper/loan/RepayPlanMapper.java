/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.loan;

import com.xiaomaoguai.fcp.domain.RepayPlan;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * The Interface RepayPlanMapper.
 */
public interface RepayPlanMapper {

	/**
	 * 新写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(RepayPlan record);

	/**
	 * 动态字段,写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(RepayPlan record);

	/**
	 * Select first plan by user id.
	 *
	 * @param userId the user id
	 * @return the repay plan
	 */
	RepayPlan selectFirstPlanByUserId(@Param("userId") Long userId, @Param("agreeRepayDate") Date agreeRepayDate);

	/**
	 * Select by loan outer no.
	 *
	 * @param loanOuterNo the loan outer no
	 * @param userId      the user id
	 * @param status      the status
	 * @return the list
	 */
	List<RepayPlan> selectByLoanOuterNo(@Param("loanOuterNo") String loanOuterNo, @Param("userId") Long userId,
										@Param("status") Integer status);

	/**
	 * Select un repay.
	 *
	 * @param loanOuterNo the loan outer no
	 * @param userId      the user id
	 * @return the list
	 */
	List<RepayPlan> selectUnRepay(@Param("loanOuterNo") String loanOuterNo, @Param("userId") Long userId);

	/**
	 * Count un repay amount.
	 *
	 * @param userId         the user id
	 * @param agreeRepayDate the agree repay date
	 * @return the repay plan
	 */
	RepayPlan countUnRepayAmount(@Param("userId") Long userId, @Param("agreeRepayDate") Date agreeRepayDate);

	/**
	 * 根据userId和loanInnerNo和期数查询还款计划
	 *
	 * @param userId        用户id
	 * @param loanInnerNo   内部借款流水号
	 * @param installmentNo 期数
	 * @return 还款计划
	 */
	RepayPlan selectByUserIdAndInnerNoAndInstaNo(@Param("userId") Long userId, @Param("loanInnerNo") String loanInnerNo, @Param("installmentNo") Integer installmentNo);

	/**
	 * Query page by user id.
	 *
	 * @param userId         the user id
	 * @param loanOuterNo    the loan outer no
	 * @param status         the status
	 * @param productCode    the product code
	 * @param agreeRepayDate the agree repay date
	 * @param pageSize       the page size
	 * @param offset         the offset
	 * @return the list
	 */
	List<RepayPlan> QueryPageByUserId(@Param("userId") Long userId, @Param("loanOuterNo") String loanOuterNo,
									  @Param("status") Integer status, @Param("productCode") String productCode,
									  @Param("agreeRepayDate") Date agreeRepayDate, @Param("pageSize") Integer pageSize,
									  @Param("offset") Integer offset);

	/**
	 * Update after repay.
	 *
	 * @param repayPlan the repay plan
	 * @param status    the status
	 * @return the int
	 */
	int updateAfterRepay(@Param("repayPlan") RepayPlan repayPlan, @Param("status") Integer status);

	/**
	 * Gets the pre repay plan.
	 *
	 * @param loanOuterNo   the loan outer no
	 * @param userId        the user id
	 * @param installmentNo the installment nos
	 * @return the pre repay plan
	 */
	List<RepayPlan> getPreRepayPlan(@Param("loanOuterNo") String loanOuterNo, @Param("userId") Long userId,
									@Param("installmentNo") String installmentNo);

	List<Integer> getInstallmentNos(@Param("userId") Long userId, @Param("loanOuterNo") String loanOuterNo,
									@Param("status") Integer status, @Param("agreeRepayDate") Date agreeRepayDate);

	int deleteByUserId(Long userId);
}