package com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums;

public enum SerializerType {
	KRYO,
	HESSIAN;
}
