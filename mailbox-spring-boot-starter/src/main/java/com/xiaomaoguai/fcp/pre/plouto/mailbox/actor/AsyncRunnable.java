package com.xiaomaoguai.fcp.pre.plouto.mailbox.actor;

/**
 * @fileName: AsyncRunnable.java
 * @author: WeiHui
 * @date: 2018/10/16 17:45
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface AsyncRunnable<T> {

	/**
	 * 异步执行
	 *
	 * @param data 执行数据
	 */
	void run(T data);

}
