package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.creditcore.preloan.dto.CreditApplyDTO
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:50
 * @since JDK 1.8
 */
class CreditApplyApi_creditListQuery implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        List<CreditApplyDTO> list = new ArrayList()
    //        CreditApplyDTO creditApplyDTO = new CreditApplyDTO()
    //        creditApplyDTO.setApplyStatus(2)
    //        list.add(creditApplyDTO)

        def resp = new BaseResp()
        resp.setResult(list)
        resp.setResultCode(ResultCode.SUCCESS)

        return resp
    }

}
