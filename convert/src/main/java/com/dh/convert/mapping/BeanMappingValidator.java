package com.dh.convert.mapping;

import com.dh.convert.rule.validator.AbstractValidator;

public class BeanMappingValidator {

	private AbstractValidator validator;
	private String errorMsg;
	public AbstractValidator getValidator() {
		return validator;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setValidator(AbstractValidator validator) {
		this.validator = validator;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
