package com.xiaomaoguai.fcp.pre.kepler.router.async;

public class RejectException extends RuntimeException {

	private static final long serialVersionUID = 2036023578393136705L;

	public RejectException(String message, Throwable cause) {
		super(message, cause);
	}

	public RejectException(String message) {
		super(message);
	}

	public RejectException(Throwable cause) {
		super(cause);
	}
}
