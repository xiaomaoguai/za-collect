package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core;

import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;

/**
 * 配置加载内置Netty容器的工厂类Bean。
 * 在当前上下文缺少ServletWebServerFactory接口实现类时（即缺少内置Servlet容器），加载EmbeddedNettyFactory
 * SpringBoot项目在引入这个maven依赖，并且排除了内置tomcat依赖、且没引入其他servlet容器（如jetty）时，就可以通过工厂类加载并启动netty容器了。
 *
 */
//springboot1.0当前Spring容器中不存在EmbeddedServletContainerFactory接口的实例
//springboot2.0改为ServletWebServerFactory
public class EmbeddedNettyAutoConfiguration {

//	@Configuration
//	@ConditionalOnMissingBean(value = ServletWebServerFactory.class, search = SearchStrategy.CURRENT)
	public static class EmbeddedNetty {

//		@Bean
		public ConfigurableServletWebServerFactory embeddedNettyFactory() {
			return new EmbeddedNettyFactory();
		}
	}
		
}
