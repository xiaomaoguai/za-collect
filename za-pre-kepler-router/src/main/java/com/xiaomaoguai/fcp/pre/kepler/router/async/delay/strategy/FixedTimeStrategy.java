package com.xiaomaoguai.fcp.pre.kepler.router.async.delay.strategy;



/**
 * 固定间隔时间
 * 类FixedTimeStrategy.java的实现描述： 类实现描述
 * @author DH 2017年5月7日 上午11:09:54
 */
public class FixedTimeStrategy implements RetryStrategy {
    
    private final long sleepTime; 
    
    public FixedTimeStrategy(long sleepTime){
        this.sleepTime = sleepTime;
        
    }
    @Override
    public Long interval(int retrytimes) {

        return sleepTime;
    }

}
