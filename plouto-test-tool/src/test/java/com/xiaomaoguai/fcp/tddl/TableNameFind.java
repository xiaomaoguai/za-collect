package com.xiaomaoguai.fcp.tddl;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/19 15:48
 * @since JDK 1.8
 */
public class TableNameFind {


	/**
	 *     <bean id="user_card" class="com.taobao.tddl.interact.rule.TableRule" init-method="init">
	 *         <property name="dbNamePattern" value="FCP_FE_BIZ_KEPLER_TST_{00}_GROUP" />
	 *         <property name="dbRuleArray">
	 *             <value>( ( Math.abs(#user_id,1,4#.longValue()) ) %4).intdiv(2)</value>
	 *         </property>
	 *         <property name="tbNamePattern" value="user_card_{0000}" />
	 *         <property name="tbRuleArray">
	 *             <value>( Math.abs(#user_id,1,4#.longValue()) ) %4</value>
	 *         </property>
	 *         <property name="allowFullTableScan" value="false" />
	 *     </bean>
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		Long userId=3505295878L;
		long l = userId /4 % 2;
		System.out.println(l);
	}

}
