package com.xiaomaoguai.fcp.YND.test;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/23 15:04
 * @since JDK 1.8
 */
@Getter
@Setter
public class RepayApplyReq extends BaseRequest {


	private static final long serialVersionUID = 782438780241325386L;

	/**
	 * 还款批次号 默认0 批量还款时使用
	 */
	private String batchNo = "0";

	/**
	 * 合作方借款协议号
	 */
	@NotBlank(message = "借款协议号不能为空")
	private String loanOuterNo;

	/**
	 * 合作方还款流水号
	 */
	@NotBlank(message = "还款流水号不能为空")
	private String repayOuterNo;

	/**
	 * 本次总的还款金额
	 */
	@NotNull(message = "还款总金额不能为空")
	@Digits(integer = 15, fraction = 2, message = "实还金额精度错误")
	private BigDecimal repayAmount;

	/**
	 * 是否提前结清 0 否 1 是
	 */
	@NotNull(message = "提前结清标识不能为空")
	@Range(min = 0, max = 1, message = "提前结清标识错误，必须0-1")
	private Integer isEarlySettle;

	private BindBankInfo cardInfo;

	/**
	 * 还款计划冲销明细
	 */
	@NotNull(message = "还款计划不能为空")
	@Valid
	private List<RepayDetailReq> repayWriteOffList;

	public boolean isBatchRepay() {
		if ("0".equals(batchNo)) {
			return false;
		}
		return true;
	}

}
