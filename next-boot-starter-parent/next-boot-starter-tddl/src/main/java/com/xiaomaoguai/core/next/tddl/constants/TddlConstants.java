/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.tddl.constants;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;

/**
 * TDDL 常量
 *
 * @author chenyao
 * @since 2018年5月17日 上午11:31:29
 */
public class TddlConstants {
    /**
     * TDDL配置前缀，支持单个数据库
     */
    public static final String CONFIG_PREFIX       = CoreConstants.BOOT_CONFIG_PREFIX + ".tddl";
    /**
     * TDDL配置前缀，支持多个数据库
     */
    public static final String MULTI_CONFIG_PREFIX = CoreConstants.BOOT_CONFIG_PREFIX + ".tddl-multi";
}
