package com.xiaomaoguai.fcp.pre.kepler.sftp;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author WeiHui
 * @date 2019/3/20 20:02
 * @since JDK 1.8
 */
@SpringBootApplication
public class OssClientApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(OssClientApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("http://localhost:9090/sts");
	}

}
