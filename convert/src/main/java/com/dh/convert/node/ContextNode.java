package com.dh.convert.node;

import java.util.HashMap;
import java.util.Map;
import com.dh.convert.constants.TypeEnum;
import com.fasterxml.jackson.databind.JsonNode;

public class ContextNode extends MapNode {
	
	@Override
	public boolean setValue(Object o, Object value) {
		return Boolean.TRUE;
	}

	public TreeNode build(){
		this.nodeName = TypeEnum.CONTEXT.getFlag();
		this.currentClass = HashMap.class;
		this.typeName = this.currentClass.getSimpleName();
		this._isMap = Boolean.TRUE;
		this._isContext = Boolean.TRUE;
		return this;
	}
	
	@Override
	public Object genObject(JsonNode jn, Object targetObject,Object currentObject,Map<String,Object> context) throws InstantiationException, IllegalAccessException {
		return getObject(jn, targetObject, context, context);
	}
	
}
