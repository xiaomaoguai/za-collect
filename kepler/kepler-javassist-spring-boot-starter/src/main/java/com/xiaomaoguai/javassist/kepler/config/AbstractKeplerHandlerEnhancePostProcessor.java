package com.xiaomaoguai.javassist.kepler.config;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/20 15:10
 * @since JDK 1.8
 */
public abstract class AbstractKeplerHandlerEnhancePostProcessor extends AbstractKeplerEnhancePostProcessor {

	protected Map<String, Object> getAllHandler() {
		return applicationContext.getBeansWithAnnotation(HandlerAnnotation.class);
	}

	protected Type getParameterizedType(Class<?> targetClass) {
		final Type genericSuperclass = targetClass.getGenericSuperclass();
		if (genericSuperclass == null) {
			return null;
		} else if (genericSuperclass instanceof ParameterizedType) {
			return genericSuperclass;
		} else {
			return getParameterizedType((Class<?>) genericSuperclass);
		}
	}

}
