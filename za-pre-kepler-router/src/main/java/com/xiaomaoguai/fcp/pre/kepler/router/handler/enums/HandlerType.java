package com.xiaomaoguai.fcp.pre.kepler.router.handler.enums;

public enum HandlerType {
	RPC,
	LOCAL;
}
