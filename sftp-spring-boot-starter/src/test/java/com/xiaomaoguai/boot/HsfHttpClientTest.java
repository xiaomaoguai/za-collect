//package com.xiaomaoguai.boot;
//
//import com.alibaba.fastjson.JSON;
//import com.xiaomaoguai.fcp.utils.HttpClientUtils;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.ByteArrayEntity;
//import org.apache.http.util.EntityUtils;
//import org.junit.Test;
//
//import java.nio.charset.Charset;
//import java.util.HashMap;
//import java.util.Map;
//
///**
// * @fileName: HsfHttpClientTest.java
// * @author: WeiHui
// * @date: 2018/8/6 17:53
// * @version: v1.0.0
// * @since JDK 1.8
// */
//public class HsfHttpClientTest {
//
//    /**
//     * 参数类型
//     */
//    private final static String ARGS_TYPE = "ArgsTypes";
//
//    /**
//     * 参数值
//     */
//    private final static String ARGS_VALUES = "ArgsObjects";
//
//    @Test
//    public void Test() throws Exception {
//
//        String url = "http://10.253.107.55:58563/com.xiaomaoguai.fcp.pre.allin.common.halo.RPCApi:1.0.0.38448fddzx/genericMethod";
//
//        Object[] paramsTypes = new Object[]{String.class};
//
//        Object[] paramsValues = new Object[]{"{\n" +
//                "  \"apiVersion\": \"1.0.0\",\n" +
//                "  \"thirdUserNo\": \"340103196111091524\",\n" +
//                "  \"reqDate\": \"2018-08-02 16:35:42\",\n" +
//                "  \"reqNo\": \"153319894296454034\",\n" +
//                "  \"apiName\": \"underwriteApi2\",\n" +
//                "  \"transName\": \"underWriteApplyCheck\",\n" +
//                "  \"productCode\": \"FDDZX\",\n" +
//                "  \"loanOuterNo\": \"ZLDDK2018080114743\",\n" +
//                "  \"loanAmount\": \"300000.00\",\n" +
//                "  \"loanDate\": \"2018-08-02 16:35:42\",\n" +
//                "  \"installmentNo\": 3,\n" +
//                "  \"installmentRate\": \"0.1000\",\n" +
//                "  \"repayWay\": 1,\n" +
//                "  \"premiumType\": 2,\n" +
//                "  \"fundOrgNo\": \"LJZXT\",\n" +
//                "  \"premium\": \"625.00\",\n" +
//                "  \"gracePeriod\": 0,\n" +
//                "  \"fixedRepayDate\": 15,\n" +
//                "  \"minFirstRepayDays\": 1,\n" +
//                "  \"interestWay\": 6,\n" +
//                "  \"dueDate\": \"2018-10-15\",\n" +
//                "  \"finalPeriodInterestWay\": 1,\n" +
//                "  \"loanUserInfo\": {\n" +
//                "    \"userName\": \"张三\",\n" +
//                "    \"certType\": 1,\n" +
//                "    \"certNo\": \"340103196111091524\",\n" +
//                "    \"phoneNo\": \"18100000000\",\n" +
//                "    \"email\": \"18186953698@qq.com\",\n" +
//                "    \"address\": \"北京|东城区|鸿福路11号\"\n" +
//                "  },\n" +
//                "  \"loanBankInfo\": {\n" +
//                "    \"bankName\": \"华夏银行\",\n" +
//                "    \"bankCode\": \"HXB\",\n" +
//                "    \"cardName\": \"张三\",\n" +
//                "    \"cardNo\": \"6226388000000095\",\n" +
//                "    \"reserveMobile\": \"18100000000\",\n" +
//                "    \"thirdUserNo\": \"\"\n" +
//                "  },\n" +
//                "  \"repayBankInfo\": {\n" +
//                "    \"bankName\": \"华夏银行\",\n" +
//                "    \"bankCode\": \"HXB\",\n" +
//                "    \"cardName\": \"张三\",\n" +
//                "    \"cardNo\": \"6226388000000095\",\n" +
//                "    \"reserveMobile\": \"18100000000\",\n" +
//                "    \"thirdUserNo\": \"\"\n" +
//                "  },\n" +
//                "  \"repayPlanList\": [\n" +
//                "    {\n" +
//                "      \"installmentNo\": 1,\n" +
//                "      \"principal\": 0,\n" +
//                "      \"interest\": \"1166.67\",\n" +
//                "      \"agreeRepayDate\": \"2018-08-15\",\n" +
//                "      \"eachPremium\": 0,\n" +
//                "      \"thirdUserNo\": \"\"\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"installmentNo\": 2,\n" +
//                "      \"principal\": 0,\n" +
//                "      \"interest\": \"2583.33\",\n" +
//                "      \"agreeRepayDate\": \"2018-09-15\",\n" +
//                "      \"eachPremium\": 0,\n" +
//                "      \"thirdUserNo\": \"\"\n" +
//                "    },\n" +
//                "    {\n" +
//                "      \"installmentNo\": 3,\n" +
//                "      \"principal\": \"300000.00\",\n" +
//                "      \"interest\": \"2500.00\",\n" +
//                "      \"agreeRepayDate\": \"2018-10-15\",\n" +
//                "      \"eachPremium\": 0,\n" +
//                "      \"thirdUserNo\": \"\"\n" +
//                "    }\n" +
//                "  ]\n" +
//                "}"};
//
//        Map<String, String> postData = new HashMap<>(2);
//        postData.put(ARGS_TYPE, JSON.toJSONString(paramsTypes));
//        postData.put(ARGS_VALUES, JSON.toJSONString(paramsValues));
//
//        String[] hsfArgs = new String[]{JSON.toJSONString(paramsTypes), JSON.toJSONString(paramsValues)};
//        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(JSON.toJSONBytes(hsfArgs));
//
//        HttpPost httpPost = new HttpPost(url);
//        httpPost.setEntity(byteArrayEntity);
//
//        CloseableHttpResponse response = HttpClientUtils.getHttpClient().execute(httpPost);
//
//        String string = EntityUtils.toString(response.getEntity(), Charset.forName("UTF-8"));
//
//        System.out.println(string);
//    }
//
//}
