package com.xiaomaoguai.fcp.pre.kepler.router.handler.enums;

public enum CallBack {
	WAIT,//等待回调
	NO,//无需回调
	YES;//需要回调
}
