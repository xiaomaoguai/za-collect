package com.dh.convert.exception;

public class ConvertValueException extends RuntimeException {

	private static final long serialVersionUID = 6241501409260278124L;
	
    public ConvertValueException(String message) {
        super(message);
    }
    public ConvertValueException(String message, Throwable cause) {
        super(message, cause);
    }
    public ConvertValueException(Throwable cause){
    	super(cause);
    }
}
