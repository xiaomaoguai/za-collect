package com.xiaomaoguai.fcp.pre.kepler.router.handler;

import com.googlecode.aviator.AviatorEvaluator;
import com.xiaomaoguai.fcp.pre.kepler.router.async.AsyncInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.async.RouterWorkPoolManager;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.BranchType;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.TailHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.Map;

/**
 * 分支流程参数
 *
 * @author DH
 */
public class Branch implements Serializable {

    private static final long serialVersionUID = 545796657827517644L;

    /**
     * 分支策略
     */
    private String rule;

    /**
     * 扩展表达式
     */
    private String extensionEl;
    /**
     * 下一个handler名称
     */
    private String nextHandlerName;

    /**
     * 异步线程池
     */
    private AsyncInfo asyncInfo;

    /**
     * 是否异步
     */
    private Boolean async = Boolean.FALSE;


    /**
     * 分支类型
     *
     * @see com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.BranchType
     */
    private BranchType branchType;

    /**
     * 尾节点实例名称
     */
    private String tailHandler;

    /**
     * 尾节点实例名称
     */
    private TailHandler defineTailHandler;

    public void setDefineTailHandler(TailHandler defineTailHandler) {
        this.defineTailHandler = defineTailHandler;
    }

    public TailHandler getDefineTailHandler() {
        return defineTailHandler;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getNextHandlerName() {
        return nextHandlerName;
    }

    public void setNextHandlerName(String nextHandlerName) {
        this.nextHandlerName = nextHandlerName;
    }

    public BranchType getBranchType() {
        return branchType;
    }

    public void setBranchType(BranchType branchType) {
        this.branchType = branchType;
    }

    public boolean isAsync() {
        return async;
    }

    public void init() {
        if (asyncInfo != null) {
            RouterWorkPoolManager.addPool(asyncInfo);
            this.async = Boolean.TRUE;
        }
    }

    public boolean excRule(Map<String, Object> env) {
        if (StringUtils.isNoneBlank(rule)) {
            return (boolean) AviatorEvaluator.execute(rule, env, Boolean.TRUE);
        }
        return false;
    }

    public String excExtensionEl(Map<String, Object> env) {
        if (StringUtils.isNoneBlank(extensionEl)) {
            return (String) AviatorEvaluator.execute(extensionEl, env, Boolean.TRUE);
        }
        return null;

    }

    public AsyncInfo getAsyncInfo() {
        return asyncInfo;
    }

    public void setAsyncInfo(AsyncInfo asyncInfo) {
        this.asyncInfo = asyncInfo;
    }

    public String getAsyncWorkPool() {
        if (async)
            return asyncInfo.getAsyncWorkPool();
        return null;
    }


    public String getExtensionEl() {
        return extensionEl;
    }

    public void setExtensionEl(String extensionEl) {
        this.extensionEl = extensionEl;
    }

    public void setTailHandler(String tailHandler) {
        this.tailHandler = tailHandler;
    }

    public String getTailHandler() {
        return tailHandler;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("nextHandlerName", nextHandlerName)
                .append("asyncInfo", asyncInfo).append("async", async).append("rule", rule)
                .append("branchType", branchType).append("tailHandler", tailHandler).toString();
    }
}
