package com.xiaomaoguai.fcp.YND.test;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/23 15:07
 * @since JDK 1.8
 */
@Setter
@Getter
public class BindBankInfo {

	/**
	 * 银行名称.
	 */
	@NotBlank(message = "银行名称不能为空")
	private String bankName;

	/**
	 * 发卡行编码.
	 */
	@NotNull(message = "发卡行编码不能为空")
	private String bankCode;

	/**
	 * 开户证件类型.
	 */
	@NotNull(message = "开户证件类型不能为空")
	private Integer certType;

	/**
	 * 开户证件号.
	 */
	@NotNull(message = "开户证件号不能为空")
	private String certNo;

	/**
	 * 银行开户姓名.
	 */
	@NotBlank(message = "银行开户姓名不能为空")
	private String cardName;

	/**
	 * 银行账号.
	 */
	@NotBlank(message = "银行账号不能为空")
	private String bankCardNo;

	/**
	 * 银行预留手机号.
	 */
	@NotNull(message = "银行预留手机号不能为空")
	private String cardPhone;

	/**
	 * 卡用途 1-放款 2-还款 3-放款还款卡.
	 */
	@NotNull(message = "卡用途不能为空")
	@Range(min = 1, max = 3, message = "卡用途错误，必须1-3")
	private Integer cardUse;

	/**
	 * 卡类型 1-借记 2-贷记 3-准贷记卡.
	 */
	@NotNull(message = "卡类型不能为空")
	@Range(min = 1, max = 3, message = "卡类型错误，必须1-3")
	private Integer cardType;

}

