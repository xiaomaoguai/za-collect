package com.xiaomaoguai.tac.cloud.apm.logging.logback.custom;

import ch.qos.logback.core.PropertyDefinerBase;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RegExUtils;

import java.net.InetAddress;

/**
 * 通过实现logback的PropertyDefinerBase方法,动态定义logback配置中的变量
 *
 * @author: laoMa
 * @date: 2019/12/9 17:17
 */
@Slf4j
public class HostNameDefinerConverter extends PropertyDefinerBase {

	@Override
	public String getPropertyValue() {
		try {
			//fix 日志收集平台收集规则
			//return InetAddress.getLocalHost().getHostName();
			return RegExUtils.replaceAll(InetAddress.getLocalHost().getHostName(), "-", "");
		} catch (Exception e) {
			log.info("=========>获取logback配置变量hostname异常", e);
		}
		return "kepler";
	}

}
