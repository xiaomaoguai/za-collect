package convert;

import java.lang.reflect.Field;
import java.util.EnumSet;

public 	enum TestEnum {
	 YES(1, "是"),
	  NO(0, "否");
	TestEnum(int code,String message){
		this.code = code;
		this.message = message;
	};
	    private int code;
	    private String message;

	    public int getCode() {
	        return code;
	    }

	    public String getMessage() {
	        return message;
	    }

	
	public static void main(String args[]){
	for (TestEnum ts : EnumSet.allOf(TestEnum.class)) {
		System.out.println(ts.ordinal());
		System.out.println(ts.getCode());
		System.out.println(ts.getMessage());
		
	}	
	System.out.println();
		for (Field  f : TestEnum.class.getDeclaredFields()) {
			System.out.println(f.getType() + f.getName());
		};
	}
	
}
