package com.xiaomaoguai.fcp.pre.kepler.mock.spi;

import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueConfigurator;
import com.xiaomaoguai.fcp.pre.kepler.mock.utils.FeignMethodInvocation;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * glue 执行方式扩展
 *
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/15 14:45
 * @since JDK 1.8
 */
public class GlueMockExecutionHandler implements MockExecutionHandler {

	private final GlueConfigurator glueConfigurator;

	public GlueMockExecutionHandler(final GlueConfigurator glueConfigurator) {
		this.glueConfigurator = glueConfigurator;
	}

	@Override
	public Object execute(final FeignMethodInvocation feignMethodInvocation, final Object[] params) {
		Class<?>[] interfaces = feignMethodInvocation.getInvocationClass().getInterfaces();
		String glueHandlerName = interfaces[0].getSimpleName() + "_" + feignMethodInvocation.getMethodName();
		Map<String, Object> glueParam = new LinkedHashMap<>(4);
		for (int i = 0; i < params.length; i++) {
			glueParam.put(String.valueOf(i), params[i]);
		}
		return glueConfigurator.run(glueHandlerName, glueParam);
	}

}
