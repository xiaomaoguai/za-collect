package com.xiaomaoguai.fcp.pre.kepler.mock.dc;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/18 16:38
 * @since JDK 1.8
 */
public class FeignMethodParamInfo {

	private String type;

	private String value;

	/**
	 * Getter for property 'type'.
	 *
	 * @return Value for property 'type'.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Setter for property 'type'.
	 *
	 * @param type Value to set for property 'type'.
	 */
	public void setType(final String type) {
		this.type = type;
	}

	/**
	 * Getter for property 'value'.
	 *
	 * @return Value for property 'value'.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Setter for property 'value'.
	 *
	 * @param value Value to set for property 'value'.
	 */
	public void setValue(final String value) {
		this.value = value;
	}

}
