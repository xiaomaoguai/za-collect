package com.xiaomaoguai.fcp.pre.kepler.oss.configuration;

import com.xiaomaoguai.fcp.pre.kepler.oss.constants.OssConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;


/**
 * @fileName: MultiOssProperties.java
 * @author: WeiHui
 * @date: 2019/1/30 15:25
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = OssConstants.MULTI_CONFIG_PREFIX)
public class MultiOssProperties {

	/**
	 * oss 属性配置
	 */
	private Map<String, OssProperties> clients;

	/**
	 * oss 属性配置
	 */
	@Data
	public static class OssProperties {

		/**
		 * oss-accessKeyId
		 */
		private String accessKeyId;

		/**
		 * oss-accessKeySecret
		 */
		private String accessKeySecret;

		/**
		 * 公网URL
		 */
		private String publicEndpoint;

		/**
		 * 内网URL
		 */
		private String privateEndpoint;

		/**
		 * oss-bucketName
		 */
		private String bucketName;

		/**
		 * oss根目录
		 */
		private String object;

		/**
		 * 生成sts信息需要
		 */
		private String arn;

	}

}
