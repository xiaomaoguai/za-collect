package com.dh.convert.rule.function;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorNil;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorRuntimeJavaType;

public class GetListFunction extends AbstractFunction {

	@Override
	public String getName() {
		return "getList";
	}

	@Override
	public AviatorObject call(Map<String, Object> env, AviatorObject arg1,AviatorObject arg2) {
		List<?> list = (List<?>) arg1.getValue(env);
		if (CollectionUtils.isEmpty(list))
			return  AviatorNil.NIL;
		Integer num = (Integer) arg2.getValue(env);
		if(num == null)
			return  AviatorNil.NIL;
		return new AviatorRuntimeJavaType(list.get(num));
	}

}
