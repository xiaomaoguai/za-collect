/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class RepayPlan extends TransRepayPlan {
    /**
     *  
     */
	@JsonIgnore
    private Long id;

    /**
     *  产品编码
     */
	@JsonIgnore
    private String productCode;

    /**
     *  渠道用户唯一标识
     */
	@JsonIgnore
    private String thirdUserNo;

    /**
     *  渠道子账户
     */
	@JsonIgnore
    private Long userId;

    /**
     *  业务子账户
     */
	@JsonIgnore
    private Long acctId;

    /**
     *  信贷核心还款计划id
     */
	@JsonIgnore
    private Long repayPlanId;

    /**
     *  借款申请号
     */
	@JsonIgnore
    private String loanOuterNo;

    /**
     *  信贷核心借款流水号
     */
	@JsonIgnore
    private String loanInnerNo;

    /**
     *  创建时间
     */
	@JsonIgnore
    private Date gmtCreated;

    /**
     *  
     */
	@JsonIgnore
    private String creator;

    /**
     *  更新时间
     */
	@JsonIgnore
    private Date gmtModified;

    /**
     *  
     */
	@JsonIgnore
    private String modifier;

    /**
     *  
     */
	@JsonIgnore
    private String isDeleted;

}