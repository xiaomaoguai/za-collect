//package com.xiaomaoguai.fcp.pre.kepler.halley;
//
//import com.xiaomaoguai.msg.api.bootstrap.MqClient;
//import com.xiaomaoguai.msg.api.common.MessageModelEnum;
//import com.xiaomaoguai.msg.api.common.SerializerTypeEnum;
//import com.xiaomaoguai.msg.api.consumer.AckAction;
//import com.xiaomaoguai.msg.api.consumer.MqMessageListener;
//import com.xiaomaoguai.msg.api.consumer.ReceiveRecord;
//import com.xiaomaoguai.msg.api.consumer.binding.ConsumerConfig;
//import com.xiaomaoguai.share.event.db.DBEvent;
//import lombok.extern.slf4j.Slf4j;
//
///**
// * @author WeiHui-Z
// * @version v1.0.0
// * @date 2019/7/31 17:23
// * @since JDK 1.8
// */
//@Slf4j
//public class ApiConsumer {
//
//	public void consumerMedium() {
//		ConsumerConfig consumerConfig = new ConsumerConfig("kafka|10.139.52.45:9092,10.139.52.57:9092,10.139.52.49:9092", "fcp-kepler-201905161412", new MqMessageListener<DBEvent>() {
//
//			@Override
//			public AckAction onMessage(ReceiveRecord<DBEvent> record) {
//				ApiConsumer.log.info("#### testConsumerMedium onMessage:" + record);
//				return AckAction.Commit;
//			}
//
//			@Override
//			public boolean isRedeliver(ReceiveRecord<DBEvent> record) {
//				return false;
//			}
//		});
//		consumerConfig.setBatchSize(100);
//		consumerConfig.setNumThreads(10);
//		consumerConfig.setGroupId("es-test-16");
//		consumerConfig.put("spring.application.name", "test");
//		consumerConfig.put("max.poll.records", 500);
//		consumerConfig.setPattern(MessageModelEnum.BROADCASTING);
//		consumerConfig.setSerializer(SerializerTypeEnum.BYTEARRAY);
//		MqClient.buildConsumer(consumerConfig);
//	}
//
//	public static void main(String[] args) throws InterruptedException {
//		ApiConsumer consumer = new ApiConsumer();
//		consumer.consumerMedium();
//	}
//
//}
