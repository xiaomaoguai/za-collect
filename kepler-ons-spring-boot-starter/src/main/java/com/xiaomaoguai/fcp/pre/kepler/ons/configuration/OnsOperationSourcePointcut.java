package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @fileName: OnsOperationSourcePointcut.java
 * @author: WeiHui
 * @date: 2019/2/28 13:54
 * @version: v1.0.0
 * @since JDK 1.8
 */
abstract class OnsOperationSourcePointcut extends StaticMethodMatcherPointcut implements Serializable {

	private static final long serialVersionUID = -5755870127811195870L;

	@Override
	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof OnsOperationSourcePointcut)) {
			return false;
		}
		OnsOperationSourcePointcut otherPc = (OnsOperationSourcePointcut) other;
		return ObjectUtils.nullSafeEquals(getOnsOperationSource(), otherPc.getOnsOperationSource());
	}

	@Override
	public int hashCode() {
		return OnsOperationSourcePointcut.class.hashCode();
	}

	@Override
	public String toString() {
		return getClass().getName() + ": " + getOnsOperationSource();
	}

	@Override
	public boolean matches(Method method, @Nullable Class<?> targetClass) {
		OnsOperationSource cas = getOnsOperationSource();
		return (cas != null && !CollectionUtils.isEmpty(cas.getOnsOperations(method, targetClass)));
	}

	/**
	 * Obtain the underlying {@link org.springframework.cache.interceptor.CacheOperationSource} (may be {@code null}).
	 * To be implemented by subclasses.
	 */
	@Nullable
	protected abstract OnsOperationSource getOnsOperationSource();
}
