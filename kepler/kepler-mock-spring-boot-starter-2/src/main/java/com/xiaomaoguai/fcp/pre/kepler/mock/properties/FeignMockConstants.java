package com.xiaomaoguai.fcp.pre.kepler.mock.properties;

import lombok.experimental.UtilityClass;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/18 15:43
 * @since JDK 1.8
 */
@UtilityClass
public final class FeignMockConstants {

	public static final String FEIGN_MOCK_ENABLE = "feign.mock.enabled";

}
