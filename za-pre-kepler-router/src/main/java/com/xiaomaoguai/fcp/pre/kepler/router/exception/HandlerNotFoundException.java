package com.xiaomaoguai.fcp.pre.kepler.router.exception;

/**
 * 获取不到指定handlerContext或者handler时抛出
 * 必须传入具体handlerName
 *
 * @author DH
 */
public class HandlerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -3111662946824666232L;

	public HandlerNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public HandlerNotFoundException(String message) {
		super(message);
	}

}
