package com.dh.convert.exception;

public class ConvertInitException extends RuntimeException {

	private static final long serialVersionUID = -2231058921871676279L;
	
    public ConvertInitException(String message) {
        super(message);
    }
    public ConvertInitException(String message, Throwable cause) {
        super(message, cause);
    }
    public ConvertInitException(Throwable cause){
    	super(cause);
    }
}
