package com.xiaomaoguai.fcp.pre.kepler.glue.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 20:15
 * @since JDK 1.8
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GlueClass implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = -6583954441346612971L;

	private String glueName;

	private String glueSource;

}
