package com.xiaomaoguai.fcp.pre.kepler.router.async;

/**
 * 当队列消息满后执行
 * 自定义请继承
 *
 * @param <O>
 * @author DH
 */
public class RejectHandler<O> {

	public void onEvent(O o) {
		throw new RejectException("超过最大消息数");
	}

}
