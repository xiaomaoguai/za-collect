package com.xiaomaoguai.fcp.hsf.autoconfigure;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 使 HsfClient 客户端起作用
 *
 * @fileName: EnableHsfClients.java
 * @author: WeiHui
 * @date: 2018/9/17 19:59
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Import(HsfClientsRegistrar.class)
public @interface EnableHsfClients {

}
