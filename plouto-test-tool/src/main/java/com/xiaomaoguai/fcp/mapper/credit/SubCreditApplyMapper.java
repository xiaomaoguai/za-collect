/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.credit;

import com.xiaomaoguai.fcp.domain.CreditApply;
import com.xiaomaoguai.fcp.domain.SubCreditApply;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * The Interface SubCreditApplyMapper.
 */
public interface SubCreditApplyMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(SubCreditApply record);

	/**
	 * 查找指定时间点的数据.
	 *
	 * @param beginTime   开始时间
	 * @param endTime     截止时间
	 * @param productCode 产品编码
	 * @return the list
	 */
	List<CreditApply> selectByCondition(@Param("beginTime") Date beginTime,
										@Param("endTime") Date endTime,
										@Param("productCode") String productCode);

	/**
	 * Select by credit apply no.
	 *
	 * @param creditApplyNo the credit apply no
	 * @return the sub credit apply
	 */
	CreditApply selectByCreditApplyNo(@Param("creditApplyNo") String creditApplyNo);

}