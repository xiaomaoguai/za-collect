package com.xiaomaoguai.tac.cloud.apm.logging.logback.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 14:10
 * @since JDK 1.8
 */
@Getter
@Setter
public class SensitiveRulesVo implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 166972383052776321L;

	/**
	 * 脱敏类型
	 */
	private String name;

	/**
	 * 脱敏字段
	 */
	private String dataField;

	/**
	 * 数据类型
	 */
	private String dataType;

}
