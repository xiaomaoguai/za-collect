package com.xiaomaoguai.fcp.pre.kepler.oss.configuration;

import com.xiaomaoguai.fcp.pre.kepler.oss.constants.OssConstants;
import com.xiaomaoguai.fcp.pre.kepler.oss.utils.OssClient;
import com.xiaomaoguai.fcp.pre.kepler.oss.utils.OssClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author WeiHui
 * @date 2019/1/30
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(OssProperties.class)
@ConditionalOnProperty(prefix = OssConstants.CONFIG_PREFIX, value = "enabled")
public class OssAutoConfiguration {

	@Bean(initMethod = "init", destroyMethod = "shutDown")
	public OssClient ossClient(OssProperties ossProperties) {
		if (log.isInfoEnabled()) {
			log.info("init oss configuration, properties: {}", ossProperties);
		}
		return OssClientUtils.buildOssClient(ossProperties);
	}

}
