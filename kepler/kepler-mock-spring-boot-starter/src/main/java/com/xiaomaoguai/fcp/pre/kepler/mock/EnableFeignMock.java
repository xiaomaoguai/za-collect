package com.xiaomaoguai.fcp.pre.kepler.mock;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 12:46
 * @since JDK 1.8
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(FeignMocksRegistrar.class)
public @interface EnableFeignMock {

	/**
	 * 扫描路径
	 *
	 * @return 扫描路径
	 */
	String[] value() default {};

}
