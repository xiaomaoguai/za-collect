package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: SubUpdateParam.java
 * @author: WeiHui
 * @date: 2018/8/18 18:16
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Setter
@Getter
public class SubUpdateParam {
	/**
	 * 外部流水号
	 */
	private String no;
	/**
	 * 变更状态
	 */
	private Integer status;
	/**
	 * 原状态
	 */
	private Integer preStatus;
}
