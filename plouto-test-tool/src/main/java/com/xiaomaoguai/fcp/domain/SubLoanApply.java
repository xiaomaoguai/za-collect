/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class SubLoanApply {

	private Long id;

	private String thirdUserNo;

	private String productCode;

	private String channelId;

	private Long userId;

	private String loanOuterNo;

	private Date applyDate;

	private BigDecimal loanAmount;

	private Integer loanStatus;

	private String creator;

	private Date gmtCreated;

	private String modifier;

	private Date gmtModified;

	private String isDeleted;

}