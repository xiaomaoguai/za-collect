/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.loan;

import com.xiaomaoguai.fcp.domain.FileSignInfo;
import org.apache.ibatis.annotations.Param;

/**
 * The Interface FileSignInfoMapper.
 */
public interface FileSignInfoMapper {

	/**
	 * 根据主键删除数据库的记录.
	 *
	 * @param id the id
	 * @return the int
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * 新写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(FileSignInfo record);

	/**
	 * 动态字段,写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(FileSignInfo record);

	/**
	 * 根据指定主键获取一条数据库记录.
	 *
	 * @param id the id
	 * @return the file sign info
	 */
	FileSignInfo selectByPrimaryKey(Long id);

	/**
	 * Select by loan inner no.
	 *
	 * @param userId      the user id
	 * @param loanInnerNo the loan inner no
	 * @param fileType    the file type
	 * @return the file sign info
	 */
	FileSignInfo selectByLoanInnerNo(@Param("userId") Long userId, @Param("loanInnerNo") String loanInnerNo,
			@Param("fileType") Integer fileType);

	/**
	 * 动态字段,根据主键来更新符合条件的数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByPrimaryKeySelective(FileSignInfo record);

	/**
	 * 根据主键来更新符合条件的数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByPrimaryKey(FileSignInfo record);
}