/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.nacos.constants;

/**
 * 配置中心的常量
 *
 * @author chenyao
 * @since 2019年6月13日 下午2:07:58
 */
public class ConfigConstants {
}
