package com.xiaomaoguai.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 批量任务信息表
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class BatchTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 归集类型：21-线下还款 22-平台代偿 23-逾期还款 24-回购
     */
    private Integer batchType;

    /**
     * 批次号 格式是：  yyyyMMdd+两位类型+两位序号
     */
    private String batchNo;

    /**
     * 批量数据生成时间（第三方为准）
     */
    private String createDate;

    /**
     * 总笔数
     */
    private Long batchCount;

    /**
     * 总金额
     */
    private BigDecimal batchAmount;

    /**
     * 0-创建任务 10-开始任务 15-获取并解析数据成功 20-全部请求成功 21-部分请求成功 22-全部请求失败 30-全部成功 31-部分失败 32-全部失败
     */
    private Integer batchState;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 文件名下载路径
     */
    private String fileDownloadPath;

    /**
     * 文件名上传路径
     */
    private String fileUploadePath;

    /**
     * 原批次号
     */
    private String orBatchNo;

    /**
     * 冗余字段1
     */
    private String addField1;

    /**
     * 冗余字段1
     */
    private String addField2;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreated;

    private String creator;

    /**
     * 更新时间
     */
    private LocalDateTime gmtModified;

    private String modifier;

    private String isDeleted;

    private Long successCount;

    private BigDecimal successAmount;

    /**
     * 测试
     */
    private String test;

    /**
     * 归集类型：21-线下还款 22-平台代偿 23-逾期还款 24-回购
     */
    private Integer batchType1;


}
