package com.xiaomaoguai.fcp.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableMap;
import com.xiaomaoguai.fcp.config.CreditCoreProperties;
import com.xiaomaoguai.fcp.config.FtSystemRequestHeader;
import com.xiaomaoguai.fcp.dto.resp.CreditCoreClearDetail;
import com.xiaomaoguai.fcp.service.CreditCoreClearService;
import com.xiaomaoguai.fcp.utils.HttpClientUtils;
import com.xiaomaoguai.fcp.utils.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @fileName: CreditCoreClearServiceImpl.java
 * @author: WeiHui
 * @date: 2018/10/25 10:39
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Service
public class CreditCoreClearServiceImpl implements CreditCoreClearService {

	@Resource
	private CreditCoreProperties creditCoreProperties;

	@Override
	public CreditCoreClearDetail clear(String productCode, Long userId, String type) {
		try {
			String post = HttpClientUtils.doGet(
					creditCoreProperties.getClearUrl(),
					FtSystemRequestHeader.getHeader(),
					ImmutableMap.of(
							"productCode", productCode,
							"userId", userId.toString(),
							"type", type));
			ObjectNode jsonNode = JsonUtils.readTree(post);
			JsonNode clearDetail = jsonNode.get("creditcoreClearDetail");
			return JsonUtils.toJavaObject(clearDetail.toString(), CreditCoreClearDetail.class);
		} catch (Exception e) {
			log.error("====>[{}]", e);
		}
		return null;
	}

}
