package com.xiaomaoguai.fcp;

import com.github.xiaomaoguai.xxr.utils.json.JsonUtils;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class RepayBaseReq {

	/**
	 *
	 */
	private static final long serialVersionUID = -7359496010246349949L;

	/**
	 * 外部还款流水号
	 */
	private String repayOuterNo;

	/**
	 * 是否提前结清
	 * 0 否
	 * 1 是
	 */
	private Integer isEarlySettle;

	/**
	 * 用户ID
	 */
	@NotNull(message = "用户ID不能为空")
	private Long userId;

	/**
	 * 借款订单号
	 */
	@NotBlank(message = "借款订单号不能为空")
	private String loanOuterNo;

	/**
	 * 还款方式
	 * 1 系统主动代扣
	 * 2用户主动还款
	 * 默认还款模式 2
	 */
	private Integer repayMode = 2;


	/**
	 * 还款期数
	 */
	@NotEmpty(message = "还款期数不能为空")
	private List<Integer> installmentNos;

	/**
	 * 1-校验跳期
	 * 0-不校验跳期
	 */
	private Integer checkIns = 1;

	/**
	 * 协议付验证码
	 */
	private String vertCode;

	/**
	 * 查询模式
	 * 0-进行减免(默认)
	 * 1-不进行减免
	 */
	private Integer queryMode = 0;

	/**
	 * 不进行异常抛出操作，只记录信息
	 * 0-关闭（默认）
	 * 1-开启
	 */
	private Integer kindMode = 0;

	public static void main(String[] args) {
		RepayBaseReq repayBaseReq = new RepayBaseReq();
		repayBaseReq.setInstallmentNos(Lists.newArrayList(0, 2, 3));
		System.out.println(JsonUtils.toFormatJsonString(repayBaseReq));
	}
}
