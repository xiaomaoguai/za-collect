package com.baomidou.mybatisplus.samples.generator.log.service;

import com.baomidou.mybatisplus.samples.generator.log.entity.Log;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangweihui
 * @since 2019-03-09
 */
public interface ILogService extends IService<Log> {

}
