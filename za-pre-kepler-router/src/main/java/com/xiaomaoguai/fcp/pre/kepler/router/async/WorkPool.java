package com.xiaomaoguai.fcp.pre.kepler.router.async;

public interface WorkPool<O> {

	public void start();
	
	public void addTask(O o);
	
	public void shutdownGracefully() ;
	
	public void shutdown();
	
	public boolean isRunning() ;
}
