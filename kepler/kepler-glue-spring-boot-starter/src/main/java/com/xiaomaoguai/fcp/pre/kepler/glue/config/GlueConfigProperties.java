package com.xiaomaoguai.fcp.pre.kepler.glue.config;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 逻辑平台-总配置
 *
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/20 17:12
 * @since JDK 1.8
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = "glue", ignoreInvalidFields = true)
public class GlueConfigProperties extends GlueConfig {

}
