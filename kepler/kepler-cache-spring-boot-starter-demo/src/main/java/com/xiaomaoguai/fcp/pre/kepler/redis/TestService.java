package com.xiaomaoguai.fcp.pre.kepler.redis;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2019/11/15 10:57
 * @since JDK 1.8
 */
@Cacheable
@Component
public class TestService {

	@CachePut(key = "'#name'", value = "hello")
	public String testCache(String name) {
		return name;
	}

}
