<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.billionsfinance.crs.dao.${moduleType}.I${module}Dao" >

	<resultMap id="BaseResultMap" type="${module}" >
		<id column="uuid" property="uuid" jdbcType="VARCHAR" />
	
	<#list moduleInfoList as moduleInfo>
		<#if moduleInfo.propertyName!="uuid">
		<result column="${moduleInfo.columnName}" 			property="${moduleInfo.propertyName}" 			jdbcType="${moduleInfo.jdbcType}" />
		</#if>
	</#list>
	</resultMap>
	
	<!-- 基础的字段 -->
	<sql id="Base_Column_List">
	     <#list moduleInfoList as moduleInfo>
	        ${moduleInfo.columnName} <#if moduleInfo_has_next>,</#if>
	     </#list>
	</sql>
	
	<!-- 多个确定条件 -->
	<sql id="multi_Where">
		<trim prefix="WHERE" prefixOverrides="AND |OR ">
				1=1
	        <#list moduleInfoList as moduleInfo>
	        <if test="${moduleInfo.propertyName} != null and ${moduleInfo.propertyName} != ''">
	            and ${moduleInfo.columnName} =${r"#{"}${moduleInfo.propertyName}, jdbcType=${moduleInfo.jdbcType}${r"}"}
	        </if>
	        </#list>
	       	    and (is_delete<![CDATA[ != ]]>'Y' or is_delete is null)
	            order by updated_time desc
		</trim>
	</sql>
	
	<!-- 分页多个确定条件 -->
	<sql id="Page_multi_Where">
		<trim prefix="WHERE" prefixOverrides="AND |OR ">
				1=1
	        <#list moduleInfoList as moduleInfo>
	        <if test="${lowModule}.${moduleInfo.propertyName} != null and ${lowModule}.${moduleInfo.propertyName} != ''">
	            and ${moduleInfo.columnName} =${r"#{"}${lowModule}${r"."}${moduleInfo.propertyName}, jdbcType=${moduleInfo.jdbcType}${r"}"}
	        </if>
	        </#list>
	       	    and (is_delete<![CDATA[ != ]]>'Y' or is_delete is null)
	            order by updated_time desc
		</trim>
	</sql>
	
	<!-- 查询列表-->
	<select id="find${module}List" resultMap="BaseResultMap" parameterType="${module}">
		select
			<include refid="Base_Column_List" />
		from ${moduleTable}
			<include refid="multi_Where" />
	</select>
	
	<!-- 新增 -->
	<insert id="create${module}" parameterType="${module}">
		<selectKey resultType="java.lang.String" order="BEFORE" keyProperty="uuid">
			SELECT lower(sys_guid()) FROM DUAL
		</selectKey>
		insert into ${moduleTable}
		(
		  <#list moduleInfoList as moduleInfo>
		 	 ${moduleInfo.columnName}<#if moduleInfo_has_next>,</#if>
		  </#list>
		)
		values
		( 
		  <#list moduleInfoList as moduleInfo>
		  	<#if moduleInfo.propertyName=="createdTime" || moduleInfo.propertyName=="updatedTime">
		  		sysdate<#if moduleInfo_has_next>,</#if>
			 <#else>
			 	${r"#{"}${moduleInfo.propertyName}${r", jdbcType="}${moduleInfo.jdbcType}${r"}"}<#if moduleInfo_has_next>,</#if>
			 </#if>
		  </#list>
		)
	</insert>
	
	<!--修改 -->
	<update id="update${module}" parameterType="${module}">
		update  ${moduleTable} set 
		<#list moduleInfoList as moduleInfo>
			<#if moduleInfo.propertyName!="uuid" && moduleInfo.propertyName != "createdTime" && moduleInfo.propertyName != "createdBy" && moduleInfo.propertyName != "isDelete" && moduleInfo.propertyName != "updatedTime">
			${moduleInfo.columnName}=${r"#{"}${moduleInfo.propertyName}${r",jdbcType="}${moduleInfo.jdbcType}},
			</#if>
		</#list>
			updated_time = sysdate
		    where
		     	uuid=${r"#{uuid, jdbcType=NUMERIC}"}
		     	and updated_time=${r"#{updatedTime, jdbcType=TIME}"}
	</update>
	
	<!--逻辑删除 -->
	<update id="delete${module}ById" parameterType="java.lang.String">
		update  ${moduleTable} set is_delete='Y' where uuid=${r"#{uuid, jdbcType=NUMERIC}"}
	</update>
	
	<!-- 物理删除 -->
	<delete id="physicalDelete${module}ById" parameterType="java.lang.String">
		delete ${moduleTable} where uuid=${r"#{uuid, jdbcType=NUMERIC}"}
	</delete>
	
	<!-- 查询一条数据 -->
	<select id="read${module}ById" resultMap="BaseResultMap" parameterType="java.lang.String">
		select
		   <include refid="Base_Column_List" />
		   from ${moduleTable}
		   where uuid=${r"#{uuid, jdbcType=NUMERIC}"}
	</select>
	
	<!-- 分页查询总记录数 -->
	<select id="select${module}SetCount" resultType="int" parameterType="map" >
		select count(1) from ${moduleTable} 
	<include refid="Page_multi_Where" />
	</select>
	
	<!-- 分页查询List -->
	<select id="select${module}Set" resultMap="BaseResultMap" parameterType="map">
		select *
			 from (select ROWNUM rn, t.*
			         from (
						select
							<include refid="Base_Column_List" />
							from ${moduleTable}
							<include refid="Page_multi_Where" />
						) t
			        where ROWNUM <![CDATA[ <= ]]> ${r"#{page.endIndex, jdbcType=INTEGER}"} ) t3
		 where t3.rn <![CDATA[ > ]]> ${r"#{page.startIndex, jdbcType=INTEGER}"}
	</select>
	
</mapper>