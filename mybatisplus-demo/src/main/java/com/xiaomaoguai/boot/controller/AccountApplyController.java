package com.xiaomaoguai.boot.controller;


import com.google.common.collect.Maps;
import com.xiaomaoguai.boot.entity.AccountApply;
import com.xiaomaoguai.boot.service.AccountApplyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

/**
 * <p>
 * 调账申请表 前端控制器
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Controller
@RequestMapping("/accountApply")
public class AccountApplyController {

    @Resource
    private AccountApplyService accountApplyService;

    @RequestMapping("/test")
    @ResponseBody
    public String Test() {
        return "ok";
    }

    @RequestMapping("/id")
    @ResponseBody
    public Collection<AccountApply> id() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("product_code", "51YND");
        map.put("account_status", "3");
        return accountApplyService.listByMap(map);
    }

}

