/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.fcp.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessResp{

	private static final long serialVersionUID = -6924842490624004434L;
	
	private Integer accessStep; 
	/**
	 * 0 -不存在其他产品在贷
	 * 1 -存在其他产品在贷
	 */
	private Integer isExist = 0;

	/**
	 * 预授信申请单号
	 */
	private String applyNo;

	@Override
	public String toString() {
		return "AccessResp{"
				+ "accessStep=" + accessStep + ", isExist=" + isExist + ", applyNo=" + applyNo + 
				"}";
	}

}
