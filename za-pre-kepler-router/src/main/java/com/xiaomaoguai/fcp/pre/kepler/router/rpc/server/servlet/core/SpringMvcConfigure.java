package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
public class SpringMvcConfigure implements WebMvcConfigurer {

	@Override
	public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {

		returnValueHandlers.add(new ContinuationHandler());
	}


}