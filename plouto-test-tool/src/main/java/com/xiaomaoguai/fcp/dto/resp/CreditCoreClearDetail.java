package com.xiaomaoguai.fcp.dto.resp;

import com.xiaomaoguai.fcp.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: CreditCoreClearDetail.java
 * @author: WeiHui
 * @date: 2018/10/26 14:32
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class CreditCoreClearDetail extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 8510034496256491714L;

	/**
	 *
	 */
	private int applyClearNum;

	private int loanClearNum;

	private int fileClearNum;

	private int allDataClearNum;

}
