/*
 * 文件名：${module}Controller.java
 * 版本信息:v1.0.0
 * 日期：${createdTime}
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */
package com.billionsfinance.crs.controller.${moduleType};

import com.billionsfinance.crs.controller.BaseController;
import com.billionsfinance.crs.model.PageResult;
import com.billionsfinance.crs.model.${moduleType}.${module};
import com.billionsfinance.crs.model.system.MessageModel;
import com.billionsfinance.crs.request.PageRequest;
import com.billionsfinance.crs.server.${moduleType}.I${module}Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
* @ FileName: ${module}Controller.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
@Controller
@RequestMapping("/${moduleType}/${lowModule}")
public class ${module}Controller extends BaseController{

	private static final Logger LOGGER = LoggerFactory.getLogger(${module}Controller.class);

	@Resource
	private I${module}Server ${lowModule}Server;

	/**
	 * 进入${module}服务列表页面
	 * @param view 跳转的视图
	 * @return  String 视图
	 * @throws Exception 异常
	 */
	@RequestMapping("/index/{view}")
	public String index(@PathVariable("view") String view) throws Exception {
		return "/${moduleType}/${lowModule}/"+view;
	}


	/**
	 * ${module}列表页面查询
	 * @param pageRequest 分页参数
	 * @return Object 返回JSON
	 * @throws Exception 异常
	 */
	@RequestMapping(value="/query", method=RequestMethod.POST)
	@ResponseBody
	public MessageModel queryList(PageRequest pageRequest)  {
		MessageModel messageModel = new MessageModel(true);
		try {
			PageResult<${module}> result = ${lowModule}Server.queryPageList(new ${module}(), pageRequest);
			messageModel.setResult(result);
		    Map<String, Object> map = new HashMap<String, Object>();
			map.put("total", result.getPage().getTotalSize());
			map.put("rows", result.getResult());
			messageModel.setResult(map);
		} catch (Exception e) {
			messageModel.setSuccess(false);
			messageModel.setMsg("查询失败");
			LOGGER.error("${module}Controller.queryList() Error:", e);
		}
		return messageModel;
	}

	/**
	 * 新增或修改${module}
	 * @param ${lowModule}  参数
	 * @return Object 返回JSON对象
	 * @throws Exception  e
	 */
	@RequestMapping("/save")
	@ResponseBody
	public MessageModel add(${module} ${lowModule}) {
		MessageModel messageModel = new MessageModel();
		try {
			//添加数据
			int rows = ${lowModule}Server.createOrUpdate${module}(${lowModule});
		    if (rows == 1) {
				messageModel.setSuccess(true);
				messageModel.setMsg("添加成功！");
			} else {
				messageModel.setSuccess(false);
				messageModel.setMsg("添加失败！");
			}
		} catch (Exception e) {
			messageModel.setSuccess(false);
			messageModel.setMsg("查询失败");
			LOGGER.error("${module}Controller.add() Error:", e);
		}
		return messageModel;
	}

	/**
	 * 删除${module}
	 * @param uuId ${module}Id  参数
	 * @return Object 返回JSON
	 */
	@RequestMapping("/del/{uuId}")
	@ResponseBody
	public MessageModel delete(@PathVariable("uuId") String uuId) {
		MessageModel messageModel = new MessageModel();

		try {
			int rows = ${lowModule}Server.delete${module}ById(uuId);
			if (rows == 1) {
				messageModel.setSuccess(true);
				messageModel.setMsg("删除成功！");
			} else {
				messageModel.setSuccess(false);
				messageModel.setMsg("删除失败！");
			}
		} catch (Exception e) {
		    messageModel.setSuccess(false);
			messageModel.setMsg("查询失败");
			LOGGER.error("${module}Controller.delete() Error:", e);
		}
		return messageModel;
	}

}
