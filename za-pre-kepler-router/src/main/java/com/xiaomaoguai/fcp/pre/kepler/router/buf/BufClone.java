package com.xiaomaoguai.fcp.pre.kepler.router.buf;

import com.rits.cloning.Cloner;

/**
 * buf深拷贝
 *
 * @author DH
 */
public class BufClone {

	private final static Cloner cloner = new Cloner();

	public static Buf clone(Buf buf) {
		return cloner.deepClone(buf);
	}

}
