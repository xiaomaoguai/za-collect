package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BatchRepayPlan {

	private String id;

	private String installmentStatus;

	private Integer installmentNo;
}
