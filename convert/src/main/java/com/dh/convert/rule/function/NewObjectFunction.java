package com.dh.convert.rule.function;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorNil;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorRuntimeJavaType;

public class NewObjectFunction extends AbstractFunction {

	 @Override
     public AviatorObject call(Map<String, Object> env, AviatorObject arg ) {
		 String className =  FunctionUtils.getStringValue(arg, env);
		 if(StringUtils.isNoneBlank(className)){
			 try {
				return new AviatorRuntimeJavaType(Class.forName(className).newInstance());
			} catch (Exception e) {
				e.printStackTrace();
			} 
		 }
		 return AviatorNil.NIL;
     }
	@Override
	public String getName() {
		return "newObject";
	}

}
