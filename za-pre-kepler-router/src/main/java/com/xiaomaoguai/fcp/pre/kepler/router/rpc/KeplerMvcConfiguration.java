package com.xiaomaoguai.fcp.pre.kepler.router.rpc;

import com.xiaomaoguai.fcp.pre.kepler.router.rpc.httpconverter.HessianHttpConverter;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.httpconverter.KryoHttpConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author DH
 */
@Configuration
public class KeplerMvcConfiguration implements WebMvcConfigurer {

    /**
     * KryoHttpConverter和HessianHttpConverter直接注入会直接出现在converters的最前端
     * 导致converters乱序造成FastJsonHttpMessageConverter顺序在StringHttpMessageConverter前面会出现重复序列化情况
     * 这里不注入Converter只在${@link WebMvcConfigurer#extendMessageConverters(List)}中设置
     *
     * @return
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(4, new KryoHttpConverter());
        converters.add(5, new HessianHttpConverter());
    }

}
