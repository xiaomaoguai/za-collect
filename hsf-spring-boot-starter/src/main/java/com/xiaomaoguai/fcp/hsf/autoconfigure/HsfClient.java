package com.xiaomaoguai.fcp.hsf.autoconfigure;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @fileName: HsfClient.java
 * @author: WeiHui
 * @date: 2018/11/20 21:33
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface HsfClient {

	/**
	 * 配置的Hsf 接口类，当只配置类时，只有该类才配置代理，否者 ： 以扫描的包都封装成 hsf 代理
	 *
	 * @return 包路径
	 */
	Class<?>[] value() default {};

	/**
	 * 当不依赖jar包时，可以只配置接口全限定名
	 *
	 * @return 包路径
	 */
	String className() default "";

	/**
	 * 包路径
	 *
	 * @return 包路径
	 */
	String[] basePackages() default {};

	/**
	 * 请求url
	 */
	String url();

	/**
	 * 请求 version
	 */
	String version() default "1.0.0";
}
