package com.xiaomaoguai.javassist.kepler.config;

import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertTemplate;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/20 15:42
 * @since JDK 1.8
 */
@Getter
@Setter
public class HandlerConvertClientFactoryBean implements FactoryBean<Object> {

	private Class<?> serviceInterface;

	private ConvertTemplate convertTemplate;

	@Override
	public Object getObject() throws Exception {
		return Proxy.newProxyInstance(
				this.getClass().getClassLoader(),
				new Class<?>[]{this.serviceInterface},
				new HandlerConvertClientInvocation(this.serviceInterface,convertTemplate));
	}

	@Override
	public Class<?> getObjectType() {
		return this.serviceInterface;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
