package com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter;

import java.lang.reflect.Method;

/**
 * @fileName: BuildDataByElMethod.java
 * @author: WeiHui
 * @date: 2018/11/26 10:19
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class BuildDataByElMethod extends AbstractConvertMethod {

    @Override
    public boolean canHandler(Method method) {
        return "buildDataByEl".equals(method.getName()) && method.getParameterCount() == 4;
    }

    @Override
    public Object invoke(Method method, Object[] args) {
        return buildDataByEl((String) args[0], (String) args[1], args[2], method.getReturnType());
    }

}
