package com.xiaomaoguai.fcp.pre.kepler.oss.utils;

import com.aliyun.oss.*;
import com.aliyun.oss.common.auth.Credentials;
import com.aliyun.oss.common.auth.CredentialsProvider;
import com.aliyun.oss.common.comm.ResponseMessage;
import com.aliyun.oss.common.comm.SignVersion;
import com.aliyun.oss.model.*;
import com.aliyuncs.auth.sts.AssumeRoleResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 新版ossClient ，
 * <p>
 * （1）不再使用过时方法,jar更新
 * (2) 增对STS 的要求，增加 STS 生成方法 ,@see genOssInfo()
 * (3) 新增 继承 OSS , 拥有更多方法, @Delegate 委托内网oss 操作
 * </p>
 *
 * @author WeiHui
 * @date 2019/1/22
 */
@Slf4j
@Setter
@Getter
public class OssClient implements OSS {

	private String accessKeyId;

	private String secretAccessKey;

	private String publicEndpoint;

	private String privateEndpoint;

	private String bucketName;

	private String object;

	/**
	 * 生成STS信息需要
	 */
	private String arn;

	private OSSClient publicOssClient;

	private OSSClient privateOssClient;

	/**
	 * 初始化
	 */
	public void init() {
		this.privateOssClient = (OSSClient) new OSSClientBuilder().build(this.privateEndpoint, this.accessKeyId, this.secretAccessKey);
		if (StringUtils.isNotBlank(this.publicEndpoint)) {
			this.publicOssClient = (OSSClient) new OSSClientBuilder().build(this.publicEndpoint, this.accessKeyId, this.secretAccessKey);
		}
	}

	/**
	 * 销毁
	 */
	public void shutDown() {
		if (this.privateOssClient != null) {
			this.privateOssClient.shutdown();
		}
		if (this.publicOssClient != null) {
			this.publicOssClient.shutdown();
		}
	}

	/**
	 * 内网下载文件
	 *
	 * @param key  文件路径key
	 * @param path 本地路径
	 */
	public void storeFile(String key, String path) {
		this.storeFile(this.getPrivateOssClient(), key, path);
	}

	/**
	 * 下载文件
	 *
	 * @param key  文件路径key
	 * @param path 本地路径
	 */
	public void storeFile(OSS ossClient, String key, String path) {
		ossClient.getObject(new GetObjectRequest(this.bucketName, key), new File(path));
	}

	/**
	 * 上传文件
	 *
	 * @param key  文件路径key
	 * @param path 本地路径
	 */
	public PutObjectResult uploadFile(OSS ossClient, String key, String path) {
		return ossClient.putObject(this.bucketName, key, new File(path));
	}

	/**
	 * 内网上传文件
	 *
	 * @param key  文件路径key
	 * @param path 本地文件路径
	 */
	public PutObjectResult uploadFile(String key, String path) {
		return this.getPrivateOssClient().putObject(this.bucketName, key, new File(path));
	}

	/**
	 * 内网上传文件
	 *
	 * @param in   文件流
	 * @param path 本地路径
	 */
	public String uploadFile(InputStream in, String path) {
		ObjectMetadata objectMeta = new ObjectMetadata();
		try {
			objectMeta.setContentLength((long) in.available());
			this.getPrivateOssClient().putObject(this.bucketName, path, in, objectMeta);
			return path;
		} catch (IOException var5) {
			log.error("上传文件出错, path={}", path, var5);
			throw new RuntimeException("上传文件出错");
		}
	}

	/**
	 * 生成临时STS信息
	 *
	 * @return STS 字符串
	 */
	public AssumeRoleResponse getOssInfo() {
		if (StringUtils.isBlank(this.arn)) {
			throw new RuntimeException("尚未配置arn信息");
		}
		return OssStsUtils.genOssInfo(this.accessKeyId, this.secretAccessKey, this.bucketName, this.arn);
	}

	/**
	 * 取bucketName下所有的文件
	 *
	 * @param bucketName
	 * @return 文件
	 */
	public List<OSSObjectSummary> listAllObjects(final String bucketName) {
		return listAllObjects(bucketName, null);
	}

	/**
	 * 取bucketName下指定前缀的所有文件
	 *
	 * @param bucketName bucketName
	 * @param prefix     prefix
	 * @return 文件
	 */
	public List<OSSObjectSummary> listAllObjects(final String bucketName, final String prefix) {
		return listAllObjects(bucketName, prefix, null);
	}

	/**
	 * 取bucketName下指定前缀的所有文件，并每次循环取maxKeys数的文件数
	 *
	 * @param bucketName bucketName
	 * @param prefix     prefix
	 * @param maxKeys    指定每次分页取的文件数
	 * @return 文件
	 */
	public List<OSSObjectSummary> listAllObjects(final String bucketName, final String prefix, Integer maxKeys) {
		maxKeys = maxKeys == null ? 100 : maxKeys;
		String nextMarker = null;
		List<OSSObjectSummary> sums = new ArrayList<>();
		ObjectListing objectListing;
		do {
			objectListing = listObjects(new ListObjectsRequest(bucketName, prefix, nextMarker, null, maxKeys));
			sums.addAll(objectListing.getObjectSummaries());
			nextMarker = objectListing.getNextMarker();
		} while (objectListing.isTruncated());
		return sums;
	}


	@Override
	public void switchCredentials(Credentials creds) {
		this.privateOssClient.switchCredentials(creds);
	}

	@Override
	public void switchSignatureVersion(SignVersion signatureVersion) {
		this.privateOssClient.switchSignatureVersion(signatureVersion);
	}

	public CredentialsProvider getCredentialsProvider() {
		return this.privateOssClient.getCredentialsProvider();
	}

	public ClientConfiguration getClientConfiguration() {
		return this.privateOssClient.getClientConfiguration();
	}

	@Override
	public Bucket createBucket(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.createBucket(bucketName);
	}

	@Override
	public Bucket createBucket(CreateBucketRequest createBucketRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.createBucket(createBucketRequest);
	}

	@Override
	public void deleteBucket(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteBucket(bucketName);
	}

	@Override
	public void deleteBucket(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteBucket(genericRequest);
	}

	@Override
	public List<Bucket> listBuckets() throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listBuckets();
	}

	@Override
	public BucketList listBuckets(ListBucketsRequest listBucketsRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listBuckets(listBucketsRequest);
	}

	@Override
	public BucketList listBuckets(String prefix, String marker, Integer maxKeys) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listBuckets(prefix, marker, maxKeys);
	}

	@Override
	public void setBucketAcl(String bucketName, CannedAccessControlList cannedACL) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setBucketAcl(bucketName, cannedACL);
	}

	@Override
	public void setBucketAcl(SetBucketAclRequest setBucketAclRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setBucketAcl(setBucketAclRequest);
	}

	@Override
	public AccessControlList getBucketAcl(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketAcl(bucketName);
	}

	@Override
	public AccessControlList getBucketAcl(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketAcl(genericRequest);
	}

	@Override
	public BucketMetadata getBucketMetadata(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketMetadata(bucketName);
	}

	@Override
	public BucketMetadata getBucketMetadata(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketMetadata(genericRequest);
	}

	@Override
	public void setBucketReferer(String bucketName, BucketReferer referer) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setBucketReferer(bucketName, referer);
	}

	@Override
	public void setBucketReferer(SetBucketRefererRequest setBucketRefererRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setBucketReferer(setBucketRefererRequest);
	}

	@Override
	public BucketReferer getBucketReferer(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketReferer(bucketName);
	}

	@Override
	public BucketReferer getBucketReferer(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketReferer(genericRequest);
	}

	@Override
	public String getBucketLocation(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketLocation(bucketName);
	}

	@Override
	public String getBucketLocation(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getBucketLocation(genericRequest);
	}

	@Override
	public boolean doesBucketExist(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesBucketExist(bucketName);
	}

	@Override
	public boolean doesBucketExist(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesBucketExist(genericRequest);
	}

	public boolean isBucketExist(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.isBucketExist(bucketName);
	}

	@Override
	public ObjectListing listObjects(String bucketName) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listObjects(bucketName);
	}

	@Override
	public ObjectListing listObjects(String bucketName, String prefix) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listObjects(bucketName, prefix);
	}

	@Override
	public ObjectListing listObjects(ListObjectsRequest listObjectsRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listObjects(listObjectsRequest);
	}

	@Override
	public VersionListing listVersions(String bucketName, String prefix) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listVersions(bucketName, prefix);
	}

	@Override
	public VersionListing listVersions(String bucketName, String prefix, String keyMarker, String versionIdMarker, String delimiter, Integer maxResults) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listVersions(bucketName, prefix, keyMarker, versionIdMarker, delimiter, maxResults);
	}

	@Override
	public VersionListing listVersions(ListVersionsRequest listVersionsRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.listVersions(listVersionsRequest);
	}

	@Override
	public PutObjectResult putObject(String bucketName, String key, InputStream input) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(bucketName, key, input);
	}

	@Override
	public PutObjectResult putObject(String bucketName, String key, InputStream input, ObjectMetadata metadata) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(bucketName, key, input, metadata);
	}

	@Override
	public PutObjectResult putObject(String bucketName, String key, File file, ObjectMetadata metadata) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(bucketName, key, file, metadata);
	}

	@Override
	public PutObjectResult putObject(String bucketName, String key, File file) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(bucketName, key, file);
	}

	@Override
	public PutObjectResult putObject(PutObjectRequest putObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(putObjectRequest);
	}

	@Override
	public PutObjectResult putObject(URL signedUrl, String filePath, Map<String, String> requestHeaders) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(signedUrl, filePath, requestHeaders);
	}

	@Override
	public PutObjectResult putObject(URL signedUrl, String filePath, Map<String, String> requestHeaders, boolean useChunkEncoding) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(signedUrl, filePath, requestHeaders, useChunkEncoding);
	}

	@Override
	public PutObjectResult putObject(URL signedUrl, InputStream requestContent, long contentLength, Map<String, String> requestHeaders) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(signedUrl, requestContent, contentLength, requestHeaders);
	}

	@Override
	public PutObjectResult putObject(URL signedUrl, InputStream requestContent, long contentLength, Map<String, String> requestHeaders, boolean useChunkEncoding) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.putObject(signedUrl, requestContent, contentLength, requestHeaders, useChunkEncoding);
	}

	@Override
	public CopyObjectResult copyObject(String sourceBucketName, String sourceKey, String destinationBucketName, String destinationKey) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.copyObject(sourceBucketName, sourceKey, destinationBucketName, destinationKey);
	}

	@Override
	public CopyObjectResult copyObject(CopyObjectRequest copyObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.copyObject(copyObjectRequest);
	}

	@Override
	public OSSObject getObject(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObject(bucketName, key);
	}

	@Override
	public ObjectMetadata getObject(GetObjectRequest getObjectRequest, File file) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObject(getObjectRequest, file);
	}

	@Override
	public OSSObject getObject(GetObjectRequest getObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObject(getObjectRequest);
	}

	@Override
	public OSSObject getObject(URL signedUrl, Map<String, String> requestHeaders) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObject(signedUrl, requestHeaders);
	}

	@Override
	public OSSObject selectObject(SelectObjectRequest selectObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.selectObject(selectObjectRequest);
	}

	@Override
	public SimplifiedObjectMeta getSimplifiedObjectMeta(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getSimplifiedObjectMeta(bucketName, key);
	}

	@Override
	public SimplifiedObjectMeta getSimplifiedObjectMeta(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getSimplifiedObjectMeta(genericRequest);
	}

	@Override
	public ObjectMetadata getObjectMetadata(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectMetadata(bucketName, key);
	}

	@Override
	public SelectObjectMetadata createSelectObjectMetadata(CreateSelectObjectMetadataRequest createSelectObjectMetadataRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.createSelectObjectMetadata(createSelectObjectMetadataRequest);
	}

	@Override
	public ObjectMetadata getObjectMetadata(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectMetadata(genericRequest);
	}

	@Override
	public ObjectMetadata headObject(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.headObject(bucketName, key);
	}

	@Override
	public ObjectMetadata headObject(HeadObjectRequest headObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.headObject(headObjectRequest);
	}

	@Override
	public AppendObjectResult appendObject(AppendObjectRequest appendObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.appendObject(appendObjectRequest);
	}

	@Override
	public void deleteObject(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteObject(bucketName, key);
	}

	@Override
	public void deleteObject(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteObject(genericRequest);
	}

	@Override
	public void deleteVersion(String bucketName, String key, String versionId) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteVersion(bucketName, key, versionId);
	}

	@Override
	public void deleteVersion(DeleteVersionRequest deleteVersionRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteVersion(deleteVersionRequest);
	}

	@Override
	public DeleteObjectsResult deleteObjects(DeleteObjectsRequest deleteObjectsRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.deleteObjects(deleteObjectsRequest);
	}

	@Override
	public DeleteVersionsResult deleteVersions(DeleteVersionsRequest deleteVersionsRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.deleteVersions(deleteVersionsRequest);
	}

	@Override
	public boolean doesObjectExist(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesObjectExist(bucketName, key);
	}

	@Override
	public boolean doesObjectExist(String bucketName, String key, boolean isOnlyInOSS) {
		return this.privateOssClient.doesObjectExist(bucketName, key, isOnlyInOSS);
	}

	@Override
	public boolean doesObjectExist(HeadObjectRequest headObjectRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesObjectExist(headObjectRequest);
	}

	@Override
	public boolean doesObjectExist(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesObjectExist(genericRequest);
	}

	@Override
	public boolean doesObjectExist(GenericRequest genericRequest, boolean isOnlyInOSS) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.doesObjectExist(genericRequest, isOnlyInOSS);
	}

	@Override
	public void setObjectAcl(String bucketName, String key, CannedAccessControlList cannedACL) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setObjectAcl(bucketName, key, cannedACL);
	}

	@Override
	public void setObjectAcl(SetObjectAclRequest setObjectAclRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setObjectAcl(setObjectAclRequest);
	}

	@Override
	public ObjectAcl getObjectAcl(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectAcl(bucketName, key);
	}

	@Override
	public ObjectAcl getObjectAcl(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectAcl(genericRequest);
	}

	@Override
	public RestoreObjectResult restoreObject(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.restoreObject(bucketName, key);
	}

	@Override
	public RestoreObjectResult restoreObject(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.restoreObject(genericRequest);
	}

	@Override
	public void setObjectTagging(String bucketName, String key, Map<String, String> tags) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setObjectTagging(bucketName, key, tags);
	}

	@Override
	public void setObjectTagging(String bucketName, String key, TagSet tagSet) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setObjectTagging(bucketName, key, tagSet);
	}

	@Override
	public void setObjectTagging(SetObjectTaggingRequest setObjectTaggingRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.setObjectTagging(setObjectTaggingRequest);
	}

	@Override
	public TagSet getObjectTagging(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectTagging(bucketName, key);
	}

	@Override
	public TagSet getObjectTagging(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		return this.privateOssClient.getObjectTagging(genericRequest);
	}

	@Override
	public void deleteObjectTagging(String bucketName, String key) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteObjectTagging(bucketName, key);
	}

	@Override
	public void deleteObjectTagging(GenericRequest genericRequest) throws com.aliyun.oss.OSSException, com.aliyun.oss.ClientException {
		this.privateOssClient.deleteObjectTagging(genericRequest);
	}

	@Override
	public URL generatePresignedUrl(String bucketName, String key, Date expiration) throws com.aliyun.oss.ClientException {
		return this.privateOssClient.generatePresignedUrl(bucketName, key, expiration);
	}

	@Override
	public URL generatePresignedUrl(String bucketName, String key, Date expiration, HttpMethod method) throws ClientException {
		return this.privateOssClient.generatePresignedUrl(bucketName, key, expiration, method);
	}

	@Override
	public URL generatePresignedUrl(GeneratePresignedUrlRequest request) throws ClientException {
		return this.privateOssClient.generatePresignedUrl(request);
	}

	@Override
	public void abortMultipartUpload(AbortMultipartUploadRequest request) throws OSSException, ClientException {
		this.privateOssClient.abortMultipartUpload(request);
	}

	@Override
	public CompleteMultipartUploadResult completeMultipartUpload(CompleteMultipartUploadRequest request) throws OSSException, ClientException {
		return this.privateOssClient.completeMultipartUpload(request);
	}

	@Override
	public InitiateMultipartUploadResult initiateMultipartUpload(InitiateMultipartUploadRequest request) throws OSSException, ClientException {
		return this.privateOssClient.initiateMultipartUpload(request);
	}

	@Override
	public MultipartUploadListing listMultipartUploads(ListMultipartUploadsRequest request) throws OSSException, ClientException {
		return this.privateOssClient.listMultipartUploads(request);
	}

	@Override
	public PartListing listParts(ListPartsRequest request) throws OSSException, ClientException {
		return this.privateOssClient.listParts(request);
	}

	@Override
	public UploadPartResult uploadPart(UploadPartRequest request) throws OSSException, ClientException {
		return this.privateOssClient.uploadPart(request);
	}

	@Override
	public UploadPartCopyResult uploadPartCopy(UploadPartCopyRequest request) throws OSSException, ClientException {
		return this.privateOssClient.uploadPartCopy(request);
	}

	@Override
	public void setBucketCORS(SetBucketCORSRequest request) throws OSSException, ClientException {
		this.privateOssClient.setBucketCORS(request);
	}

	@Override
	public List<SetBucketCORSRequest.CORSRule> getBucketCORSRules(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketCORSRules(bucketName);
	}

	@Override
	public List<SetBucketCORSRequest.CORSRule> getBucketCORSRules(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketCORSRules(genericRequest);
	}

	@Override
	public void deleteBucketCORSRules(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketCORSRules(bucketName);
	}

	@Override
	public void deleteBucketCORSRules(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketCORSRules(genericRequest);
	}

	@Override
	public ResponseMessage optionsObject(OptionsRequest request) throws OSSException, ClientException {
		return this.privateOssClient.optionsObject(request);
	}

	@Override
	public void setBucketLogging(SetBucketLoggingRequest request) throws OSSException, ClientException {
		this.privateOssClient.setBucketLogging(request);
	}

	@Override
	public BucketLoggingResult getBucketLogging(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketLogging(bucketName);
	}

	@Override
	public BucketLoggingResult getBucketLogging(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketLogging(genericRequest);
	}

	@Override
	public void deleteBucketLogging(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketLogging(bucketName);
	}

	@Override
	public void deleteBucketLogging(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketLogging(genericRequest);
	}

	@Override
	public void putBucketImage(PutBucketImageRequest request) throws OSSException, ClientException {
		this.privateOssClient.putBucketImage(request);
	}

	@Override
	public GetBucketImageResult getBucketImage(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketImage(bucketName);
	}

	@Override
	public GetBucketImageResult getBucketImage(String bucketName, GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketImage(bucketName, genericRequest);
	}

	@Override
	public void deleteBucketImage(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketImage(bucketName);
	}

	@Override
	public void deleteBucketImage(String bucketName, GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketImage(bucketName, genericRequest);
	}

	@Override
	public void putImageStyle(PutImageStyleRequest putImageStyleRequest) throws OSSException, ClientException {
		this.privateOssClient.putImageStyle(putImageStyleRequest);
	}

	@Override
	public void deleteImageStyle(String bucketName, String styleName) throws OSSException, ClientException {
		this.privateOssClient.deleteImageStyle(bucketName, styleName);
	}

	@Override
	public void deleteImageStyle(String bucketName, String styleName, GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteImageStyle(bucketName, styleName, genericRequest);
	}

	@Override
	public GetImageStyleResult getImageStyle(String bucketName, String styleName) throws OSSException, ClientException {
		return this.privateOssClient.getImageStyle(bucketName, styleName);
	}

	@Override
	public GetImageStyleResult getImageStyle(String bucketName, String styleName, GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getImageStyle(bucketName, styleName, genericRequest);
	}

	@Override
	public List<Style> listImageStyle(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.listImageStyle(bucketName);
	}

	@Override
	public List<Style> listImageStyle(String bucketName, GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.listImageStyle(bucketName, genericRequest);
	}

	@Override
	public void setBucketProcess(SetBucketProcessRequest setBucketProcessRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketProcess(setBucketProcessRequest);
	}

	@Override
	public BucketProcess getBucketProcess(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketProcess(bucketName);
	}

	@Override
	public BucketProcess getBucketProcess(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketProcess(genericRequest);
	}

	@Override
	public void setBucketWebsite(SetBucketWebsiteRequest setBucketWebSiteRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketWebsite(setBucketWebSiteRequest);
	}

	@Override
	public BucketWebsiteResult getBucketWebsite(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketWebsite(bucketName);
	}

	@Override
	public BucketWebsiteResult getBucketWebsite(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketWebsite(genericRequest);
	}

	@Override
	public void deleteBucketWebsite(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketWebsite(bucketName);
	}

	@Override
	public void deleteBucketWebsite(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketWebsite(genericRequest);
	}

	@Override
	public BucketVersioningConfiguration getBucketVersioning(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketVersioning(bucketName);
	}

	@Override
	public BucketVersioningConfiguration getBucketVersioning(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketVersioning(genericRequest);
	}

	@Override
	public void setBucketVersioning(SetBucketVersioningRequest setBucketVersioningRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketVersioning(setBucketVersioningRequest);
	}

	@Override
	public String generatePostPolicy(Date expiration, PolicyConditions conds) {
		return this.privateOssClient.generatePostPolicy(expiration, conds);
	}

	@Override
	public String calculatePostSignature(String postPolicy) throws ClientException {
		return this.privateOssClient.calculatePostSignature(postPolicy);
	}

	@Override
	public void setBucketLifecycle(SetBucketLifecycleRequest setBucketLifecycleRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketLifecycle(setBucketLifecycleRequest);
	}

	@Override
	public List<LifecycleRule> getBucketLifecycle(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketLifecycle(bucketName);
	}

	@Override
	public List<LifecycleRule> getBucketLifecycle(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketLifecycle(genericRequest);
	}

	@Override
	public void deleteBucketLifecycle(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketLifecycle(bucketName);
	}

	@Override
	public void deleteBucketLifecycle(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketLifecycle(genericRequest);
	}

	@Override
	public void setBucketTagging(String bucketName, Map<String, String> tags) throws OSSException, ClientException {
		this.privateOssClient.setBucketTagging(bucketName, tags);
	}

	@Override
	public void setBucketTagging(String bucketName, TagSet tagSet) throws OSSException, ClientException {
		this.privateOssClient.setBucketTagging(bucketName, tagSet);
	}

	@Override
	public void setBucketTagging(SetBucketTaggingRequest setBucketTaggingRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketTagging(setBucketTaggingRequest);
	}

	@Override
	public TagSet getBucketTagging(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketTagging(bucketName);
	}

	@Override
	public TagSet getBucketTagging(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketTagging(genericRequest);
	}

	@Override
	public void deleteBucketTagging(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketTagging(bucketName);
	}

	@Override
	public void deleteBucketTagging(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketTagging(genericRequest);
	}

	@Override
	public void addBucketReplication(AddBucketReplicationRequest addBucketReplicationRequest) throws OSSException, ClientException {
		this.privateOssClient.addBucketReplication(addBucketReplicationRequest);
	}

	@Override
	public List<ReplicationRule> getBucketReplication(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplication(bucketName);
	}

	@Override
	public List<ReplicationRule> getBucketReplication(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplication(genericRequest);
	}

	@Override
	public void deleteBucketReplication(String bucketName, String replicationRuleID) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketReplication(bucketName, replicationRuleID);
	}

	@Override
	public void deleteBucketReplication(DeleteBucketReplicationRequest deleteBucketReplicationRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketReplication(deleteBucketReplicationRequest);
	}

	@Override
	public BucketReplicationProgress getBucketReplicationProgress(String bucketName, String replicationRuleID) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplicationProgress(bucketName, replicationRuleID);
	}

	@Override
	public BucketReplicationProgress getBucketReplicationProgress(GetBucketReplicationProgressRequest getBucketReplicationProgressRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplicationProgress(getBucketReplicationProgressRequest);
	}

	@Override
	public List<String> getBucketReplicationLocation(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplicationLocation(bucketName);
	}

	@Override
	public List<String> getBucketReplicationLocation(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketReplicationLocation(genericRequest);
	}

	@Override
	public void addBucketCname(AddBucketCnameRequest addBucketCnameRequest) throws OSSException, ClientException {
		this.privateOssClient.addBucketCname(addBucketCnameRequest);
	}

	@Override
	public List<CnameConfiguration> getBucketCname(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketCname(bucketName);
	}

	@Override
	public List<CnameConfiguration> getBucketCname(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketCname(genericRequest);
	}

	@Override
	public void deleteBucketCname(String bucketName, String domain) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketCname(bucketName, domain);
	}

	@Override
	public void deleteBucketCname(DeleteBucketCnameRequest deleteBucketCnameRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketCname(deleteBucketCnameRequest);
	}

	@Override
	public BucketInfo getBucketInfo(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketInfo(bucketName);
	}

	@Override
	public BucketInfo getBucketInfo(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketInfo(genericRequest);
	}

	@Override
	public BucketStat getBucketStat(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketStat(bucketName);
	}

	@Override
	public BucketStat getBucketStat(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketStat(genericRequest);
	}

	@Override
	public void setBucketStorageCapacity(String bucketName, UserQos userQos) throws OSSException, ClientException {
		this.privateOssClient.setBucketStorageCapacity(bucketName, userQos);
	}

	@Override
	public void setBucketStorageCapacity(SetBucketStorageCapacityRequest setBucketStorageCapacityRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketStorageCapacity(setBucketStorageCapacityRequest);
	}

	@Override
	public UserQos getBucketStorageCapacity(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketStorageCapacity(bucketName);
	}

	@Override
	public UserQos getBucketStorageCapacity(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketStorageCapacity(genericRequest);
	}

	@Override
	public void setBucketEncryption(SetBucketEncryptionRequest setBucketEncryptionRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketEncryption(setBucketEncryptionRequest);
	}

	@Override
	public ServerSideEncryptionConfiguration getBucketEncryption(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketEncryption(bucketName);
	}

	@Override
	public ServerSideEncryptionConfiguration getBucketEncryption(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketEncryption(genericRequest);
	}

	@Override
	public void deleteBucketEncryption(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketEncryption(bucketName);
	}

	@Override
	public void deleteBucketEncryption(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketEncryption(genericRequest);
	}

	@Override
	public void setBucketPolicy(String bucketName, String policyText) throws OSSException, ClientException {
		this.privateOssClient.setBucketPolicy(bucketName, policyText);
	}

	@Override
	public void setBucketPolicy(SetBucketPolicyRequest setBucketPolicyRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketPolicy(setBucketPolicyRequest);
	}

	@Override
	public GetBucketPolicyResult getBucketPolicy(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketPolicy(genericRequest);
	}

	@Override
	public GetBucketPolicyResult getBucketPolicy(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketPolicy(bucketName);
	}

	@Override
	public void deleteBucketPolicy(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketPolicy(genericRequest);
	}

	@Override
	public void deleteBucketPolicy(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketPolicy(bucketName);
	}

	@Override
	public UploadFileResult uploadFile(UploadFileRequest uploadFileRequest) throws Throwable {
		return this.privateOssClient.uploadFile(uploadFileRequest);
	}

	@Override
	public DownloadFileResult downloadFile(DownloadFileRequest downloadFileRequest) throws Throwable {
		return this.privateOssClient.downloadFile(downloadFileRequest);
	}

	@Override
	public CreateLiveChannelResult createLiveChannel(CreateLiveChannelRequest createLiveChannelRequest) throws OSSException, ClientException {
		return this.privateOssClient.createLiveChannel(createLiveChannelRequest);
	}

	@Override
	public void setLiveChannelStatus(String bucketName, String liveChannel, LiveChannelStatus status) throws OSSException, ClientException {
		this.privateOssClient.setLiveChannelStatus(bucketName, liveChannel, status);
	}

	@Override
	public void setLiveChannelStatus(SetLiveChannelRequest setLiveChannelRequest) throws OSSException, ClientException {
		this.privateOssClient.setLiveChannelStatus(setLiveChannelRequest);
	}

	@Override
	public LiveChannelInfo getLiveChannelInfo(String bucketName, String liveChannel) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelInfo(bucketName, liveChannel);
	}

	@Override
	public LiveChannelInfo getLiveChannelInfo(LiveChannelGenericRequest liveChannelGenericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelInfo(liveChannelGenericRequest);
	}

	@Override
	public LiveChannelStat getLiveChannelStat(String bucketName, String liveChannel) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelStat(bucketName, liveChannel);
	}

	@Override
	public LiveChannelStat getLiveChannelStat(LiveChannelGenericRequest liveChannelGenericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelStat(liveChannelGenericRequest);
	}

	@Override
	public void deleteLiveChannel(String bucketName, String liveChannel) throws OSSException, ClientException {
		this.privateOssClient.deleteLiveChannel(bucketName, liveChannel);
	}

	@Override
	public void deleteLiveChannel(LiveChannelGenericRequest liveChannelGenericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteLiveChannel(liveChannelGenericRequest);
	}

	@Override
	public List<LiveChannel> listLiveChannels(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.listLiveChannels(bucketName);
	}

	@Override
	public LiveChannelListing listLiveChannels(ListLiveChannelsRequest listLiveChannelRequest) throws OSSException, ClientException {
		return this.privateOssClient.listLiveChannels(listLiveChannelRequest);
	}

	@Override
	public List<LiveRecord> getLiveChannelHistory(String bucketName, String liveChannel) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelHistory(bucketName, liveChannel);
	}

	@Override
	public List<LiveRecord> getLiveChannelHistory(LiveChannelGenericRequest liveChannelGenericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getLiveChannelHistory(liveChannelGenericRequest);
	}

	@Override
	public void generateVodPlaylist(String bucketName, String liveChannelName, String PlaylistName, long startTime, long endTime) throws OSSException, ClientException {
		this.privateOssClient.generateVodPlaylist(bucketName, liveChannelName, PlaylistName, startTime, endTime);
	}

	@Override
	public void generateVodPlaylist(GenerateVodPlaylistRequest generateVodPlaylistRequest) throws OSSException, ClientException {
		this.privateOssClient.generateVodPlaylist(generateVodPlaylistRequest);
	}

	@Override
	public OSSObject getVodPlaylist(String bucketName, String liveChannelName, long startTime, long endTime) throws OSSException, ClientException {
		return this.privateOssClient.getVodPlaylist(bucketName, liveChannelName, startTime, endTime);
	}

	@Override
	public OSSObject getVodPlaylist(GetVodPlaylistRequest getVodPlaylistRequest) throws OSSException, ClientException {
		return this.privateOssClient.getVodPlaylist(getVodPlaylistRequest);
	}

	@Override
	public String generateRtmpUri(String bucketName, String liveChannelName, String PlaylistName, long expires) throws OSSException, ClientException {
		return this.privateOssClient.generateRtmpUri(bucketName, liveChannelName, PlaylistName, expires);
	}

	@Override
	public String generateRtmpUri(GenerateRtmpUriRequest generateRtmpUriRequest) throws OSSException, ClientException {
		return this.privateOssClient.generateRtmpUri(generateRtmpUriRequest);
	}

	@Override
	public void createSymlink(String bucketName, String symLink, String targetObject) throws OSSException, ClientException {
		this.privateOssClient.createSymlink(bucketName, symLink, targetObject);
	}

	@Override
	public void createSymlink(CreateSymlinkRequest createSymlinkRequest) throws OSSException, ClientException {
		this.privateOssClient.createSymlink(createSymlinkRequest);
	}

	@Override
	public OSSSymlink getSymlink(String bucketName, String symLink) throws OSSException, ClientException {
		return this.privateOssClient.getSymlink(bucketName, symLink);
	}

	@Override
	public OSSSymlink getSymlink(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getSymlink(genericRequest);
	}

	@Override
	public GenericResult processObject(ProcessObjectRequest processObjectRequest) throws OSSException, ClientException {
		return this.privateOssClient.processObject(processObjectRequest);
	}

	@Override
	public void setBucketRequestPayment(String bucketName, Payer payer) throws OSSException, ClientException {
		this.privateOssClient.setBucketRequestPayment(bucketName, payer);
	}

	@Override
	public void setBucketRequestPayment(SetBucketRequestPaymentRequest setBucketRequestPaymentRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketRequestPayment(setBucketRequestPaymentRequest);
	}

	@Override
	public GetBucketRequestPaymentResult getBucketRequestPayment(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketRequestPayment(bucketName);
	}

	@Override
	public GetBucketRequestPaymentResult getBucketRequestPayment(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketRequestPayment(genericRequest);
	}

	@Override
	public void setBucketQosInfo(String bucketName, BucketQosInfo bucketQosInfo) throws OSSException, ClientException {
		this.privateOssClient.setBucketQosInfo(bucketName, bucketQosInfo);
	}

	@Override
	public void setBucketQosInfo(SetBucketQosInfoRequest setBucketQosInfoRequest) throws OSSException, ClientException {
		this.privateOssClient.setBucketQosInfo(setBucketQosInfoRequest);
	}

	@Override
	public BucketQosInfo getBucketQosInfo(String bucketName) throws OSSException, ClientException {
		return this.privateOssClient.getBucketQosInfo(bucketName);
	}

	@Override
	public BucketQosInfo getBucketQosInfo(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketQosInfo(genericRequest);
	}

	@Override
	public void deleteBucketQosInfo(String bucketName) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketQosInfo(bucketName);
	}

	@Override
	public void deleteBucketQosInfo(GenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketQosInfo(genericRequest);
	}

	@Override
	public UserQosInfo getUserQosInfo() throws OSSException, ClientException {
		return this.privateOssClient.getUserQosInfo();
	}

	@Override
	public SetAsyncFetchTaskResult setAsyncFetchTask(String bucketName, AsyncFetchTaskConfiguration asyncFetchTaskConfiguration) throws OSSException, ClientException {
		return this.privateOssClient.setAsyncFetchTask(bucketName, asyncFetchTaskConfiguration);
	}

	@Override
	public SetAsyncFetchTaskResult setAsyncFetchTask(SetAsyncFetchTaskRequest setAsyncFetchTaskRequest) throws OSSException, ClientException {
		return this.privateOssClient.setAsyncFetchTask(setAsyncFetchTaskRequest);
	}

	@Override
	public GetAsyncFetchTaskResult getAsyncFetchTask(String bucketName, String taskId) throws OSSException, ClientException {
		return this.privateOssClient.getAsyncFetchTask(bucketName, taskId);
	}

	@Override
	public GetAsyncFetchTaskResult getAsyncFetchTask(GetAsyncFetchTaskRequest getAsyncFetchTaskRequest) throws OSSException, ClientException {
		return this.privateOssClient.getAsyncFetchTask(getAsyncFetchTaskRequest);
	}

	@Override
	public CreateVpcipResult createVpcip(CreateVpcipRequest createVpcipRequest) throws OSSException, ClientException {
		return this.privateOssClient.createVpcip(createVpcipRequest);
	}

	@Override
	public List<Vpcip> listVpcip() throws OSSException, ClientException {
		return this.privateOssClient.listVpcip();
	}

	@Override
	public void deleteVpcip(DeleteVpcipRequest deleteVpcipRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteVpcip(deleteVpcipRequest);
	}

	@Override
	public void createBucketVpcip(CreateBucketVpcipRequest createBucketVpcipRequest) throws OSSException, ClientException {
		this.privateOssClient.createBucketVpcip(createBucketVpcipRequest);
	}

	@Override
	public void deleteBucketVpcip(DeleteBucketVpcipRequest deleteBucketVpcipRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteBucketVpcip(deleteBucketVpcipRequest);
	}

	@Override
	public List<VpcPolicy> getBucketVpcip(GenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getBucketVpcip(genericRequest);
	}

	@Override
	public void createUdf(CreateUdfRequest createUdfRequest) throws OSSException, ClientException {
		this.privateOssClient.createUdf(createUdfRequest);
	}

	@Override
	public UdfInfo getUdfInfo(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getUdfInfo(genericRequest);
	}

	@Override
	public List<UdfInfo> listUdfs() throws OSSException, ClientException {
		return this.privateOssClient.listUdfs();
	}

	@Override
	public void deleteUdf(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteUdf(genericRequest);
	}

	@Override
	public void uploadUdfImage(UploadUdfImageRequest uploadUdfImageRequest) throws OSSException, ClientException {
		this.privateOssClient.uploadUdfImage(uploadUdfImageRequest);
	}

	@Override
	public List<UdfImageInfo> getUdfImageInfo(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getUdfImageInfo(genericRequest);
	}

	@Override
	public void deleteUdfImage(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteUdfImage(genericRequest);
	}

	@Override
	public void createUdfApplication(CreateUdfApplicationRequest createUdfApplicationRequest) throws OSSException, ClientException {
		this.privateOssClient.createUdfApplication(createUdfApplicationRequest);
	}

	@Override
	public UdfApplicationInfo getUdfApplicationInfo(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		return this.privateOssClient.getUdfApplicationInfo(genericRequest);
	}

	@Override
	public List<UdfApplicationInfo> listUdfApplications() throws OSSException, ClientException {
		return this.privateOssClient.listUdfApplications();
	}

	@Override
	public void deleteUdfApplication(UdfGenericRequest genericRequest) throws OSSException, ClientException {
		this.privateOssClient.deleteUdfApplication(genericRequest);
	}

	@Override
	public void upgradeUdfApplication(UpgradeUdfApplicationRequest upgradeUdfApplicationRequest) throws OSSException, ClientException {
		this.privateOssClient.upgradeUdfApplication(upgradeUdfApplicationRequest);
	}

	@Override
	public void resizeUdfApplication(ResizeUdfApplicationRequest resizeUdfApplicationRequest) throws OSSException, ClientException {
		this.privateOssClient.resizeUdfApplication(resizeUdfApplicationRequest);
	}

	@Override
	public UdfApplicationLog getUdfApplicationLog(GetUdfApplicationLogRequest getUdfApplicationLogRequest) throws OSSException, ClientException {
		return this.privateOssClient.getUdfApplicationLog(getUdfApplicationLogRequest);
	}

	@Override
	public void shutdown() {
		if (this.privateOssClient != null) {
			this.privateOssClient.shutdown();
		}
		if (this.publicOssClient != null) {
			this.publicOssClient.shutdown();
		}
	}

}
