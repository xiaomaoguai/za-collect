package com.xiaomaoguai.fcp.pre.kepler.glue.config;

import lombok.Data;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/20 17:13
 * @since JDK 1.8
 */
@Data
public class GlueRedisConfig {

	private Boolean cluster = false;

	private Boolean sentinel = false;

	/**
	 * cluster url example:ip:port;ip:port.
	 */
	private String clusterUrl;

	/**
	 * sentinel url example:ip:port;ip:port.
	 */
	private String sentinelUrl;

	private String masterName;

	private String hostName;

	private int port;

	private String password;

	private int maxTotal = 8;

	private int maxIdle = 8;

	private int minIdle;

	private long maxWaitMillis = -1L;

	private long minEvictableIdleTimeMillis = 1800000L;

	private long softMinEvictableIdleTimeMillis = 1800000L;

	private int numTestsPerEvictionRun = 3;

	private Boolean testOnCreate = false;

	private Boolean testOnBorrow = false;

	private Boolean testOnReturn = false;

	private Boolean testWhileIdle = false;

	private long timeBetweenEvictionRunsMillis = -1L;

	private boolean blockWhenExhausted = true;

	private int timeOut = 10000;

}
