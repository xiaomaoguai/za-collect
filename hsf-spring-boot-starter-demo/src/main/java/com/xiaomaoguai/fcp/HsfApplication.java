package com.xiaomaoguai.fcp;

import com.alibaba.fastjson.JSON;
import com.xiaomaoguai.fcp.hsf.autoconfigure.EnableHsfClients;
import com.xiaomaoguai.fcp.service.MyInvoke;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * @fileName: HsfApplication.java
 * @author: WeiHui
 * @date: 2018/11/20 21:09
 * @version: v1.0.0
 * @since JDK 1.8
 */
@EnableHsfClients
@SpringBootApplication
public class HsfApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext applicationContext = SpringApplication.run(HsfApplication.class, args);
		MyInvoke myInvoke = applicationContext.getBean(MyInvoke.class);
		Object data2 = myInvoke.getData2();
		System.out.println(JSON.toJSONString(data2));
	}

}
