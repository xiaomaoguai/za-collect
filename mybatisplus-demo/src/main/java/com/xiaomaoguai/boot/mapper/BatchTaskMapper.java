package com.xiaomaoguai.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaoguai.boot.entity.BatchTask;

/**
 * <p>
 * 批量任务信息表 Mapper 接口
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
public interface BatchTaskMapper extends BaseMapper<BatchTask> {

}
