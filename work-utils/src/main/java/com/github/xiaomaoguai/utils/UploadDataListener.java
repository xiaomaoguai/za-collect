package com.github.xiaomaoguai.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/26 16:20
 * @since JDK 1.8
 */
public class UploadDataListener extends AnalysisEventListener<UploadData> {

	private static final Logger log = LoggerFactory.getLogger(UploadDataListener.class);

	@Getter
	private List<UploadData> list = new ArrayList<>();

	@Override
	public void invoke(UploadData uploadData, AnalysisContext analysisContext) {
		list.add(uploadData);
	}

	@Override
	public void doAfterAllAnalysed(AnalysisContext analysisContext) {
		log.info("所有数据解析完成！，条数: {}", list.size());
	}

}
