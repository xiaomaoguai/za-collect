package com.xiaomaoguai.fcp.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xiaomaoguai.fcp.utils.Decimal2StringSerialize;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class BaseRepayPlan {

    /**
     * 当前期数
     */
    private Integer installmentNo;

    /**
     * 约定还款日
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    private Date agreeRepayDate;

    /**
     * 应还本金
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal principal;

    /**
     * 应还利息
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal interest;

    /**
     * 应还服务费
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal charge;

    /**
     * 罚息
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal penalty;

    /**
     * 其他费用
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal otherAmount;

    /**
     * 状态 0 未还 1已还
     */
    private Integer status;

    /**
     * AHEAD(0, "当期提前还款"), NORMAL(1, "当期正常还款"), OVERDUE(2, "当期逾期还款"),
     */
    private Integer repayType;

    /**
     * 当期标识 0-非当期 1-当期
     */
    private Integer currentFlag;

    /**
     * 减免金额
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal reduceAmount;

    /**
     * 每期保费
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal policyRate;

}
