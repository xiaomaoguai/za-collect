package com.xiaomaoguai.fcp.pre.kepler.mock.utils;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 装夹方法
 *
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 14:29
 * @since JDK 1.8
 */
public class FeignClassInfoHolder {

	/**
	 * feign class 缓存
	 */
	private static Set<Class<?>> FEIGN_CLASS_NAME_SET = new HashSet<>(64);

	/**
	 * key-请求URI , method -》feign 方法
	 */
	private static Set<Method> FEIGN_METHOD_INFO = new HashSet<>(64);

	/**
	 * 添加装类
	 *
	 * @param feignClass 装类
	 */
	public static void addFeignClass(final Class<?> feignClass) {
		FEIGN_CLASS_NAME_SET.add(feignClass);
	}

	/**
	 * 添加装类
	 *
	 * @param feignClass 装类
	 */
	public static void addFeignClasses(final Set<Class<?>> feignClass) {
		FEIGN_CLASS_NAME_SET.addAll(feignClass);
	}

	/**
	 * 会假装类
	 *
	 * @return {@link Set<String>}
	 */
	public static Set<Class<?>> getFeignClasses() {
		return Collections.unmodifiableSet(FEIGN_CLASS_NAME_SET);
	}

	/**
	 * 添加装方法
	 *
	 * @param method 方法
	 */
	public static void addFeignMethod(final Method method) {
		FEIGN_METHOD_INFO.add(method);
	}

	/**
	 * 有映射
	 *
	 * @param method 方法
	 * @return boolean
	 */
	public static boolean hasMapped(Method method) {
		return FEIGN_METHOD_INFO.contains(method);
	}

}
