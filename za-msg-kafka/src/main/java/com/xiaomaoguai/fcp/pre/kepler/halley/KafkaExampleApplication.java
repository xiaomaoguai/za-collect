package com.xiaomaoguai.fcp.pre.kepler.halley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * 项目启动类
 *
 * @author zhangweihui
 * @since 2018年12月17日 下午4:54:45
 */
@EnableAspectJAutoProxy
@SpringBootApplication
public class KafkaExampleApplication {

	/**
	 * 项目启动类
	 *
	 * @param args 启动参数
	 */
	public static void main(String[] args) {
		SpringApplication.run(KafkaExampleApplication.class, args);
	}

}
