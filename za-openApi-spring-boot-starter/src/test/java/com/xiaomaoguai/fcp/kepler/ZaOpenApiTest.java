package com.xiaomaoguai.fcp.kepler;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/10 20:11
 * @since JDK 1.8
 */
public class ZaOpenApiTest {

	public static void main(String[] args) {
		String env = "dev";
		String version = "1.0.0";
		String appkey = "2d0ea1d7febb99d066a794e7cc923564";
		String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAONBVq2U/oAWQd0u7nFuZDjVsQuE4WgqGoKsY9SNiSOH4NmjVP3UBAzJxISQQ1islRFaHVm2lFvwDNidiwE62A5K/vxJNFNIMAkLcyPRhIMUxxBOBppltS7H/+/M5Ei3xgAbRuUkOU+agjt2j8800IJ7/pVxh2RNxrNIEQSyVcMvAgMBAAECgYBXPpZQPYsXEXRnvcS8t2yyhRdbHMCMhN14nUYWK+AiS9/+rb3LVMHZRvyzB89TE66G4tmxv25lfVrxLkpXRof6+1yL17dbmRwiKeXUXQxhMHzYgl1NOfUoZV8Ui57N4On9748zh1Bamhvsj9ZmSsJ6ZPxv7G9iU6mPtOzme+ODQQJBAPxXO4Hb0qSa2G20qzz4R2hiGzWPAvLMduL3mkvxWBc8gq7egw7R0fr40GmeswlG1TtAuz2o9pGZv5PgzsScWRMCQQDmjPsZM1nOkG957bM9XKHxRye723juh6quqc9peNN9NhFPS8fY93dUlVDiGIa1qgVp3CoL09v/8QILpTpQ7ez1AkBQiLahNzr+9bxlJugPyV1g3w64BTB3tPGsdkF0Q05N/C3pCXLiY+yUIJzDWLbjGwwqoPohL6+hwGP4GiNjdFKpAkBQMmhCGt+5f/qXEj1QMgHPGS5UJYMKjjysJzuT98ixfHZG/BhXe7WRwaLyExAA71SUv1YM6TRU8nQAswf3ENxlAkEAvVLQ4FSLwdMLwmvQSGJjLfZdxZN6uu/v2G7xx4MqL1lsumGTt0xoeV2Hg7uKV3211XA9k0ZnKxardrN8EvPDfw==";
		String serviceName = "xiaomaoguai.cfp.api.beforeLoan";

		Map<String, String> param = new HashMap<>(16);
		param.put("appKey", appkey);
		param.put("serviceName", serviceName);
		param.put("bizContent", "");
		param.put("timestamp", FastDateFormat.getInstance("yyyyMMddHHmmssSSS").format(new Date()));
		param.put("format", "json");
		param.put("sign", "");
		param.put("signType", "RSA");
		param.put("charset", "UTF-8");
		param.put("version", "1.0.0");



	}

}
