package com.xiaomaoguai.fcp.es;

import java.util.List;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/30 10:15
 * @since JDK 1.8
 */
public class DemoSearchResult {

	private List<Demo> demos;

	private long totalSize;

	public void setDemos(List<Demo> demos) {
		this.demos = demos;
	}

	public List<Demo> getDemos() {
		return demos;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public long getTotalSize() {
		return totalSize;
	}

}
