package com.xiaomaoguai.fcp.pre.kepler.glue.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface Hmily spi.
 *
 * @author xiaoyu
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface GlueSPI {

    /**
     * Value string.
     *
     * @return the string
     */
    String value() default "";
}
