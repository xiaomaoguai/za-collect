package com.xiaomaoguai.fcp;

import com.xiaomaoguai.fcp.fcp.GateWayApplication;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = GateWayApplication.class)
public class BaseTest {

	protected Logger log = LoggerFactory.getLogger(getClass());

}