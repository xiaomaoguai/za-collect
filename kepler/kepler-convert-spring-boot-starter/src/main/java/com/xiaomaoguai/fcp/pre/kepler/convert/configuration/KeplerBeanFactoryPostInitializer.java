package com.xiaomaoguai.fcp.pre.kepler.convert.configuration;

import com.dh.convert.rule.RuleLoader;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.runtime.type.AviatorFunction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.util.Map;

/**
 * @author huwenxuan
 * @date 2019/1/1
 */
@Slf4j
public class KeplerBeanFactoryPostInitializer implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		Map<String, AviatorFunction> aviatorFunctionMap = beanFactory.getBeansOfType(AviatorFunction.class);
		if (aviatorFunctionMap.size() > 0) {
			log.info("初始化自定义RuleLoader...");
			aviatorFunctionMap.forEach((k, v) -> {
				log.info("加载Rule:{}", k);
				AviatorEvaluator.addFunction(v);
			});
		}
		RuleLoader.initRule();
	}

}
