package com.xiaomaoguai.fcp.config;

import com.xiaomaoguai.fcp.hsf.autoconfigure.HsfClient;

/**
 * @fileName: LoanHsfClient.java
 * @author: WeiHui
 * @date: 2018/11/20 22:10
 * @version: v1.0.0
 * @since JDK 1.8
 */
@HsfClient(basePackages = "com.xiaomaoguai.creditcore.cashloan.api", url = "${hsf.creditcore.cashloan.url}", version = "${hsf.creditcore.cashloan.version}")
public interface Loan2HsfClient {

}
