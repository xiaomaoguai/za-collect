package com.xiaomaoguai.fcp.pre.kepler.glue.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 17:47
 * @since JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum RepositorySupportEnum {

	/**
	 * Db compensate cache type enum.
	 */
	DB("db"),

	/**
	 * File compensate cache type enum.
	 */
	FILE("file"),

	/**
	 * Redis compensate cache type enum.
	 */
	REDIS("redis"),

	/**
	 * Mongodb compensate cache type enum.
	 */
	MONGODB("mongodb"),

	/**
	 * Zookeeper compensate cache type enum.
	 */
	ZOOKEEPER("zookeeper");

	/**
	 *
	 */
	private final String support;

	/**
	 * Acquire compensate cache type compensate cache type enum.
	 *
	 * @param support the compensate cache type
	 * @return the compensate cache type enum
	 */
	public static RepositorySupportEnum acquire(final String support) {
		Optional<RepositorySupportEnum> repositorySupportEnum =
				Arrays.stream(RepositorySupportEnum.values())
						.filter(v -> Objects.equals(v.getSupport(), support))
						.findFirst();
		return repositorySupportEnum.orElse(RepositorySupportEnum.ZOOKEEPER);
	}

}
