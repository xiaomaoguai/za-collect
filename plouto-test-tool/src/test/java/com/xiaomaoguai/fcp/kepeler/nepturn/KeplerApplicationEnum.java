package com.xiaomaoguai.fcp.kepeler.nepturn;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 17:26
 * @since JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum KeplerApplicationEnum {

	PRELOAN("za-pre-kepler-preloan"),

	INLOAN("za-pre-kepler-inloan"),

	POSTLOAN("za-pre-kepler-postloan"),

	BATCH("za-pre-kepler-batch"),

	FIN("za-pre-kepler-fin"),

	INSURANCE("za-pre-kepler-insurance");

	private final String appName;

}
