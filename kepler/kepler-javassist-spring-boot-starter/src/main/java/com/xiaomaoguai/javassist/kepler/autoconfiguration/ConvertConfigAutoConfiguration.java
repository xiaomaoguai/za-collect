package com.xiaomaoguai.javassist.kepler.autoconfiguration;

import com.googlecode.aviator.runtime.type.AviatorObject;
import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertManager;
import com.xiaomaoguai.javassist.kepler.listener.convertor.ConvertConfigurator;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 格转相关模块自动注册
 * 依赖kepler-convert包
 *
 * @author huwenxuan
 * @date 2019/8/21.
 */
@Configuration
@ConditionalOnClass({ConvertManager.class, AviatorObject.class})
public class ConvertConfigAutoConfiguration {

    @Bean
    public ConvertConfigurator convertConfigurator() {
        return new ConvertConfigurator();
    }

}
