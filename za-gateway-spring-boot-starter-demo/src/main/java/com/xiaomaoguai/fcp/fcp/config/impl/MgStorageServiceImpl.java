package com.xiaomaoguai.fcp.fcp.config.impl;

import com.mg.swagger.framework.service.MgStorageService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 申明为@Service之后网页上才能使用存储能力，同时需要在@EnableSwagger2的地方添加@EnableSwaggerMgUi注解，才能开启存储的接口<br/>
 * 开放存储能力的好处：<br/>
 * 所有网页的配置、调试值都可以存储到服务器的数据库中，便于团队所有人的调试，一人配置，所有人受益<br/>
 * 如果不开启的话，数据是存放在浏览器的localStorage中，每个人、每个浏览器都得配置一次才能使用<br/>
 *
 * @author 暮光：城中城
 * @since 2018年8月19日
 */
@Service
public class MgStorageServiceImpl implements MgStorageService {

	@Resource
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public String get(String key) {
		return redisTemplate.opsForValue().get(key);
	}

	/**
	 * 使用数据库来存储，例：
	 * storageMapper.updateOrInsert(key, value);
	 */
	@Override
	public void put(String key, String value) {
		redisTemplate.opsForValue().set(key, value);
	}

}
