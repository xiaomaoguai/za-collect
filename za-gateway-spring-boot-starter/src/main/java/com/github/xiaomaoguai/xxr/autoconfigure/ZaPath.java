package com.github.xiaomaoguai.xxr.autoconfigure;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 请求路径
 *
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface ZaPath {

	/**
	 * url 路径
	 *
	 * @return 请求路径
	 */
	String value();

	/**
	 * 接口描述
	 *
	 * @return 接口描述
	 */
	String desc() default "接口描述";
}
