package com.xiaomaoguai.fcp.pre.kepler.router.utils;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Charsets;

import java.nio.charset.Charset;

public class SerializeUtils {

	private static final Charset CHARSET = Charsets.UTF_8;

	/**
	 * 没有内容空串
	 */
	private static final byte[] USELESSBUFFER = "".getBytes(Charsets.UTF_8);

	public static byte[] encode(Object stuff) {
		String resp = null;
		if (stuff instanceof String) {
			resp = (String) stuff;
		} else {
			resp = JSON.toJSONString(stuff);
		}
		return resp == null ? USELESSBUFFER : resp.getBytes(CHARSET);
	}

}