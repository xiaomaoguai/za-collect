package com.xiaomaoguai.fcp.fcp.config.redis;

import java.util.EnumSet;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CacheManager 配置
 *
 * @ FileName: RedisCacheKeyEnum.java
 * @ Author: WeiHui-Z
 * @ Date: 2017/10/12 13:56
 * @ Version: v1.0.0
 */
public enum RedisCacheKeyEnum {

	/**
	 * 缓存配置
	 */
	NAME_CACHE("name", 3000L);

	/**
	 * 缓存名称
	 */
	private final String key;

	/**
	 * 过期秒数
	 */
	private final Long expireSeconds;

	RedisCacheKeyEnum(String key, Long expireSeconds) {
		this.key = key;
		this.expireSeconds = expireSeconds;
	}

	private static final Map<String, Long> CACHE_EXPIRES = new ConcurrentHashMap<>();

	private static final Set<String> CACHE_NAMES = new HashSet<>();

	static {
		EnumSet.allOf(RedisCacheKeyEnum.class).forEach(v -> {
			CACHE_NAMES.add(v.key);
			CACHE_EXPIRES.put(v.key, v.expireSeconds);
		});
	}

	public static Map<String, Long> getCacheExpires() {
		return CACHE_EXPIRES;
	}

	public static Set<String> getCacheNames() {
		return CACHE_NAMES;
	}
}
