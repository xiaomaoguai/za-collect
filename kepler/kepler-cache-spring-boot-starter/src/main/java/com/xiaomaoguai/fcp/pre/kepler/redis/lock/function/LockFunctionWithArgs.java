package com.xiaomaoguai.fcp.pre.kepler.redis.lock.function;

/**
 * 使用锁执行方法的时候，使用的回调函数
 * <ul>
 * <li>回调函数有入参
 * </ul>
 *
 * @author Jerry.Chen
 * @since 2018年5月9日 下午5:03:35
 */
public interface LockFunctionWithArgs extends BaseLockFunction {
	/**
	 * 获取锁成功的时候执行
	 *
	 * @param args 入参
	 */
	void onLockSuccess(Object... args);

	/**
	 * 获取锁失败的时候执行
	 *
	 * @param args 入参
	 */
	void onLockFailed(Object... args);
}
