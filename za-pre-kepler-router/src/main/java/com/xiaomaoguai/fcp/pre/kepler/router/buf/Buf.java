package com.xiaomaoguai.fcp.pre.kepler.router.buf;

import com.xiaomaoguai.fcp.pre.kepler.router.handler.RouterInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.CallBack;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.Instruction;

import java.util.List;
import java.util.Map;

public interface Buf {

	String getId();

	void setId(String id);

	List<RouterInfo> getRemoteInfos();

	void setRemoteInfos(List<RouterInfo> remoteInfos);

	Buf put(String key, Object value);

	Map<String, Object> getContext();

	Buf setContext(Map<String, Object> context);

	Object get(String key);

	Buf remove(String key);

	void clear();

	int size();

	Buf object2Buf(Object o);

	String toJsonString();

	Buf jsonString2Buf(String s);

	Buf addRemoteInfo(RouterInfo routerInfo);

	Buf addRemoteInfo(List<RouterInfo> routerInfos);

	boolean getRpcBuf();

	void setRpcBuf(boolean rpcBuf);

	String getNextHandlerName();

	void setNextHandlerName(String nextHandlerName);

	CallBack getCallBack();

	void setCallBack(CallBack callBack);

	Throwable getException();

	String getExceptionHandlerName();

	void setException(Throwable e);

	void setExceptionHandlerName(String exceptionHandlerName);

	String getCurrentRouterName();

	String getCurrentHandlerName();

	void setCurrentRouterName(String currentRouterName);

	void setCurrentHandlerName(String currentHandlerName);

	Instruction getInstruction();

	void setInstruction(Instruction instruction);

}
