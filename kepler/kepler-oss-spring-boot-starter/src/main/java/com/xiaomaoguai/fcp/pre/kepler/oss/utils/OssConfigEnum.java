package com.xiaomaoguai.fcp.pre.kepler.oss.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @fileName: OssConfigEnum.java
 * @author: WeiHui
 * @date: 2018/7/9 16:20
 * @version: v1.0.0
 * @since JDK 1.7
 */
@Getter
@AllArgsConstructor
public enum OssConfigEnum implements IOssClient {

	/**
	 * 信贷核心
	 */
	CORE_SYSTEM_OSS("prd-core-system",
			"LTAIwd3zTsFamavb",
			"XsZPNbarPWf5qSwGs0Poaxw2yC3pJG",
			"http://oss-cn-hzjbp-b-internal.aliyuncs.com"),

	/**
	 * 投承保
	 */
	TCB_OSS("financial-tech",
			"LTAIZxyrQSO0PlJC",
			"3XIs2wzAcoAaltnv8WxxX4ZaGvJEFY",
			"http://oss-cn-hzjbp-b-internal.aliyuncs.com"),

	/**
	 * 投承保
	 */
	YND_OSS("za-pub-out",
			"LTAIxoFwBuQOVUiL",
			"EAPnRPQ5Eq5I0ht2ocdLkTiYZIMk18",
			"http://oss-cn-hzfinance-internal.aliyuncs.com"),

	/**
	 * 账务系统
	 */
	ID_IMG_OSS(
			"p-za-dynasty",
			"LTAIFi7WOx7VgF2S",
			"kJF3xetEe4brBUY2enPNTD52nlYZJk",
			"http://oss-cn-hzjbp-b-internal.aliyuncs.com");

	private final String bucketName;

	private final String accessKeyId;

	private final String accessKeySecret;

	private final String endpoint;

	@Override
	public OssClient getClient() {
		return getClient(this);
	}

}
