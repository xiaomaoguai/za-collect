package com.xiaomaoguai.fcp.pre.kepler.delay.task.zk.config;

import com.xiaomaoguai.fcp.pre.kepler.delay.task.DelayTaskMsg;
import org.apache.curator.framework.recipes.queue.DistributedDelayQueue;

import java.util.HashMap;
import java.util.Map;

/**
 * @fileName: DistributedDelayQueueFactory.java
 * @author: WeiHui
 * @date: 2019/2/14 14:16
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class DistributedDelayQueueFactory {

	/**
	 * 分布式队列， 队列对应的queue
	 */
	private final static Map<String, DistributedDelayQueue<DelayTaskMsg>> QUEUE_MAPPING = new HashMap<>();

	/**
	 * 添加映射
	 *
	 * @param queueName
	 * @param distributedDelayQueue
	 */
	public static void addQueueMapping(String queueName, DistributedDelayQueue<DelayTaskMsg> distributedDelayQueue) {
		QUEUE_MAPPING.put(queueName, distributedDelayQueue);
	}

	/**
	 * 获取相对应的队列
	 *
	 * @param queueName
	 * @return
	 */
	public static DistributedDelayQueue<DelayTaskMsg> getQueueMapping(String queueName) {
		return QUEUE_MAPPING.get(queueName);
	}

}
