package com.dh.convert.rule.validator;

import java.util.Map;

import com.google.common.collect.Maps;

public class ValidatorContext {
	private final static Map<String, AbstractValidator> validatorContext = Maps.newHashMap();

	public static void put(String validatorName, AbstractValidator validator) {
		validatorContext.put(validatorName, validator);
	}

	public static boolean contains(String validatorName) {
		return validatorContext.containsKey(validatorName);
	}

	public static AbstractValidator get(String validatorName) {
		return validatorContext.get(validatorName);
	}
	
}
