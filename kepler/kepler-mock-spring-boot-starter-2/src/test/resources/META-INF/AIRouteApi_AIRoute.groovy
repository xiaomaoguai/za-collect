package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock


import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.creditcore.snaproute.dto.RouteTrialDTO
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class AIRouteApi_AIRoute implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        BaseResp<RouteTrialDTO> baseResp = new BaseResp()

        RouteTrialDTO routeTrialDTO =new RouteTrialDTO()
        routeTrialDTO.setFundOrgCode("大辉郎银行")
        routeTrialDTO.setFundOrgName("大辉郎银行")
        routeTrialDTO.setInterestJsonInfo("{\"needOpenAccount\":false,\"fundRepayDateType\":\"WEEKDAYS\",\"fundRepayDateValue\":1,\"withdrawWayList\":[\"FUND_CHANNEL\"],\"equalInstallmentDto\":{\"fundRepayWay\":\"EQUAL_INSTALLMENT\",\"dailyRateList\":[{\"effectiveDate\":\"2000-10-01\",\"invalidDate\":\"9999-10-01\",\"rate\":0.00019444,\"rateType\":\"DAILY_RATE\"}],\"monthRateList\":[{\"effectiveDate\":\"2000-10-01\",\"invalidDate\":\"9999-10-01\",\"rate\":0.00583333,\"rateType\":\"MONTH_RATE\"}],\"yearRateList\":[{\"effectiveDate\":\"2000-10-01\",\"invalidDate\":\"9999-10-01\",\"rate\":0.07,\"rateType\":\"YEAR_RATE\"}]}}")

        baseResp.setResultCode(ResultCode.SUCCESS)
        baseResp.setResult(routeTrialDTO)
        return baseResp
    }

}
