package com.xiaomaoguai.fcp.kepeler.riskFiled;

import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.spider.HtmlBean;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 18:07
 * @since JDK 1.8
 */
@Gecco(matchUrl = "http://sztest.xiaomaoguai.com/books/kepler_zx_doc/base/fqlzxRiskInfo.html", pipelines = "consolePipeline")
public class RiskInfo implements HtmlBean {

	private static final long serialVersionUID = 8796968947209785819L;

}
