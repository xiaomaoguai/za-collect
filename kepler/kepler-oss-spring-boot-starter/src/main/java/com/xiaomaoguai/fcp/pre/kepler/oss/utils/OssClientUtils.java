package com.xiaomaoguai.fcp.pre.kepler.oss.utils;

import com.xiaomaoguai.fcp.pre.kepler.oss.configuration.MultiOssProperties;
import com.xiaomaoguai.fcp.pre.kepler.oss.configuration.OssProperties;
import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;

/**
 * @author WeiHui
 * @date 2019/3/20 19:36
 * @since JDK 1.8
 */
@UtilityClass
public final class OssClientUtils {

    /**
     * 构建OssClient
     *
     * @param ossProperties oss配置属性
     * @return OssClient
     */
    public static OssClient buildOssClient(OssProperties ossProperties) {
        String accessKeyId = ossProperties.getAccessKeyId();
        String accessKeySecret = ossProperties.getAccessKeySecret();
        String privateEndpoint = ossProperties.getPrivateEndpoint();
        String object = ossProperties.getObject();
        String arn = ossProperties.getArn();
        String bucketName = ossProperties.getBucketName();
        return getOssClient(accessKeyId, accessKeySecret, privateEndpoint, arn, bucketName, object);
    }

    /**
     * 构建OssClient
     *
     * @param ossProperties oss配置属性
     * @return OssClient
     */
    public static OssClient buildOssClient(MultiOssProperties.OssProperties ossProperties) {
        String accessKeyId = ossProperties.getAccessKeyId();
        String accessKeySecret = ossProperties.getAccessKeySecret();
        String privateEndpoint = ossProperties.getPrivateEndpoint();
        String object = ossProperties.getObject();
        String arn = ossProperties.getArn();
        String bucketName = ossProperties.getBucketName();
        return getOssClient(accessKeyId, accessKeySecret, privateEndpoint, arn, bucketName, object);
    }

    /**
     * 构建ossClient
     *
     * @param accessKeyId     accessKeyId
     * @param accessKeySecret accessKeySecret
     * @param privateEndpoint 内网配置
     * @param arn             sts需要的信息
     * @param bucketName      文件夹
     * @return ossClient
     */
    public static OssClient getOssClient(String accessKeyId, String accessKeySecret, String privateEndpoint, String arn, String bucketName) {
        return getOssClient(accessKeyId, accessKeySecret, privateEndpoint, arn, bucketName, null);
    }

    /**
     * 构建ossClient
     *
     * @param accessKeyId     accessKeyId
     * @param accessKeySecret accessKeySecret
     * @param privateEndpoint 内网配置
     * @param arn             sts需要的信息
     * @param bucketName      文件夹
     * @param object          根目录
     * @return
     */
    public static OssClient getOssClient(String accessKeyId, String accessKeySecret, String privateEndpoint, String arn, String bucketName, String object) {
        if (StringUtils.isNoneBlank(bucketName, accessKeyId, accessKeySecret, privateEndpoint, arn)) {
            OssClient ossClient = new OssClient();
            ossClient.setBucketName(bucketName);
            ossClient.setAccessKeyId(accessKeyId);
            ossClient.setSecretAccessKey(accessKeySecret);
            ossClient.setPrivateEndpoint(privateEndpoint);
            ossClient.setObject(object);
            ossClient.setArn(arn);
            return ossClient;
        } else {
            throw new IllegalArgumentException("accessKeyId,accessKeySecret,privateEndpoint,bucketName 等信息不能为空");
        }
    }
}
