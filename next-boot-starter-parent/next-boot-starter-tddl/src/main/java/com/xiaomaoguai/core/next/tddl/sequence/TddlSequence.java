/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.tddl.sequence;

import com.taobao.tddl.client.sequence.exception.SequenceException;
import com.taobao.tddl.client.sequence.impl.GroupSequence;

import app.myoss.cloud.core.exception.BizRuntimeException;
import app.myoss.cloud.mybatis.table.Sequence;
import app.myoss.cloud.mybatis.table.TableInfo;
import lombok.Data;

/**
 * TDDL 序列生成器
 *
 * @author chenyao
 * @since 2018年5月15日 下午6:52:35
 */
@Data
public class TddlSequence implements Sequence {
    private TableInfo     tableInfo;
    private GroupSequence sequence;

    @Override
    public Object nextValue(Object parameter) {
        try {
            return this.sequence.nextValue();
        } catch (SequenceException ex) {
            throw new BizRuntimeException("get TDDL sequence nextValue failed, sequenceName = "
                    + this.sequence.getName() + ", tableName = " + this.tableInfo.getTableName(), ex);
        }
    }
}
