import com.xiaomaoguai.fcp.pre.kepler.glue.handler.GlueHandler

class TestGlue implements GlueHandler {

    @Override
    Object handle(Map<String, Object> params) {
        return "hello world!"
    }
}
