package com.xiaomaoguai.fcp.pre.kepler.mock;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * 装夹方法
 *
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 14:29
 * @since JDK 1.8
 */
public class FeignMethodHolder {

	/**
	 * 方法设置
	 */
	private static Set<Method> METHOD_SET = new HashSet<>(64);

	/**
	 * get方法设置
	 *
	 * @return {@link Set<Method>}
	 */
	public static Set<Method> getMethodSet() {
		return Collections.unmodifiableSet(METHOD_SET);
	}

	/**
	 * 添加方法
	 *
	 * @param method 方法
	 */
	public static void addMethod(final Method method) {
		METHOD_SET.add(method);
	}

}
