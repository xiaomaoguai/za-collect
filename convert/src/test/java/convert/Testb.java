package convert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.util.TypeUtils;
import com.dh.convert.beancopy.Bean2BeanConvertor;
import com.dh.convert.deserializer.String2BeanConvertor;
import com.dh.convert.mapping.BeanMapping;
import com.dh.convert.mapping.BeanMappingObject;
import com.dh.convert.rule.RuleLoader;
import com.dh.convert.serializer.Bean2StringConvertor;
import com.dh.convert.utils.JsonNodeUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;

public class Testb extends ParentT {
	public String getTb() {
		return tb;
	}
	public TestEnum getTbe() {
		return tbe;
	}

	public void setTbe(TestEnum tbe) {
		this.tbe = tbe;
	}
	public void setTb(String tb) {
		this.tb = tb;
	}
	public String getTb1() {
		return tb1;
	}

	
	public void setTb1(String tb1) {
		this.tb1 = tb1;
	}
	
	public En getEn() {
		return en;
	}
	public void setEn(En en) {
		this.en = en;
	}

	private String tb;
	private String tb1;
	private List<TbEn> tbList;
	private En en;
	private TestEnum tbe;
	private BigDecimal tbig;
	public BigDecimal getTbig() {
		return tbig;
	}
	public void setTbig(BigDecimal tbig) {
		this.tbig = tbig;
	}
	public List<TbEn> getTbList() {
		return tbList;
	}

	public void setTbList(List<TbEn> tbList) {
		this.tbList = tbList;
	}

	public static void main(String[] args) throws Exception{
		
		RuleLoader.initRule();
		Test a = new Test();
		a.setDateString("2018-1-20");
		En e1 = new En();
		e1.setEn1("en1111");
		En e2 = new En();
		e2.setEn1("en2222");
		En e3 = new En();
		e3.setEn1("en3333");
		a.setA(1);
		a.setBig(new BigDecimal("510.000"));
		a.setEnList(Lists.newArrayList(e1,e2,e3));
		a.setTestEnum(TestEnum.NO);
//		BeanMapping cm = new BeanMapping();
//		BeanMappingObject bo20 = new BeanMappingObject();
//		bo20.setSrcField("en1");
//		bo20.setTargetField("*list.b");
//		bo20.setCollectionMapping("Y");
//		bo20.setType("String");
//		BeanMappingObject bo21 = new BeanMappingObject();
//		bo21.setSrcField("en1");
//		bo21.setType("String");
//		bo21.setCollectionMapping("Y");
//		bo21.setTargetField("*list.c");
//		
//		cm.init(Map.class, Lists.newArrayList(bo20,bo21));
		
		System.out.println(com.dh.convert.jackson.JSON.toJsonString(a));
		BeanMappingObject bo101 = new BeanMappingObject();
		bo101.setSrcField("dateString");
		bo101.setTargetField("dateTString");
		bo101.setType("String");
		bo101.setEl("getDate(cv,'yyyy-MM-dd','M')");
		BeanMappingObject bo100 = new BeanMappingObject();
		bo100.setSrcField("enList");
		bo100.setTargetField("context.*enTemplist");
		bo100.setType("List");

		BeanMappingObject bo1000 = new BeanMappingObject();
		bo1000.setSrcField("{*{enTemplist*}.en1}");
		bo1000.setTargetField("*enTemplist2<convert/En>.en1");
		bo1000.setCollectionMapping("Y");
		bo1000.setType("String");
		BeanMappingObject bo = new BeanMappingObject();
		bo.setSrcField("{#*{enTemplist}.[2].en1}");
		bo.setTargetField("tb");
		bo.setType("String");
		
		BeanMappingObject bo2 = new BeanMappingObject();
		bo2.setSrcField("{*{enTemplist*}.en1}");
		bo2.setTargetField("*tbList.tben1");
		bo2.setValidate("NotBlank");
		bo2.setCollectionMapping("Y");
		bo2.setType("String");
		
		BeanMappingObject bo001 = new BeanMappingObject();
		bo001.setSrcField("enList-0");
		bo001.setTargetField("en");
		//bo001.setType("convert.En");
		
		BeanMappingObject bo002 = new BeanMappingObject();
		bo002.setSrcField("enList-2.en1");
		bo002.setTargetField("en.en2");
		//bo002.setType("String");
		
		BeanMappingObject bo1 = new BeanMappingObject();
		bo1.setSrcField("a");
		bo1.setTargetField("tb1");
		bo1.setDefaultValue("aaaaaaaa");
		bo1.setEl("getNo(nil,nil)");
		bo1.setType("String");
		bo1.setValidate("NotBlank(\"tb1 不能为空\")");
		BeanMappingObject bo11 = new BeanMappingObject();
		bo11.setSrcField("enList-0.en1");
		bo11.setTargetField("context.$list.e");
		bo11.setValidate("NotBlank");
		bo11.setType("String");
		bo11.setCollectionMapping("N");
		BeanMappingObject bo12 = new BeanMappingObject();
//		bo12.setSrcField("testEnum");
		bo12.setTargetField("tbe");
//		bo12.setValidate("NotNull");
		bo12.setType("message");
//		bo12.setDefaultValue("YES");
		bo12.setEl("str('是')");
//		BeanMappingObject bo10 = new BeanMappingObject();
////		bo10.setSrcField("big");
//		bo10.setTargetField("tb");
//		bo10.setType("String");
//		bo10.setEl("addDate('2018-12-10',1,'yyyy-MM-dd','D')");
		BeanMappingObject bo20 = new BeanMappingObject();
//		bo10.setSrcField("big");
		bo20.setTargetField("contextTest");
		bo20.setType("String");
		bo20.setEl("JSONS(list)");

		BeanMappingObject bo201 = new BeanMappingObject();
//		bo10.setSrcField("big");
        bo201.setTargetField("@productInfo.supportPeriod");
        bo201.setType("String");
        bo201.setEl("co.tbe=='NO' ? '' : nil");
	
		BeanMappingObject bo202 = new BeanMappingObject();
		bo202.setTargetField("@productInfo");
		bo202.setType("Map");
		bo202.setEl("co.tbe=='NO' ? '' : nil");
		
		String s = JSON.toJSONString(a);
		System.out.println("原报文："+s);
		BeanMapping bm = new BeanMapping();
		bm.init( Testb.class,Lists.newArrayList(bo101,bo100,bo002,bo001,bo11,bo,bo2,bo1,bo12,bo20,bo201,bo202,bo1000));
		Object b = new Bean2BeanConvertor().convert(a , bm.getTargetNode());
		System.out.println(JSON.toJSONString( b));
//		System.out.println(TypeUtils.cast(TestEnum.YES, TestEnum.class, null));
		
//		BeanMappingObject bo3 = new BeanMappingObject();
//		bo3.setSrcField("enList-1.en1");
//		bo3.setTargetField("tb");
//		bo3.setType("String");
//		bo3.setDefaultValue("haha");
//		BeanMappingObject bo5 = new BeanMappingObject();
//		bo5.setSrcField("enList-1.en1");
//		bo5.setTargetField("*tbList.e1");
//		bo5.setType("String");
//		
//		BeanMappingObject bo6 = new BeanMappingObject();
//		bo6.setSrcField("enList.en1");
//		bo6.setTargetField("*tbList1.tb1");
//		bo6.setType("String");
//		
//		BeanMappingObject bo7 = new BeanMappingObject();
//		bo7.setSrcField("enList.en");
//		bo7.setTargetField("*tbList1.tb2");
//		bo7.setType("String");
//		
//		BeanMappingObject bo4 = new BeanMappingObject();
////		bo4.setSrcField("a");
//		bo4.setTargetField("tb4");
//		bo4.setType("String");
////		bo1.setDefaultValue("aaaaaaaa");
//		bo4.setEl(" 'hello ' + #co.tbList1.[0].tb1");
//		BeanMappingObject bo8 = new BeanMappingObject();
//		bo8.setSrcField("enList.en1");
//		bo8.setTargetField("context.*testList.en");
//		bo8.setType("String");
//		bo8.setColltionMapping("Y");
//		bo8.setEl("#cle.en1");
//		//bo8.setDefaultValue("[1,2,3]");
////		bo1.setValidate("NotBlank(\"tb1 不能为空\")");
//		BeanMappingObject bo9 = new BeanMappingObject();
////		bo4.setSrcField("a");
//		bo9.setTargetField("tbList3");
//		bo9.setType("String");
//		bo9.setEl("JSOND('{\"ab\":\"hahaab\"}')");
////		bo1.setValidate("NotBlank(\"tb1 不能为空\")");
//		BeanMappingObject bo10 = new BeanMappingObject();
//		bo10.setSrcField("big");
//		bo10.setTargetField("big");
//		bo10.setType("String");
//		BeanMapping cm = new BeanMapping();
//		cm.init(Map.class, Lists.newArrayList(bo3,bo5,bo6,bo7,bo4,bo8,bo9,bo10));
//		String [] s ={"1","2","3"};
//		String c = new Bean2StringConvertor().convert(s, cm.getTargetNode());
		System.out.println(b);
//		Expression el =AviatorEvaluator.compile("#getList(temlist,n).big");
//		Map<String,Object> map = Maps.newHashMap();
//		map.put("temlist",Lists.newArrayList(a));
//		map.put("n",0);
//		System.out.println(el.execute(map));
		
		
	}
}
