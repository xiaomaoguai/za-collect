/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.core.constants;

/**
 * 核心常量
 *
 * @author chenyao
 * @since 2019年6月12日 下午6:00:20
 */
public class CoreConstants {
    /**
     * Next Boot 配置前缀
     */
    public static final String BOOT_CONFIG_PREFIX         = "xiaomaoguai.next-boot";
    /**
     * 核心配置前缀
     */
    public static final String CONFIG_PREFIX              = BOOT_CONFIG_PREFIX + ".core";

    /**
     * 网关平台配置前缀
     *
     * @see com.xiaomaoguai.core.next.core.config.CoreCommonProperties#nextGateway
     */
    public static final String NEXT_GATEWAY_CONFIG_PREFIX = CONFIG_PREFIX + ".next-gateway";
}
