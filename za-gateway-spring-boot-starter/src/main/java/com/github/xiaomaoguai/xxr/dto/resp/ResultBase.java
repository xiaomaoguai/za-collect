package com.github.xiaomaoguai.xxr.dto.resp;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ResultBase<T> implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = -3849832973309584090L;

	/**
	 * 正常返回码
	 */
	private static final String OK_CODE = "0000";

	private String reqNo;

	private String respCode;

	private String respMessage;

	private T value;

	public boolean isSuccess() {
		return OK_CODE.equals(this.respCode);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
