package com.xiaomaoguai.fcp;

import com.xiaomaoguai.fcp.utils.JsonUtils;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @fileName: RequestMatrixTest.java
 * @author: WeiHui
 * @date: 2018/10/29 10:11
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class RequestMatrixTest {

	public static final String param = "appId=1000006&compressed=false&key=RZyRrZpBOaJen%2BxqCZ6g%2FNI8KFmQ%2BgMd4QEvqyZA3Kx7M%2BSmKm6HCC%2BQiA2WZ0Ys4fV1hSxCh6TLa7t%2FyiqZoMqRfWkFAULpHX0VrhF%2FfZRf%2FcACFaRKqdns0g4pIYOeVcDVs%2FI7m6an%2FqjBwoHYWPOxBRsPtXMgC6pLHYE%2FlnU%3D&method=mi.user.credit.apply&params=TlSq92DlYk51WSxdD7H7QGDyjm097UhIktTQQCuFAO6yuyuN%2BAezeY7BZpWDZFo3vD2tWLtQS35pXv6kUJNGeghvOElTifLfY8b%2BrMQwPftmi68uHri0A6qkJC%2B17%2Bzt9y2HeDU4du%2FGOy8QZTesS3PnOErwcwBc60EPiBkpU3J6xAO3vAlZBXqfg7sf7oTBtzth5enkLxvyfEgQg3VYU6MM0klZ%2B%2F5EtVYyCsnCOiEhLlD6m8rS1cl1%2B50pPq6bAX041ic0fEuqj8COWdK2VxDS454I5GU6uQpd9xT25AOJz4NXJTcc0DhjdGUXTQKAOoeB6R6B41I0kQHDVfFyDFvW7mGrPLLzpo4UT%2BZ8dk%2FtpQqmPTi1%2Bc1wrjWPLz1BCWzq4lQxW3AVmu6qO5tKWJCOc4ZTfhYKvYNBxoefNofl9PCmAMeQB7Ka6EKVxzP5F5Mxsgbs6zUVC1phgi6IvSP6qdjafgvN3oGgASlbjrIeptmCv%2FY9OC1CgC2pIJdyVn5PynBLnxNsD%2BSadX84DCp8XOLGy4BuOgDmiFGuhFmxcj%2FCP%2FdvzXbP4lLcOFA%2BM6nWqvrAOtVMVuKdv0QqlfF%2FbDdB1YCCXbZpPlXPoFwOsPcG7thKTAOX195dBkavCm5WiwFUfGnMOBAQh73R570biMNWq527qy1J8th5f5IMkEP%2BR%2BlYWt4uDfS%2FBtUGaEw1vJ5sErwTMze2RPFo98qzWHULUjryc1aT8OYUZ4zN0cJuskKoXiBaXll7SkHrWRI3ziEpay0A6zOxuMOcopEKTZ7qgA%2FPD%2BOKoWjjwM08mP%2FkRH7cosMSQXNevyu%2Fc7IAfK743o3zmTqxtFrJOBOZ1vB45YSXmdkNnOvPcMG4oSCI405JO%2BnjEjdJ3FVaUwpePIVr66Fo%2FTPoQij38QzV6WI05yIZTNy2Eeu02I0lGo%2F013MOjIUGtHETlxRswU7g%2BkgIuh%2FPMCVi0Np4%2Fs7uDPsxcfCl7bH2X%2BQ7KJM%2Fh6zOmQyMX9MyyGVKcV8GzoChfZLaANHuSuQ3z2dP0E8yc4KI6pDLMcndPJxK65nxddwwQ34k6R2aVsKQ30qVAhdEAvIffQVbukVAYXSGMdgW6A2hqgEsmz9gxBvvOXLLYuoTs69HlQ1UWfHIopEzc%2FfnMjAAw8AHP1gLbnncLIFjUjZ%2F6qqc6jTDcOs%2BH6n%2FgbSB9gfQfuFB9KhNWE9FldZv%2FstVNWWkx5uWILENq7jQ9lybv%2BfxswL6sqhoMvJsl0d9lvLlmnn46bfA7c4ko5vrh4j%2FzIl3iGOKeeXWndPZm9B8I3Q2Wlq%2FT%2BqM2JA%3D&requestId=3308792298237591552&sign=hi8Y-KeVzhWOjUwCAibHwM6jAxoLAU3rSL-soBXZbeb-2WW0aVi6b2cIb44wooWTJGILFGpBbGtxrA6DNoQ5eYFD9RcILJHXN2VMWsIg2EO9tHemKrB_7CznnNuJjSUE4rj6k4Wtov6NdHbVG3MBfEL4Je1lUrwS4B-lvzobIUQ&timestamp=1540778381033&version=1.0";

	public static void main(String[] args) {
		String[] split = param.split("&");
		Map<String, String> paramMap = new HashMap<>();
		for (String s : split) {
			String[] split1 = s.split("=");
			paramMap.put(split1[0], split1[1]);
		}
		System.out.println(JsonUtils.toJsonString(paramMap));
	}

	@Test
	public void test() throws Exception {

	    try {
			Date date = new Date(1540643108000L);
			System.out.println("date = " + date);
		} catch (Exception e) {
	    	e.printStackTrace();
		}
	}

}
