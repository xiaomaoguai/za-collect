package com.xiaomaoguai.fcp.fcp.dto.req;

import com.github.xiaomaoguai.xxr.dto.req.BizReq;
import org.hibernate.validator.constraints.Range;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @fileName: BindCardReq.java
 * @author: WeiHui
 * @date: 2018/9/13 13:51
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class BindCardReq extends BizReq {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 1783135635700456942L;


	@NotNull(message="卡信息不能为空")
	@Valid
	private CardInfo cardInfo;

	/**
	 * 交易类型 1-发送验证码
	 * 2-绑卡
	 */
	@NotNull(message="交易类型不能为空")
	@Range(min=1,max=2,message="交易类型必须1-2")
	private Integer transType;
	/**
	 * 验证码
	 * 账户系统未绑卡必传
	 */
	private String mobileMessage;

	/**
	 * 资金方编码
	 */
	private String fundCode;

	/**
	 * 是否强制入库
	 * 0-否 1-是
	 */
	private Integer logUserCardInfo = 0;
}
