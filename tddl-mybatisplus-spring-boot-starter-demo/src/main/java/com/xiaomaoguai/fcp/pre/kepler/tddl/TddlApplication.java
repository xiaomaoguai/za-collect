package com.xiaomaoguai.fcp.pre.kepler.tddl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @fileName: TddlApplication.java
 * @author: WeiHui
 * @date: 2019/2/11 17:35
 * @version: v1.0.0
 * @since JDK 1.8
 */
@SpringBootApplication
@MapperScan("com.xiaomaoguai.fcp.pre.kepler.tddl.mapper")
public class TddlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TddlApplication.class, args);
	}

}
