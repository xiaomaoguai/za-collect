/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.nacos.ribbon;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.cloud.nacos.ribbon.ConditionalOnRibbonNacos;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.ServerList;

/**
 * integrated Ribbon by default
 *
 * @author chenyao
 * @since 2019年6月19日 下午6:18:05
 * @see com.alibaba.cloud.nacos.ribbon.NacosRibbonClientConfiguration
 */
@Configuration
@ConditionalOnRibbonNacos
public class ZaNacosRibbonClientConfiguration {
    /**
     * 创建查询服务实例的对象
     *
     * @param config
     *            org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration#ribbonClientConfig()
     * @param nacosDiscoveryProperties Nacos服务发现配置属性
     * @return 查询服务实例的对象
     */
    @Bean
    public ServerList<?> ribbonServerList(IClientConfig config, NacosDiscoveryProperties nacosDiscoveryProperties) {
        ZaNacosServerList serverList = new ZaNacosServerList(nacosDiscoveryProperties);
        serverList.initWithNiwsConfig(config);
        return serverList;
    }
}
