package com.xiaomaoguai.fcp.aviator;

import com.googlecode.aviator.AviatorEvaluator;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @fileName: Test.java
 * @author: WeiHui
 * @date: 2018/11/23 10:23
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class DDD {

	@Test
	public void testSeqFilterMapWithProperty() {
		Map<Long, User> idUserMap = new HashMap<>(3);
		idUserMap.put(1L, new User(1L, 25, "张三"));
		idUserMap.put(2L, new User(2L, 25, "李四"));
		idUserMap.put(3L, new User(3L, 27, "王五"));
		Map<String, Object> env = new HashMap<>();
		env.put("data", idUserMap);
		Object result = AviatorEvaluator
				.execute("filter(data,seq.and(seq.eq(25,'value.age'),seq.eq('李四','value.name')))", env);
		Map map = (Map) result;
		assertEquals(1, map.size());
		for (Object o : map.values()) {
			User user = (User) o;
			assertEquals("李四", user.getName());
			assertTrue(user.getAge() == 25);
		}

		Object result2 = AviatorEvaluator
				.execute("filter(data,seq.and(seq.eq(25,'value.age'),seq.eq('李四','value.name')))", env);

		System.out.println(result2);
	}


}
