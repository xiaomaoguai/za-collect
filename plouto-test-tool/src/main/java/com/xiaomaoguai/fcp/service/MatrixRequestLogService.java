package com.xiaomaoguai.fcp.service;

import com.xiaomaoguai.fcp.dto.MatrixRequestLog;

import java.util.List;

/**
 * @fileName: MatrixRequestLogService.java
 * @author: WeiHui
 * @date: 2018/10/22 20:21
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface MatrixRequestLogService {

	List<MatrixRequestLog> findByThirdUserNo(String thirdUserNo);

	List<MatrixRequestLog> findByString(String query);

}
