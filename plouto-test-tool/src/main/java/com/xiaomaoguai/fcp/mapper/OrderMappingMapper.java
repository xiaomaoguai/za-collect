/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper;

import com.xiaomaoguai.fcp.domain.UserAccount;
import org.apache.ibatis.annotations.Param;

/**
 * The Interface OrderMappingMapper.
 */
public interface OrderMappingMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(UserAccount record);

	int updateByUnique(UserAccount record);

	UserAccount selectByOrderNo(@Param("productCode") String productCode, @Param("orderNo") String orderNo, @Param("channelId") String channelId);

	UserAccount selectOrderNoByUserId(@Param("orderNo") String orderNo, @Param("userId") Long userId);
}