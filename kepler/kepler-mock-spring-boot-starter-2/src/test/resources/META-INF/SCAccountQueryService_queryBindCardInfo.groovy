package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.account.api.response.BindCardAuthorizedResponse
import com.xiaomaoguai.account.common.beans.ResultBase
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class SCAccountQueryService_queryBindCardInfo implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        def bindCardAuthorizedResponse = new BindCardAuthorizedResponse();
        bindCardAuthorizedResponse.setBindStatus(1.byteValue())

        return ResultBase.success(bindCardAuthorizedResponse)
    }

}
