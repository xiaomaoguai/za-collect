/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.tddl.config;

import lombok.Data;

/**
 * TDDL Sequence 属性配置
 *
 * @author chenyao
 * @since 2018年7月15日 下午4:50:33
 */
@Data
public class TddlSequenceProperties {
    /**
     * 自适应调整
     */
    private boolean adjust                = true;
    /**
     * 重试次数，在多个groupDataSource的场景下，建议设置成1-2次。默认为2次
     */
    private int     retryTimes            = 2;
    /**
     * 数据源的个数
     */
    private int     dsCount               = 2;
    /**
     * 内步长 ,默认为1000，取值在1-100000之间
     */
    private int     innerStep             = 1000;
    /**
     * 使用的表的表名 ，默认为sequence
     */
    private String  tableName             = "sequence";
    /**
     * id生成器的字段名,默认为name
     */
    private String  nameColumnName        = "name";
    /**
     * 存值的列的字段名,默认为value
     */
    private String  valueColumnName       = "value";
    /**
     * 存修改时间的字段名 ,默认为gmt_modified
     */
    private String  gmtModifiedColumnName = "gmt_modified";
}
