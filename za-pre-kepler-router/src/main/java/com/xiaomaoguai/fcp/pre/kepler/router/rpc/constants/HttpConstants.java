package com.xiaomaoguai.fcp.pre.kepler.router.rpc.constants;

/**
 * http协议头信息
 *
 * @author DH
 */
public class HttpConstants {

	public static final String HEADER_X_FORWARDED_FOR = "X-Forwarded-For";
	public static final String HEADER_CONTENT_LENGTH = "Content-Length";
	public static final String HEADER_CONTENT_TYPE = "Content-Type";
	public static final String HEADER_CONNECTION = "Connection";
	public static final String HEADER_SERVER = "Ultron-Server";
	public static final String HEADER_REQUEST_ID = "Request-Id";
	public static final String HEADER_CONNECTION_KEEPALIVE = "keep-alive";
	public static final String HEADER_CONNECTION_CLOSE = "close";
	public static final String HEADER_CONTENT_TYPE_TEXT = "application/text";
	public static final String HEADER_CONTENT_TYPE_JSON = "application/json";
	public static final String HEADER_CONTENT_TYPE_FORM="application/x-www-form-urlencoded";
	public static final String JSON_SUBTYPE = "json";
	public static final String FORM_SUBTYPE="x-www-form-urlencoded";
	/**
	 * 协程Attribute
	 */
	public static final String CONTINUATION = "CONTINUATION";

	public static final char VARIABLE = '{';
}
