package com.xiaomaoguai.fcp;

import com.alibaba.fastjson.JSONObject;
import com.xiaomaoguai.scorpoin.biz.common.CommonRequest;
import com.xiaomaoguai.scorpoin.biz.common.CommonResponse;
import com.xiaomaoguai.scorpoin.common.xiaomaoguaiApiClient;
import com.xiaomaoguai.scorpoin.common.xiaomaoguaiOpenException;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/2 16:47
 * @since JDK 1.8
 */
public class RequestZA {

	public static void main(String[] args) throws xiaomaoguaiOpenException {
		//以下参数为：环境参数、接口版本号、appKey、开发者rsa私钥、接口名
		String env = "dev";
		String version = "1.0.0";
		String appkey = "2d0ea1d7febb99d066a794e7cc923564";
		String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAONBVq2U/oAWQd0u7nFuZDjVsQuE4WgqGoKsY9SNiSOH4NmjVP3UBAzJxISQQ1islRFaHVm2lFvwDNidiwE62A5K/vxJNFNIMAkLcyPRhIMUxxBOBppltS7H/+/M5Ei3xgAbRuUkOU+agjt2j8800IJ7/pVxh2RNxrNIEQSyVcMvAgMBAAECgYBXPpZQPYsXEXRnvcS8t2yyhRdbHMCMhN14nUYWK+AiS9/+rb3LVMHZRvyzB89TE66G4tmxv25lfVrxLkpXRof6+1yL17dbmRwiKeXUXQxhMHzYgl1NOfUoZV8Ui57N4On9748zh1Bamhvsj9ZmSsJ6ZPxv7G9iU6mPtOzme+ODQQJBAPxXO4Hb0qSa2G20qzz4R2hiGzWPAvLMduL3mkvxWBc8gq7egw7R0fr40GmeswlG1TtAuz2o9pGZv5PgzsScWRMCQQDmjPsZM1nOkG957bM9XKHxRye723juh6quqc9peNN9NhFPS8fY93dUlVDiGIa1qgVp3CoL09v/8QILpTpQ7ez1AkBQiLahNzr+9bxlJugPyV1g3w64BTB3tPGsdkF0Q05N/C3pCXLiY+yUIJzDWLbjGwwqoPohL6+hwGP4GiNjdFKpAkBQMmhCGt+5f/qXEj1QMgHPGS5UJYMKjjysJzuT98ixfHZG/BhXe7WRwaLyExAA71SUv1YM6TRU8nQAswf3ENxlAkEAvVLQ4FSLwdMLwmvQSGJjLfZdxZN6uu/v2G7xx4MqL1lsumGTt0xoeV2Hg7uKV3211XA9k0ZnKxardrN8EvPDfw==";
		String serviceName = "xiaomaoguai.cfp.api.beforeLoan";

		xiaomaoguaiApiClient client = new xiaomaoguaiApiClient(env, appkey, privateKey, version);
		CommonRequest request = new CommonRequest(serviceName);

		JSONObject params = new JSONObject();
		params.put("param","{\n" +
				"  \"routerName\": \"insuranceBindCardRouter\",\n" +
				"  \"apiVersion\": \"1.0.0\",\n" +
				"  \"thirdUserNo\": \"fqlzx42232619920805551920180620111002\",\n" +
				"  \"reqDate\": \"2019-09-10 16:00:00\",\n" +
				"  \"reqNo\": \"zabx20190509160000982438\",\n" +
				"  \"productCode\": \"FQLZX\",\n" +
				"  \"apiName\": \"bindCard\",\n" +
				"  \"bankName\": \"招商银行\",\n" +
				"  \"bankCode\": \"CMB \",\n" +
				"  \"cardName\": \"王红梅\",\n" +
				"  \"bankCardNo\": \"6212262008023561377\",\n" +
				"  \"cardPhone\": \"13553557985\",\n" +
				"  \"certType\": 1,\n" +
				"  \"certNo\": \"530113197609187489\",\n" +
				"  \"cardUse\": \"WITHDRAW_AND_REPAY\",\n" +
				"  \"transType\": 3,\n" +
				"  \"smsCode\": \"666666\",\n" +
				"  \"cardType\": \"DEBIT_CARD\",\n" +
				"  \"identityValiStartDate\": \"20180818\",\n" +
				"  \"identityValiEndDate\": \"20380818\"\n" +
				"}");

		request.setParams(params);

		CommonResponse response = (CommonResponse) client.call(request);
		System.out.println(response);
	}

}
