/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.cache.constants;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;

/**
 * 缓存常量
 *
 * @author chenyao
 * @since 2018年5月22日 下午5:35:39
 */
public class CacheConstants {
    /**
     * 缓存配置前缀
     */
    public static final String CACHE_PREFIX = CoreConstants.BOOT_CONFIG_PREFIX + ".cache";
    /**
     * Redis 配置前缀
     */
    public static final String REDIS_PREFIX = CACHE_PREFIX + ".redis";

    /**
     * 内存缓存的cacheName
     */
    public static final String MEMORY_CACHE = "memoryCache";
}
