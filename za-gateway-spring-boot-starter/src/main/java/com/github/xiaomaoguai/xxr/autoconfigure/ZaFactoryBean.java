package com.github.xiaomaoguai.xxr.autoconfigure;

import com.github.xiaomaoguai.xxr.properties.ZaProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ZaFactoryBean<T> implements FactoryBean<T> {

	@Setter
	@Getter
	private Class<T> interfaceClz;

	@Setter
	@Getter
	private ZaProperties zaProperties;

	@Override
	@SuppressWarnings("unchecked")
	public T getObject() throws Exception {
		ZaRemoteInvocationHandler invocationHandler = new ZaRemoteInvocationHandler(zaProperties);
		return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class<?>[]{interfaceClz}, invocationHandler);
	}

	@Override
	public Class<?> getObjectType() {
		return interfaceClz;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}
}
