package com.dh.convert.rule.function;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorObject;

public class DateCheckFunction extends AbstractFunction{

	 @Override
     public AviatorBoolean call(Map<String, Object> env, AviatorObject arg ,AviatorObject arg1) {
		 String regexp =  FunctionUtils.getStringValue(arg1, env);
		 String date =  FunctionUtils.getStringValue(arg, env);
		 if(StringUtils.isBlank(regexp) || StringUtils.isBlank(date)){
			 return AviatorBoolean.TRUE;
		 }
		 SimpleDateFormat sdf = new SimpleDateFormat(regexp);
	        try {
	            return sdf.format(sdf.parse(date)).equals(date)?AviatorBoolean.TRUE:AviatorBoolean.FALSE;
	        } catch (ParseException e) {
	            return AviatorBoolean.FALSE;
	        }
         
     }
	@Override
	public String getName() {
		return "dateCheck";
	}

}
