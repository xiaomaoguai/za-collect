package com.xiaomaoguai.fcp.pre.kepler.router.rpc.options;


public class ServerOptions {

	/**
	 * max tcp accept compulish queue.
	 *
	 * NOT effect on max connections restriction. just a hint value for linux
	 * listen(fd, backlog) increase the number if you have lots of short
	 * connection which sockets will be filled in tcp accept compulish queue too
	 * much.
	 */
	public static final int CONNECTIONS = 1024;

	/**
	 * max connections in this httpserver instance. default is 8192
	 *
	 * incoming http connection over the number of maxConnections will be reject
	 * and return http code 503
	 */
	public static final int MAX_CONNETIONS = 8192;

	/**
	 * max received packet size. default is 16MB
	 */
	public static final int MAX_PACKET_SIZE = 16 * 1024 * 1024;

	/**
	 * network socket io threads number. default is cpu core - 1
	 *
	 * NOT set the number more than cpu core number
	 */
	public static final int IO_THREADS = Runtime.getRuntime().availableProcessors() - 1;

	public static final int ACCPET_THREADS = (int) (IO_THREADS * 0.3) & 0xFFFFFFFE;

	/**
	 * logic handler threads number. default is 128
	 *
	 * we suggest adjust the ioThreads number bigger if there are more. critical
	 * region code or block code in your handler logic (io intensive). and
	 * smaller if your code has no block almost (cpu intensive)
	 */
	public static final int WORKER_THREADS = 128;

	/**
	 * logic handler timeout. default is 30s
	 */
	public static final int TIMEOUT = 30 * 1000;

	/**
	 * TCP禁用Nagle算法
	 */
	public static final boolean TCP_NODELAY = Boolean.TRUE;


}
