/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import com.xiaomaoguai.fcp.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class User extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 7048772692729864077L;

	private Long id;

	private Long esId;

	private String channelId;

	private String productCode;

	private String thirdUserNo;

	private Long userId;

	private Long acctId;

	private Integer certType;

	private String certNo;

	private String userName;

	private String userMobile;

	private Integer accessStep;
	
	private BigDecimal policyRate;

}