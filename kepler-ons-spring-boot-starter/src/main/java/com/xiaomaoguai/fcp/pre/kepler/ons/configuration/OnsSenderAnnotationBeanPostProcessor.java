package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import org.springframework.aop.framework.autoproxy.AbstractBeanFactoryAwareAdvisingPostProcessor;
import org.springframework.beans.factory.BeanFactory;

/**
 * @fileName: OnsSenderAnnotationBeanPostProcessor.java
 * @author: WeiHui
 * @date: 2019/3/1 19:59
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class OnsSenderAnnotationBeanPostProcessor  extends AbstractBeanFactoryAwareAdvisingPostProcessor {

	private static final long serialVersionUID = 1489640255652203913L;

	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		super.setBeanFactory(beanFactory);
	}

}
