package com.xiaomaoguai.fcp.pre.kepler.convert.core.support;

import com.dh.convert.BeanConvertor;
import com.dh.convert.beancopy.Bean2BeanConvertor;
import com.dh.convert.deserializer.String2BeanConvertor;
import com.dh.convert.serializer.Bean2StringConvertor;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 * 转换器单例
 *
 * @author huwenxuan
 * @date 2018/12/26
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum ConvertSupport {

	/**
	 * String2Bean
	 */
	S2B,

	/**
	 * Bean2Bean
	 */
	B2B,

	/**
	 * Bean2String
	 */
	B2S;

	/**
	 * 根据枚举获取对应的转换器单例
	 *
	 * @param convertSupport
	 * @return
	 */
	public static BeanConvertor getConvert(ConvertSupport convertSupport) {
		switch (convertSupport) {
			case B2B:
				return getBeanToBeanConvert();
			case S2B:
				return getStringToBeanConvert();
			case B2S:
				return getBeanToStringConvert();
			default:
				return getBeanToBeanConvert();
		}
	}

	/**
	 * 明确获取Bean2Bean转换器单例
	 *
	 * @return
	 */
	public static BeanConvertor getBeanToBeanConvert() {
		return ConverterInstanceHolder.BEAN_2_BEAN_CONVERT;
	}

	/**
	 * 明确获取Bean2String转换器单例
	 *
	 * @return
	 */
	public static BeanConvertor getBeanToStringConvert() {
		return ConverterInstanceHolder.BEAN_2_STRING_CONVERT;
	}

	/**
	 * 明确获取Bean2String转换器单例
	 *
	 * @return
	 */
	public static BeanConvertor getBeanConvertByClass(Class<?> clazz) {
		return String.class.isAssignableFrom(clazz) ? getBeanToStringConvert() : getBeanToBeanConvert();
	}

	/**
	 * 明确获取String2Bean转换器单例
	 *
	 * @return
	 */
	public static BeanConvertor getStringToBeanConvert() {
		return ConverterInstanceHolder.STRING_2_BEAN_CONVERT;
	}

	/**
	 * 静态内部类实现单例
	 */
	private static class ConverterInstanceHolder {

		/**
		 * BEAN_2_BEAN_CONVERT单例
		 */
		private static final Bean2BeanConvertor BEAN_2_BEAN_CONVERT = new Bean2BeanConvertor();

		/**
		 * STRING_2_BEAN_CONVERT单例
		 */
		private static final String2BeanConvertor STRING_2_BEAN_CONVERT = new String2BeanConvertor();

		/**
		 * BEAN_2_STRING_CONVERT单例
		 */
		private static final Bean2StringConvertor BEAN_2_STRING_CONVERT = new Bean2StringConvertor();

	}

}
