package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import com.xiaomaoguai.fcp.pre.kepler.ons.anno.OnsSender;
import org.aopalliance.aop.Advice;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.lang.NonNull;

/**
 * @fileName: OnsSenderAnnotationAdvisor.java
 * @author: WeiHui
 * @date: 2019/3/1 20:01
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class OnsSenderAnnotationAdvisor extends AbstractPointcutAdvisor implements BeanFactoryAware {

	private static final long serialVersionUID = -5361503223138353268L;

	private Advice advice;

	private Pointcut pointcut;

	@Override
	@NonNull
	public Pointcut getPointcut() {
		return  new AnnotationMatchingPointcut(OnsSender.class, true);
	}

	@Override
	@NonNull
	public Advice getAdvice() {
		return null;
	}

	/**
	 * Set the {@code BeanFactory} to be used when looking up executors by qualifier.
	 */
	@Override
	public void setBeanFactory(BeanFactory beanFactory) {
		if (this.advice instanceof BeanFactoryAware) {
			((BeanFactoryAware) this.advice).setBeanFactory(beanFactory);
		}
	}

}
