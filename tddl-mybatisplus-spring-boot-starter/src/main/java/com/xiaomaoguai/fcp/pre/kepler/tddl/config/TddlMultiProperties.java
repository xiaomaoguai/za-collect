/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.fcp.pre.kepler.tddl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Primary;

import java.util.List;

/**
 * TDDL 属性配置，支持多个数据库
 *
 * @author chenyao
 * @since 2018年7月15日 下午4:37:48
 */
@Data
@ConfigurationProperties(prefix = TddlConstants.MULTI_CONFIG_PREFIX)
public class TddlMultiProperties {
	/**
	 * 是否启用 TDDL 自动配置
	 */
	private Boolean            enabled;
	/**
	 * TDDL 属性配置
	 */
	private List<TddlProperty> databases;

	/**
	 * TDDL 属性配置
	 */
	@Data
	public static class TddlProperty {
		/**
		 * 生成的 Bean Name 前缀，如果未设置则使用 {@link #appName} 作为前缀
		 */
		private String                 beanNamePrefix;
		/**
		 * {@link Primary}
		 */
		private boolean                primary;
		/**
		 * TDDL appName
		 */
		private String                 appName;
		/**
		 * TDDL groupName
		 */
		private String                 groupName;

		/**
		 * TDDL Sequence 属性配置
		 */
		private TddlSequenceProperties sequenceConfig = new TddlSequenceProperties();

		//private MybatisProperties      mybatis;
	}
}
