package com.xiaomaoguai.fcp.kepeler.nepturn;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 21:04
 * @since JDK 1.8
 */
public class MinorGC {

	private final static int _1MB = 1024 * 1024;

	/**
	 * -Xms20M -Xmx20M -Xmn10M -XX:+PrintGCDetails
	 * @param args
	 */
	public static void main(String[] args) {
		byte[] allocation1, allocation2, allocation3, allocation4;
		allocation1 = new byte[2 * _1MB];
		allocation2 = new byte[2 * _1MB];
		allocation3 = new byte[2 * _1MB];
		allocation4 = new byte[4 * _1MB];
	}

}
