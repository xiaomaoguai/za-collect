/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.fcp.pre.kepler.redis.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.xiaomaoguai.fcp.pre.kepler.redis.constants.CacheConstants;

import lombok.Data;

/**
 * 类AliYunRedisProperties的实现描述：阿里云 Redis 自动配置属性
 *
 * @author chenyao
 * @since 2018年5月22日 下午5:34:07
 */
@Data
@ConfigurationProperties(prefix = CacheConstants.REDIS_PREFIX)
public class RedisProperties {
	/**
	 * 阿里云Redis: zookeeper 中的 path 路径，即 WestWorld 中申请的 redisConfigKey
	 */
	private String redisConfigKey;
	/**
	 * 阿里云Redis: zookeeper 的 server 地址，默认不需要设置
	 */
	private String redisZookeeperServer;
}
