package com.dh.convert;

import com.dh.convert.node.TreeNode;

public interface BeanConvertor<S> {
	
	public  Object convert(S srcContent,TreeNode targetNode) throws Exception;

}
