package com.xiaomaoguai.fcp.pre.plouto.mailbox.autoconfigure;

import com.xiaomaoguai.fcp.pre.plouto.mailbox.IMailBox;

/**
 * @fileName: MailBoxRegister.java
 * @author: WeiHui
 * @date: 2018/11/19 21:47
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface MailBoxRegister {

	/**
	 * 注册mailBox
	 *
	 * @return mailBox array
	 */
	IMailBox[] register();

}
