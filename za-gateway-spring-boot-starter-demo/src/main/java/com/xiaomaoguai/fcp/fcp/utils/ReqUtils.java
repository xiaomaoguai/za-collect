package com.xiaomaoguai.fcp.fcp.utils;

import com.github.xiaomaoguai.xxr.utils.security.AES;
import com.google.common.base.Joiner;

/**
 * @fileName: ReqUtils.java
 * @author: WeiHui
 * @date: 2018/9/13 16:05
 * @version: v1.0.0
 * @since JDK 1.8
 */
public final class ReqUtils {

	private ReqUtils() {
	}

	public static String buildToken(String thirdUserNo, String channelId, String aesKey) {
		long currentTimeMillis = System.currentTimeMillis() / 1000;
		String token = thirdUserNo + "|" + currentTimeMillis + "|" + channelId;
		return AES.encryptToBase64(token, aesKey);
	}

	public static String buildToken(String thirdUserNo, String aesKey) {
		long currentTimeMillis = System.currentTimeMillis() / 1000;
		String token = thirdUserNo + "|" + currentTimeMillis + "|" + "FSL";
		return AES.encryptToBase64(token, aesKey);
	}

	public static String buildReqUrl(String url, String apiName, String transName) {
		return Joiner.on("/").join(url, apiName, transName);
	}

	public static boolean isSuccess(String respCode) {
		return "0000".equals(respCode);
	}
}
