//package org.apache.skywalking.apm.plugin.kepler.define;
//
//import com.lmax.disruptor.WorkHandler;
//
///**
// * @author WeiHui-Z
// * @version v1.0.0
// * @date 2019/6/5 21:41
// * @since JDK 1.8
// */
//@TraceCrossDisruptor
//public class DisruptorEventWrapper<T> implements WorkHandler<T> {
//
//	final WorkHandler<T> workHandler;
//
//	public DisruptorEventWrapper(WorkHandler<T> workHandler) {
//		this.workHandler = workHandler;
//	}
//
//	public static <T> DisruptorEventWrapper<T> of(WorkHandler<T> r) {
//		return new DisruptorEventWrapper<>(r);
//	}
//
//	@Override
//	public void onEvent(T event) throws Exception {
//		this.workHandler.onEvent(event);
//	}
//
//}
