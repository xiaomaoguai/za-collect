package com.xiaomaoguai.fcp.pre.kepler.mock;

import com.alibaba.fastjson.JSON;
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueConfigurator;
import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertTemplate;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 14:45
 * @since JDK 1.8
 */
@Setter
@Getter
public class MockHandler {

	private ConvertTemplate convertTemplate;

	private GlueConfigurator glueConfigurator;

	private String className;

	private String methodName;

	public MockHandler(final String className, final String methodName) {
		this.className = className;
		this.methodName = methodName;
	}

	@ResponseBody
	public Object invoke(@RequestBody String param) {
		// Groovy 方式
		//格转 方式
		//请求第三方模式 挡板模式
		String glueHandlerName = className + "_" + methodName;
		return glueConfigurator.run(glueHandlerName, JSON.parseObject(param));
	}

}
