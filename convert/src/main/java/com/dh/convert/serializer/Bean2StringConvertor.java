package com.dh.convert.serializer;

import java.util.Map;
import com.dh.convert.BeanConvertor;
import com.dh.convert.constants.Constants;
import com.dh.convert.jackson.JSON;
import com.dh.convert.node.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;

public class Bean2StringConvertor implements BeanConvertor<Object> {

	@Override
	public String convert(Object srcContent, TreeNode targetNode) throws Exception {
		JsonNode jn = JSON.valueToTree(srcContent);
		Map<String,Object>context =Maps.newHashMap();
		Map<String,Object> o = Maps.newHashMap();
		context.put(Constants.CURRENT_OBJECT, o );
		context.put(Constants.SRC_OBJECT, srcContent );
		targetNode.genObject(jn, o,null,context);
		return JSON.toJsonString(o);
	}


}
