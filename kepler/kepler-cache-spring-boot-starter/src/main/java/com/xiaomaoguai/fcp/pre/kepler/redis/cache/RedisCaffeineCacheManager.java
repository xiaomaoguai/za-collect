package com.xiaomaoguai.fcp.pre.kepler.redis.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.xiaomaoguai.fcp.pre.kepler.redis.configuration.CacheRedisCaffeineProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * @fileName: RedisCaffeineCacheManager.java
 * @author: WeiHui
 * @date: 2018/4/28 10:34
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class RedisCaffeineCacheManager implements CacheManager {

	private final Logger logger = LoggerFactory.getLogger(RedisCaffeineCacheManager.class);

	private ConcurrentMap<String, Cache> cacheMap = new ConcurrentHashMap<>();

	private CacheRedisCaffeineProperties cacheRedisCaffeineProperties;

	private RedisTemplate<Object, Object> redisTemplate;

	private boolean dynamic;

	private Set<String> cacheNames;

	public RedisCaffeineCacheManager(CacheRedisCaffeineProperties cacheRedisCaffeineProperties,
									 RedisTemplate<Object, Object> redisTemplate) {
		super();
		this.cacheRedisCaffeineProperties = cacheRedisCaffeineProperties;
		this.redisTemplate = redisTemplate;
		this.dynamic = cacheRedisCaffeineProperties.isDynamic();
		this.cacheNames = cacheRedisCaffeineProperties.getCacheNames();
	}

	@Override
	public Cache getCache(String name) {
		Cache cache = cacheMap.get(name);
		if (cache != null) {
			return cache;
		}
		if (!dynamic && !cacheNames.contains(name)) {
			return cache;
		}

		cache = new RedisCaffeineCache(name, redisTemplate, caffeineCache(), cacheRedisCaffeineProperties);
		Cache oldCache = cacheMap.putIfAbsent(name, cache);
		logger.debug("create cache instance, the cache name is : {}", name);
		return oldCache == null ? cache : oldCache;
	}

	private com.github.benmanes.caffeine.cache.Cache<Object, Object> caffeineCache() {
		Caffeine<Object, Object> cacheBuilder = Caffeine.newBuilder();
		long expireAfterAccess = cacheRedisCaffeineProperties.getCaffeine().getExpireAfterAccess();
		if (expireAfterAccess > 0) {
			cacheBuilder.expireAfterAccess(expireAfterAccess, TimeUnit.MILLISECONDS);
		}
		long expireAfterWrite = cacheRedisCaffeineProperties.getCaffeine().getExpireAfterWrite();
		if (expireAfterWrite > 0) {
			cacheBuilder.expireAfterWrite(expireAfterWrite, TimeUnit.MILLISECONDS);
		}
		int initialCapacity = cacheRedisCaffeineProperties.getCaffeine().getInitialCapacity();
		if (initialCapacity > 0) {
			cacheBuilder.initialCapacity(initialCapacity);
		}
		long maximumSize = cacheRedisCaffeineProperties.getCaffeine().getMaximumSize();
		if (maximumSize > 0) {
			cacheBuilder.maximumSize(maximumSize);
		}
//        long refreshAfterWrite = cacheRedisCaffeineProperties.getCaffeine().getRefreshAfterWrite();
//        if (refreshAfterWrite > 0) {
//            cacheBuilder.refreshAfterWrite(refreshAfterWrite, TimeUnit.MILLISECONDS);
//        }
		return cacheBuilder.build();
	}

	@Override
	public Collection<String> getCacheNames() {
		return this.cacheNames;
	}

	public void clearLocal(String cacheName, Object key) {
		Cache cache = cacheMap.get(cacheName);
		if (cache == null) {
			return;
		}
		RedisCaffeineCache redisCaffeineCache = (RedisCaffeineCache) cache;
		redisCaffeineCache.clearLocal(key);
	}

}
