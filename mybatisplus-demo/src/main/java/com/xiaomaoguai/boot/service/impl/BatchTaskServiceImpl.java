package com.xiaomaoguai.boot.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaoguai.boot.entity.BatchTask;
import com.xiaomaoguai.boot.mapper.BatchTaskMapper;
import com.xiaomaoguai.boot.service.BatchTaskService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 批量任务信息表 服务实现类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Service
public class BatchTaskServiceImpl extends ServiceImpl<BatchTaskMapper, BatchTask> implements BatchTaskService {

}
