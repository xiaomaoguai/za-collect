/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.core.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * 全局code代码，用于接口之间交互
 *
 * @author chenyao
 * @since 2018年5月23日 上午1:17:43
 */
@Getter
@AllArgsConstructor
public enum CodeMsgEnum {
    /**
     * 成功
     */
    OK("OK", "成功"),
    /**
     * 任务未完成
     */
    TASK_NOT_COMPLETE("taskIsNotComplete", "任务未完成"),
    /**
     * 记录找不到
     */
    RECORD_NOT_EXISTS("recordIsNotExists", "记录找不到"),
    /**
     * 没有匹配的记录
     */
    NOT_MATCH_RECORDS("notMatchRecords", "没有匹配的记录"),
    /**
     * 字段没有值
     */
    VALUE_IS_BLANK("fieldValueIsBlank", "字段没有值"),
    /**
     * 字段的值无效，不符合预期的格式
     */
    VALUE_IS_INVALID("fieldValueIsInvalid", "字段的值无效，不符合预期的格式"),
    /**
     * 匹配到了多条的记录
     */
    MORE_RECORDS("moreRecords", "匹配到了多条的记录"),
    /**
     * 重复提交
     */
    REPEATED_SUBMIT("repeatedSubmit", "重复提交"),
    /**
     * 非法请求
     */
    ILLEGAL_REQUEST("illegalRequest", "非法请求"),
    /**
     * 更新缓存异常
     */
    UPDATE_CACHE_EXCEPTION("updateCacheException", "更新缓存异常"),
    /**
     * 系统异常
     */
    SYSTEM_EXCEPTION("systemException", "系统异常"),
    /**
     * 远程调用异常
     */
    REMOTE_EXCEPTION("remoteException", "远程调用异常"),
    /**
     * 黑名单用户
     */
    USER_IS_BLACKLIST("userIsBlacklist", "黑名单用户"),
    /**
     * 灰名单用户
     */
    USER_IS_GRAY_LIST("userIsGrayList", "灰名单用户"),
    /**
     * 存在相同的记录，幂等校验失败
     */
    RECORD_ALREADY_EXISTS("recordAlreadyExists", "存在相同的记录，幂等校验失败");

    private String code;
    private String msg;

    /**
     * 根据错误代码查找枚举
     *
     * @param code 错误代码
     * @return 匹配的枚举类
     */
    public static CodeMsgEnum getEnumByCode(String code) {
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (CodeMsgEnum item : CodeMsgEnum.values()) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        return null;
    }
}
