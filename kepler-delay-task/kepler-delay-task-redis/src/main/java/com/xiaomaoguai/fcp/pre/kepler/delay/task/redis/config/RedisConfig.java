package com.xiaomaoguai.fcp.pre.kepler.delay.task.redis.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @fileName: RedisConfig.java
 * @author: WeiHui
 * @date: 2019/2/13 11:47
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Configuration
@ConditionalOnMissingBean(value = RedisConfig.class)
@ConditionalOnProperty(prefix = "spring.redis", value = "enabled", havingValue = "true")
public class RedisConfig {

	@Bean
	@ConditionalOnMissingBean(value = RedisTemplate.class)
	public RedisTemplate<?, ?> redisTemplate(RedisConnectionFactory redisConnectionFactory) {
		RedisTemplate<?, ?> template = new RedisTemplate<>();
		template.setConnectionFactory(redisConnectionFactory);
		template.setKeySerializer(keySerializer());
		template.setValueSerializer(valueSerializer());
		template.setHashKeySerializer(keySerializer());
		template.setHashValueSerializer(valueSerializer());
		return template;
	}

	@Bean
	@ConditionalOnMissingBean(value = StringRedisSerializer.class)
	public StringRedisSerializer keySerializer() {
		return new StringRedisSerializer();
	}

	@Bean
	@ConditionalOnMissingBean(value = GenericJackson2JsonRedisSerializer.class)
	public GenericJackson2JsonRedisSerializer valueSerializer() {
		return new GenericJackson2JsonRedisSerializer();
	}

}
