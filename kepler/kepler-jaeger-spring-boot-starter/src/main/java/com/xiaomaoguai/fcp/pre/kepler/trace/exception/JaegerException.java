package com.xiaomaoguai.fcp.pre.kepler.trace.exception;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 11:10
 * @since JDK 1.8
 */
public class JaegerException extends RuntimeException {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 3489451896604776165L;

	public JaegerException(Throwable cause) {
		super(cause);
	}

}
