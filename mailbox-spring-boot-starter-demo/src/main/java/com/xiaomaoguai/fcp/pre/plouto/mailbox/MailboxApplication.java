package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.xiaomaoguai.fcp.pre.plouto.mailbox.autoconfigure.MailBoxTemplate;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * @fileName: MailboxApplication.java
 * @author: WeiHui
 * @date: 2018/11/6 11:13
 * @version: v1.0.0
 * @since JDK 1.8
 */
@SpringBootApplication
public class MailboxApplication implements CommandLineRunner {

	@Resource
	private MailBoxTemplate mailBoxTemplate;

	public static void main(String[] args) {
		SpringApplication.run(MailboxApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		mailBoxTemplate.call(TestMailEnum.TestActor, "hello world");
	}

}
