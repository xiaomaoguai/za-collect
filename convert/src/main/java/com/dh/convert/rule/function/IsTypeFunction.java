package com.dh.convert.rule.function;

import java.util.Map;
import com.dh.convert.utils.ClassUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorObject;

public class IsTypeFunction extends AbstractFunction {

	 @Override
     public AviatorBoolean call(Map<String, Object> env, AviatorObject arg ,AviatorObject arg1) {
		 String type =  FunctionUtils.getStringValue(arg1, env);
		 Object o =  FunctionUtils.getJavaObject(arg, env);
		 if(o  == null){
			 return AviatorBoolean.FALSE;
		 }
		 
		 switch(TypeEnum.valueOf(type)){
		 case S:
			 return AviatorBoolean.valueOf(o.getClass().getSimpleName().equals("String"));
		 case A:
			 return AviatorBoolean.valueOf(o.getClass().isArray());
		 case ENUM:
			 return AviatorBoolean.valueOf(o.getClass().isEnum());
		 case MAP:
			 return AviatorBoolean.valueOf(ClassUtils.isMap(o.getClass()));
		 case C:
			 return  AviatorBoolean.valueOf(ClassUtils.isColltion(o.getClass()));
		 case N:
			 return AviatorBoolean.valueOf(ClassUtils.isNumber(o.getClass()));
		default:
			 return AviatorBoolean.FALSE; 
		 }
		
     }
	 
	@Override
	public String getName() {
		return "isType";
	}
	
	enum TypeEnum{
		S,
		N,
		MAP,
		A,
		C,
		ENUM;
	}

}
