package com.xiaomaoguai.fcp.pre.kepler.convert.core;

import com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter.BuildDataDefaultMethod;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author WeiHui
 * @date 2019/1/25
 */
public class ConvertClientInvocationHandler extends BuildDataDefaultMethod implements InvocationHandler {

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		return super.invoke(method, args);
	}

}
