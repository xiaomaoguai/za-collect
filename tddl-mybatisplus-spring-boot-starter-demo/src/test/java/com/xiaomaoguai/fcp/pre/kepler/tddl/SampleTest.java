package com.xiaomaoguai.fcp.pre.kepler.tddl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaoguai.fcp.pre.kepler.tddl.entity.UserCard;
import com.xiaomaoguai.fcp.pre.kepler.tddl.mapper.UserCardMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = TddlApplication.class)
public class SampleTest {

	@Autowired
	private UserCardMapper userCardMapper;

	@Test
	public void testSelect() {
		System.out.println(("----- selectAll method test ------"));
		UserCard card = new UserCard();
		card.setUserId(100L);
		QueryWrapper<UserCard> queryWrapper = new QueryWrapper<>(card);
		UserCard userCard = userCardMapper.selectOne(queryWrapper);
		System.out.println(JSON.toJSONString(userCard));
	}

	@Test
	public void testSequence() {
		UserCard u = new UserCard();
		u.setProductCode("WLTEST");
		u.setUserId(103L);
		u.setThirdUserNo("");
		u.setBankName("");
		u.setCardName("");
		u.setCertNo("");
		u.setBankCardNo("");
		u.setCardPhone("");
		u.setCardType("");
		u.setCardUse("");
		u.setBankCode("");
		u.setCardStatus("");
		u.setIsDefault("");
		u.setMerchantNo("");
		u.setAgreementInfo("");
		u.setAgreementChannelId("");
		u.setCreator("");
		u.setModifier("");
		u.setGmtCreated(new Date());
		u.setGmtModified(new Date());
		u.setIsDeleted("N");

		userCardMapper.insert(u);
	}

}
