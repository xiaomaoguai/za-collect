package com.xiaomaoguai.fcp.service;

import com.xiaomaoguai.fcp.config.LoanHsfClient;
import com.xiaomaoguai.creditcore.cashloan.api.LoanApi;
import com.xiaomaoguai.creditcore.cashloan.req.LoanQueryReq;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/24 14:32
 * @since JDK 1.8
 */
@Component
public class MyInvoke {

	@Resource
	private LoanApi loanApi;

	@Resource
	private LoanHsfClient loanHsfClient;

	public Object getData() {
		LoanQueryReq loanQueryReq = new LoanQueryReq();
		loanQueryReq.setUserId(31500072L);
		loanQueryReq.setProductCode("SZXXR");
		loanQueryReq.setLoanOuterNo("Loan_1527477165065975085");
		return loanApi.loanQuery(loanQueryReq);
	}

	public Object getData2() {
		LoanQueryReq loanQueryReq = new LoanQueryReq();
		loanQueryReq.setUserId(31500072L);
		loanQueryReq.setProductCode("SZXXR");
		loanQueryReq.setLoanOuterNo("Loan_1527477165065975085");
		return loanHsfClient.loanQuery(loanQueryReq);
	}

//	@GetMapping("/id3")
//	public Object getData3() {
//		LoanQueryReq loanQueryReq = new LoanQueryReq();
//		loanQueryReq.setUserId(31500072L);
//		loanQueryReq.setProductCode("SZXXR");
//		loanQueryReq.setLoanOuterNo("Loan_1527477165065975085");
//		JSONObject jsonObject = loanHsfClient3.loanQuery(loanQueryReq);
//
//		log.info("====>{}", jsonObject);
//
//		return jsonObject;
//	}

}
