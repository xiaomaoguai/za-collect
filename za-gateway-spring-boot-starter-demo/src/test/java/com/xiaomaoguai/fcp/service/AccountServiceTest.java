package com.xiaomaoguai.fcp.service;

import com.github.xiaomaoguai.xxr.dto.req.BizReq;
import com.github.xiaomaoguai.xxr.dto.resp.ResultBase;
import com.xiaomaoguai.fcp.BaseTest;
import com.xiaomaoguai.fcp.fcp.dto.AccessResp;
import com.xiaomaoguai.fcp.fcp.dto.BankList;
import com.xiaomaoguai.fcp.fcp.dto.req.AuthNameReq;
import com.xiaomaoguai.fcp.fcp.service.AccountService;
import org.junit.Test;

import javax.annotation.Resource;

/**
 * @fileName: AccountServiceTest.java
 * @author: WeiHui
 * @date: 2018/9/12 17:03
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class AccountServiceTest extends BaseTest {

	@Resource
	private AccountService accountService;

	private String thirdUserNo = "18565891406";

	@Test
	public void testAuthName() {
		BizReq bizReq = new BizReq();
		bizReq.setThirdUserNo(thirdUserNo);
		ResultBase<BankList> resultBase = accountService.authName(bizReq);

		log.info(" result: {}", resultBase);
	}

	@Test
	public void testQueryBankList() {
		BizReq bizReq = new BizReq();
		bizReq.setThirdUserNo(thirdUserNo);
		ResultBase<BankList> resultBase = accountService.queryBankList(bizReq);
		log.info(" result:  {}", resultBase);
	}

	@Test
	public void certAuTest() {
		AuthNameReq au = new AuthNameReq();
		au.setThirdUserNo(thirdUserNo);
		au.setCertAddress("广东省深圳市...");
		au.setCertExpireDate("20170605-20370605");
		au.setCertNo("2342656788767889865");
		au.setCertOrderNo("ocr15363101751688");
		au.setOpenId("o_pnr0s7IaSi_63qg7qjKwwflP9A");
		au.setUserMobile("12345534334");
		au.setUserName("张*");
		ResultBase<AccessResp> resultBase = accountService.auth(au);

		log.info(" result: {}", resultBase);
	}
}
