/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.cache.redis.config;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

import com.xiaomaoguai.core.next.cache.constants.CacheConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.zookeeper.ZooKeeper;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertySource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import app.myoss.cloud.core.constants.DeployEnvEnum;
import app.myoss.cloud.core.constants.MyossConstants;
import app.myoss.cloud.core.exception.BizRuntimeException;
import lombok.extern.slf4j.Slf4j;

/**
 * An {@link ApplicationContextInitializer} ，获取"阿里云 Redis"连接信息进行自动配置。
 * <p>
 * 会配置以下几个节点key，可以在项目中的 application.yml 中进行覆盖
 *
 * <pre>
 * map.put(&quot;spring.redis.host&quot;, redisInfo.getHost());
 * map.put(&quot;spring.redis.port&quot;, redisInfo.getPort());
 * map.put(&quot;spring.redis.database&quot;, redisInfo.getDbNo());
 * map.put(&quot;spring.redis.timeout&quot;, &quot;1s&quot;);
 * map.put(&quot;spring.redis.password&quot;, redisInfo.getPassword());
 * </pre>
 *
 * @author chenyao
 * @since 2018年5月22日 下午6:49:09
 * @see org.springframework.boot.SpringApplication#applyInitializers(ConfigurableApplicationContext)
 */
@Slf4j
public class AliYunRedisEnvironmentPropertySourceLocator
        implements ApplicationContextInitializer<ConfigurableApplicationContext>, Ordered {
    /**
     * The default order for the processor.
     *
     * <pre>
     * 如果项目中有这个依赖
     * &lt;dependency&gt;
     *     &lt;groupId&gt;org.springframework.cloud&lt;/groupId&gt;
     *     &lt;artifactId&gt;spring-cloud-context&lt;/artifactId&gt;
     * &lt;/dependency&gt;
     * 会在它后面执行初始化
     * {@code org.springframework.cloud.bootstrap.config.PropertySourceBootstrapConfiguration#getOrder()}
     *
     * 这样就可以支持从动态配置中心中获取配置的 redisKey
     * &lt;dependency&gt;
     *     &lt;groupId&gt;org.springframework.cloud&lt;/groupId&gt;
     *     &lt;artifactId&gt;spring-cloud-starter-config&lt;/artifactId&gt;
     * &lt;/dependency&gt;
     * </pre>
     */
    public static final int    DEFAULT_ORDER                  = Ordered.HIGHEST_PRECEDENCE + 11;
    /**
     * 属性配置名字
     */
    public static final String PROPERTY_SOURCE_NAME           = "defaultProperties";
    /**
     * Bootstrap Property Source Name:
     * {@code org.springframework.cloud.bootstrap.config.PropertySourceBootstrapConfiguration#BOOTSTRAP_PROPERTY_SOURCE_NAME}
     */
    public static final String BOOTSTRAP_PROPERTY_SOURCE_NAME = "bootstrapProperties";
    /**
     * zookeeper 开发环境地址
     */
    public static final String ZK_SERVER_DEV                  = "zk-node1.dev.za.net:2181";
    /**
     * zookeeper 测试环境地址
     */
    public static final String ZK_SERVER_TEST                 = "zk-node1.test.za.net:2181";
    /**
     * zookeeper 预发环境地址
     */
    public static final String ZK_SERVER_PRE                  = "zk-node1.pre.za.net:2181";
    /**
     * zookeeper 生产环境地址
     */
    public static final String ZK_SERVER_PRD                  = "zk-node1.prd.za.net:2181,zk-node2.prd.za.net:2181,zk-node3.prd.za.net:2181";

    @Override
    public int getOrder() {
        return DEFAULT_ORDER;
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        ConfigurableEnvironment environment = applicationContext.getEnvironment();
        Binder binder = Binder.get(environment);
        BindResult<RedisProperties> bindResult = binder.bind(CacheConstants.REDIS_PREFIX, RedisProperties.class);
        if (!bindResult.isBound()) {
            return;
        }
        RedisProperties redisProperties = bindResult.get();
        String redisConfigKey = redisProperties.getRedisConfigKey();
        if (StringUtils.isBlank(redisConfigKey)) {
            log.debug("redisConfigKey is not config, ignore AliYun Redis auto configuration");
            return;
        }

        String zkServer = redisProperties.getRedisZookeeperServer();
        if (StringUtils.isBlank(zkServer)) {
            if (DeployEnvEnum.isTest()) {
                zkServer = ZK_SERVER_TEST;
            } else if (DeployEnvEnum.isPre()) {
                zkServer = ZK_SERVER_PRE;
            } else if (DeployEnvEnum.isPrd()) {
                zkServer = ZK_SERVER_PRD;
            } else {
                zkServer = ZK_SERVER_TEST;
            }
        }

        RedisInfo redisInfo = null;
        int retryTime = 0;
        int retryMax = 3;
        while (retryTime < retryMax) {
            try {
                redisInfo = getRedisInfo(zkServer, redisConfigKey);
                retryTime = retryMax;
            } catch (Exception ex) {
                retryTime++;
                if (retryTime >= retryMax) {
                    throw new BizRuntimeException("get redis config info from zookeeper failed. zkServer: " + zkServer
                            + ", redisConfigKey: " + redisConfigKey, ex);
                }
            }
        }

        JSONObject map = new JSONObject();
        map.put("spring.redis.host", redisInfo.getHost());
        map.put("spring.redis.port", redisInfo.getPort());
        map.put("spring.redis.database", redisInfo.getDbNo());
        map.put("spring.redis.timeout", "1s");
        log.info("AliYun Redis config info: " + map.toJSONString() + ", zkServer: " + zkServer + ", redisConfigKey: "
                + redisConfigKey);
        map.put("spring.redis.password", redisInfo.getPassword());

        MutablePropertySources propertySources = environment.getPropertySources();
        addOrReplace(propertySources, map);
    }

    private RedisInfo getRedisInfo(String server, String redisConfigKey) throws Exception {
        ZooKeeper zooKeeper = new ZooKeeper(server, 10000, (event) -> log.info(event.toString()));
        String data = new String(zooKeeper.getData(redisConfigKey, false, null), MyossConstants.DEFAULT_CHARSET);
        RedisInfo redisInfo = JSON.parseObject(data, RedisInfo.class);
        zooKeeper.close();
        Objects.requireNonNull(redisInfo, "无效的 redisConfigKey, 请检查是否配置错误");
        return redisInfo;
    }

    private void addOrReplace(MutablePropertySources propertySources, Map<String, Object> map) {
        MapPropertySource target = null;
        PropertySource<?> bootstrapProperties = propertySources.get(BOOTSTRAP_PROPERTY_SOURCE_NAME);
        if (bootstrapProperties instanceof CompositePropertySource) {
            // 使用了 spring-cloud-context/spring-cloud-starter-config 才会有
            CompositePropertySource composite = (CompositePropertySource) bootstrapProperties;
            for (PropertySource<?> propertySource : composite.getPropertySources()) {
                if ("configService".equals(propertySource.getName())
                        && propertySource instanceof CompositePropertySource) {
                    // {@link org.springframework.cloud.config.client.ConfigServicePropertySourceLocator#locate}
                    CompositePropertySource configService = (CompositePropertySource) propertySource;
                    target = new MapPropertySource(PROPERTY_SOURCE_NAME, map);
                    configService.addPropertySource(target);
                    return;
                }
            }
        }

        PropertySource<?> source = propertySources.get(PROPERTY_SOURCE_NAME);
        if (source instanceof MapPropertySource) {
            target = (MapPropertySource) source;
            for (Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                if (!target.containsProperty(key)) {
                    target.getSource().put(key, entry.getValue());
                }
            }
        }
        if (target == null) {
            target = new MapPropertySource(PROPERTY_SOURCE_NAME, map);
        }
        if (!propertySources.contains(PROPERTY_SOURCE_NAME)) {
            propertySources.addLast(target);
        }
    }
}
