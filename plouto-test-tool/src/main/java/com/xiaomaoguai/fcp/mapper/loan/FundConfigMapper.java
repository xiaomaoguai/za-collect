/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.loan;

import com.xiaomaoguai.fcp.domain.FundConfig;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * The Interface FundConfigMapper.
 */
public interface FundConfigMapper {

	/**
	 * 根据主键删除数据库的记录.
	 *
	 * @param id the id
	 * @return the int
	 */
	int deleteByPrimaryKey(Long id);

	/**
	 * 新写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(FundConfig record);

	/**
	 * 动态字段,写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(FundConfig record);

	/**
	 * 根据指定主键获取一条数据库记录.
	 *
	 * @param id the id
	 * @return the fund config
	 */
	FundConfig selectByPrimaryKey(Long id);

	/**
	 * 动态字段,根据主键来更新符合条件的数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByPrimaryKeySelective(FundConfig record);

	/**
	 * 根据主键来更新符合条件的数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByPrimaryKey(FundConfig record);


	/**
	 * Gets the config.
	 *
	 * @param productCode the product code
	 * @param fundCode    the fund code
	 * @return the config
	 */
	FundConfig getConfig(@Param("productCode") String productCode, @Param("fundCode") String fundCode);


	/**
	 * Select by state.
	 *
	 * @param productCode the product code
	 * @param state       the state
	 * @return the list
	 */
	List<FundConfig> selectByState(@Param("productCode") String productCode, @Param("state") String state);
	
	FundConfig selectConfigForRate(@Param("fundCode")String fundConde,@Param("state")Integer state);
	

}