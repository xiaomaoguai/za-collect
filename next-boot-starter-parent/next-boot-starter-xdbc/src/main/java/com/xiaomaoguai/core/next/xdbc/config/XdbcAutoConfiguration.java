/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.xdbc.config;

import org.springframework.context.annotation.Configuration;

/**
 * Xdbc服务自动配置
 *
 * @author chenyao
 * @since 2019年11月02日 下午12:47:40
 */
@Configuration
public class XdbcAutoConfiguration {
}
