package com.xiaomaoguai.fcp.pre.kepler.ons.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @fileName: OnsListener.java
 * @author: WeiHui
 * @date: 2019/2/26 19:33
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OnsListener {

}
