//import com.xiaomaoguai.msg.api.MessageRecord;
//import com.xiaomaoguai.msg.api.bootstrap.MqClient;
//import com.xiaomaoguai.msg.api.common.MessagePriorityEnum;
//import com.xiaomaoguai.msg.api.common.SerializerTypeEnum;
//import com.xiaomaoguai.msg.api.producer.MqProducer;
//import com.xiaomaoguai.msg.api.producer.OnExceptionContext;
//import com.xiaomaoguai.msg.api.producer.SendCallback;
//import com.xiaomaoguai.msg.api.producer.SendResult;
//import com.xiaomaoguai.msg.api.producer.bingding.ProducerConfig;
//import com.xiaomaoguai.msg.sample.TestMessageBean;
//
///**
// * @fileName: Test.java
// * @author: WeiHui
// * @date: 2019/2/26 15:13
// * @version: v1.0.0
// * @since JDK 1.8
// */
//public class Test {
//
//	public static void main(String[] args) {
//		ProducerConfig config = new ProducerConfig("ons|http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal", "D-TEST-KEPLER-TEST-KEPLER-190226", MessagePriorityEnum.MEDIUM);
//		config.setSerializer(SerializerTypeEnum.JSON);
//		config.put("ProducerId", "PID-D-TEST-KEPLER-190226");
//		config.put("AccessKey", "LTAITPZlbVtAsEPr");
//		config.put("SecretKey", "ef7khpMK8wVNbwTmmrndfd9fIgwvbE");
//
//		TestMessageBean bean = new TestMessageBean();
//		bean.setName("TestMessageBean");
//
//		MessageRecord<TestMessageBean> record = new MessageRecord<>();
//		record.setMessage(bean);
//		record.setTag("tag1");
//		record.setStartDeliverTime(5000L);
//		record.putUserProperties("userKey1", "userVal1");
//
//		MqProducer producer = MqClient.buildProducer(config);
//		producer.send(record);
//		producer.sendOneway(record);
//		producer.sendAsync(record, new SendCallback() {
//
//			@Override
//			public void onSuccess(SendResult sendResult) {
//				System.out.println("Send successful. " + sendResult);
//			}
//
//			@Override
//			public void onException(OnExceptionContext context) {
//				System.out.println("Send failed. message:" + context.getMessageRecord() + ", error:" + context.getException());
//			}
//		});
//	}
//
//}
