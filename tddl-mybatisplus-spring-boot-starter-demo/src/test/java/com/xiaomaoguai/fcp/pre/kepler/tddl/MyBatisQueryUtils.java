package com.xiaomaoguai.fcp.pre.kepler.tddl;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.InputStream;

/**
 * @Description:
 * @FileName: MyBatisQueryUtils.java
 * @author :WeiHui.Zhang
 * @Date: 2016年6月6日 下午7:55:57
 * @Version:V1.00
 */
public final class MyBatisQueryUtils {

	private SqlSessionFactory sessionFactory = null;
	
	/**
	 * 构造方法
	 */
	private MyBatisQueryUtils() {
		// mybatis的配置文件
		String resource = "mybatis-config.xml";
		// 使用类加载器加载mybatis的配置文件（它也加载关联的映射文件）
		InputStream is = MyBatisQueryUtils.class.getClassLoader().getResourceAsStream(resource);
		// 构建sqlSession的工厂
		sessionFactory = new SqlSessionFactoryBuilder().build(is);
	}

	public static MyBatisQueryUtils getInstance() {
		return MyBatisQueryUtilsStaticHolder.HOLDER;
	}

	public SqlSessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	/**
	 * Description 单例模式
	 * Date 2016年8月25日 下午7:48:26
	 * @version V1.0
	 */
	private static class MyBatisQueryUtilsStaticHolder {
		private static final MyBatisQueryUtils HOLDER = new MyBatisQueryUtils();
	}

}
