/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.apm.constants;

/**
 * 应用性能监控常量
 *
 * @author chenyao
 * @since 2019年6月12日 下午6:22:31
 */
public class ApmConstants {
}
