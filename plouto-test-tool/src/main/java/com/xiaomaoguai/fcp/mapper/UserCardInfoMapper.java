/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper;


import com.xiaomaoguai.fcp.domain.UserCardInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * The Interface UserCardInfoMapper.
 */
public interface UserCardInfoMapper {

	/**
	 * Insert selective.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(UserCardInfo record);

	/**
	 * Select by user id.
	 *
	 * @param userId     the user id
	 * @param bankCardNo the bank card no
	 * @param isDefault  the is default
	 * @return the list
	 */
	List<UserCardInfo> selectByUserId(@Param("userId") Long userId
			, @Param("bankCardNo") String bankCardNo
			, @Param("isDefault") Integer isDefault);

	/**
	 * Update by user id.
	 *
	 * @param userId    the user id
	 * @param isDefault the is default
	 * @return the int
	 */
	int updateByUserId(@Param("userId") Long userId, @Param("bankCardNo") String bankCardNo, @Param("isDefault") Integer isDefault);

	int unBindCard(@Param("userId") Long userId, @Param("bankCardNo") String bankCardNo);

	int deleteByUserId(Long userId);
}