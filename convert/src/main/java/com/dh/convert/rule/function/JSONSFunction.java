package com.dh.convert.rule.function;

import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorNil;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorString;

public class JSONSFunction extends AbstractFunction{

	  @Override
      public AviatorObject call(Map<String, Object> env, AviatorObject arg) {
		 Object src = arg.getValue(env);
		 if(src == null)
			 return AviatorNil.NIL;
		return  new AviatorString(JSON.toJSONString(src));
          
      }
	@Override
	public String getName() {
		return "JSONS";
	}

}
