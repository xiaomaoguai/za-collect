package com.xiaomaoguai.fcp.pre.kepler.mock.zktool;

import com.xiaomaoguai.fcp.pre.kepler.zk.utils.ZookeeperContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.junit.Before;

import java.io.File;
import java.nio.charset.StandardCharsets;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 17:36
 * @since JDK 1.8
 */
public class ZkEnumBaseTest {

	@Before
	public void beforeTest() {
		CuratorFramework curatorFramework = ZkEnum.TEST.getCuratorFramework();
		ZookeeperContext.setClient(curatorFramework);
	}

	protected void createOrUpdateData(String mockClassName) throws Exception {
		String nodePath = "/application/glue/" + StringUtils.substring(mockClassName, 0, mockClassName.length() - 7);
		String filePath = AZkEnumTest.class.getClassLoader().getResource("").getPath() + "META-INF/" + mockClassName;
		ZookeeperContext.createOrUpdateData(nodePath, FileUtils.readFileToString(new File(filePath), StandardCharsets.UTF_8));
	}

}
