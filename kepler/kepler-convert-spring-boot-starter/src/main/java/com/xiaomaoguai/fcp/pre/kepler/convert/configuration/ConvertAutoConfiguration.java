package com.xiaomaoguai.fcp.pre.kepler.convert.configuration;

import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertManager;
import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertTemplate;
import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertTemplateInvocationHandler;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huwenxuan
 * @date 2019/1/11
 */
@Configuration
public class ConvertAutoConfiguration {

	/**
	 * 格转信息管理器
	 *
	 * @return 格转信息管理器
	 */
	@Bean
	public ConvertManager convertManager() {
		return new ConvertManager();
	}

	/**
	 * 初始化
	 *
	 * @return 加载规则
	 */
	@Bean
	public KeplerBeanFactoryPostInitializer keplerBeanFactoryPostInitializer() {
		return new KeplerBeanFactoryPostInitializer();
	}

	/**
	 * 加载  ConvertTemplate
	 *
	 * @return ConvertTemplate
	 */
	@Bean
	public ConvertTemplate convertTemplate() {
		ProxyFactory proxyFactory = new ProxyFactory();
		proxyFactory.addInterface(ConvertTemplate.class);
		proxyFactory.addAdvice(new ConvertTemplateInvocationHandler());
		proxyFactory.setOptimize(false);
		return (ConvertTemplate) proxyFactory.getProxy(getClass().getClassLoader());
	}

}
