package com.xiaomaoguai.fcp.pre.kepler.trace.constants;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/6 9:55
 * @since JDK 1.8
 */
public final class KeplerTraceConstant {

	public static final String KEPLER_OPENTRACING_JAEGER_ENABLED = "opentracing.jaeger.enabled";

	public static final String KEPLER_OPENTRACING_JAEGER_METRICS_ENABLED = "opentracing.jaeger.metrics.enabled";

	public static final String KEPLER_JAEGER_TRACE_SEPARATE_SPAN_ENABLED = "kepler.jaeger.trace.separate.span.enabled";

	public final static String STANDARD_HEAD_HANDLER_TYPE_NAME = "com.xiaomaoguai.fcp.pre.kepler.common.handler.StandardHeadHandler";

	public static final String TRACER_BUILDER_CUSTOMIZER_BEAN_NAME = "zaTracerBuilderCustomizer";

	public final static String KEPLER_JAEGER_DATA_DESENSITIZATION_BEAN_NAME = "jaegerDataDesensitization";

	public final static String HANDLER_INTERCEPTOR_FACTORY_BEAN_NAME = "handlerInterceptorFactory";

	public final static String HEAD_HANDLER_AROUND_INTERCEPTOR_BEAN_NAME = "headHandlerAroundInterceptor";

	public final static String COMMON_HANDLER_AROUND_INTERCEPTOR_BEAN_NAME = "commonHandlerAroundInterceptor";

	public final static String TAIL_HANDLER_AROUND_INTERCEPTOR_BEAN_NAME = "tailHandlerAroundInterceptor";

	public static final String TAG_COMPONENT_VALUE_HEAD = "keplerHead";

	public static final String TAG_COMPONENT_VALUE_TAIL = "keplerTail";

	public static final String TAG_COMPONENT_VALUE_HANDLER = "keplerHandler";

	public static final String SPAN_TAG_PRODUCT_CODE = "productCode";

	public static final String SPAN_TAG_ROUTER_NAME = "routerName";

	public static final String SPAN_TAG_HANDLER_NAME = "handlerName";

	public static final String SPAN_TAG_URL = "url";

	public static final String SPAN_TAG_RPC_URL = "rpcUrl";

	public static final String SPAN_TAG_LOCK_KEY = "lockKey";

	public static final String SPAN_TAG_UN_LOCK_KEY = "unLockKey";

	public static final String SPAN_TAG_MOCK = "mock";

	public static final String SPAN_TAG_BEFORE_CONVERT = "beforeConvert";

	public static final String SPAN_TAG_AFTER_CONVERT = "afterConvert";

	public static final String SPAN_TAG_BEFORE_ROUTER_IN = "beforeRouterIn";

	public static final String SPAN_TAG_AFTER_ROUTER_IN = "afterRouterIn";

	public static final String SPAN_TAG_BEFORE_ROUTER_OUT = "beforeRouterOut";

	public static final String SPAN_TAG_AFTER_ROUTER_OUT = "afterRouterOut";

	public static final String SPAN_TAG_THIRD_USER_NO = "thirdUserNo";

	public static final String SPAN_TAG_CREDIT_APPLY_NO = "creditApplyNo";

	public static final String SPAN_TAG_LOAN_OUTER_NO = "loanOuterNo";

	public static final String SPAN_TAG_REPAY_OUTER_NO = "repayOuterNo";

}
