package com.xiaomaoguai.fcp.kepeler.riskFiled;

import com.geccocrawler.gecco.spider.HtmlBean;
import lombok.Data;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 18:04
 * @since JDK 1.8
 */
@Data
public class RiskFieldInfo implements HtmlBean {

	private static final long serialVersionUID = 4193946016197181547L;

	/**
	 * 字段名
	 */
	private String desc;

	/**
	 * 字段code
	 */
	private String code;

	/**
	 * 属性
	 */
	private String type;

	/**
	 * 	是否必须
	 */
	private String must;

}
