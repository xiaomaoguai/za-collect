package com.xiaomaoguai.fcp.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @fileName: TransRepayPlan.java
 * @author: WeiHui
 * @date: 2018/9/4 23:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class TransRepayPlan extends BaseRepayPlan {

	/**
	 * 已还本金
	 */
	@JsonIgnore
	private BigDecimal paidPrincipal;

	/**
	 * 已还利息
	 */
	@JsonIgnore
	private BigDecimal paidInterest;

	/**
	 * 已还服务费
	 */
	@JsonIgnore
	private BigDecimal paidCharge;

	/**
	 * 已还罚息
	 */
	@JsonIgnore
	private BigDecimal paidPenalty;

	/**
	 * 已还其他费用
	 */
	@JsonIgnore
	private BigDecimal paidOtherAmount;

	/**
	 * 还款时间
	 */
	@JsonIgnore
	private Date repayTime;
}
