package com.xiaomaoguai.fcp.pre.kepler.router.buf;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import com.alibaba.fastjson.JSON;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.RouterInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.CallBack;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.Instruction;

public class CallbackWaitBuf implements Buf,Serializable{

	private static final long serialVersionUID = 1991840161460191320L;

	private String id;
	
	private CallBack callBack = CallBack.WAIT; 
	
	@Override
	public List<RouterInfo> getRemoteInfos() {
		return null;
	}

	@Override
	public void setRemoteInfos(List<RouterInfo> remoteInfos) {
		
	}

	@Override
	public Buf put(String key, Object value) {
		return null;
	}
	
	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public Map<String, Object> getContext() {
		return null;
	}

	@Override
	public Object get(String key) {
		return null;
	}

	@Override
	public Buf remove(String key) {
		return null;
	}

	@Override
	public void clear() {
	}

	@Override
	public int size() {
		return 0;
	}

	@Override
	public Buf object2Buf(Object o) {
		return this;
	}

	@Override
	public String toJsonString() {
		return JSON.toJSONString(this);
	}

	@Override
	public Buf jsonString2Buf(String s) {
		return JSON.parseObject(s, this.getClass());
	}

	@Override
	public Buf addRemoteInfo(RouterInfo routerInfo) {
		return null;
	}

	@Override
	public Buf addRemoteInfo(List<RouterInfo> routerInfos) {
		return null;
	}

	@Override
	public boolean getRpcBuf() {
		return Boolean.TRUE;
	}

	@Override
	public String getNextHandlerName() {
		return null;
	}

	@Override
	public void setNextHandlerName(String nextHandlerName) {
		
	}

	@Override
	public CallBack getCallBack() {
		return callBack;
	}

	@Override
	public void setCallBack(CallBack callBack) {
		this.callBack = callBack;
	}

	@Override
	public void setRpcBuf(boolean rpcBuf) {
	}


	@Override
	public String getExceptionHandlerName() {
		return null;
	}

	@Override
	public Throwable getException() {
		return null;
	}

	@Override
	public Buf setContext(Map<String, Object> context) {
		return this;
	}

	@Override
	public void setException(Throwable e) {
	}

	@Override
	public void setExceptionHandlerName(String exceptionHandlerName) {
	}

	@Override
	public String getCurrentRouterName() {
		return null;
	}

	@Override
	public String getCurrentHandlerName() {
		return null;
	}

	@Override
	public void setCurrentRouterName(String currentRouterName) {
		
	}

	@Override
	public void setCurrentHandlerName(String currentHandlerName) {
	}

	@Override
	public Instruction getInstruction() {
		return Instruction.BREAK;
	}

	@Override
	public void setInstruction(Instruction instruction) {
	}


}
