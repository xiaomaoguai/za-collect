//package com.xiaomaoguai.fcp.pre.kepler.halley;
//
//import com.xiaomaoguai.msg.api.annotation.Consumer;
//import com.xiaomaoguai.msg.api.common.MessagePriorityEnum;
//import com.xiaomaoguai.msg.api.common.SerializerTypeEnum;
//import com.xiaomaoguai.msg.api.consumer.AckAction;
//import com.xiaomaoguai.msg.api.consumer.ReceiveRecord;
//import com.xiaomaoguai.share.event.db.DBEvent;
//import org.springframework.stereotype.Service;
//
///**
// * @author WeiHui-Z
// * @version v1.0.0
// * @date 2019/7/31 19:34
// * @since JDK 1.8
// */
//@Service
//public class ApiConsumerService {
//
//	@Consumer(topic = "fcp-kepler-201905161412", priority = MessagePriorityEnum.MEDIUM, numThreads = 5, serializer = SerializerTypeEnum.BYTEARRAY)
//	public AckAction consumerLow(ReceiveRecord<DBEvent> message){
//		System.out.println("ConsumerService.consumerLow message:" + message);
//		return AckAction.Commit;
//	}
//
//}
