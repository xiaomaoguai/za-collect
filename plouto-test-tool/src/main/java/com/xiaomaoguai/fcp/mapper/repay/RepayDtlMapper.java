/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.repay;


import com.xiaomaoguai.fcp.domain.RepayDtl;
import com.xiaomaoguai.fcp.domain.RepayInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 还款明细
 *
 * @fileName: RepayDtlMapper.java
 * @author: WeiHui
 * @date: 2018/9/4 15:19
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface RepayDtlMapper {

	/**
	 * Insert.
	 *
	 * @param repayDtls the repay dtls
	 * @return the int
	 */
	int insert(RepayDtl repayDtls);

	/**
	 * Select by repay outer no.
	 *
	 * @param repayOuterNo the repay outer no
	 * @param userId       the user id
	 * @return the list
	 */
	List<RepayDtl> selectByRepayOuterNo(@Param("repayOuterNo") String repayOuterNo, @Param("userId") Long userId);

	/**
	 * Update by repay outer no.
	 *
	 * @param repayInfo the repay info
	 * @param status    the status
	 * @return the int
	 */
	int updateByRepayOuterNo(@Param("repayInfo") RepayInfo repayInfo, @Param("status") Integer status);


	List<RepayDtl> selectList(RepayDtl repayDtl);

	/**
	 * 查询状态不为失败的还款详细信息
	 *
	 * @param repayDtl
	 * @return
	 */
	List<RepayDtl> selectDoingAndSuccessList(RepayDtl repayDtl);

	int deleteByUserId(Long userId);
}