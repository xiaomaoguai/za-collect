/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.reactive.constants;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;

/**
 * web响应式常量
 *
 * @author chenyao
 * @since 2019年6月14日 下午3:22:04
 */
public class WebReactiveConstants {
    /**
     * web flux配置前缀
     */
    public static final String CONFIG_PREFIX = CoreConstants.BOOT_CONFIG_PREFIX + ".webflux";
}
