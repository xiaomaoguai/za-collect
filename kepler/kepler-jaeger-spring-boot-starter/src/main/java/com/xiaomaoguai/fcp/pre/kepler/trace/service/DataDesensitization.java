package com.xiaomaoguai.fcp.pre.kepler.trace.service;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 17:12
 * @since JDK 1.8
 */
public interface DataDesensitization {

	/**
	 * 数据脱敏接口
	 *
	 * @param source 原始数据
	 * @return 脱敏数据
	 */
	String dataDesensitization(String source);

}
