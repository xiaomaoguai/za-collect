package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core;


import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.request.NettyHttpServletRequest;

import java.io.IOException;

/**
 * 分发器，除了传统的forward和include，把正常的Servlet调用也放在这里dispatch()方法
 */
public class NettyRequestDispatcher implements RequestDispatcher {

	private final FilterChain filterChain;

	NettyRequestDispatcher(ServletContext context, FilterChain filterChain) {
		this.filterChain = filterChain;
	}

	@Override
	public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		request.setAttribute(NettyHttpServletRequest.DISPATCHER_TYPE, DispatcherType.FORWARD);
		// TODO implement
	}

	@Override
	public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		request.setAttribute(NettyHttpServletRequest.DISPATCHER_TYPE, DispatcherType.INCLUDE);
		// TODO implement
	}

	//    @continuable
	public void dispatch(ServletRequest request, ServletResponse response) throws ServletException, IOException {
		request.setAttribute(NettyHttpServletRequest.DISPATCHER_TYPE, DispatcherType.ASYNC);
		filterChain.doFilter(request, response);
	}
}
