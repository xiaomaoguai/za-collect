package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock


import com.xiaomaoguai.account.api.response.QueryAccountInfoResponse
import com.xiaomaoguai.account.common.beans.ResultBase
import com.xiaomaoguai.account.dto.ChannelAccountDTO
import com.xiaomaoguai.account.dto.PoliceAccountAuthDTO
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class SCAccountQueryService_queryAccount implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        QueryAccountInfoResponse queryAccountInfoResponse = new QueryAccountInfoResponse()
        queryAccountInfoResponse.setChannelAccountDTO(new ChannelAccountDTO())
        queryAccountInfoResponse.setPoliceAccountAuthDTO(new PoliceAccountAuthDTO())

        return ResultBase.success(queryAccountInfoResponse)
    }

}
