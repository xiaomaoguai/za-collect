package com.xiaomaoguai.fcp.kepeler.riskFiled;

import com.xiaomaoguai.fcp.kepeler.nepturn.HttpClientUtils;
import org.apache.http.HttpException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 17:59
 * @since JDK 1.8
 */
public class RiskFiledReader {

	public static String url ="http://sztest.xiaomaoguai.com/books/kepler_zx_doc/base/fqlzxRiskInfo.html";

	public static void main(String[] args) throws HttpException {
		String html = HttpClientUtils.doGet(url);
		Document document = Jsoup.parse(html, url,Parser.xmlParser());

		Elements ele = document.select("#book-search-results > div.search-noresults > section > table > tbody > tr:nth-child(1) > td:nth-child(2)");

		System.out.println(ele.text());

	}

}
