package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.google.common.collect.Lists
import com.xiaomaoguai.account.api.response.AccountOpenResponse
import com.xiaomaoguai.account.common.beans.ResultBase
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler
import org.apache.commons.lang3.RandomUtils

import java.util.concurrent.atomic.AtomicLong

/**
 *  mock开户接口的数据
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 14:45
 * @since JDK 1.8
 */
class SCPersonAccountService_openAccount implements GlueHandler {

    AtomicLong atomicLong = new AtomicLong(999999)

    @Override
    Object handle(final Map<String, Object> params) {
        AccountOpenResponse accountOpenResponse = new AccountOpenResponse();
        //accountOpenResponse.setCaId(atomicLong.addAndGet(1))
        accountOpenResponse.setCaId(System.currentTimeMillis() + RandomUtils.nextLong())
        accountOpenResponse.setBaId(10086L)
        accountOpenResponse.setPaId(0L)
        accountOpenResponse.setChannelAccountNo("")
        accountOpenResponse.setChannelAccountName("")
        accountOpenResponse.setChannelId(0L)
        accountOpenResponse.setSubChannelId(0L)
        accountOpenResponse.setBizCat("")
        accountOpenResponse.setEacctNo("")
        accountOpenResponse.setAcctValues(0)
        accountOpenResponse.setAuthorityList(Lists.newArrayList())
        return ResultBase.success(accountOpenResponse)
    }

}
