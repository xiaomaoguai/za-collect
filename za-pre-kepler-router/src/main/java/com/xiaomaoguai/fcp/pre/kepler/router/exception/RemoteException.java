package com.xiaomaoguai.fcp.pre.kepler.router.exception;

/**
 * @author huwenxuan
 * @date 2019/1/23
 */
public class RemoteException extends RuntimeException {

    private static final long serialVersionUID = -8701401309305637181L;

    public RemoteException(String message) {
        super(String.format("远程调用异常:%s", message));
    }
    
    public RemoteException(Throwable e) {
        super(e);
    }
}
