package com.dh.convert.lock;

import java.util.concurrent.atomic.AtomicLong;

public class ReadLock {

	private AtomicLong lock = new AtomicLong(0);

	public void readIncrement() {
		lock.getAndIncrement();
	}

	public void readDecrement() {
		lock.getAndDecrement();
	}

	public void getReadLock() {
		while (true) {
			if (lock.weakCompareAndSet(0, 0)) {
				break;
			}
		}
	}

}
