package com.xiaomaoguai.fcp.fcp.controller.r360;

import com.github.xiaomaoguai.xxr.utils.http.HttpClientUtil;
import com.github.xiaomaoguai.xxr.utils.json.JsonUtils;
import com.xiaomaoguai.fcp.fcp.dto.ResponseModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.http.HttpException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @fileName: R360HelperController.java
 * @author: WeiHui
 * @date: 2018/9/20 13:46
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@RestController
@RequestMapping("/r360")
@Api(value = "r360请求走matrix调用")
public class R360HelperController {

	@PostMapping("/info")
	@ApiOperation(value = "r360报文加密")
	public ResponseModel<Object> info(@RequestBody String content) throws Exception {
		log.info("\n===========r360报文开始============\n{}", JsonUtils.formatJson(content));
		Map<String, String> params = new HashMap<>(2);
		String value = JsonUtils.toJsonString(content);
		//String sign = Base64Utils.encode(RSAUtils.generateSHA1withRSASigature(value, CommonKey.privateKey));
		String sign = "ddddd";
		params.put("biz_data", value);
		params.put("sign", sign);
		String reqJson = JsonUtils.toJsonString(params);
		log.info("====>请求数据:  {}", JsonUtils.formatJson(reqJson));
		return new ResponseModel<>(reqJson);
	}

	@PostMapping("/yaCe")
	@ApiOperation(value = "r360压测开始")
	public ResponseModel<Object> yaCe() throws Exception {
		ExecutorService service = Executors.newFixedThreadPool(100);

		Map<String, Object> bizParam = new HashMap<>();
		bizParam.put("md5", "1F439D92D4A051F3DCF44D3E76766798");
		bizParam.put("user_name", "ES未有数据");
		bizParam.put("product_id", 10196193);

		String value = JsonUtils.toJsonString(bizParam);
		//String sign = Base64Utils.encode(RSAUtils.generateSHA1withRSASigature(value, CommonKey.privateKey));
		String sign = "ddddd";
		Map<String, String> params = new HashMap<>(2);
		params.put("biz_data", value);
		params.put("sign", sign);

		String reqJson = JsonUtils.toJsonString(params);
		log.info("====>请求数据:  {}", JsonUtils.formatJson(reqJson));
		int i = 100000;
		while (i > 0) {
			service.execute(() -> {
				try {
					StopWatch watch = StopWatch.createStarted();
					String post = HttpClientUtil.doPost("ddd", reqJson);
					watch.stop();
					log.info("====>请求结果:  {},{}ms", post, watch.getTime(TimeUnit.MILLISECONDS));
				} catch (HttpException e) {
					e.printStackTrace();
				}
			});
		}
		return new ResponseModel<>(true);
	}

}
