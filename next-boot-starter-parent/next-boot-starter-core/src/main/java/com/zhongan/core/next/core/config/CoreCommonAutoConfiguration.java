/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.core.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 核心自动配置
 *
 * @author chenyao
 * @since 2018年5月24日 上午10:55:29
 */
@EnableConfigurationProperties(CoreCommonProperties.class)
@Configuration
public class CoreCommonAutoConfiguration {
    /**
     * 核心自动配置
     */
    public CoreCommonAutoConfiguration() {
    }
}
