package com.xiaomaoguai.fcp.pre.kepler.tddl.sequence;

import com.baomidou.mybatisplus.core.metadata.TableInfo;

/**
 * 序列生成器接口
 *
 * @author Jerry.Chen
 * @since 2018年4月29日 下午12:59:19
 * @see SequenceKey
 * @see SequenceKeyGenerator
 */
public interface Sequence {
	/**
	 * 设置数据库表结构信息，会在生成 {@link SequenceKeyGenerator} 初始化的时候调用
	 *
	 * @param tableInfo 数据库表结构信息
	 */
	default void setTableInfo(TableInfo tableInfo) {
		// do nothing
	}

	/**
	 * 获取数据库表结构信息
	 *
	 * @return 数据库表结构信息
	 */
	default TableInfo getTableInfo() {
		// do nothing
		return null;
	}

	/**
	 * 设置代理的 "序列生成器" class 对象
	 *
	 * @param clazz 序列生成器 class 对象
	 */
	default void setSequenceDelegateClass(Class clazz) {
		// do nothing
	}

	/**
	 * 获取代理的 "序列生成器" class 对象
	 *
	 * @return 序列生成器 class 对象
	 */
	default Class getSequenceDelegateClass() {
		return null;
	}

	/**
	 * 设置代理的 "序列生成器" 对象
	 *
	 * @param delegate 序列生成器
	 */
	default void setSequenceDelegate(Object delegate) {
		// do nothing
	}

	/**
	 * 获取代理的 "序列生成器" 对象
	 *
	 * @param <T> 范型
	 * @return 序列生成器
	 */
	default <T> T getSequenceDelegate() {
		return null;
	}

	/**
	 * 生成下一个序列值
	 *
	 * @param parameter 待保存的实体对象
	 * @return 下一个序列值
	 */
	Object nextValue(Object parameter);
}
