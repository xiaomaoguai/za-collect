package com.xiaomaoguai.fcp.pre.kepler.router.test;

import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.AbstractHandler;

public class CheckHandler extends AbstractHandler {

	@Override
	public void handle(HandlerContext hc) {

		hc.putBuf("checkCode", "0000");
		System.out.println(hc.getBuf().toString() + "checkHandler handle=============");
	}

	@Override
	public void beforeHandle(HandlerContext hc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void genInnerParam(HandlerContext hc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void exceptionCaught(RouterException cause) {
		// TODO Auto-generated method stub

	}

}
