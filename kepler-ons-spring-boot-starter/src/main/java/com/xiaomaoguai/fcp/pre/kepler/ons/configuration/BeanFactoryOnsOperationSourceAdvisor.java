package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;
import org.springframework.lang.Nullable;

/**
 * @fileName: BeanFactoryOnsOperationSourceAdvisor.java
 * @author: WeiHui
 * @date: 2019/2/28 13:53
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class BeanFactoryOnsOperationSourceAdvisor extends AbstractBeanFactoryPointcutAdvisor {

	private static final long serialVersionUID = -6425959716187758559L;

	@Nullable
	private OnsOperationSource onsOperationSource;

	private final OnsOperationSourcePointcut pointcut = new OnsOperationSourcePointcut() {

		private static final long serialVersionUID = 4706096973664908537L;

		@Override
		@Nullable
		protected OnsOperationSource getOnsOperationSource() {
			return onsOperationSource;
		}
	};

	/**
	 * 得到切入点
	 *
	 * @return 切入点
	 */
	@Override
	public Pointcut getPointcut() {
		return this.pointcut;
	}
}
