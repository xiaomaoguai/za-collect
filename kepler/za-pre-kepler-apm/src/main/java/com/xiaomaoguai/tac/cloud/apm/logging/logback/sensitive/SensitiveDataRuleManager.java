package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive;

import com.xiaomaoguai.tac.cloud.apm.logging.logback.SensitiveDataRule;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 15:07
 * @since JDK 1.8
 */
public class SensitiveDataRuleManager {

	/**
	 * 脱敏规则集合
	 */
	public static Set<SensitiveDataRule> dataRules = new CopyOnWriteArraySet<>();

}
