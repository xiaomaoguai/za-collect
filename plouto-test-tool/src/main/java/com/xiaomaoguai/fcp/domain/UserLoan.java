/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class UserLoan extends CreditApply {

	private static final long serialVersionUID = -6290917389103480837L;

	/**
	 * 授信申请号
	 */
	private String creditNo;

	/**
	 * 借款申请号
	 */
	private String loanOuterNo;

	/**
	 * 信贷核心借款流水号
	 */
	private String loanInnerNo;

	/**
	 * 资金方编码
	 */
	private String fundCode;

	/**
	 * 借款金额
	 */
	private BigDecimal loanAmount;

	/**
	 *
	 */
	private BigDecimal installmentRate;

	/**
	 * 期数
	 */
	private Integer installmentNo;

	/**
	 * 还款方式 0等额本息
	 */
	private Byte repayWay;

	/**
	 * 借款用途
	 */
	private int loanPupose;

	/**
	 * 放款卡信息
	 */
	private String loanCardInfo;

	/**
	 * 借款时间
	 */
	private Date loanDate;

	/**
	 * 放款时间
	 */
	private Date withdrawDate;

	/**
	 * 失败描述信息
	 */
	private String errorMsg;

	/**
	 * 结清状态 0未结清 1已结清
	 */
	private Byte settleStatus;

	/**
	 * r360借款请求
	 */
	private Integer loanTerm;

	private Integer termUnit;

	//private String verifyCode;

	/**
	 * 融360借款应答
	 */
	private Integer isNeedVerify;

	/**
	 * 是否会发送验证码
	 */
	private Integer needVerifyCode;

	/**
	 * 借款响应
	 */
	private Integer loanStatus;

	private BigDecimal remainAmount;
	
}