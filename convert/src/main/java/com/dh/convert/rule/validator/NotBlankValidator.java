package com.dh.convert.rule.validator;

import org.apache.commons.lang3.StringUtils;

@Validator("NotBlank")
public class NotBlankValidator extends AbstractValidator<String> {

	@Override
	public void initialize() {
	}

	@Override
	public boolean isValid(String value) {
		return StringUtils.isNoneBlank(value);
	}
	

}
