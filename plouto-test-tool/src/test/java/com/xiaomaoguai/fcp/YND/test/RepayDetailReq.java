package com.xiaomaoguai.fcp.YND.test;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/23 15:05
 * @since JDK 1.8
 */
@Getter
@Setter
public class RepayDetailReq {

	/**
	 * 第几期
	 */
	@NotNull(message = "借款期数不能为空")
	private Integer installmentNo;

	/**
	 * 当期标识
	 */
	@NotNull(message = "当期标识不能为空")
	@Range(min = 0, max = 1, message = "当期标识参数错误，必须0-1")
	private Integer currentFlag;

	/**
	 * 实还本金
	 */
	@NotNull(message = "实还本金不能为空")
	@Digits(integer = 15, fraction = 2, message = "实还本金精度错误")
	private BigDecimal paidPrincipal;

	/**
	 * 实还利息
	 */
	@NotNull(message = "实还利息不能为空")
	@Digits(integer = 15, fraction = 2, message = "实还利息精度错误")
	private BigDecimal paidInterest;

	/**
	 * 实还服务费
	 */
	@NotNull(message = "实还服务费不能为空")
	@Digits(integer = 15, fraction = 2, message = "实还服务费精度错误")
	private BigDecimal paidFee;

	/**
	 * 用户实际还款日
	 */
	@NotBlank(message = "用户实际还款日不能为空")
	private String actualRepayDate;

	/**
	 * 还款类型 0-提前还款 1-正常还款 2-逾期还款
	 */
	@NotNull(message = "还款类型不能为空")
	@Range(min = 0, max = 2, message = "还款类型参数错误，必须0-2")
	private Integer repayType;

}

