package com.xiaomaoguai.fcp.pre.kepler.router.rpc.client.okhttp;

import java.io.Serializable;
import java.util.Map;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums.HttpMethod;

/**
 * request封装类
 *
 */
public class Request implements Serializable {
    private static final long serialVersionUID = -2585065451136206831L;
    private final HttpMethod method;
    private final Map<String,String> headers;
    private final String url;

    Request(RequestBuilder builder) {
        method = builder.method;
        headers = builder.headers;
        this.url = builder.url;
    }

    /**
     * 创建bulider类
     * 
     */
    public RequestBuilder toBuilder() {
        return new RequestBuilder(this);
    }

  

    public HttpMethod method() {
        return method;
    }

    public Map<String,String>  headers() {
        return headers;
    }


    public String url() {
        return url;
    }

}
