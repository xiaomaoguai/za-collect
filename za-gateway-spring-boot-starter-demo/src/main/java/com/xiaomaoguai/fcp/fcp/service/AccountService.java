package com.xiaomaoguai.fcp.fcp.service;

import com.github.xiaomaoguai.xxr.autoconfigure.ZaClient;
import com.github.xiaomaoguai.xxr.autoconfigure.ZaPath;
import com.github.xiaomaoguai.xxr.dto.req.BizReq;
import com.github.xiaomaoguai.xxr.dto.resp.ResultBase;
import com.xiaomaoguai.fcp.fcp.dto.AccessResp;
import com.xiaomaoguai.fcp.fcp.dto.BankList;
import com.xiaomaoguai.fcp.fcp.dto.req.AuthNameReq;
import com.xiaomaoguai.fcp.fcp.dto.req.BindCardReq;

/**
 * @fileName: AccountService.java
 * @author: WeiHui
 * @date: 2018/9/12 16:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
@ZaClient
public interface AccountService {

	@ZaPath("/accountApi/authName")
	ResultBase<BankList> authName(BizReq bizReq);

	@ZaPath("/accountApi/queryBankList")
	ResultBase<BankList> queryBankList(BizReq bizReq);

	@ZaPath("/accountApi/authName")
	ResultBase<AccessResp> auth(AuthNameReq bizReq);

	@ZaPath("/accountApi/bindCard")
	ResultBase<Object> bindCard(BindCardReq bizReq);

}
