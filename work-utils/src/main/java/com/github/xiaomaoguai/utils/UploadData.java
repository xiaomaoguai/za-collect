package com.github.xiaomaoguai.utils;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/26 16:21
 * @since JDK 1.8
 */
@Data
public class UploadData {

	private String name;

	private String thirdUserNo;

	private String loanOuterNo;

	private String platform;

	private String num;

	private String curNo;

	private String actualRepayDate;

	private BigDecimal principal;

	private BigDecimal interest;

	private BigDecimal amount;

}
