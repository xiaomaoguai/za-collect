//package org.apache.skywalking.apm.plugin.kepler;
//
//import org.apache.skywalking.apm.agent.core.context.ContextManager;
//import org.apache.skywalking.apm.agent.core.context.ContextSnapshot;
//import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.EnhancedInstance;
//import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.InstanceMethodsAroundInterceptor;
//import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.MethodInterceptResult;
//
//import java.lang.reflect.Method;
//
///**
// * @author WeiHui-Z
// * @version v1.0.0
// * @date 2019/6/5 22:15
// * @since JDK 1.8
// */
//public class WorkEventInvokeInterceptor implements InstanceMethodsAroundInterceptor {
//
//	@Override
//	public void beforeMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
//							 MethodInterceptResult result) throws Throwable {
//		ContextManager.createLocalSpan("Disruptor/" + objInst.getClass().getName() + "/" + method.getName());
//		ContextSnapshot cachedObjects = (ContextSnapshot) objInst.getSkyWalkingDynamicField();
//		if (cachedObjects != null) {
//			ContextManager.continued(cachedObjects);
//		}
//	}
//
//	@Override
//	public Object afterMethod(EnhancedInstance objInst, Method method, Object[] allArguments, Class<?>[] argumentsTypes,
//							  Object ret) throws Throwable {
//		ContextManager.stopSpan();
//		// clear ContextSnapshot
//		objInst.setSkyWalkingDynamicField(null);
//		return ret;
//	}
//
//	@Override
//	public void handleMethodException(EnhancedInstance objInst, Method method, Object[] allArguments,
//									  Class<?>[] argumentsTypes, Throwable t) {
//		ContextManager.activeSpan().errorOccurred().log(t);
//	}
//
//}
