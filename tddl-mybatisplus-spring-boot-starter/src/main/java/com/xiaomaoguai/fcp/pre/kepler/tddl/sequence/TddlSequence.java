package com.xiaomaoguai.fcp.pre.kepler.tddl.sequence;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.taobao.tddl.client.sequence.exception.SequenceException;
import com.taobao.tddl.client.sequence.impl.GroupSequence;
import com.xiaomaoguai.fcp.pre.kepler.tddl.exception.TddlRuntimeException;
import lombok.Data;

/**
 * 类TddlSequence的实现描述：TDDL 序列生成器
 *
 * @author chenyao
 * @since 2018年5月15日 下午6:52:35
 */
@Data
public class TddlSequence implements Sequence {

	private TableInfo tableInfo;

	private GroupSequence sequence;

	@Override
	public Object nextValue(Object parameter) {
		try {
			return this.sequence.nextValue();
		} catch (SequenceException ex) {
			throw new TddlRuntimeException("get TDDL sequence nextValue failed, sequenceName = "
					+ this.sequence.getName() + ", tableName = " + this.tableInfo.getTableName(), ex);
		}
	}

}
