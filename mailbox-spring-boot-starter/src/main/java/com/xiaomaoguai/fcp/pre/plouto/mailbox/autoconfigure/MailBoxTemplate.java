package com.xiaomaoguai.fcp.pre.plouto.mailbox.autoconfigure;

import com.dh.mailbox2.spi.Mailboxs;
import com.dh.mailbox2.wrapper.MSG;
import com.xiaomaoguai.fcp.pre.plouto.mailbox.IMailBox;
import com.xiaomaoguai.fcp.pre.plouto.mailbox.actor.AsyncData;
import com.xiaomaoguai.fcp.pre.plouto.mailbox.actor.AsyncRunnable;
import lombok.experimental.Delegate;
import org.springframework.util.Assert;

import javax.annotation.Resource;

/**
 * 模板方法
 *
 * @fileName: MailBoxTemplate.java
 * @author: WeiHui
 * @date: 2018/11/5 20:54
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class MailBoxTemplate {

	/**
	 * inject
	 */
	@Resource
	@Delegate
	private Mailboxs mailboxs;

	public boolean call(IMailBox mailBox, MSG msg) {
		Assert.isTrue(msg != null && msg.getData() != null, "MSG的数据不能为空");
		String actorName = mailBox.getClazz().getSimpleName();
		return call(actorName, msg);
	}

	public boolean call(IMailBox mailBox, Object data) {
		Assert.isTrue(data != null, "MSG的数据不能为空");
		String actorName = mailBox.getClazz().getSimpleName();
		return call(actorName, null, data);
	}

	public boolean call(IMailBox mailBox, String subject, Object data) {
		Assert.isTrue(data != null, "MSG的数据不能为空");
		String actorName = mailBox.getClazz().getSimpleName();
		return call(actorName, subject, data);
	}

	public boolean call(String mailBoxName, String subject, Object data) {
		Assert.isTrue(data != null, "MSG的数据不能为空");
		MSG msg = new MSG(data);
		if (subject != null) {
			msg.setSubject(subject);
		}
		return call(mailBoxName, msg);
	}

	public boolean call(String mailBoxName, Object data) {
		Assert.isTrue(data != null, "MSG的数据不能为空");
		return call(mailBoxName, null, data);
	}

	public boolean callAsync(Object data, AsyncRunnable asyncRunnable) {
		AsyncData asyncData = new AsyncData();
		asyncData.setAsyncRunnable(asyncRunnable);
		asyncData.setParam(data);
		return call("AsyncActor", asyncData);
	}

}
