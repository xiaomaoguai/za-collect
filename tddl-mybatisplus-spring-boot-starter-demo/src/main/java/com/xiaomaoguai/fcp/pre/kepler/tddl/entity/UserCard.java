/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.fcp.pre.kepler.tddl.entity;


import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * This class corresponds to the database table user_card_info
 * <p>
 * Database Table Remarks: 用户卡信息表
 * </p>
 *
 * @author chenwenjing001
 * @since 2019年1月11日 下午3:47:09
 */
@ToString(callSuper = true)
@Accessors(chain = true)
@Data
@KeySequence("SEQ_USER")
@TableName("user_card")
public class UserCard {

	private static final long serialVersionUID = 1L;

	/**
	 * Database Column Name: user_card_info.product_code
	 * <p>
	 * Database Column Remarks: 产品编码
	 * </p>
	 */
	private String productCode;

	/**
	 * Database Column Name: user_card_info.user_id
	 * <p>
	 * Database Column Remarks: 众安用户编号
	 * </p>
	 */
	private Long userId;

	/**
	 * Database Column Name: user_card_info.third_user_no
	 * <p>
	 * Database Column Remarks: 合作方用户账号
	 * </p>
	 */
	private String thirdUserNo;

	/**
	 * Database Column Name: user_card_info.bank_name
	 * <p>
	 * Database Column Remarks: 发卡行名称
	 * </p>
	 */
	private String bankName;

	/**
	 * Database Column Name: user_card_info.card_name
	 * <p>
	 * Database Column Remarks: 户名
	 * </p>
	 */
	private String cardName;

	/**
	 * Database Column Name: user_card_info.cert_no
	 * <p>
	 * Database Column Remarks: 身份证
	 * </p>
	 */
	private String certNo;

	/**
	 * Database Column Name: user_card_info.bank_card_no
	 * <p>
	 * Database Column Remarks: 银行卡号
	 * </p>
	 */
	private String bankCardNo;

	/**
	 * Database Column Name: user_card_info.card_phone
	 * <p>
	 * Database Column Remarks: 预留手机号
	 * </p>
	 */
	private String cardPhone;

	/**
	 * Database Column Name: user_card_info.card_type
	 * <p>
	 * Database Column Remarks: 卡类型 DEBIT_CARD-借记卡 CREDIT_CARD-贷记卡
	 * SEMI_CREDIT_CARD-准贷记卡
	 * </p>
	 */
	private String cardType;

	/**
	 * Database Column Name: user_card_info.card_use
	 * <p>
	 * Database Column Remarks: 卡用途：WITHDRAW-放款 REPAY-还款
	 * WITHDRAW_AND_REPAY-放款还款卡
	 * </p>
	 */
	private String cardUse;

	/**
	 * Database Column Name: user_card_info.bank_code
	 * <p>
	 * Database Column Remarks: 发卡行编码
	 * </p>
	 */
	private String bankCode;

	/**
	 * Database Column Name: user_card_info.card_status
	 * <p>
	 * Database Column Remarks: 绑卡状态，BIND-绑定，UNBIND-解绑
	 * </p>
	 */
	private String cardStatus;

	/**
	 * Database Column Name: user_card_info.is_default
	 * <p>
	 * Database Column Remarks: 是否是默认卡，N-否，Y-是
	 * </p>
	 */
	private String isDefault;

	/**
	 * Database Column Name: user_card_info.merchant_no
	 * <p>
	 * Database Column Remarks:
	 * </p>
	 */

	private String merchantNo;


	private String agreementInfo;


	private String agreementChannelId;


	/**
	 * Database Column Name: creator
	 * <p>
	 * Database Column Remarks: 创建者
	 * </p>
	 */
	private String creator;

	/**
	 * Database Column Name: modifier
	 * <p>
	 * Database Column Remarks: 修改者
	 * </p>
	 */
	private String modifier;

	/**
	 * Database Column Name: gmt_created
	 * <p>
	 * Database Column Remarks: 创建时间
	 * </p>
	 */
	private Date gmtCreated;

	/**
	 * Database Column Name: gmt_modified
	 * <p>
	 * Database Column Remarks: 修改时间
	 * </p>
	 */
	private Date gmtModified;

	private String isDeleted;
}
