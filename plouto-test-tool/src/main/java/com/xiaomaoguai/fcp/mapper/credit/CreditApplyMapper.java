package com.xiaomaoguai.fcp.mapper.credit;

import com.xiaomaoguai.fcp.domain.CreditApply;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * The Interface CreditApplyMapper.
 */
public interface CreditApplyMapper {

	/**
	 * Insert selective.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(CreditApply record);

	/**
	 * Select by user id.
	 *
	 * @param userId        the user id
	 * @param creditApplyNo the credit apply no
	 * @param applyStatus   the apply status
	 * @return the list
	 */
	List<CreditApply> selectByUserId(@Param("userId") Long userId
			, @Param("creditApplyNo") String creditApplyNo
			, @Param("applyStatus") Integer applyStatus
	);

	/**
	 * Select list.
	 *
	 * @param userId the user id
	 * @return the list
	 */
	List<CreditApply> selectList(Long userId);

	/**
	 * Update by credit no.
	 *
	 * @param creditApply the credit apply
	 * @param applyStatus the apply status
	 * @return the int
	 */
	int updateByCreditNo(@Param("creditApply") CreditApply creditApply, @Param("applyStatus") Integer applyStatus);

	/**
	 * Update active status by credit no.
	 *
	 * @param userId        the user id
	 * @param creditApplyNo the credit apply no
	 * @param activeStatus  the active status
	 * @return the int
	 */
	int updateActiveStatusByCreditNo(@Param("userId") Long userId, @Param("creditApplyNo") String creditApplyNo, @Param("activeStatus") Integer activeStatus);

	/**
	 * Update active status by credit contract no.
	 *
	 * @param userId           the user id
	 * @param creditContractNo the credit contract no
	 * @param activeStatus     the active status
	 * @return the int
	 */
	int updateActiveStatusByCreditContractNo(@Param("userId") Long userId, @Param("creditContractNo") String creditContractNo, @Param("activeStatus") Integer activeStatus);


	List<CreditApply> selectListByApplyNo(@Param("userId") Long userId, @Param("creditApplyNo") String applyNo);

	/**
	 * select CreditApply for ImageTransferActor
	 *
	 * @param userId
	 * @return CreditApply
	 */
	CreditApply selectCreditApplyByUserId(Long userId);

	int deleteByUserId(Long userId);
}