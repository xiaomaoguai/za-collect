package com.xiaomaoguai.fcp.controller;

import com.google.common.collect.Lists;
import com.xiaomaoguai.fcp.dao.MatrixRequestLogRepository;
import com.xiaomaoguai.fcp.dto.MatrixRequestLog;
import com.xiaomaoguai.fcp.service.MatrixRequestLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * @fileName: EsController.java
 * @author: WeiHui
 * @date: 2018/10/22 19:38
 * @version: v1.0.0
 * @since JDK 1.8
 */
@RestController
@RequestMapping("/es")
@Api(value = "请求查询")
public class EsController implements MatrixRequestLogService {

	@Resource
	private MatrixRequestLogRepository matrixRequestLogRepository;

	@Override
	public List<MatrixRequestLog> findByThirdUserNo(String thirdUserNo) {
		return null;
	}

	@Override
	@PostMapping("findByString")
	@ApiOperation(value = "根据【关键字】查询")
	public List<MatrixRequestLog> findByString(@RequestBody String query) {
		QueryStringQueryBuilder queryBuilder = new QueryStringQueryBuilder(query);
		PageRequest pageRequest = PageRequest.of(0, 10);
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
				.withQuery(queryBuilder)
				.withPageable(pageRequest)
				.withSort(new FieldSortBuilder("gmt_created").order(SortOrder.DESC))
				.build();
		Iterable<MatrixRequestLog> searchResult = matrixRequestLogRepository.search(searchQuery);
		return Lists.newArrayList(searchResult);
	}

}
