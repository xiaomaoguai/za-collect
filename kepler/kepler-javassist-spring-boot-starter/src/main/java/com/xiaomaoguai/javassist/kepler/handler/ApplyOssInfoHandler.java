package com.xiaomaoguai.javassist.kepler.handler;


import com.xiaomaoguai.javassist.kepler.config.HandlerAnnotation;
import com.xiaomaoguai.javassist.kepler.dto.StandardBaseDTO;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.AbstractHandler;

/**
 * 获取STS信息
 *
 * @author huwenxuan
 * @date 2019/01/01
 */
@HandlerAnnotation(desc = "获取STS信息", author = "JohnWho")
public class ApplyOssInfoHandler extends AbstractHandler<StandardBaseDTO> {

	@Override
	public void handle(HandlerContext<StandardBaseDTO> hc) {
		StandardBaseDTO standardBaseDTO = hc.getInnerParam();
	}

}
