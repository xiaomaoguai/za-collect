package com.xiaomaoguai.fcp.pre.kepler.redis.listener.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/4 15:23
 * @since JDK 1.8
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RedisListener {

	/**
	 * 监听 key
	 *
	 * @return redis监听key
	 */
	String value();

}
