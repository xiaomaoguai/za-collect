package com.xiaomaoguai.boot.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaoguai.boot.entity.AccountApply;
import com.xiaomaoguai.boot.mapper.AccountApplyMapper;
import com.xiaomaoguai.boot.service.AccountApplyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 调账申请表 服务实现类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Service
public class AccountApplyServiceImpl extends ServiceImpl<AccountApplyMapper, AccountApply> implements AccountApplyService {

}
