package com.xiaomaoguai.fcp.common;

/**
 * @fileName: EsDocument.java
 * @author: WeiHui
 * @date: 2018/10/22 20:40
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class EsDocument {

	public static final String  INDEX = "fcp_fe_szbiz_pluto";

	public static final String  TYPE = "matrix_request_log";

}
