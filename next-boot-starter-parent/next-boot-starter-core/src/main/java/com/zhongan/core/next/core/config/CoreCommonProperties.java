/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.core.config;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 核心自动配置属性
 * <p>
 * 配置文件信息请查看下面3个文件，可以在项目中的 application.yml 中进行覆盖:
 * <ul>
 * <li>core-config/application.yml
 * <li>core-config/application-pre.yml
 * <li>core-config/application-prd.yml
 * </ul>
 *
 * @author chenyao
 * @since 2018年5月24日 上午10:57:38
 * @see app.myoss.cloud.core.spring.boot.config.CoreCommonEnvironmentPostProcessor
 */
@Data
@ConfigurationProperties(prefix = CoreConstants.CONFIG_PREFIX)
public class CoreCommonProperties {
    /**
     * 网关平台服务配置
     */
    private NextGateway nextGateway;

    /**
     * 网关平台服务配置
     */
    @Data
    public static class NextGateway {
        /**
         * 网关内网地址，推荐在非 boom3 中使用
         */
        private String serverUrl;
        /**
         * 网关 kubernetes 服务地址，推荐在 boom3 中使用，性能最好
         */
        private String svcServerUrl;
    }

}
