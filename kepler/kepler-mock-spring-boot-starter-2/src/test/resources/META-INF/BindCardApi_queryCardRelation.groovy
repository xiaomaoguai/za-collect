package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.creditcore.preloan.dto.BankCardDTO
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler
/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class BindCardApi_queryCardRelation implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        com.xiaomaoguai.creditcore.common.dto.BaseResp<BankCardDTO>  baseResp = new BaseResp("911")

        BankCardDTO bankCardDTO = new BankCardDTO()
        bankCardDTO.setCardNo("mock123456")
        bankCardDTO.setBankCode("mock123456")
        bankCardDTO.setBankName("大辉银行")
        bankCardDTO.setCertiNo("mock123456")
        bankCardDTO.setCertiName("mock123456")
        bankCardDTO.setPreStayPhone("mock123456")
        bankCardDTO.setPayChannelId("mock123456")

        baseResp.setResultCode(ResultCode.SUCCESS)
        baseResp.setResult(bankCardDTO)
        return baseResp
    }

}
