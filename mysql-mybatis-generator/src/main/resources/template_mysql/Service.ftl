/*
 * 文件名：I${module}Service.java
 * 版本信息:v1.0.0
 * 日期：${createdTime} 
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */ 
package com.billionsfinance.crs.service.${moduleType};

import java.util.List;
import com.billionsfinance.crs.request.PageRequest;
import com.billionsfinance.crs.model.PageResult;
import com.billionsfinance.crs.model.ResponseModel;

import com.billionsfinance.crs.model.${moduleType}.${module};


/**
* @ FileName: I${module}Service.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
public interface I${module}Service {

	/**
	 * 查询${modelName}列表
	 * @param model  参数
	 * @return List  结果
	 * @throws Exception 异常 
	 */
	List<${module}> find${module}List(${module} model) throws Exception;
	
	/**
	 * 分页查询
	 * @param ${lowModule}  参数
	 * @param pageV0       参数
	 * @return PageResult  参数
	 * @throws Exception 异常  
	 */
	PageResult<${module}> select${module}Set(${module} ${lowModule}, PageRequest pageV0) throws Exception;

    /**
    *新增或修改${moduleName}
    *@param ${lowModule}   参数
    *@return int 受影响的行数 
    *@throws Exception 异常  
    */
     int  createOrUpdate${module}(${module} ${lowModule}) throws Exception;
     
    /**
    *新增或修改${moduleName}
    *@param ${lowModule}  参数
    *@return ResponseModel 受影响的行数和处理过后的Module
    *@throws Exception 异常 
    */
    ResponseModel createOrUpdate(${module} ${lowModule}) throws Exception;
    
    /**
    *根据ID查询${moduleName}
    *@param uuId 要查询的对象的ID
    *@return ${module}  参数
    *@throws Exception   异常
    */
    ${module} read${module}ById(String uuId) throws Exception;
    
    /**
    *逻辑删除${moduleName}
    *@param uuId  参数
    *@return int 受影响条数
    *@throws Exception 异常  
    */
    int delete${module}ById(String uuId) throws Exception;
    
    /**
    *物理删除${moduleName}
    *@param uuId 参数
    *@return int 受影响条数
    *@throws Exception 异常   异常
    */
    int physicalDelete${module}ById(String uuId) throws Exception;
    
}
