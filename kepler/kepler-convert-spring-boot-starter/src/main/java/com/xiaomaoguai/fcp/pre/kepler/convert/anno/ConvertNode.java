package com.xiaomaoguai.fcp.pre.kepler.convert.anno;

import java.lang.annotation.*;

/**
 * @fileName: ConvertNode.java
 * @author: WeiHui
 * @date: 2018/11/24 22:00
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConvertNode {

	String value();
}
