/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.hsf.excetion;

/**
 * HSF业务异常信息
 *
 * @author chenyao
 * @since 2016年9月7日 上午11:48:31
 */
public class HsfBizException extends RuntimeException {
    private static final long serialVersionUID = -6166048944310830745L;

    /**
     * HSF业务异常信息
     */
    public HsfBizException() {
        super();
    }

    /**
     * HSF业务异常信息
     *
     * @param message 错误信息
     */
    public HsfBizException(String message) {
        super(message);
    }

    /**
     * HSF业务异常信息
     *
     * @param message 错误信息
     * @param cause 异常信息
     */
    public HsfBizException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * HSF业务异常信息
     *
     * @param cause 异常信息
     */
    public HsfBizException(Throwable cause) {
        super(cause);
    }
}
