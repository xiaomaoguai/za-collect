/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.kafka.config;

import org.springframework.context.annotation.Configuration;

/**
 * Kafka自动配置
 *
 * @author chenyao
 * @since 2019年6月13日 上午10:31:29
 */
@Configuration
public class KafkaAutoConfiguration {
}
