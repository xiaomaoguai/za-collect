package com.dh.convert.rule.function;

import java.math.BigDecimal;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.util.TypeUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorString;

/**
 * 
 * 类Yuan2FenFunction.java的实现描述：yuan2fen
 * 
 * @author chenwenjing001 2018年11月20日 下午1:47:53
 */
public class Yuan2FenFunction extends AbstractFunction {

	@Override
	public String getName() {
		return "yuan2fen";
	}

	@Override
	public AviatorString call(Map<String, Object> env, AviatorObject arg1) {
		String amount = TypeUtils.castToString(arg1.getValue(env));
		if (StringUtils.isBlank(amount)) {
			return new AviatorString(BigDecimal.ZERO.toString());
		}
		BigDecimal v = new BigDecimal(amount);
		return new AviatorString(v.movePointRight(2).setScale(0, 1).toString());
	}
}
