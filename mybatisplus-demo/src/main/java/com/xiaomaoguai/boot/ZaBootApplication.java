package com.xiaomaoguai.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class ZaBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZaBootApplication.class, args);
        System.err.println("http://localhost:8080/info");
        System.err.println("http://localhost:8080/accountApply/id");
        System.err.println("http://localhost:8080/batchTask/id");
        System.err.println("http://localhost:8080/creditApply/id");
    }
}
