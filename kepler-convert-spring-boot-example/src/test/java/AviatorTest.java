import com.googlecode.aviator.AviatorEvaluator;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 14:51
 * @since JDK 1.8
 */
public class AviatorTest {

	@Test
	public void test() {
		Map<String, Object> env = new HashMap<String, Object>();
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(3);
		list.add(20);
		list.add(10);
		env.put("list", list);

		env.put("withdrawDate",new Date());
		env.put("applyDate", DateUtils.addDays(new Date(),1));

		Object resultBool = AviatorEvaluator.execute("withdrawDate <= applyDate", env);
		System.out.println(resultBool);


		Object result = AviatorEvaluator.execute("count(list)", env);
		System.out.println(result);  // 3
		result = AviatorEvaluator.execute("reduce(list,+,0)", env);
		System.out.println(result);  // 33
	}

}
