package com.xiaomaoguai.fcp.pre.kepler.convert.core.support;

import com.dh.convert.mapping.BeanMappingObject;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author huwenxuan
 * @date 2018/12/27
 */
@Data
public class ConvertZkNode implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = -4993443471484553359L;

	/**
	 * targetNode节点值
	 */
	private String targetObject;

	/**
	 * beanMapping节点内容
	 */
	private List<BeanMappingObject> beanMapping;

}
