package com.xiaomaoguai.fcp.pre.kepler.trace.interceptor;

import com.xiaomaoguai.fcp.pre.kepler.trace.exception.JaegerException;
import io.opentracing.Span;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/8 20:17
 * @since JDK 1.8
 */
public interface HandlerAroundInterceptor {

	/**
	 * 检查是否匹配
	 *
	 * @param pjp 切面点
	 * @return true-匹配 false-匹配下一个
	 */
	boolean accept(ProceedingJoinPoint pjp);

	/**
	 * 获取该span的名称
	 *
	 * @param pjp 切面点
	 * @return span的名称
	 */
	String getOperationName(ProceedingJoinPoint pjp);

	/**
	 * 前搜集span信息
	 *
	 * @param pjp  切面点
	 * @param span jaeger span
	 * @throws com.xiaomaoguai.fcp.pre.kepler.trace.exception.JaegerException 自定义jaeger异常
	 */
	void beforeMethod(ProceedingJoinPoint pjp, Span span) throws JaegerException;

	/**
	 * 后搜集span信息
	 *
	 * @param pjp  切面点
	 * @param span jaeger span
	 * @throws JaegerException 自定义jaeger异常
	 */
	void afterMethod(ProceedingJoinPoint pjp, Span span) throws JaegerException;


}
