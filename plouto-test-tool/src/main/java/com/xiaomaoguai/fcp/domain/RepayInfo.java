package com.xiaomaoguai.fcp.domain;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 还款信息
 */
@Getter
@Setter
public class RepayInfo extends UserLoan {

	private static final long serialVersionUID = -3059249938229264754L;

	/**
	 * 还款请求时间
	 */
	private Date repayReqDate;

	/**
	 * 还款外部流水号
	 */
	private String repayOuterNo;

	/**
	 * 还款内部流水号
	 */
	private String repayInnerNo;

	/**
	 * 还款金额
	 */
	private BigDecimal repayAmount;

	/**
	 * 还款期数
	 */
	private List<Integer> installmentNos;

	/**
	 * 还款期数字符串
	 */
	private String installNoString;

	/**
	 * 实际还款日
	 */
	private String actualRepayDate;

	/**
	 * 还款方式 2-主动还款 3-系统代扣
	 */
	private Integer repayMode;

	/**
	 * 是否提前结清 0-否 1-是
	 */
	private Integer isEarlySettle;

	/**
	 * 还款状态
	 */
	private Integer repayStatus;

	/**
	 * 还款返回码
	 */
	private String repayCode;

	/**
	 * 还款返回信息
	 */
	private String respMsg;

	/**
	 * 查询模式
	 * false-非查询模式
	 * true-查询模式
	 */
	private Boolean queryMode = Boolean.FALSE;

	/**
	 * 不进行异常抛出操作，只记录信息
	 * 0-关闭（默认）
	 * 1-开启
	 */
	private Integer kindMode = 0;

	/**
	 * 1-校验跳期
	 * 0-不校验跳期
	 */
	private Integer checkIns = 1;

	/**
	 * FALSE-不幂等已还
	 * TRUE-幂等已还
	 */
	private boolean idemFlag = Boolean.FALSE;

	/**
	 * 当前请求还款期数
	 * 二进制--》十进制
	 */
	private Integer currentNum;

	/**
	 * 已还账单期数
	 * 二进制--》十进制
	 */
	private Integer paidNum;

	private Date repayCompleteDate;

	/**
	 * 未还账单期数
	 * 二进制--》十进制
	 */
	private Integer unpaidNum;

	/**
	 * 还款计划Map
	 */
	private Map<Integer, RepayPlan> repayPlanMap;

	/**
	 * 预还款账单
	 */
	private List<BaseRepayPlan> tryRepayPlanList;

	/**
	 * 整笔借款单是否可还款
	 * 0-整笔不可
	 * 1-不受限制
	 */
	private Integer repayAvailable = 1;


	private BigDecimal totalPrin;


	private BigDecimal totalInterest;


	private BigDecimal totalCharge;


	private BigDecimal totalPenalty;

	private BigDecimal totalReduce;

	/**
	 * 是否计算剩余期数的相关金额
	 * true-需要计算
	 * false-默认不需要计算
	 */
	private Boolean calRemainModel = Boolean.FALSE;

	/**
	 * 剩余期应还总金额（不包含当前试算传入期数）
	 */
	private BigDecimal remainRepayAmount;

	/**
	 * 剩余期应还本金（不包含当前试算传入期数）
	 */
	private BigDecimal remainTotalPrin;

	/**
	 * 剩余期应还利息（不包含当前试算传入期数）
	 */
	private BigDecimal remainTotalInterest;

	/**
	 * 剩余期应还服务费（不包含当前试算传入期数）
	 */
	private BigDecimal remainTotalCharge;

	/**
	 * 剩余期应还罚息（不包含当前试算传入期数）
	 */
	private BigDecimal remainTotalPenalty;

	/**
	 * 剩余期减免金（不包含当前试算传入期数）
	 */
	private BigDecimal remainTotalReduce;

	/**
	 * 是否需要代扣 ,默认为true
	 */
	private Boolean needWithhold = Boolean.TRUE;

	public String InstallmentNos2String() {
		this.installNoString = Joiner.on(",").skipNulls().join(installmentNos);
		return this.installNoString;
	}

	public List<Integer> String2InstallmentNos(String installmentNoString) {
		this.installmentNos = Lists.transform(Splitter.on(",").trimResults().splitToList(installmentNoString),
				new Function<String, Integer>() {

					@Override
					public Integer apply(String input) {
						return Integer.valueOf(input);
					}
				});
		return installmentNos;
	}

	/**
	 * 预还款期数是否已还校验
	 *
	 * @return
	 */
	public boolean checkIsNotPaid() {
		return (paidNum & currentNum) == 0;
	}

	/**
	 * 已还幂等
	 */
	public boolean idemPaid() {
		return idemFlag && (paidNum | currentNum) == paidNum;
	}

	/**
	 * 预还款期数超出未还账单期数范围校验
	 */
	public boolean checkIsNotOver() {
		return (unpaidNum | currentNum) == unpaidNum;
	}



}