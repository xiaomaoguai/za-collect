/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.nacos.constants;

/**
 * Nacos服务发现常量
 *
 * @author chenyao
 * @since 2019年6月20日 上午9:44:32
 */
public class NacosDiscoveryConstants {
    /**
     * 环境变量 SERVICE_TAGS
     */
    public static final String ENV_SERVICE_TAGS    = "SERVICE_TAGS";

    /**
     * Nacos服务元数据 SERVICE_DOMAIN
     */
    public static final String META_SERVICE_DOMAIN = "SERVICE_DOMAIN";
}
