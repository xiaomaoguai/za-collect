package com.xiaomaoguai.fcp.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.xiaomaoguai.fcp.utils.Decimal2StringSerialize;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class CreditApply extends UserAccount {

	private static final long serialVersionUID = -2957120352504815597L;

	private String reqNo;

    /**
     * 申请流水号
     */
    private String creditApplyNo;

    /**
     * 申请日期
     */
    private Date applyDate;

    /**
     * 风控扩展信息JSON格式
     */
    private String riskInfo;

    /**
     * 授信申请状态: 1-处理中 2-成功 3-拒绝
     */
    private Integer applyStatus;

    /**
     * 标识用户的授信是否激活，1代表激活，2代表未激活，3代表授信成功后注销
     */
    private Integer activeStatus;

    /**
     * 授信额度
     */
    private BigDecimal creditAmount;

    /**
     * 风险定价费率
     */
    private BigDecimal interestRate;

    /**
     * 授信评级
     */
    private String creditRating;

    /**
     * 授信协议号
     */
    private String creditContractNo;

    /**
     * 授信额度过期时间
     */
    private Date expireDate;

    /**
     * 决策系统 1-决策系统2.0 2-增信平台
     */
    private Integer creditType;

    /**
     * 备注
     */
    private String remark;

    //请求参数
//    @NotBlank(message="人脸识别编码不能为空")
//    private String faceOrderNo;

    //    private String creditApplyNo;
    private String clientIp;

    private String deviceId;

    //返回参数

    /**
     * 可用额度
     */
    @JsonSerialize(using = Decimal2StringSerialize.class)
    private BigDecimal availableAmount;

    /**
     * 黑暗期 授信失败再次授信倒数天数 授信失败的情况下必填
     */
    private Integer reMainingDay;

    /**
     * 用户评级
     */
    private String creditLevel;

    /**
     * 已使用额度
     */
    private BigDecimal usedAmount;
}
