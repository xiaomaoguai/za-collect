/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.tddl.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import app.myoss.cloud.mybatis.plugin.ParameterHandlerCustomizer;
import app.myoss.cloud.mybatis.plugin.impl.DefaultParameterHandlerCustomizer;

/**
 * Mybatis 参数处理自动配置
 *
 * @author chenyao
 * @since 2018年5月17日 下午3:20:06
 */
@Configuration
public class MybatisParameterHandlerAutoConfiguration {
    /**
     * 创建Mybatis 参数处理自动配置
     *
     * @return Mybatis 参数处理自动配置
     */
    @ConditionalOnMissingBean
    @Bean
    public ParameterHandlerCustomizer persistenceParameterHandler() {
        return new DefaultParameterHandlerCustomizer();
    }
}
