package com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter;

import java.lang.reflect.Method;

/**
 * @fileName: IConvertMethod.java
 * @author: WeiHui
 * @date: 2018/11/26 10:18
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface IConvertMethod {

    /**
     * 该方法是否能够处理
     *
     * @param method 方法
     * @return true-可以
     */
    boolean canHandler(Method method);

    /**
     * 处理方法的逻辑
     *
     * @param method 方法
     * @param args   入参
     * @return 出参
     */
    Object invoke(Method method, Object[] args);

}
