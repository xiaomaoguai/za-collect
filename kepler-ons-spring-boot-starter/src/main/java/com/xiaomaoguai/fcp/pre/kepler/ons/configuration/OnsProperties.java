package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @fileName: OnsProperties.java
 * @author: WeiHui
 * @date: 2019/2/26 19:44
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = "ons")
public class OnsProperties {

	/**
	 * Group ID，客户端ID
	 */
	private String groupId;

	/**
	 * AccessKey, 用于标识、校验用户身份
	 */
	private String accessKey;

	/**
	 * SecretKey, 用于标识、校验用户身份
	 */
	private String secretKey;

	/**
	 * 消息队列服务接入点
	 */
	private String onsAddr;
}
