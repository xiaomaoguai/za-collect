package com.dh.convert.rule.validator;


@Validator("NotNull")
public class NotNullValidator extends AbstractValidator<Object> {

	@Override
	public void initialize() {
	}

	@Override
	public boolean isValid(Object value) {
		return value!=null;
	}
	

}
