/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Setter
@Getter
public class FundConfig {
    /**
     *
     */
    private Long id;

    /**
     *  机构编码
     */
    private String partnerNo;

    /**
     *  机构名称
     */
    private String partnerName;

    /**
     *  核心产品平台配置产品编码
     */
    private String productCode;

    /**
     *  资金通道名称
     */
    private String fundName;

    /**
     *  资金通道代码
     */
    private String fundCode;

    /**
     *  年化率
     */
    private BigDecimal yearRate;

    /**
     *  状态 1-可用 2-当前使用 3-失效
     */
    private String state;

    /**
     *  备注
     */
    private String memo;

    /**
     * 险种 1-信用险 2-保证险
     */
    private Integer insuranceType;

    /**
     *  冗余字段1
     */
    private String addField1;

    /**
     *  冗余字段2
     */
    private String addField2;

    /**
     *  创建时间
     */
    private Date gmtCreated;

    /**
     *  创建者
     */
    private String creator;

    /**
     *  更新时间
     */
    private Date gmtModified;

    /**
     *  修改者
     */
    private String modifier;

    /**
     *
     */
    private String isDeleted;

}