package com.dh.convert.rule.function;

import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorObject;
import org.apache.commons.lang3.StringUtils;
import java.util.Map;

/**
 * @author HCC 2019/4/17 15:24
 */
public class StringEqualsAnyFunction extends AbstractFunction {

    @Override
    public String getName() {
        return "string.equalsAny";
    }

    @Override
    public AviatorObject call(Map<String, Object> env, AviatorObject arg1, AviatorObject arg2) {
        String str = FunctionUtils.getStringValue(arg1, env);
        String searchStrings = FunctionUtils.getStringValue(arg2, env);
        String[] searchArray = StringUtils.split(searchStrings, ",");

        if(null != searchArray) {
            for(String item : searchArray) {
                if(item.equals(str)) {
                    return AviatorBoolean.TRUE;
                }
            }
        }
        return AviatorBoolean.FALSE;
    }
}
