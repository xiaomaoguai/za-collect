package com.xiaomaoguai.fcp.kepeler.nepturn;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 17:06
 * @since JDK 1.8
 */
public class NeptuneServiceMain {

	private static String url = "http://ops-neptune-test.xiaomaoguaionline.com/config/v2/queryConfig.json";

	private static String url2 = "http://ops-neptune-test.xiaomaoguaionline.com/config/v2/saveConfig.json";

	@Test
	public void testQuery() {
		try {
			String param = "{\"profile\":\"test\",\"name\":\"za-pre-orgs-mics\",\"label\":\"\"}";
			System.out.println(query(param));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 新拉boom3分支后， 新增neptune 以及 zk   application.kepler.url配置
	 */
	@Test
	public void testCreateKeplerBranchNeptuneConfig() {
		try {
			String projectId = "8032";
			createKeplerBranchNeptuneConfig(projectId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * {"name":"za-pre-kepler-preloan","profile":"test","label":"7777","type":"properties","source":"dd=22","connectConfig":["za-pre-kepler-insurance/test/public"]}
	 */
	public void createKeplerBranchNeptuneConfig(String projectId) throws Exception {
		for (KeplerApplicationEnum applicationEnum : KeplerApplicationEnum.values()) {
			String appName = applicationEnum.getAppName();
			CreateKeplerBranchNeptuneConfigDTO keplerDefault = CreateKeplerBranchNeptuneConfigDTO.keplerDefault(appName, projectId);
			String result = HttpClientUtils.doPost(url2, getHeader(), JSON.toJSONString(keplerDefault));
			System.out.println("====>appName: " + appName + ",result: " + result);
		}
	}

	public String query(String param) throws Exception {
		return HttpClientUtils.doPost(url, getHeader(), param);
	}

	private static Map<String, String> getHeader() {
		return ImmutableMap.of("Content-Type", "application/json", "Cookie", "_za_sso_session_=8UqTASG5wKFRsvB1l8fKyELTJepSZpqclWaHMha74hxKEGgqUwQ5tRadtJ4BlobEoXyOnvmCmr8QfQ9naCzc6zUJwmDX0kwYspzCliZMULGpthAArvvwn113ZzoaZoeYRSTIcrZCThi9t3Y9JZkRnNOUb3zNSYx+EHXb5Zxw9cSnS4eaGPh5fBf9GX3BK43SNRwPL8EdJKcpZQnoekAXPgVs/H0tRA7HbnAUwc3KQE4dXHYHJ9MPu2TuVnG/NDUbUD1DKDnZgMzv036NfC/MwPVf7d/GflQ1qrJV1x5LlpsOgrxa2zuSoio7C/VULS+YmqqFSWgRJpivPuq2bgGloXpo/8dAEVVjolrLFMwEzOdmzZ+rzAazKxDqT3kPW9gF");
	}

}
