package com.xiaomaoguai.fcp.kepler;

import com.xiaomaoguai.fcp.kepler.utlis.security.RSA;
import org.junit.Test;

import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/10 10:32
 * @since JDK 1.8
 */
public class RSATest {

	//publicKey = MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDjQVatlP6AFkHdLu5xbmQ41bELhOFoKhqCrGPUjYkjh+DZo1T91AQMycSEkENYrJURWh1ZtpRb8AzYnYsBOtgOSv78STRTSDAJC3Mj0YSDFMcQTgaaZbUux//vzORIt8YAG0blJDlPmoI7do/PNNCCe/6VcYdkTcazSBEEslXDLwIDAQAB
	//privateKey = MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAONBVq2U/oAWQd0u7nFuZDjVsQuE4WgqGoKsY9SNiSOH4NmjVP3UBAzJxISQQ1islRFaHVm2lFvwDNidiwE62A5K/vxJNFNIMAkLcyPRhIMUxxBOBppltS7H/+/M5Ei3xgAbRuUkOU+agjt2j8800IJ7/pVxh2RNxrNIEQSyVcMvAgMBAAECgYBXPpZQPYsXEXRnvcS8t2yyhRdbHMCMhN14nUYWK+AiS9/+rb3LVMHZRvyzB89TE66G4tmxv25lfVrxLkpXRof6+1yL17dbmRwiKeXUXQxhMHzYgl1NOfUoZV8Ui57N4On9748zh1Bamhvsj9ZmSsJ6ZPxv7G9iU6mPtOzme+ODQQJBAPxXO4Hb0qSa2G20qzz4R2hiGzWPAvLMduL3mkvxWBc8gq7egw7R0fr40GmeswlG1TtAuz2o9pGZv5PgzsScWRMCQQDmjPsZM1nOkG957bM9XKHxRye723juh6quqc9peNN9NhFPS8fY93dUlVDiGIa1qgVp3CoL09v/8QILpTpQ7ez1AkBQiLahNzr+9bxlJugPyV1g3w64BTB3tPGsdkF0Q05N/C3pCXLiY+yUIJzDWLbjGwwqoPohL6+hwGP4GiNjdFKpAkBQMmhCGt+5f/qXEj1QMgHPGS5UJYMKjjysJzuT98ixfHZG/BhXe7WRwaLyExAA71SUv1YM6TRU8nQAswf3ENxlAkEAvVLQ4FSLwdMLwmvQSGJjLfZdxZN6uu/v2G7xx4MqL1lsumGTt0xoeV2Hg7uKV3211XA9k0ZnKxardrN8EvPDfw==

	@Test
	public void test() {
	    try {
			Map<String, String> pair = RSA.generateKeyPair();

			String publicKey = pair.get("publicKey");
			String privateKey = pair.get("privateKey");

			System.out.println("publicKey =" + publicKey);
			System.out.println("privateKey =" + privateKey);
		} catch (Exception e) {
	    	e.printStackTrace();
		}
	}

	//MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBALimg5JGICvRB3dZBxAnoSvvwwpqbYdq7746qrjtorNpH3+mO4Z4clZ2Al3a6PnjCCMBbVhlAhyr2pPvyO5hZTc7+ihBo+WG1BmdxJ5XtqlaKGHY22tW65KuJHKOiY1C6igORpBZFCqLtLX4NqANTGssnEzSDbGmqWhcEKPT7u1rAgMBAAECgYBH77c6eU93QowiFvEFfirwLwxRNn1NUH/uIHT6broAI2g+FskB7lK9FGR/pBU3pRc6bAfpdBCKkvkowTYKiK8hV6s6OOOqsKYf6wNokz4AQ0a5rvkeJfx96EonnZzDKA0pk/GQQEskMKEo6z28jBcI5vF2bxj93qwlLyGHT86G4QJBAOuub3pLe5qPd6DJL7GREGQZv+hGNJMTjv/GtdxhGBSD65KN3/K2URjXJsu3msObJBBvccqVeUIkijwmB9o4oe0CQQDIkc8d6ps174VhfvIXdkv74fwAhGXmMEo4fhBcSUHWM6+Jr+deB9DohkWdSgrDntvircjGbC26SWQgu0JS7kG3AkBCgeEXg8GHpkC98MgytZsIhHOxWk5vyq8p4qtjW0ruTyPlIYA2/9f2yJA8IL3dwSP2N1zHLFMhpSoVOy4wGJcpAkAnjwqoveMLCkjmZ+Fb20s1TOclOvfWJOQerE6RxSklFGCk6XrawvPjSMhmM3pYBvcbiGc3K5MBUQRcqFJ2hOOVAkAr8/DBhtXTU251oYzL8JICnAXkmQ1enntY2F2YHwKiiZfR7I03JPdXq3qRp/eeO78JzFvGpXOwUf5pVfVNSNeA
	//MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC4poOSRiAr0Qd3WQcQJ6Er78MKam2Ha++Oqq47aKzaR9/pjuGeHJWdgJd2uj54wgjAW1YZQIcq9qT78juYWU3O/ooQaPlhtQZncSeV7apWihh2NtrVuuSriRyjomNQuooDkaQWRQqi7S1+DagDUxrLJxM0g2xpqloXBCj0+7tawIDAQAB

}
