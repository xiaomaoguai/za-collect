/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.hsf.http.annotation;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * {@link HsfHttpRequestScan} 的复数，支持配置多个使用HTTP请求的HSF接口
 *
 * @author chenyao
 * @since 2016年9月21日 上午9:15:29
 * @see HsfHttpRequestScan
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(HsfHttpRequestScannerRegistrar.class)
public @interface HsfHttpRequestScans {
    /**
     * 使用注解扫描HSF接口并注册，用于快速配置需要使用HTTP请求的HSF接口
     *
     * @return 支持配置多个使用HTTP请求的HSF接口
     */
    HsfHttpRequestScan[] value();
}
