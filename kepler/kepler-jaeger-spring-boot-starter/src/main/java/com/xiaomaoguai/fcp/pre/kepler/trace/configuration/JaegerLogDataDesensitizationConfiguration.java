package com.xiaomaoguai.fcp.pre.kepler.trace.configuration;

import com.xiaomaoguai.fcp.pre.kepler.trace.constants.KeplerTraceConstant;
import com.xiaomaoguai.fcp.pre.kepler.trace.service.DataDesensitization;
import com.xiaomaoguai.fcp.pre.kepler.trace.service.NonDataDesensitization;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 19:33
 * @since JDK 1.8
 */
@ConditionalOnProperty(value = KeplerTraceConstant.KEPLER_OPENTRACING_JAEGER_ENABLED, havingValue = "true", matchIfMissing = true)
public class JaegerLogDataDesensitizationConfiguration {

	@Bean(name = KeplerTraceConstant.KEPLER_JAEGER_DATA_DESENSITIZATION_BEAN_NAME)
	@ConditionalOnMissingBean(name = KeplerTraceConstant.KEPLER_JAEGER_DATA_DESENSITIZATION_BEAN_NAME)
	public DataDesensitization dataDesensitization() {
		return new NonDataDesensitization();
	}

}
