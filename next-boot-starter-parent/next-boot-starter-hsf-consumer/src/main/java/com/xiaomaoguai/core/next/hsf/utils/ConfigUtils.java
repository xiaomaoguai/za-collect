/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.hsf.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 类ConfigUtils的实现描述：hsf配置工具类
 *
 * @author chenyao
 * @since 2018年1月16日 下午11:51:38
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigUtils {
    /**
     * 组装服务请求url
     *
     * @param environment 应用环境变量
     * @param defaultDomainValue 默认的域名属性值
     * @param specialDomainProperty 自定义域名属性名（从 environment 中取值）
     * @param servicePath 接口path路径
     * @param specialServiceVersionProperty 自定义接口版本属性名（从 environment 中取值）
     * @param defaultServiceVersion
     *            接口默认版本（specialServiceVersionProperty获取不到值的时候会被使用）
     * @return 服务请求完整url
     */
    public static String getServiceUrl(Environment environment, String defaultDomainValue, String specialDomainProperty,
                                       String servicePath, String specialServiceVersionProperty,
                                       String defaultServiceVersion) {
        String domain = environment.getProperty(specialDomainProperty);
        String specialServiceVersion = environment.getProperty(specialServiceVersionProperty);
        return StringUtils.defaultIfEmpty(domain, defaultDomainValue) + servicePath
                + StringUtils.defaultIfEmpty(specialServiceVersion, defaultServiceVersion);

    }
}
