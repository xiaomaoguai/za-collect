package com.xiaomaoguai.fcp.pre.kepler.router.handler.enums;

public enum Instruction {
	INVOKE_PIPE,//调用指定责任链
	RETRY,//重试
	BREAK,//中断
	CONTINUE,//递归继续
	EXCUTE,//执行
	EXIT;//退出责任链
}
