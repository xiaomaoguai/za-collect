package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive;

import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.SensitiveDataRuleService;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.SensitiveRulesManageService;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.SensitiveService;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.impl.SensitiveDataRuleServiceImpl;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.impl.SensitiveRulesManageServiceImpl;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.impl.SensitiveServiceImpl;
import org.springframework.context.annotation.Bean;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 15:14
 * @since JDK 1.8
 */
public class SensitiveBean {

	@Bean
	public SensitiveService sensitiveService() {
		return new SensitiveServiceImpl();
	}

	@Bean
	public SensitiveDataRuleService sensitiveDataRuleService() {
		return new SensitiveDataRuleServiceImpl();
	}

	@Bean
	public SensitiveRulesManageService sensitiveRulesManageService() {
		return new SensitiveRulesManageServiceImpl();
	}

}
