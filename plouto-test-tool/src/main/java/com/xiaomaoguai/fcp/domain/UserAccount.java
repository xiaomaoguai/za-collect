package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * 用户信息
 *
 * @author THINK
 */
@Getter
@Setter
public class UserAccount extends User {

	private static final long serialVersionUID = -1936456983413247198L;

	//订单号
	private String orderNo;

	//订单状态
	private Integer status;

	//原订单状态
	private List<Integer> preStatus;

	private String certOrderNo;

	private String faceOrderNo;

	private String certAddress;

	private String certExpireDate;

	private String addressJson;

	private Integer monthSalary;

	private String openId;

	private String linkInfoJson;

	private Integer creditStatus;

	private Integer isoverdue;

	private Integer isloaning;

	private Date creditApplyTime;

	private String addInfo;

	private Integer preCreditStatus;

	private Date preCreditApplyTime;

	/**
	 * 0 -不存在其他产品在贷 1 -存在其他产品在贷
	 */
	private Integer isExist = 0;

	/**
	 * 判断是否是重复实名标志
	 */
	private Integer repeatAuth = 0;

	private Date gmtCreated;

	private String creator;

	private Date gmtModified;

	private String modifier;

	private String isDeleted;

	//融360MD5撞库字段 手机号+身份证号
    private String mobileCertMd5;;

	//对应r360附加信息中推送过来的影像信息
	private String ocrBackUrl;

	private String ocrFrontUrl;

	private String ocrLivePhotoUrl;


	/**
	 * ===========新增小米银行卡信息字段===============
	 * 卡号
	 */
	private String bankCardNo;

	/**
	 * 银行预留电话
	 */
	private String cardPhone;

	/**
	 * 验证码
	 */
	private String verifyCode;

	/**
	 * 发卡行编码
	 */
	private String bankCode;

	/**
	 * 发卡行名称
	 */
	private String bankName;

	/**
	 * 授信申请号
	 */
//	private String creditApplyNo;

	/**
	 * 构建更新的orderMapping
	 *
	 * @param prefixApplyNo 数据库业务流水号
	 * @param productCode   order状态
	 * @param status        order状态
	 * @return OrderMapping
	 */
	public static UserAccount buildOrderMapping(String prefixApplyNo, String productCode, Integer status) {
		UserAccount mapping = new UserAccount();
		mapping.setProductCode(productCode);
		mapping.setChannelId(prefixApplyNo.split("_")[0]);
		mapping.setOrderNo(prefixApplyNo.split("_")[2]);
		mapping.setStatus(status);
		return mapping;
	}

}