package com.baomidou.mybatisplus.samples.generator.log.mapper;

import com.baomidou.mybatisplus.samples.generator.log.entity.Log;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangweihui
 * @since 2019-03-09
 */
public interface LogMapper extends BaseMapper<Log> {

}
