package com.xiaomaoguai.fcp.mapper.credit;

import com.xiaomaoguai.fcp.domain.CreditLevel;
import org.apache.ibatis.annotations.Param;

public interface CreditLevelMapper {

	int insert(CreditLevel record);

	CreditLevel selectByUk(@Param("productCode") String productCode, @Param("creditRating") String creditRating, @Param("creditType") Integer creditType);

	int updateByPrimaryKeySelective(CreditLevel record);

}