package com.xiaomaoguai.fcp.pre.kepler.glue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * 项目启动类
 *
 * @author zhangweihui
 * @since 2018年12月17日 下午4:54:45
 */
@EnableCaching
@SpringBootApplication
public class RedisExampleApplication {

	/**
	 * 项目启动类
	 *
	 * @param args 启动参数
	 */
	public static void main(String[] args) {
		SpringApplication.run(RedisExampleApplication.class, args);
	}

}
