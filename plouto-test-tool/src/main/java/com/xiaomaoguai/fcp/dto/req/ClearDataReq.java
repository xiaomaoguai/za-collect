package com.xiaomaoguai.fcp.dto.req;

import com.xiaomaoguai.fcp.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: ClearDataReq.java
 * @author: WeiHui
 * @date: 2018/10/26 13:35
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ClearDataReq extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 7693407256051474968L;

	private Long userId;

	private String type = "4";
}
