package com.xiaomaoguai.javassist.kepler.dc;

import com.xiaomaoguai.javassist.kepler.config.DCConvertClient;
import com.xiaomaoguai.javassist.kepler.dto.StandardBaseDTO;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/2 14:29
 * @since JDK 1.8
 */
public interface ApplyOssInfoHandlerDCConvertClient extends DCConvertClient<StandardBaseDTO> {

}
