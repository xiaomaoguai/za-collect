package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class RepayDtl {

    private Long id;

    private Long userId;

    private String loanOuterNo;

    private String loanInnerNo;

    private String repayOuterNo;

    private String repayInnerNo;

    private Integer repayMode;

    private Integer repayStatus;

    private Long planId;

    private Integer installmentNo;

    private Integer currentFlag;

    private BigDecimal principal = BigDecimal.ZERO;

    private BigDecimal interest = BigDecimal.ZERO;

    private BigDecimal charge = BigDecimal.ZERO;

    private BigDecimal paidPrincipal = BigDecimal.ZERO;

    private BigDecimal paidInterest = BigDecimal.ZERO;

    private BigDecimal paidCharge = BigDecimal.ZERO;

    private BigDecimal paidPen = BigDecimal.ZERO;

    private BigDecimal discountPrin = BigDecimal.ZERO;

    private BigDecimal discountInt = BigDecimal.ZERO;

    private BigDecimal discountCharge = BigDecimal.ZERO;

    private String agreedRepayDate;

    private String actualRepayDate;

    private Integer repayType;

    private Date gmtCreated;

    private String creator;

    private Date gmtModified;

    private String modifier;

    private String isDeleted;
    
    private List<Integer> installmentNos;


}