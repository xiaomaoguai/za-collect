package com.xiaomaoguai.fcp.pre.kepler.mock.zktool;

import com.xiaomaoguai.fcp.pre.kepler.zk.utils.ZookeeperContext;
import org.junit.Test;

import java.io.File;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 17:36
 * @since JDK 1.8
 */
public class AZkEnumTest extends ZkEnumBaseTest {

	@Test
	public void addAll() throws Exception {
		File file = new File(fileDirectory);
		if (file.isDirectory()) {
			for (final File listFile : file.listFiles()) {
				String name = listFile.getName();
				if (!"kepler-mock-spring-boot-starter.kotlin".equals(name)) {
					createOrUpdateData(name);
				}
			}
		}
	}

	@Test
	public void testAdd1() throws Exception {
		String mockClassName = "SCPersonAccountService_openAccount.groovy";
		createOrUpdateData(mockClassName);
	}

	@Test
	public void testAdd2() throws Exception {
		String mockClassName = "SCAccountAuthService_policeAuth.groovy";
		createOrUpdateData(mockClassName);
	}

	@Test
	public void testDelete() throws Exception {
		ZookeeperContext.delete("/application/glue/kepler-mock-spring-boot-starter.kotlin");
	}

}
