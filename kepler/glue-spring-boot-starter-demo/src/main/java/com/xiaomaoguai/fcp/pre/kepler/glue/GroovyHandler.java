package com.xiaomaoguai.fcp.pre.kepler.glue;//package com.xiaomaoguai.fcp.pre.kepler.common.handler;
//
//import com.xiaomaoguai.fcp.pre.kepler.glue.autoconfiguration.GlueBootstrap;
//import com.xiaomaoguai.fcp.pre.kepler.router.annotation.HandlerAnnotation;
//import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
//import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.AbstractHandler;
//import org.apache.commons.collections4.MapUtils;
//
//import javax.annotation.Resource;
//import java.util.Map;
//
///**
// * @author WeiHui-Z
// * @version v1.0.0
// * @date 2019/9/26 21:18
// * @since JDK 1.8
// */
//@HandlerAnnotation(desc = "Groovy动态执行器", author = "WeiHui-Z", share = true)
//public class GroovyHandler extends AbstractHandler<Map<String, Object>> {
//
//	@Resource
//	private GlueBootstrap glueBootstrap;
//
//	@Override
//	public void handle(HandlerContext<Map<String, Object>> hc) {
//		Map<String, Object> innerParam = hc.getInnerParam();
//		String glueName = MapUtils.getString(innerParam, "glueName");
//		Object run = glueBootstrap.run(glueName, innerParam);
//		hc.getBuf().put("run", run);
//	}
//
//}
