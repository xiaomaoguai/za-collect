package com.xiaomaoguai.fcp.pre.kepler.router.async.delay.strategy;


public class DefaultStrategy implements RetryStrategy {

	private static final int[] DEFAULT_INTERVALS = new int[]{30, 30, 1 * 60, 5 * 60, 10 * 60, 30 * 60, 60 * 60, 2 * 3600,
			5 * 3600, 10 * 3600, 15 * 3600, 24 * 3600};

	@Override
	public Long interval(int retrytimes) {
		if (retrytimes < DEFAULT_INTERVALS.length) {
			return DEFAULT_INTERVALS[retrytimes] * 1000L;
		} else
			return DEFAULT_INTERVALS[DEFAULT_INTERVALS.length - 1] * 1000L;
	}

}
