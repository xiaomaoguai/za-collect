package com.xiaomaoguai.fcp.pre.kepler.router.router;

import com.xiaomaoguai.fcp.pre.kepler.router.handler.RouterInfo;

import java.util.List;

public interface RouterConfig {

	List<RouterInfo> getRouterInfos(String routerName);

}
