/*
 * 文件名：${module}ServerImpl.java
 * 版本信息:v1.0.0
 * 日期：${createdTime} 
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */
package com.billionsfinance.crs.server.impl.${moduleType};

import java.util.List;

import javax.annotation.Resource;
import com.billionsfinance.crs.model.${moduleType}.${module};
import com.billionsfinance.crs.server.${moduleType}.I${module}Server;
import com.billionsfinance.crs.service.${moduleType}.I${module}Service;
import com.billionsfinance.crs.request.PageRequest;
import com.billionsfinance.crs.model.PageResult;
import com.billionsfinance.crs.model.ResponseModel;

/**
* @ FileName: ${module}ServerImpl.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
public class ${module}ServerImpl implements I${module}Server{

    @Resource
	private I${module}Service ${lowModule}Service;

	@Override
	public List<${module}> find${module}List(${module} ${lowModule}) throws Exception{
		return ${lowModule}Service.find${module}List(${lowModule});
	}
	
	@Override
	public PageResult<${module}> queryPageList(${module} ${lowModule}, PageRequest pageV0) throws Exception{
		return ${lowModule}Service.select${module}Set(${lowModule},  pageV0);
	}
	
    @Override
    public int createOrUpdate${module}(${module} ${lowModule}) throws Exception{
        return ${lowModule}Service.createOrUpdate${module}(${lowModule});
    }
    
    @Override
    public ResponseModel createOrUpdate(${module} ${lowModule}) throws Exception{
        return ${lowModule}Service.createOrUpdate(${lowModule});
    }
    
    @Override
    public ${module} read${module}ById(String uuId) throws Exception{
        return ${lowModule}Service.read${module}ById(uuId);
    }
    
    @Override
    public int delete${module}ById(String uuId) throws Exception{
        return ${lowModule}Service.delete${module}ById(uuId);
    }
    
    @Override
    public int physicalDelete${module}ById(String uuId) throws Exception{
    	return ${lowModule}Service.physicalDelete${module}ById(uuId);
    }	
    
}
