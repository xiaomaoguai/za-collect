/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.cache.redis.config;

import lombok.Data;

/**
 * Zookeeper 中保存的 Redis 配置信息
 *
 * @author chenyao
 * @since 2018年5月22日 下午7:11:32
 */
@Data
public class RedisInfo {
    private String  host;
    private Integer port;
    private int     dbNo;
    private String  password;
}
