package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class HttpRequestException extends RuntimeException {
	private static final long serialVersionUID = 1230075872621447489L;

	public HttpRequestException(Throwable cause) {
		super(cause);
	}

	public HttpRequestException(String message) {
		super(message);
	}

	public HttpRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}
