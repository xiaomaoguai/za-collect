package com.xiaomaoguai.fcp.pre.kepler.glue.autoconfiguration;

import com.xiaomaoguai.fcp.pre.kepler.glue.config.GlueConfigProperties;
import com.xiaomaoguai.fcp.pre.kepler.glue.service.GlueInitService;
import com.xiaomaoguai.fcp.pre.kepler.glue.service.impl.GlueInitServiceImpl;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/26 21:13
 * @since JDK 1.8
 */
@Configuration
@EnableConfigurationProperties(GlueConfigProperties.class)
public class GlueAutoConfiguration {

	@Resource
	private GlueConfigProperties glueConfigProperties;

	@Bean
	public GlueInitService glueInitService() {
		return new GlueInitServiceImpl();
	}

	@Bean
	public GlueBootstrap glueBootstrap(GlueInitService glueInitService) {
		GlueBootstrap glueBootstrap = new GlueBootstrap(glueInitService);
		glueBootstrap.setGlueZookeeperConfig(glueConfigProperties.getGlueZookeeperConfig());
		glueBootstrap.setRepositorySupport(glueConfigProperties.getRepositorySupport());
		return glueBootstrap;
	}

}
