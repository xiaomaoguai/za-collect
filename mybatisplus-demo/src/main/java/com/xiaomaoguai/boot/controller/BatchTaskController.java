package com.xiaomaoguai.boot.controller;


import com.google.common.collect.Maps;
import com.xiaomaoguai.boot.entity.BatchTask;
import com.xiaomaoguai.boot.service.BatchTaskService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Map;

/**
 * <p>
 * 批量任务信息表 前端控制器
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Controller
@RequestMapping("/batchTask")
public class BatchTaskController {

    @Resource
    private BatchTaskService batchTaskService;

    @RequestMapping("/id")
    @ResponseBody
    public Collection<BatchTask> id() {
        Map<String, Object> map = Maps.newHashMap();
        map.put("product_code", "51YND");
        return batchTaskService.listByMap(map);
    }

}

