package com.xiaomaoguai.fcp.es;

import com.frameworkset.orm.annotation.ESId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/30 10:10
 * @since JDK 1.8
 */
@Getter
@Setter
public class Demo {

	@ESId
	private long demoId;

	private Date agentStarttime;

	private Date agentStarttimezh;

	private String applicationName;

	private String contentbody;

	private String name;

	private String orderId;

	private int contrastStatus;

}
