package com.xiaomaoguai.fcp.service.impl;

import com.xiaomaoguai.fcp.common.PloutoConstansts;
import com.xiaomaoguai.fcp.dto.ResponseModel;
import com.xiaomaoguai.fcp.dto.enums.ClearTypeEnum;
import com.xiaomaoguai.fcp.dto.resp.ClearDataResp;
import com.xiaomaoguai.fcp.dto.resp.CreditCoreClearDetail;
import com.xiaomaoguai.fcp.mapper.UserAccountDtlMapper;
import com.xiaomaoguai.fcp.mapper.UserAccountMapper;
import com.xiaomaoguai.fcp.mapper.UserCardInfoMapper;
import com.xiaomaoguai.fcp.mapper.UserStateMapper;
import com.xiaomaoguai.fcp.mapper.credit.CreditApplyMapper;
import com.xiaomaoguai.fcp.mapper.loan.RepayPlanMapper;
import com.xiaomaoguai.fcp.mapper.loan.UserLoanMapper;
import com.xiaomaoguai.fcp.mapper.repay.RepayDtlMapper;
import com.xiaomaoguai.fcp.mapper.repay.RepayInfoMapper;
import com.xiaomaoguai.fcp.service.ClearDataService;
import com.xiaomaoguai.fcp.service.CreditCoreClearService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @fileName: ClearDataServiceImpl.java
 * @author: WeiHui
 * @date: 2018/10/25 10:37
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Service
public class ClearDataServiceImpl implements ClearDataService {

	@Resource
	private UserAccountMapper userAccountMapper;

	@Resource
	private UserAccountDtlMapper userAccountDtlMapper;

	@Resource
	private UserStateMapper userStateMapper;

	@Resource
	private UserCardInfoMapper userCardInfoMapper;

	@Resource
	private CreditApplyMapper creditApplyMapper;

	@Resource
	private UserLoanMapper userLoanMapper;

	@Resource
	private RepayPlanMapper repayPlanMapper;

	@Resource
	private RepayInfoMapper repayInfoMapper;

	@Resource
	private RepayDtlMapper repayDtlMapper;

	@Resource
	private CreditCoreClearService creditCoreClearService;

	@Override
	public int clearCreditDataByUserId(Long userId) {
		int i = userAccountMapper.deleteByUserId(userId);
		int i1 = userAccountDtlMapper.deleteByUserId(userId);
		int i2 = userStateMapper.deleteByUserId(userId);
		int i3 = userCardInfoMapper.deleteByUserId(userId);
		int i4 = creditApplyMapper.deleteByUserId(userId);
		return i + i1 + i2 + i3 + i4;
	}

	@Override
	public int clearLoanDataByUserId(Long userId) {
		int i4 = userLoanMapper.deleteByUserId(userId);
		int i5 = repayPlanMapper.deleteByUserId(userId);
		return i4 + i5;
	}

	@Override
	public int clearRepayDataByUserId(Long userId) {
		int i6 = repayInfoMapper.deleteByUserId(userId);
		int i7 = repayDtlMapper.deleteByUserId(userId);
		return i6 + i7;
	}

	@Override
	public ClearDataResp clearByUserIdAndType(Long userId, String type) {
		ClearDataResp clearDataResp = new ClearDataResp();
		int creditNum = 0;
		int loanNum = 0;
		int repayNum = 0;
		ClearTypeEnum value = ClearTypeEnum.getValue(type);
		switch (value) {
			case CREDIT:
				creditNum = clearCreditDataByUserId(userId);
				break;
			case LOAN:
				loanNum = clearLoanDataByUserId(userId);
				break;
			case REPAY:
				repayNum = clearRepayDataByUserId(userId);
				break;
			case ALL:
				creditNum = clearCreditDataByUserId(userId);
				loanNum = clearLoanDataByUserId(userId);
				repayNum = clearRepayDataByUserId(userId);
				break;
			default:
				break;
		}
		CreditCoreClearDetail clear = creditCoreClearService.clear(PloutoConstansts.PRODUCT_CODE, userId, type);
		if (clear != null) {
			creditNum += clear.getApplyClearNum();
			loanNum += clear.getLoanClearNum();
			repayNum += clear.getFileClearNum();
		}
		clearDataResp.setApplyClearNum(creditNum);
		clearDataResp.setLoanClearNum(loanNum);
		clearDataResp.setRepayClearNum(repayNum);
		clearDataResp.setAllDataClearNum(repayNum + loanNum + creditNum);
		return clearDataResp;
	}

	@Override
	public ResponseModel<Object> clearByThirdUserNo(String thirdUserNo, String channelId) {
		return null;
	}

	@Override
	public ResponseModel<Object> clearByUserMobile(Long userId, String channelId) {
		return null;
	}
}
