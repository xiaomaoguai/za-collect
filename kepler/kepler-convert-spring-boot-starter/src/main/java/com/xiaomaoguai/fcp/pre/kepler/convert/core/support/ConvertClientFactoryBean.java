package com.xiaomaoguai.fcp.pre.kepler.convert.core.support;

import com.xiaomaoguai.fcp.pre.kepler.convert.core.ConvertClientInvocationHandler;
import org.springframework.beans.factory.FactoryBean;

import java.lang.reflect.Proxy;

/**
 * @fileName: ConvertFactoryBean.java
 * @author: WeiHui
 * @date: 2018/11/24 22:09
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ConvertClientFactoryBean implements FactoryBean<Object> {

	private Class<?> serviceInterface;

	@Override
	public Object getObject() throws Exception {
		return Proxy.newProxyInstance(
				this.getClass().getClassLoader(),
				new Class<?>[]{this.serviceInterface},
				new ConvertClientInvocationHandler());
	}

	@Override
	public Class<?> getObjectType() {
		return this.serviceInterface;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

	/**
	 * 设置接口类型
	 *
	 * @param serviceInterface 接口
	 */
	public void setServiceInterface(Class<?> serviceInterface) {
		if (serviceInterface != null && !serviceInterface.isInterface()) {
			throw new IllegalArgumentException("'serviceInterface' must be an interface");
		}
		this.serviceInterface = serviceInterface;
	}

	/**
	 * Return the interface of the service to access.
	 */
	public Class<?> getServiceInterface() {
		return this.serviceInterface;
	}

}
