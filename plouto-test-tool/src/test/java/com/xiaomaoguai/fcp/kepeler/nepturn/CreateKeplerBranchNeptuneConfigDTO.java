package com.xiaomaoguai.fcp.kepeler.nepturn;

import lombok.Data;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 17:37
 * @since JDK 1.8
 */
@Data
public class CreateKeplerBranchNeptuneConfigDTO {

	/**
	 * {"name":"za-pre-kepler-preloan","profile":"test","label":"7777","type":"properties","source":"dd=22","connectConfig":["za-pre-kepler-insurance/test/public"]}
	 */
	private String name;

	private String profile;

	private String label;

	private String type;

	private String source;

	private String[] connectConfig;

	/**
	 * kepelr默认
	 *
	 * @param appName
	 * @param projectId
	 * @return
	 */
	public static CreateKeplerBranchNeptuneConfigDTO keplerDefault(String appName, String projectId) {
		CreateKeplerBranchNeptuneConfigDTO neptuneConfigDTO = new CreateKeplerBranchNeptuneConfigDTO();
		neptuneConfigDTO.setName(appName);
		neptuneConfigDTO.setProfile("test");
		neptuneConfigDTO.setLabel(projectId);
		neptuneConfigDTO.setType("properties");
		neptuneConfigDTO.setSource("kepler.application.url=" + projectId);
		neptuneConfigDTO.setConnectConfig(new String[]{"za-pre-kepler-insurance/test/public"});
		return neptuneConfigDTO;
	}

}
