/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserImageTransfer {

	/**
	 *
	 */
	private Long id;
	/**
	 * 产品编码
	 */
	private String productCode;

	/**
	 * 授信流水号
	 */
	private String creditNo;

	/**
	 * 账户号
	 */
	private Long userId;

	/**
	 * 身份证OCR订单号
	 */
	private String ocrOrderNo;

	/**
	 * 身份证正面OSS地址
	 */
	private String frontOcrUrl;

	/**
	 * 身份证反面OSS地址
	 */
	private String backOcrUrl;

	/**
	 * 活体影像订单号
	 */
	private String liveOrderNo;

	/**
	 * 活体识别时的照片OSS地址
	 */
	private String livePhotoUrl;

	/**
	 * 活体识别时的视频OSS地址
	 */
	private String liveVideoUrl;
	/**
	 * 上传结果( 按frontOcrUrl、backOcrUrl、livePhotoUrl、liveVideoUrl标识 转存10进制字符)
	 */
	private Integer state;

	/**
	 * 创建人
	 */
	private String creator;

	/**
	 * 创建时间
	 */
	private Date gmtCreated;

	/**
	 * 更新人
	 */
	private String modifier;

	/**
	 * 修改时间
	 */
	private Date gmtModified;

	/**
	 * 是否删除
	 */
	private String isDeleted;

}