package com.xiaomaoguai.fcp.pre.kepler.mock.dc;

import com.xiaomaoguai.fcp.pre.kepler.mock.spi.MockExecutionHandler;
import com.xiaomaoguai.fcp.pre.kepler.mock.utils.FeignMethodInvocation;

/**
 * 动态父类
 *
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 14:45
 * @since JDK 1.8
 */
public class DCMockHandler {

	public MockExecutionHandler mockExecutionHandler;

	public FeignMethodInvocation feignMethodInvocation;

	/**
	 * Getter for property 'mockExecutionHandler'.
	 *
	 * @return Value for property 'mockExecutionHandler'.
	 */
	public MockExecutionHandler getMockExecutionHandler() {
		return mockExecutionHandler;
	}

	/**
	 * Setter for property 'mockExecutionHandler'.
	 *
	 * @param mockExecutionHandler Value to set for property 'mockExecutionHandler'.
	 */
	public void setMockExecutionHandler(final MockExecutionHandler mockExecutionHandler) {
		this.mockExecutionHandler = mockExecutionHandler;
	}

	/**
	 * Getter for property 'methodInvocation'.
	 *
	 * @return Value for property 'methodInvocation'.
	 */
	public FeignMethodInvocation getFeignMethodInvocation() {
		return feignMethodInvocation;
	}

	/**
	 * Setter for property 'methodInvocation'.
	 *
	 * @param feignMethodInvocation Value to set for property 'methodInvocation'.
	 */
	public void setFeignMethodInvocation(final FeignMethodInvocation feignMethodInvocation) {
		this.feignMethodInvocation = feignMethodInvocation;
	}

}
