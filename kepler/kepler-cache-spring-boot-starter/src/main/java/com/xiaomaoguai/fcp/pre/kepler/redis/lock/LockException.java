package com.xiaomaoguai.fcp.pre.kepler.redis.lock;

import org.springframework.core.NestedRuntimeException;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/5/24 16:13
 * @since JDK 1.8
 */
public class LockException extends NestedRuntimeException {

	public LockException(String msg) {
		super(msg);
	}

	public LockException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
