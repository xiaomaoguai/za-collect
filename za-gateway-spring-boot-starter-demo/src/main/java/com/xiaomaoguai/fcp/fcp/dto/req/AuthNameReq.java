package com.xiaomaoguai.fcp.fcp.dto.req;

import com.github.xiaomaoguai.xxr.dto.req.BizReq;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @fileName: AuthNameReq.java
 * @author: WeiHui
 * @date: 2018/9/13 13:35
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class AuthNameReq extends BizReq {

	private static final long serialVersionUID = -2205661604913177766L;

	/**
	 * 身份证
	 */
	@NotBlank(message = "身份证不能为空")
	private String certNo;

	/**
	 * 手机号码
	 */
	private String userMobile;

	/**
	 * 姓名
	 */
	private String userName;

	/**
	 * OCR订单号
	 */
	@NotBlank(message = "OCR订单" + "不能为空")
	@Length(max = 64, message = "OCR订单不超过64个字符")
	private String certOrderNo;

	/**
	 * 身份证地址
	 */
	@Length(max = 50, message = "身份证地址不超过50个字符")
	private String certAddress;

	/**
	 * 身份证有效期
	 */
	@Length(max = 25, message = "身份证有效期不超过25个字符")
	private String certExpireDate;

	/**
	 * 微信openId
	 */
	@NotBlank(message = "openId不能为空")
	@Length(max = 64, message = "微信openId不超过64个字符")
	private String openId;

}
