package com.xiaomaoguai.fcp.pre.kepler.oss.utils;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/9 11:08
 * @since JDK 1.8
 */
public interface IOssClient {

	/**
	 * Getter for property 'client'.
	 *
	 * @return Value for property 'client'.
	 */
	OssClient getClient();

	default OssClient getClient(OssConfigEnum configEnum) {
		OssClient ossClient = new OssClient();
		ossClient.setBucketName(configEnum.getBucketName());
		ossClient.setAccessKeyId(configEnum.getAccessKeyId());
		ossClient.setSecretAccessKey(configEnum.getAccessKeySecret());
		ossClient.setPrivateEndpoint(configEnum.getEndpoint());
		ossClient.init();
		return ossClient;
	}

}
