/*
 * 文件名：I${module}Dao.java
 * 版本信息:v1.0.0
 * 日期：${createdTime} 
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */
package com.billionsfinance.crs.dao.${moduleType};

import java.util.List;

import java.sql.SQLException;
import org.apache.ibatis.annotations.Param;

import com.billionsfinance.crs.model.Page;
import com.billionsfinance.crs.model.PageResult;
import com.billionsfinance.crs.model.${moduleType}.${module};


/**
* @ FileName: I${module}Dao.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
public  interface I${module}Dao {

	/**
	 * 查询LIST
	 * @param ${lowModule}  参数
	 * @return List<${module}> 
	 * @throws SQLException  e
	 */
	List<${module}> find${module}List(${module} ${lowModule}) throws SQLException;
	
	/**
	 * 根据ID得到Model
	 * @param uuId  参数
	 * @return ${module} 
	 * @throws SQLException  e
	 */
	${module} read${module}ById(String uuId) throws SQLException;
	
	/**
	 * 新增Model
	 * @param ${lowModule}   参数
	 * @return int 受影响行数
	 * @throws SQLException e
	 */
	int create${module}(${module} ${lowModule}) throws SQLException;
	
	/**
	 * 修改
	 * @param ${lowModule}  参数
	 * @return int 受影响行数
	 * @throws SQLException  e
	 */
	int update${module}(${module} ${lowModule}) throws SQLException;
	
	/**
	 * update is_delte='Y' 逻辑删除
	 * @param uuId 要删除的对象ID
	 * @return int 受影响行数
	 * @throws SQLException e
	 */
	int delete${module}ById(String uuId) throws SQLException;
	
	/**
	 * 物理删除
	 * @param uuId 要物理删除的对象ID
	 * @return int 受影响行数 
	 * @throws SQLException e
	 */
	int physicalDelete${module}ById(String uuId) throws SQLException;
	
	/**
	 * 分页查询
	 * @param ${lowModule}  参数
	 * @param pageVO 参数
	 * @return PageResult<${module}> 参数
	 * @throws SQLException e
	 */
	PageResult<${module}> select${module}Set(@Param("${lowModule}") ${module} ${lowModule}, @Param("page") Page pageVO) throws SQLException;
	
}
