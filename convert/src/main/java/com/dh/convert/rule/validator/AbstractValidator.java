package com.dh.convert.rule.validator;


public abstract class AbstractValidator<T> {
	
	
	public void register(String validatorName) {
		ValidatorContext.put(validatorName, this);
	}
	
	public abstract void initialize();

    public abstract boolean isValid(T value);

  
}
