/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.loan;

import com.xiaomaoguai.fcp.domain.SubLoanApply;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * The Interface SubLoanApplyMapper.
 */
public interface SubLoanApplyMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(SubLoanApply record);

	/**
	 * 查找指定时间点的数据.
	 *
	 * @param beginTime   开始时间
	 * @param endTime     截止时间
	 * @param productCode 产品编码
	 * @return the list
	 */
	List<SubLoanApply> selectByCondition(@Param("beginTime") Date beginTime, @Param("endTime") Date endTime,
										 @Param("productCode") String productCode);
}