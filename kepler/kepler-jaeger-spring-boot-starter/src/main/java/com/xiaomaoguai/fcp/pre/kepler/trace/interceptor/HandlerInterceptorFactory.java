package com.xiaomaoguai.fcp.pre.kepler.trace.interceptor;

import com.xiaomaoguai.fcp.pre.kepler.trace.spi.AbstractSingletonFactory;

/**
 * oss Or Sftp 配置
 *
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/17 21:34
 * @since JDK 1.8
 */
public class HandlerInterceptorFactory extends AbstractSingletonFactory<String, HandlerAroundInterceptor> {

}