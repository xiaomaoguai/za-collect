package com.xiaomaoguai.fcp.pre.kepler.glue.utils;

import com.google.common.collect.Lists;
import lombok.experimental.UtilityClass;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/29 14:16
 * @since JDK 1.8
 */
@UtilityClass
public class FieldUtils {

	/**
	 * 获取实例的所有属性
	 *
	 * @param instance 实例
	 * @return 所有属性
	 */
	public static List<Field> getDeclaredFields(Class<?> instance) {
		List<Field> fieldList = Lists.newArrayList();
		if (instance != null) {
			Field[] declaredFields = instance.getDeclaredFields();
			fieldList.addAll(Arrays.asList(declaredFields));

			Class<?> superclass = instance.getSuperclass();
			while (!superclass.isAssignableFrom(Object.class)) {
				Field[] fields = superclass.getDeclaredFields();
				fieldList.addAll(Arrays.asList(fields));
				superclass = superclass.getSuperclass();
			}
		}
		return fieldList;
	}

	/**
	 * spring bean不注入
	 *
	 * @param field 属性
	 * @return 是否是spring属性
	 */
	public static boolean isSpringBean(Field field) {
		return field.getAnnotation(Resource.class) != null || field.getAnnotation(Autowired.class) != null;
	}

	/**
	 * 判断是否是 static && final 类型，针对这个类型不设置值
	 *
	 * @param field 属性
	 * @return 是否是static && final 类型
	 */
	public static boolean isStaticFinal(Field field) {
		int modifiers = field.getModifiers();
		return Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers);
	}

	/**
	 * 判断是否是 static 类型，针对这个类型不设置值
	 *
	 * @param field 属性
	 * @return 是否是static 类型
	 */
	public static boolean isStatic(Field field) {
		int modifiers = field.getModifiers();
		return Modifier.isStatic(modifiers);
	}

}
