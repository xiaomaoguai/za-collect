package com.xiaomaoguai.fcp.pre.kepler.router.async;

/**
 * AsyncWorkPool ringbuffer的事件
 *
 * @param <O>
 * @author DH
 */
public class HandlerEvent<O> {

	private O data;

	public HandlerEvent() {
	}

	public HandlerEvent(O data) {
		this.data = data;
	}

	public O getData() {
		return data;
	}

	public void setData(O data) {
		this.data = data;
	}

}
