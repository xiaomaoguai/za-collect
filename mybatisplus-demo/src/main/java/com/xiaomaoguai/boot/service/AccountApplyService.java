package com.xiaomaoguai.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaoguai.boot.entity.AccountApply;

/**
 * <p>
 * 调账申请表 服务类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
public interface AccountApplyService extends IService<AccountApply> {

}
