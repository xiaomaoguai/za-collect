package com.xiaomaoguai.fcp.YND.test;

import com.xiaomaoguai.fcp.kepler.utils.KeplerFileUtils;
import org.junit.Test;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/23 14:54
 * @since JDK 1.8
 */
public class KeplerFileUtilsTest {

	//加密公钥
	public static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCeoXCxMupCMX+MAjWaYulyIsypH8ju5YLPmf4E9MGYZccZZSAgLCnVGKBM9qcXKR46tK64eBb3OU+amS4LaCyubz2B4fxMPG+XyeBidklYcdD+we8ERC/SB78NUTph04NbvMrj4XFRo55D9YOFCOmR3yvZXT9M+UZIDWMCnuF/iQIDAQAB";

	/**
	 * 文件加密
	 */
	@Test
	public void testFileEn() {
		try {
			KeplerFileUtils.encryptFile(publicKey, "D:\\testData\\test\\repay_20190724_201907242173.txt", "D:\\testData\\test\\enc\\repay_20190724_201907242173.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文件解密
	 */
	@Test
	public void testFileDes() {
		try {
			KeplerFileUtils.decryptFile(publicKey, "D:\\repay_20190720_201907202101.txt", "D:\\repay_20190720_201907202101_des.txt");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
