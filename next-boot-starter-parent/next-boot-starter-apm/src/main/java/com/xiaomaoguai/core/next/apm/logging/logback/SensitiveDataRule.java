/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.apm.logging.logback;

import com.alibaba.fastjson.annotation.JSONField;
import com.xiaomaoguai.datasecure.common.enums.SensitiveRulesEnum;

import lombok.Data;

/**
 * 敏感信息过滤规则
 *
 * @author chenyao
 * @since 2019年1月29日 下午3:12:34
 */
@Data
public class SensitiveDataRule {
    /**
     * 过滤的字段名
     */
    private String             fieldName;
    /**
     * 脱敏规则
     */
    @JSONField
    private SensitiveRulesEnum format;
}
