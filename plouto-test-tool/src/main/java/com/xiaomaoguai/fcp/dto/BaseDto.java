package com.xiaomaoguai.fcp.dto;


import java.io.Serializable;

/**
 * 基础DTO
 *
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class BaseDto implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 1L;

}
