package com.xiaomaoguai.fcp.fcp.controller.xiaomi;

import com.alibaba.fastjson.JSON;
import com.github.xiaomaoguai.xxr.utils.http.HttpClientUtil;
import com.github.xiaomaoguai.xxr.utils.json.JsonUtils;
import com.xiaomaoguai.fcp.fcp.dto.ResponseModel;
import com.xiaomaoguai.fcp.fcp.utils.mi.MiSecurityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @fileName: XiaoMiHelperController.java
 * @author: WeiHui
 * @date: 2018/9/20 13:46
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@RestController
@RequestMapping("/mi")
@Api(value = "小米请求走matrix调用")
public class XiaoMiHelperController {

	@PostMapping("/info")
	@ApiOperation(value = "小米请求matrix")
	public ResponseModel<Object> info(@RequestBody String content) throws Exception {
		log.info("\n===========报文开始============\n{}", JsonUtils.formatJson(content));

		String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCvt1zLpChEIPgQsgA8eMjQFbSm8CVkExGJ3jeCGZykq6JHdwlXibpW56b+44g2yYvir2N8U/OUFB+0JPY62co0nwW145pcexlwIKU07wg8B+cWOAX/fXUoq2/0AeMuBL2OSd4HF3kbuLpLqDSl8M/EIkV7qHOQup7g3qRAv9B56wIDAQAB";
		Map<String, String> request = new HashMap<>(10);
		request.put("appId", "12345");
		request.put("requestId", "1877381763824616454");
		request.put("method", "mi.user.apply.query");
		request.put("version", "1.0");
		request.put("timestamp", "1519374500000");
		request.put("compressed", "false");

		String aesKey = MiSecurityUtils.generate128AesKey();
		String aesKeyEncrypt = MiSecurityUtils.encryptForKey(aesKey, publicKey);

		request.put("key", aesKeyEncrypt);
		request.put("params", MiSecurityUtils.encryptForBizParam(content, aesKey));
		request.put("sign", MiSecurityUtils.doSign(request, "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAK+3XMukKEQg+BCyADx4yNAVtKbwJWQTEYneN4IZnKSrokd3CVeJulbnpv7jiDbJi+KvY3xT85QUH7Qk9jrZyjSfBbXjmlx7GXAgpTTvCDwH5xY4Bf99dSirb/QB4y4EvY5J3gcXeRu4ukuoNKXwz8QiRXuoc5C6nuDepEC/0HnrAgMBAAECgYBeooJ3qpGFH7x6QWOfLe35I7eHsQ5yN1LazbNXfjO+/DEKDDIb0zFoXJqz7BcQarvL+mm6K/yjY7MvcjiquWEMzfjCLJ3ct71d21cEPCKL+GhhxkkCMLgIPGL3gMahj7+PKPxMg0slqB12lnfyRMkmqG3DAMrlX1+40MZpRLtWAQJBAP2oTkXbGmrteZMDntX5/NnT4CecsCyXTJQw/olGvLf+OmwiC1rxGNvBLBZFUtnt+92zkDQ/ukgnYj1gVZHRgjMCQQCxVsn8NY72OFbs3ty/awaV59oMYzbCZxQcCeWpXUvFMjjhmMf7fUUaOTDVIwSqS4jHUrUJzXB7tq5SW94aAKFpAkEA0KPpjggEmMMwZr5pnbN8SWe/TpXRSzw/3vzVwPCQNf9E6sCt+mWpfshLjR4EYgEgVGFoUbHBdav/YGCmwcREhQJAcVMc0GcJbJ6kLcpdFpW2J6V52eCs5Z4pY6GPeaN9AA8P+lo/sZ5jwlY5tnoiSRDr3zQjLJh0ARrEWy0JyWQNwQJBANaCgigdxNQEd+/9Xb89BlLoBvbHqsnCJF3fDRPE0+bdLfYG42D4MqzgXPHjJy8ZT190Y+j1nssgsMZjIU84uII="));

		String jsonString = JSON.toJSONString(request);

		log.info("====>请求参数:{}", jsonString);

		String post = HttpClientUtil.doPost("http://10.253.102.240:9999/SZXXR/xiaoMi/credit/creditApply", jsonString);
		log.info("====>请求结果：{}", post);
		return new ResponseModel<>(post);
	}

}
