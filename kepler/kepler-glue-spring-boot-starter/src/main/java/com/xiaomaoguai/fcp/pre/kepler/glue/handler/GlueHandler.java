package com.xiaomaoguai.fcp.pre.kepler.glue.handler;

import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/20 16:54
 * @since JDK 1.8
 */
public interface GlueHandler {

	/**
	 * 业务处理
	 *
	 * @param params 参数
	 * @return 返回值
	 */
	Object handle(Map<String, Object> params);

}
