package com.xiaomaoguai.fcp.pre.kepler.ons.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @fileName: OnsSender.java
 * @author: WeiHui
 * @date: 2019/2/27 20:54
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface OnsSender {

	/**
	 * 主题
	 *
	 * @return 主题
	 */
	String topic();

	/**
	 * 业务
	 *
	 * @return 业务
	 */
	String tags() default "*";

	/**
	 * ons延迟消息延迟时间 秒
	 *
	 * @return 延迟时间
	 */
	long delaySeconds() default 0;

}
