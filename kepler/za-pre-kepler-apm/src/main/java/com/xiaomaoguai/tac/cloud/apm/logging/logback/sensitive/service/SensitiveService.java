package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 17:07
 * @since JDK 1.8
 */
public interface SensitiveService {

	/**
	 * 脱敏功能
	 *
	 * @param source 脱敏原始数据
	 * @return 脱敏之后的数据
	 */
	String filterMessage(String source);

}
