package com.xiaomaoguai.fcp.pre.kepler.ons.test;

import com.aliyun.openservices.ons.api.Message;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.SendResult;

import java.util.Properties;

/**
 * @fileName: ProducerTest.java
 * @author: WeiHui
 * @date: 2019/2/26 19:35
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ProducerTest {

	public static void main(String[] args) {
		Properties properties = new Properties();
		properties.setProperty(PropertyKeyConst.GROUP_ID, "PID-D-TEST-KEPLER-190226");
		properties.setProperty(PropertyKeyConst.AccessKey, "LTAIpGP7VrW41aZo");
		properties.setProperty(PropertyKeyConst.SecretKey, "0tsiVNTlzHSTT2z0nx3fAOLbULmRLY");
		properties.setProperty(PropertyKeyConst.ONSAddr, "http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal");
		Producer producer = ONSFactory.createProducer(properties);
		producer.start();

		String body = "{ddddddddddddddd}";
		Message msg = new Message(
				"D-TEST-KEPLER-TEST-KEPLER-190226",
				"*",
				body.getBytes());
		msg.setStartDeliverTime(System.currentTimeMillis() + 5000);

		SendResult sendResult = producer.send(msg);
		System.out.println(sendResult);
	}
}
