package com.github.xiaomaoguai.xxr.dto.resp;

import com.github.xiaomaoguai.xxr.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class PublicRespDTO extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 8217590554128616496L;

	/**
	 * 返回码
	 */
	private String respCode;

	/**
	 * 返回码
	 */
	private String respMessage;

	/**
	 * 请求流水号
	 */
	private String reqNo;

	/**
	 * 返回加签内容
	 */
	private String sign;

	/**
	 * 返回内容
	 */
	private String respContent;

	/**
	 * 返回时间
	 */
	private Date respDate;
}
