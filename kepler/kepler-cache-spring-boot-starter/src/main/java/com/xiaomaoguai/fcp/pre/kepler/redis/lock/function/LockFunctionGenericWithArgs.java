package com.xiaomaoguai.fcp.pre.kepler.redis.lock.function;

/**
 * 使用锁执行方法的时候，使用的回调函数
 * <ul>
 * <li>回调函数有返回值
 * <li>回调函数有入参
 * </ul>
 *
 * @param <T> 泛型
 * @author Jerry.Chen
 * @since 2018年5月9日 下午5:02:32
 */
public interface LockFunctionGenericWithArgs<T> extends BaseLockFunction {
	/**
	 * 获取锁成功的时候执行
	 *
	 * @param args 入参
	 * @return 返回值
	 */
	T onLockSuccess(Object... args);

	/**
	 * 获取锁失败的时候执行
	 *
	 * @param args 入参
	 * @return 返回值
	 */
	T onLockFailed(Object... args);
}
