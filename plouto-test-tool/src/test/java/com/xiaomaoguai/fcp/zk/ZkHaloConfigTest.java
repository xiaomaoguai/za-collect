package com.xiaomaoguai.fcp.zk;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaomaoguai.fcp.pre.kepler.zk.utils.ZookeeperContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.CuratorFramework;
import org.junit.Before;
import org.junit.Test;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2019/11/25 22:12
 * @since JDK 1.8
 */
public class ZkHaloConfigTest {

	@Before
	public void beforeTest() {
		CuratorFramework curatorFramework = ZkEnum.TEST.getCuratorFramework();
		ZookeeperContext.setClient(curatorFramework);
	}

	@Test
	public void changeNewHaloConfig() {
		try {
			String path = "/product/MSXJZX/halo";
			String haloData = ZookeeperContext.getData(path);
			JSONObject jsonObject = JSON.parseObject(haloData);

			updateConfig(jsonObject, "requestConfigs");
			updateConfig(jsonObject, "onsConfigs");

			String updateConfig = jsonObject.toJSONString();

			ZookeeperContext.createOrUpdateData(path, updateConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateConfig(JSONObject jsonObject, String nodeName) {
		JSONArray requestConfigs = jsonObject.getJSONArray(nodeName);
		for (int i = 0; i < requestConfigs.size(); i++) {
			String serverUrl = requestConfigs.getJSONObject(i).getString("serverUrl");
			requestConfigs.getJSONObject(i).put("enable", "on");
			requestConfigs.getJSONObject(i).put("dispatcherUrl", serverUrl);
			requestConfigs.getJSONObject(i).put("serverUrl", "http://8058-fcp-prebiz-za-pre-kepler-halo.test.za.biz/invoke");
		}
	}

	@Test
	public void rollbackHaloConfig() {
		try {
			String path = "/product/FQL2/halo";
			String serverUrl = null;

			String haloData = ZookeeperContext.getData(path);
			JSONObject jsonObject = JSON.parseObject(haloData);

			updateConfig2(serverUrl, jsonObject, "requestConfigs");
			updateConfig2(serverUrl, jsonObject, "onsConfigs");

			String updateConfig = jsonObject.toJSONString();

			ZookeeperContext.createOrUpdateData(path, updateConfig);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateConfig2(String serverUrl, JSONObject jsonObject, String nodeName) {
		JSONArray requestConfigs = jsonObject.getJSONArray(nodeName);
		for (int i = 0; i < requestConfigs.size(); i++) {
			String dispatcherUrl = requestConfigs.getJSONObject(i).getString("dispatcherUrl");
			requestConfigs.getJSONObject(i).remove("enable");
			requestConfigs.getJSONObject(i).remove("dispatcherUrl");
			requestConfigs.getJSONObject(i).put("serverUrl", StringUtils.defaultIfBlank(serverUrl, dispatcherUrl));
		}
	}

}
