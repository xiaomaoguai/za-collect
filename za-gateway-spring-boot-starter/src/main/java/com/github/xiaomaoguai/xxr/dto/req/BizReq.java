package com.github.xiaomaoguai.xxr.dto.req;

import com.github.xiaomaoguai.xxr.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * 业务参数基类
 *
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class BizReq extends BaseDto {

	private static final long serialVersionUID = 2064653996176857491L;

	/**
	 * 用户ID
	 */
	private String thirdUserNo;

	/**
	 * 客户端
	 */
	private String client;

	/**
	 * ip地址
	 */
	private String clientIP;

	/**
	 * mac地址
	 */
	private String clientMAC;

	/**
	 * 请求token
	 */
	private String token;

	/**
	 * 请求时间
	 */
	private String reqDate;
}
