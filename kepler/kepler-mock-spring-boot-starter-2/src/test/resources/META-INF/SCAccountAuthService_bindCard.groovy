package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.account.api.response.BindCardAuthorizedResponse
import com.xiaomaoguai.account.common.beans.ResultBase
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:49
 * @since JDK 1.8
 */
class SCAccountAuthService_bindCard implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        BindCardAuthorizedResponse bindCardAuthorizedResponse = new BindCardAuthorizedResponse()
        bindCardAuthorizedResponse.setId(0L)
        bindCardAuthorizedResponse.setCaId(0L)
        bindCardAuthorizedResponse.setCardNo(params.get("cardNo"))
        bindCardAuthorizedResponse.setPayChannelId("10086")
        bindCardAuthorizedResponse.setChannelId(params.get("channelId"))
        bindCardAuthorizedResponse.setSubChannelId(params.get("subChannelId"))
        bindCardAuthorizedResponse.setBizCat("")
        bindCardAuthorizedResponse.setCertiNo(params.get("certiNo"))
        bindCardAuthorizedResponse.setCertiType("")
        bindCardAuthorizedResponse.setCertiName("")
        bindCardAuthorizedResponse.setPreStayPhone("")
        bindCardAuthorizedResponse.setCardCategory("")
        bindCardAuthorizedResponse.setBankCode("mockCode")
        bindCardAuthorizedResponse.setBankName(params.get("openName"))
        bindCardAuthorizedResponse.setBindStatus((byte)1)
        bindCardAuthorizedResponse.setEacctNo("")
        bindCardAuthorizedResponse.setEacctExtra("")
        bindCardAuthorizedResponse.setExtraInfo("")
        bindCardAuthorizedResponse.setSingleQuota(0L)
        bindCardAuthorizedResponse.setDayQuota(0L)
        bindCardAuthorizedResponse.setMerchantNo("10086-1")
        bindCardAuthorizedResponse.setAgreementInfo("哈哈哈")
        bindCardAuthorizedResponse.setCreditInfo("")
        bindCardAuthorizedResponse.setAgreementShare("")
        bindCardAuthorizedResponse.setChannelResCode("")
        bindCardAuthorizedResponse.setChannelResMsg("")
        return ResultBase.success(bindCardAuthorizedResponse)
    }

}
