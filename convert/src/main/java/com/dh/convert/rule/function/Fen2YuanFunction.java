package com.dh.convert.rule.function;

import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.alibaba.fastjson.util.TypeUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorRuntimeJavaType;

/**
 * 
 * 类Fen2YuanFunction.java的实现描述：fen2yuan
 * @author chenwenjing001 2018年11月20日 下午1:48:04
 */
public class Fen2YuanFunction extends AbstractFunction {

	@Override
	public String getName() {
		return "fen2yuan";
	}

	@Override
	public AviatorObject call(Map<String, Object> env, AviatorObject arg1) {
		String amount = TypeUtils.castToString(arg1.getValue(env));
		if (StringUtils.isBlank(amount)) {
            return new AviatorRuntimeJavaType(BigDecimal.ZERO);
        }
		BigDecimal v = new BigDecimal(amount);
		return new AviatorRuntimeJavaType(v.movePointLeft(2).setScale(2, BigDecimal.ROUND_DOWN));
	}
}
