package com.xiaomaoguai.fcp.fcp.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @fileName: BankList.java
 * @author: WeiHui
 * @date: 2018/9/12 21:57
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class BankList {

	private static final long serialVersionUID = 1670658730820896735L;

	private List<String> bankInfos;

	@Override
	public String toString() {
		return "BankList{" +
				"bankInfos=" + bankInfos +
				'}';
	}
}
