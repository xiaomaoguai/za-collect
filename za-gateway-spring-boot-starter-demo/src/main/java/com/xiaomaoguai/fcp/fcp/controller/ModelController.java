
package com.xiaomaoguai.fcp.fcp.controller;

import com.xiaomaoguai.fcp.fcp.dto.ResponseModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 主要展示参数、返回值的定义
 *
 * @author 暮光：城中城
 * @since 2018年8月8日
 */
@Api(value = "参数结果集展示控制器", tags = "参数结果集展示控制器")
@RestController
@RequestMapping("/model/user")
public class ModelController {

	@PostMapping("/info")
	@ApiOperation(value = "通过ID和类型获取用户信息", response = UserInfoVo.class)
	public ResponseModel<UserInfoVo> info(UserIdParam param) {
		UserInfoVo userInfoVo = new UserInfoVo("张三", 18, 1);
		userInfoVo.setSchoolInfo(new UserInfoVo.SchoolInfo("四川工程职业技术学院", "德阳市泰山南路二段801号", "0838-2651110"));
		return new ResponseModel<>(userInfoVo);
	}

}
