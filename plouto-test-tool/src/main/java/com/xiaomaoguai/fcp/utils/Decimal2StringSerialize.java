package com.xiaomaoguai.fcp.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * @fileName: Decimal2StringSerialize.java
 * @author: WeiHui
 * @date: 2018/10/23 14:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class Decimal2StringSerialize extends JsonSerializer<BigDecimal> {

	public Decimal2StringSerialize() {
	}

	@Override
	public void serialize(BigDecimal value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
		gen.writeString(value.toString());
	}
}
