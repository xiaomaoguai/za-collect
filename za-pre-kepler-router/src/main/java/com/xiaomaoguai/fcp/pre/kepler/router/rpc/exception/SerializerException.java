package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class SerializerException extends RuntimeException {

	private static final long serialVersionUID = 5708247333177966727L;
	private String serializerType;

	public SerializerException(String serializerType,Throwable cause) {
		super(String.format("%s 序列化/反序列化异常", serializerType),cause);
	}

	public SerializerException(String serializerType, String message) {
		super(String.format("%s %s", serializerType,message));
	}

	public SerializerException(String serializerType, String message, Throwable cause) {
		super(String.format("%s %s", serializerType,message), cause);
	}

	public String getSerializerType() {
		return this.serializerType;
	}

	public void setSerializerType(String serializerType) {
		this.serializerType = serializerType;
	}

}
