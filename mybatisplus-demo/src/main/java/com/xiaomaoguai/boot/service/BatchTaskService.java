package com.xiaomaoguai.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaoguai.boot.entity.BatchTask;

/**
 * <p>
 * 批量任务信息表 服务类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
public interface BatchTaskService extends IService<BatchTask> {

}
