package com.xiaomaoguai.fcp.pre.kepler.glue.utils;


import com.xiaomaoguai.fcp.pre.kepler.glue.exception.GlueException;

/**
 * AssertUtils.
 *
 * @author xiaoyu
 */
public final class AssertUtils {

	private AssertUtils() {

	}

	/**
	 * Not null.
	 *
	 * @param obj the obj
	 */
	public static void notNull(final Object obj) {
		if (obj == null) {
			throw new GlueException("argument invalid,Please check");
		}
	}

}
