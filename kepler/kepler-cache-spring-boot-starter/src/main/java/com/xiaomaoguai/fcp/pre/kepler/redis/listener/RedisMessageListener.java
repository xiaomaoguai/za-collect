package com.xiaomaoguai.fcp.pre.kepler.redis.listener;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/4 15:33
 * @since JDK 1.8
 */
public interface RedisMessageListener<T> {

	/**
	 * redis监听-处理消息类
	 *
	 * @param t 消息实体
	 */
	void handleMessage(T t);

}
