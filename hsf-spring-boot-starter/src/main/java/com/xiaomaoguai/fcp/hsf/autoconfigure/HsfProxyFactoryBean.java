package com.xiaomaoguai.fcp.hsf.autoconfigure;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.FactoryBean;

/**
 * @fileName: HsfProxyFactoryBean.java
 * @author: WeiHui
 * @date: 2018/7/14 15:16
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class HsfProxyFactoryBean extends HsfClientInterceptor implements FactoryBean<Object> {

	private Object serviceProxy;

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		this.serviceProxy = new ProxyFactory(getServiceInterface(), this).getProxy(getBeanClassLoader());
	}

	@Override
	public Object getObject() {
		return this.serviceProxy;
	}

	@Override
	public Class<?> getObjectType() {
		return getServiceInterface();
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
