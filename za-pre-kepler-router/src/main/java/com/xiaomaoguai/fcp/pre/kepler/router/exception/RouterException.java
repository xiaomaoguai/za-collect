package com.xiaomaoguai.fcp.pre.kepler.router.exception;

import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.HandleResult;

public class RouterException extends RuntimeException {

	private final static String MESSAGE = "router pipeline exception";

	private static final long serialVersionUID = -7581801670826094982L;

	private HandlerContext handlerContext;
	
	
	public RouterException() {
	}

	public RouterException(HandlerContext handlerContext, Throwable cause) {
		super(MESSAGE, cause);
		handlerContext.setHandlerResult(HandleResult.FAIL);
		this.handlerContext = handlerContext;
	}
	
	public RouterException(HandlerContext handlerContext,String message, Throwable cause) {
		super(message, cause);
		handlerContext.setHandlerResult(HandleResult.FAIL);
		this.handlerContext = handlerContext;
	}
	public RouterException(String message, Throwable cause) {
		super(message, cause);
	}

	public RouterException(String message) {
		super(message);
	}

	public RouterException(Throwable cause) {
		super(cause);
	}

	public HandlerContext getHandlerContext() {
		return handlerContext;
	}
	
	public RouterException clear(){
		this.handlerContext = null;
		return this;
	}
}
