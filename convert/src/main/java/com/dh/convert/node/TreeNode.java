package com.dh.convert.node;

import java.lang.reflect.Field;
import java.util.Map;

import com.dh.convert.mapping.BeanMappingObject;
import com.fasterxml.jackson.databind.JsonNode;

public interface TreeNode {

	public TreeNode bulid(Field f);
	public TreeNode setParent(TreeNode parentNode); 
	public TreeNode getNode(String path);
	public Class<?> getCurrentClass();
	public String getNodeName();
	public Object getRootObject();
	public Object genObject(JsonNode jn,Object targetObject,Object currentObject,Map<String,Object> context) throws InstantiationException, IllegalAccessException ;
	public boolean setValue(Object o, Object value);
	public void bulidMappingNode(String path,BeanMappingObject bmo);
	public Field getF();
	public void setCurrentClass(Class<?> currentClass) ;
	public void isArray(boolean _isArray);
	public void isColltion(boolean _isColltion) ;
	public void isCommon(boolean _isCommon);
	public void isObject(boolean _isObject) ;
	public void isMap(boolean _isMap);
	public void isEnum(boolean _isEnum);
	public boolean isEnum();
	public void isCollectionMapping(boolean isCollectionMapping);
	public boolean isCollectionMapping();
	public Object writeValue(JsonNode jn, BeanMappingObject bmo,Object value);
	public Object getTargetValue(JsonNode jn, BeanMappingObject bmo, Object targetObject,
			Map<String, Object> context, Integer num);
	public void setSrcPath(String srcPath);
	public String getSrcPath();
	public void setSrcListName(String srcListName);
	public String getSrcListName();
	public void setNodeName(String nodeName);
	public TreeNode setEntryClass(Class<?> cls);
	public TreeNode setChlidNode(String path,BeanMappingObject bmo);
	public boolean isNestIng();
	public void isNestIng(boolean _isNestIng);
	public boolean isContext();
}
