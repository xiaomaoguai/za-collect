package com.xiaomaoguai.fcp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;


/**
 * 统一返回
 *
 * @fileName: ResultModel.java
 * @author: WeiHui
 * @date: 2018/8/14 16:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ResponseModel<T> extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 7255476495391169021L;

	public static final int NO_LOGIN = -1;

	public static final int SUCCESS = 0;

	public static final int CHECK_FAIL = 1;

	public static final int NO_PERMISSION = 2;

	public static final int UNKNOWN_EXCEPTION = -99;

	/**
	 * 结果实体
	 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private T data;

	/**
	 * 结果码
	 */
	private int code = SUCCESS;

	/**
	 * 结果描述
	 */
	private String msg;

	public ResponseModel() {
		super();
	}

	public ResponseModel(T data) {
		super();
		this.data = data;
	}

	public ResponseModel(Throwable e) {
		super();
		this.msg = e.toString();
		this.code = UNKNOWN_EXCEPTION;
	}

}
