package com.xiaomaoguai.boot.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaomaoguai.boot.entity.CreditApply;
import com.xiaomaoguai.boot.service.CreditApplyService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Collection;

/**
 * <p>
 * 授信申请表 前端控制器
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-04
 */
@Controller
@RequestMapping("/creditApply")
public class CreditApplyController {

    @Resource
    private CreditApplyService creditApplyService;

    @RequestMapping("/id")
    @ResponseBody
    public Collection<CreditApply> id() {
        return creditApplyService.list(new QueryWrapper<CreditApply>()
                .lambda().eq(CreditApply::getProductCode, "51YND")
                .and(e -> e.like(CreditApply::getUserName, "张")));
    }

}

