/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class OrderMapping extends User {

	//订单号
	private String orderNo;

	//订单状态
	private Integer status;

	private Date gmtCreated;

	private String creator;

	private Date gmtModified;

	private String modifier;

	private String isDeleted;

}