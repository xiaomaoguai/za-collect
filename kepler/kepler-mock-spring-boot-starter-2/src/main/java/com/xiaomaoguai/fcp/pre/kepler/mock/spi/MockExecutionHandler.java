package com.xiaomaoguai.fcp.pre.kepler.mock.spi;

import com.xiaomaoguai.fcp.pre.kepler.mock.utils.FeignMethodInvocation;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/15 14:44
 * @since JDK 1.8
 */
public interface MockExecutionHandler {

	/**
	 * 执行
	 *
	 * @param feignMethodInvocation 方法
	 * @param params 执行参数
	 * @return {@link Object}
	 */
	Object execute(FeignMethodInvocation feignMethodInvocation, Object[] params);

}
