package com.dh.convert.rule.validator;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

@Validator("Phone")
public class PhoneValidator extends AbstractValidator<String>{

	private static final Pattern PHONE_REG = Pattern.compile("^(13[0-9]|14[5-9]|15[0-3,5-9]|16[56]|17[0-8]|18[0-9]|19[89])\\d{8}$");

	private String regexp;

    @Override
    public void initialize() {
    }

    @Override
    public boolean isValid(String value) {
        if (StringUtils.isBlank(value)) {
            return true;
        }
        if(StringUtils.isBlank(regexp)) {
            return PHONE_REG.matcher(value).matches();
        }else{
        	return Pattern.compile(regexp).matcher(value).matches();
        }
    }
    
}