/*
 * 文件名：${module}ServiceImpl.java
 * 版本信息:v1.0.0
 * 日期：${createdTime} 
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */
package com.billionsfinance.crs.service.impl.${moduleType};

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.billionsfinance.crs.dao.${moduleType}.I${module}Dao;
import com.billionsfinance.crs.model.${moduleType}.${module};
import com.billionsfinance.crs.service.${moduleType}.I${module}Service;
import com.billionsfinance.crs.model.ResponseModel;
import com.billionsfinance.crs.model.PageResult;
import com.billionsfinance.crs.service.impl.BaseServiceImpl;
import com.billionsfinance.crs.request.PageRequest;


/**
* @ FileName: ${module}ServiceImpl.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
@Service("${lowModule}Service")
public class ${module}ServiceImpl extends BaseServiceImpl<${module}> implements I${module}Service{
	
	@Resource
	private I${module}Dao ${lowModule}Dao;

	@Override
	public List<${module}> find${module}List(${module} ${lowModule}) throws Exception{
		return ${lowModule}Dao.find${module}List(${lowModule});
	}
	
	@Override
	public PageResult<${module}> select${module}Set(${module} ${lowModule}, PageRequest pageV0) throws Exception{
		return  ${lowModule}Dao.select${module}Set(${lowModule},pageV0);
	}
	
    @Override
    public ResponseModel  createOrUpdate(${module} ${lowModule}) throws Exception{
    	ResponseModel responseModel=new ResponseModel();
    	this.setSaveOrUpdateInfo(${lowModule});
        responseModel.setNum(${lowModule}.getUuid() == null?${lowModule}Dao.create${module}(${lowModule}):${lowModule}Dao.update${module}(${lowModule}));
        responseModel.setObject(${lowModule});
        return responseModel;
    }
	
    @Override
    public int  createOrUpdate${module}(${module} ${lowModule}) throws Exception{
    	this.setSaveOrUpdateInfo(${lowModule});
        return ${lowModule}.getUuid() == null?${lowModule}Dao.create${module}(${lowModule}):${lowModule}Dao.update${module}(${lowModule});
    }

    @Override
    public ${module} read${module}ById(String uuId) throws Exception{
        return ${lowModule}Dao.read${module}ById(uuId);
    }

    @Override
    public int delete${module}ById(String uuId) throws Exception{
        return ${lowModule}Dao.delete${module}ById(uuId);
    }

    @Override
    public int physicalDelete${module}ById(String uuId) throws Exception{
    	return ${lowModule}Dao.physicalDelete${module}ById(uuId);
    }
  
}
