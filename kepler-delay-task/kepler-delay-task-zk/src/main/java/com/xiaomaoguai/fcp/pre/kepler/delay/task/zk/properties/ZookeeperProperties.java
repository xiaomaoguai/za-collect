package com.xiaomaoguai.fcp.pre.kepler.delay.task.zk.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @fileName: ZookeeperProperties.java
 * @author: dh
 * @date: 2019/1/11 17:30
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = "kepler")
public class ZookeeperProperties {

	@NestedConfigurationProperty
	private Zookeeper zk = new Zookeeper();

}

