package com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums.servlet;

import javax.servlet.Servlet;

/**
 * @author Leibniz.Hu
 * Created on 2017-08-25 12:28.
 */
public class MappingData {

	Servlet servlet = null;

	String servletName;

	String redirectPath;

	public void recycle() {
		servlet = null;
		servletName = null;
		redirectPath = null;
	}

}
