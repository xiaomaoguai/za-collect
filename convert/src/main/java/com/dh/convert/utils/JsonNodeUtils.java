package com.dh.convert.utils;

import org.apache.commons.lang3.StringUtils;

import com.dh.convert.constants.Constants;
import com.fasterxml.jackson.databind.JsonNode;

public class JsonNodeUtils {

	public static JsonNode getJsonNode(JsonNode node, String path, Integer num) {
		if (StringUtils.isBlank(path) || node == null)
			return null;
		if (node.isArray()) {
			if (num != null) {
				node = node.get(num);
				return getJsonNode(node, path, null);
			} else {
				return node;
			}
		}
		int index1 = path.indexOf(Constants.POINT);
		if (index1 == -1) {
			int index2 = path.indexOf(Constants.LINE);
			if (index2 != -1) {
				num = Integer.valueOf(path.substring(index2 + 1));
				path = path.substring(0, index2);
				JsonNode childNode = node.get(path);
				if(childNode!=null)
					return childNode.get(num);
				else
					return null;
			}
			return node.get(path);
		} else {
			String s1 = path.substring(0, index1);
			String s2 = path.substring(index1 + 1);
			int index2 = s1.indexOf(Constants.LINE);
			if (index2 != -1) {
				Integer num1 = Integer.valueOf(s1.substring(index2 + 1));
				s1 = s1.substring(0, index2);
				JsonNode listNode = node.get(s1);
				if(listNode == null)
					return null;
				node = listNode.get(num1);
			} else {
				node = node.get(s1);
			}
			return getJsonNode(node, s2, num);
		}
	}
}
