package com.xiaomaoguai.fcp.kepler.config;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 三套环境的众安公钥
 *
 * @author linwusheng 2015年11月10日 下午5:46:11
 */
@Getter
@AllArgsConstructor
public enum ZaEnvEnum {

	/**
	 * 开发环境
	 */
	DEV(
			"dev",
			"http://opengw.daily.xiaomaoguai.com/Gateway.do",
			"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIgHnOn7LLILlKETd6BFRJ0GqgS2Y3mn1wMQmyh9zEyWlz5p1zrahRahbXAfCfSqshSNfqOmAQzSHRVjCqjsAw1jyqrXaPdKBmr90DIpIxmIyKXv4GGAkPyJ/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB",
			"开发环境众安公钥"),

	/**
	 * 测试环境
	 */
	TEST(
			"iTest",
			"http://opengw.daily.xiaomaoguai.com/Gateway.do",
			"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDIgHnOn7LLILlKETd6BFRJ0GqgS2Y3mn1wMQmyh9zEyWlz5p1zrahRahbXAfCfSqshSNfqOmAQzSHRVjCqjsAw1jyqrXaPdKBmr90DIpIxmIyKXv4GGAkPyJ/6FTFY99uhpiq0qadD/uSzQsefWo0aTvP/65zi3eof7TcZ32oWpwIDAQAB",
			"测试环境众安公钥"),

	/**
	 * UAT环境
	 */
	UAT(
			"uat",
			"https://opengw-uat.xiaomaoguai.com/Gateway.do",
			"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDsNVusWhi5ZezrQFBGxZQkg303fp6sVVl8pZZolfmI4gc5KL/OjthrziPZTrvF5RMuOXFpPXvwmQnR9FfdiDIt7ci5fMnG+IwtH7WtE1jYoXugsobFVI9ZD82MvgB/i6M+ZnIBerM//5nfTDiA9f0Hf2BdfYHMOp/6OFePNkb3uQIDAQAB",
			"预发环境众安公钥"),

	/**
	 * 生产环境
	 */
	PRD(
			"prd",
			"https://opengw.xiaomaoguai.com/Gateway.do",
			"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDFNndmLlsi8NYQpvZNK/b6kSjN99lwWnWbAHxfBcBYQHx5mZBR8XkkIajSiYo29f7zmM0eAI8OSo6FY16bSt23RzThd+MvDBQC6axDCgGag5992AVGItU8LtWPBrM6XRbtN3+rjIteKhNDOUbEvp60S9/8uoEfnqekd/nEG9I4mQIDAQAB",
			"生产环境众安公钥");

	private String envCode;

	private String url;

	private String publicKey;

	private String description;

	public static ZaEnvEnum get(String envCode) {
		for (ZaEnvEnum an : ZaEnvEnum.values()) {
			if (null != envCode && envCode.equals(an.envCode)) {
				return an;
			}
		}
		return null;
	}

}
