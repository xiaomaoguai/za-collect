package com.xiaomaoguai.fcp._R360XYX.prd;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2019/11/29 14:24
 * @since JDK 1.8
 */
public class FileUtils2Test {

	//生产解密归集文件
	private static final String KEY4 = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBANqbx/ESFGel864AVWxHiTZL1/NKIZj8QA37+wYvA/C+ZSQk+3w3/GEWi147EQsAanzUIk7nVShRkf5L+A1yoptmc3o8fOQzUGb08yaLhjvh2oVmX1wFGoNbWEHCl1Dmj1N2ZcsXpOro3dnASLCX18G0y8Zm5dEP2AntGFPH3gx/AgMBAAECgYEAhB8su1VbAb25xgsrlbKyih9SmBbQsX6bVBhcAcO+lOztgefcGppXrXILr1rxlP2eXm1zgs6rmB+cpd3SQIZtIXLB963oAaqjCl3BSH899/AUDxEtWbTdOS4ySzR5hLdtGefImy5P9IHSQvSHmb0vEjaxk7qnkHiy1g6oX9qbeZECQQDyhk+nlNKYjhS3YkzFFeVb886JIJxob43zZnl0tQYcZCiJ8CSHJbDDi/xREcD7hh2w+/giIkUiDSbAGBfLbK0ZAkEA5sFI9ZxRDkv2CBrMku5FltdbW2dU3wg8IubYGgtqhjdIs0UjZRBEujn3MQkFE4OZNcEKhfgNeaC6zBA6NXchVwJATwPDX+30zw87YKP+Lxf7KgI9ACBH1R7DONHiMEvqOcXID2jV/dIhzRQGmMMYBNqlQcxWA4j2zSlLvroaxoQZwQJAVYK+5LV+nAuuKqaztKczhohbxG1wWA4XKt/cqRod6YrHGBfA3AzBSzl7zuIGzYpw1iipG6gmZjIXxtMknvd8lQJBAOwxiOSwmQB2XjhwfLV/o/EewV+psjUPqKOs31Frglp5ughZpv8AxaKev33gW3nMrOUru2q7e3lNrq2HiIcD/0E=";

	public static void main(String[] args) {
		com.xiaomaoguai.fcp.utils.FileUtils2.decryptFile(KEY4, "D://back_card_258402268023062.jpg", "D://1.1.jpg");
	}

}
