package com.xiaomaoguai.fcp.pre.kepler.redis.lock.function;

/**
 * 使用锁执行方法的时候，使用的回调函数基类
 *
 * @author Jerry.Chen
 * @since 2018年5月9日 下午5:04:21
 */
public interface BaseLockFunction {
	/**
	 * 尝试获取锁的次数，默认一次
	 *
	 * @return 尝试获取锁的次数
	 */
	default int tryLockTimes() {
		return 1;
	}

	/**
	 * 重复尝试获取锁的时间，休眠时间，默认10ms
	 *
	 * @return 重复尝试获取锁的时间
	 */
	default long tryLockSleepTime() {
		return 10L;
	}
}
