package com.xiaomaoguai.fcp.pre.kepler.tddl;

import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Map;

/**
 * @fileName: MyBatisTest.java
 * @author: WeiHui
 * @date: 2019/3/12 17:48
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class MyBatisTest {

	public static void main(String[] args) {
		MyBatisQueryUtils instance = MyBatisQueryUtils.getInstance();
		SqlSession sqlSession = instance.getSessionFactory().openSession();
		List<Map<String, String>> generateCodeModules = sqlSession.selectList("GenerateCodeModuleMapper.findList", "log");
	}

}
