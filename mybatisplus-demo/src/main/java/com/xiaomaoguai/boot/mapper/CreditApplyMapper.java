package com.xiaomaoguai.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaoguai.boot.entity.CreditApply;

/**
 * <p>
 * 授信申请表 Mapper 接口
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-04
 */
public interface CreditApplyMapper extends BaseMapper<CreditApply> {

}
