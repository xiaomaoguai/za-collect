package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @fileName: SelectListParam.java
 * @author: WeiHui
 * @date: 2018/8/18 18:15
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Setter
@Getter
public class SelectListParam {

	/**
	 * 产品编码
	 */
	@NotBlank
	private String productCode;
	/**
	 * 开始时间
	 */
	private String beginTime;
	/**
	 * 截止时间
	 */
	private String endTime;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 交易类型 credit loan repay
	 */
	@NotBlank
	private String transType;

	@Override
	public String toString() {
		return "SelectListParam [productCode=" + productCode + ", beginTime=" + beginTime + ", endTime=" + endTime
				+ ", status=" + status + "]";
	}

}
