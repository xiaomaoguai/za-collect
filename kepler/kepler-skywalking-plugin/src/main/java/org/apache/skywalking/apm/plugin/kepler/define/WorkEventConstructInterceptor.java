package org.apache.skywalking.apm.plugin.kepler.define;

import org.apache.skywalking.apm.agent.core.context.ContextManager;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.EnhancedInstance;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.InstanceConstructorInterceptor;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/5 22:13
 * @since JDK 1.8
 */
public class WorkEventConstructInterceptor implements InstanceConstructorInterceptor {

	@Override
	public void onConstruct(EnhancedInstance objInst, Object[] allArguments) {
		if (ContextManager.isActive()) {
			objInst.setSkyWalkingDynamicField(ContextManager.capture());
		}
	}

}
