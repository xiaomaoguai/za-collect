package com.xiaomaoguai.fcp.pre.kepler.router.test;

import com.alibaba.fastjson.JSON;
import com.xiaomaoguai.fcp.pre.kepler.router.buf.MapBuf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.AbstractHandler;

public class FirstHandler extends AbstractHandler<String> {


	@Override
	public void beforeHandle(HandlerContext<String> hc) {

	}

	@Override
	public void genInnerParam(HandlerContext<String> hc) {

	}

	@Override
	public void exceptionCaught(RouterException cause) {
	}

	@Override
	public void handle(HandlerContext<String> hc) {
		MapBuf buf = new MapBuf();
		buf.getContext().putAll(JSON.parseObject(hc.getInnerParam()));
		hc.getRouter().setBuf(buf);
	}

}
