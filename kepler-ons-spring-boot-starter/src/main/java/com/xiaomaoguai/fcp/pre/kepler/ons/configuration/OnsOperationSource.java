package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import org.springframework.cache.interceptor.CacheOperation;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @fileName: OnsOperationSource.java
 * @author: WeiHui
 * @date: 2019/2/28 13:57
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface OnsOperationSource {

	/**
	 * Return the collection of cache operations for this method, or {@code null}
	 * if the method contains no <em>cacheable</em> annotations.
	 *
	 * @param method      the method to introspect
	 * @param targetClass the target class (may be {@code null}, in which case
	 *                    the declaring class of the method must be used)
	 * @return all cache operations for this method, or {@code null} if none found
	 */
	@Nullable
	Collection<CacheOperation> getOnsOperations(Method method, @Nullable Class<?> targetClass);
}
