/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.common.config;

import org.springframework.context.annotation.Configuration;

/**
 * 通用服务自动配置
 *
 * @author chenyao
 * @since 2018年8月15日 下午12:47:40
 */
@Configuration
public class CommonAutoConfiguration {
}
