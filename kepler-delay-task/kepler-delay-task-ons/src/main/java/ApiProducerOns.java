//import com.aliyun.openservices.ons.api.PropertyKeyConst;
//
//
///**
// * @fileName: ApiProducerOns.java
// * @author: WeiHui
// * @date: 2019/2/26 15:59
// * @version: v1.0.0
// * @since JDK 1.8
// */
//public class ApiProducerOns {
//
//	public void testProducer() {
//		ProducerConfig config = new ProducerConfig(
//				"ons|http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal"
//				,"D-TEST-KEPLER-TEST-KEPLER-190226",
//				MessagePriorityEnum.MEDIUM);
//
//		// 序列化方式，支持String/byte[]/json/protobuf
//		config.setSerializer(SerializerTypeEnum.JSON);
//		config.put(PropertyKeyConst.ProducerId, "PID-D-TEST-KEPLER-190226");
//		config.put(PropertyKeyConst.AccessKey, "LTAIpGP7VrW41aZo");
//		config.put(PropertyKeyConst.SecretKey, "0tsiVNTlzHSTT2z0nx3fAOLbULmRLY");
//
//		TestMessageBean bean = new TestMessageBean();
//		bean.setName("TestMessageBean");
//		MessageRecord<TestMessageBean> record = new MessageRecord<>();
//		record.setMessage(bean);
//		// 消息标签
//		record.setTag("tag1");
//		// 延迟/定时发送
//		record.setStartDeliverTime(5000);
//		// 用户自定义参数
//		record.putUserProperties("userKey1", "userVal1");
//
//		MqProducer producer = MqClient.buildProducer(config);
//		// 异步
//		producer.sendOneway(record);
//		producer.sendOneway(record);
//		producer.sendOneway(record);
//		producer.sendOneway(record);
//		producer.sendOneway(record);
//		producer.sendOneway(record);
//
//		// 异步回調
////		producer.sendAsync(record, new SendCallback() {
////			@Override
////			public void onSuccess(SendResult sendResult) {
////				System.out.println("Send successful. " + sendResult);
////			}
////
////			@Override
////			public void onException(OnExceptionContext context) {
////				System.out.println("Send failed. message:" + context.getMessageRecord() + ", error:" + context.getException());
////			}
////		});
//	}
//
//	public static void main(String[] args) {
//		ApiProducerOns apiProducerOns = new ApiProducerOns();
//		apiProducerOns.testProducer();
//	}
//
//}
