/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.repay;

import com.xiaomaoguai.fcp.domain.SubRepayApply;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * The Interface SubRepayApplyMapper.
 */
public interface SubRepayApplyMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(SubRepayApply record);

	List<SubRepayApply> selectByCondition(@Param("beginTime") Date beginTime,
										  @Param("endTime") Date endTime,
										  @Param("productCode") String productCode);
}