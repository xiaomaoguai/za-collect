package com.xiaomaoguai.fcp.kepeler.av;

import com.google.common.collect.Maps;
import com.googlecode.aviator.AviatorEvaluator;

import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/29 16:23
 * @since JDK 1.8
 */
public class AvTestMain {

	public static void main(String[] args) {
		Map<String, Object> env = Maps.newHashMapWithExpectedSize(8);
		env.put("policyStatus", "SUCCESS");
		env.put("respCode", "0001");

	//	policyStatus == nil && respCode =='0000' ? 'PROCESSING' :(respCode !='0000'?nil:policyStatus)
		Object result = AviatorEvaluator.execute("policyStatus == nil && respCode =='0000' ? 'PROCESSING' :(respCode !='0000'?nil:policyStatus)", env);
		System.out.println(result);
	}

}
