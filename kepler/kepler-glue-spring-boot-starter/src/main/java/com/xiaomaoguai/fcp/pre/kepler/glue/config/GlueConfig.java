package com.xiaomaoguai.fcp.pre.kepler.glue.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 19:16
 * @since JDK 1.8
 */
@Getter
@Setter
public class GlueConfig {

	/**
	 * repositorySupport.
	 * {@linkplain com.xiaomaoguai.fcp.pre.kepler.glue.enums.RepositorySupportEnum}
	 */
	private String repositorySupport = "zookeeper";

	/**
	 * redis配置
	 */
	@NestedConfigurationProperty
	private GlueRedisConfig glueRedisConfig;

	/**
	 * zk配置
	 */
	@NestedConfigurationProperty
	private GlueZookeeperConfig glueZookeeperConfig;

}
