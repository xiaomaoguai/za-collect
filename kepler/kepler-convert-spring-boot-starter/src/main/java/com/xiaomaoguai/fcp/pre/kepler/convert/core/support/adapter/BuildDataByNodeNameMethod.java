package com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter;

import java.lang.reflect.Method;

/**
 * @fileName: BuildDataByNodeNameMethod.java
 * @author: WeiHui
 * @date: 2018/11/26 10:20
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class BuildDataByNodeNameMethod extends AbstractConvertMethod {

    @Override
    public boolean canHandler(Method method) {
        return "buildDataByNodeName".equals(method.getName()) && method.getParameterCount() == 3;
    }

    @Override
    public Object invoke(Method method, Object[] args) {
        return buildData((String) args[0], args[1], method.getReturnType());
    }

}
