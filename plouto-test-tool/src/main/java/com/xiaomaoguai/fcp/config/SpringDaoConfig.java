package com.xiaomaoguai.fcp.config;

import com.taobao.tddl.client.jdbc.TDataSource;
import com.taobao.tddl.client.sequence.impl.GroupSequence;
import com.taobao.tddl.client.sequence.impl.GroupSequenceDao;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * mysql数据源
 *
 * @author chenwenjing001
 */
@Slf4j
@Configuration
@MapperScan(basePackages = "com.xiaomaoguai.fcp.mapper", sqlSessionFactoryRef = "sqlSessionFactory")
@EnableTransactionManagement(proxyTargetClass = true)
public class SpringDaoConfig {

	@Bean(name = "tDataSource", initMethod = "init")
	@Primary
	public TDataSource tDataSource(Environment env) {
		log.info("appName:" + env.getProperty("tddl.appname"));
		TDataSource tDataSource = new TDataSource();
		tDataSource.setAppName(env.getProperty("tddl.appname"));
		tDataSource.setDynamicRule(true);
		return tDataSource;
	}

	@Bean(name = "sqlSessionFactory")
	@Primary
	public SqlSessionFactory sqlSessionFactory(@Qualifier("tDataSource") TDataSource tDataSource) throws Exception {
		SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
		sqlSessionFactoryBean.setDataSource(tDataSource);
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		Resource[] resources = resolver.getResources("/mapper/*Mapper.xml");
		sqlSessionFactoryBean.setMapperLocations(resources);
		return sqlSessionFactoryBean.getObject();
	}

	@Bean
	@Primary
	public PlatformTransactionManager transactionManager(TDataSource tDataSource) {
		DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
		dataSourceTransactionManager.setDataSource(tDataSource);
		return dataSourceTransactionManager;
	}

	@Bean
	@Primary
	public TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
		TransactionTemplate transactionTemplate = new TransactionTemplate();
		transactionTemplate.setTransactionManager(transactionManager);
		return transactionTemplate;
	}

	/**
	 * Id sequence dao.
	 *
	 * @param env the env
	 * @return the group sequence dao
	 */
	@Bean(initMethod = "init")
	public GroupSequenceDao idSequenceDao(Environment env) {
		GroupSequenceDao groupSequenceDao = new GroupSequenceDao();
		groupSequenceDao.setAppName(env.getProperty("tddl.appname"));
		groupSequenceDao.setDscount(Integer.valueOf(env.getProperty("tddl.seq.dscount")));
		List<String> list = new ArrayList<>();
		list.add(env.getProperty("tddl.dbgroupkey"));
		groupSequenceDao.setDbGroupKeys(list);
//        groupSequenceDao.setInnerStep(2000);
		groupSequenceDao.setAdjust(true);
		groupSequenceDao.setRetryTimes(2);
		groupSequenceDao.setTableName("sequence");
		groupSequenceDao.setNameColumnName("name");
		groupSequenceDao.setValueColumnName("value");
		groupSequenceDao.setGmtModifiedColumnName("gmt_modified");
		return groupSequenceDao;
	}

	/**
	 * 用户卡信息sequence
	 *
	 * @param idSequenceDao
	 * @return
	 */
	@Bean(name = "seqUserCardInfo", initMethod = "init")
	public GroupSequence seqUserCardInfo(GroupSequenceDao idSequenceDao) {
		GroupSequence seqUserCardInfo = new GroupSequence();
		seqUserCardInfo.setSequenceDao(idSequenceDao);
		seqUserCardInfo.setName("user_card_info");
		return seqUserCardInfo;
	}

	/**
	 * user_account_dtl sequence
	 *
	 * @return
	 */
	@Bean(name = "seqUserAccountDtl", initMethod = "init")
	public GroupSequence seqUserAccountDtl(GroupSequenceDao idSequenceDao) {
		GroupSequence seqUserAccountDtl = new GroupSequence();
		seqUserAccountDtl.setSequenceDao(idSequenceDao);
		seqUserAccountDtl.setName("user_account_dtl");
		return seqUserAccountDtl;
	}

	/**
	 * user_state sequence
	 *
	 * @return
	 */
	@Bean(name = "seqUserState", initMethod = "init")
	public GroupSequence seqUserState(GroupSequenceDao idSequenceDao) {
		GroupSequence seqUserState = new GroupSequence();
		seqUserState.setSequenceDao(idSequenceDao);
		seqUserState.setName("user_state");
		return seqUserState;
	}

	/**
	 * credit_apply sequence
	 *
	 * @param idSequenceDao
	 * @return
	 */
	@Bean(name = "seqCreditApply", initMethod = "init")
	public GroupSequence seqCreditApply(GroupSequenceDao idSequenceDao) {
		GroupSequence seqCreditApply = new GroupSequence();
		seqCreditApply.setSequenceDao(idSequenceDao);
		seqCreditApply.setName("credit_apply");
		return seqCreditApply;
	}

	/**
	 * user_loan sequence
	 *
	 * @param idSequenceDao
	 * @return
	 */
	@Bean(name = "seqUserLoan", initMethod = "init")
	public GroupSequence seqUserLoan(GroupSequenceDao idSequenceDao) {
		GroupSequence seq = new GroupSequence();
		seq.setSequenceDao(idSequenceDao);
		seq.setName("user_loan");
		return seq;
	}

	/**
	 * repay_plan sequence
	 *
	 * @param idSequenceDao
	 * @return
	 */
	@Bean(name = "seqRepayPlan", initMethod = "init")
	public GroupSequence seqRepayPlan(GroupSequenceDao idSequenceDao) {
		GroupSequence seq = new GroupSequence();
		seq.setSequenceDao(idSequenceDao);
		seq.setName("repay_plan");
		return seq;
	}

	/**
	 * order_mapping_info_es sequence
	 *
	 * @param idSequenceDao
	 * @return
	 */
	@Bean(name = "seqOrderMapping", initMethod = "init")
	public GroupSequence seqOrderMapping(GroupSequenceDao idSequenceDao) {
		GroupSequence seq = new GroupSequence();
		seq.setSequenceDao(idSequenceDao);
		seq.setName("order_mapping_info_es");
		return seq;
	}

	/**
	 * repay_info sequence
	 *
	 * @param idSequenceDao idSequenceDao
	 * @return GroupSequence
	 */
	@Bean(name = "seqRepayInfo", initMethod = "init")
	public GroupSequence seqRepayInfo(GroupSequenceDao idSequenceDao) {
		GroupSequence seq = new GroupSequence();
		seq.setSequenceDao(idSequenceDao);
		seq.setName("repay_info");
		return seq;
	}

	/**
	 * repay_dtl sequence
	 *
	 * @param idSequenceDao idSequenceDao
	 * @return GroupSequence
	 */
	@Bean(name = "seqRepayDtl", initMethod = "init")
	public GroupSequence seqRepayDtl(GroupSequenceDao idSequenceDao) {
		GroupSequence seq = new GroupSequence();
		seq.setSequenceDao(idSequenceDao);
		seq.setName("repay_dtl");
		return seq;
	}

}
