/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.servlet.constants;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;

/**
 * web mvc常量
 *
 * @author chenyao
 * @since 2019年6月27日 上午11:31:29
 */
public class WebMvcConstants {
    /**
     * web mvc配置前缀
     */
    public static final String CONFIG_PREFIX = CoreConstants.BOOT_CONFIG_PREFIX + ".webmvc";
}
