/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.common;

import com.xiaomaoguai.core.next.web.constants.WebConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 健康检查 Reset API 接口
 *
 * @author chenyao
 * @since 2018年5月17日 上午11:05:58
 */
@ConditionalOnProperty(prefix = WebConstants.CONFIG_PREFIX + ".page.health", name = "enabled", matchIfMissing = true)
@RestController
public class HealthController {
    /**
     * ok页面
     *
     * @return 接口返回信息
     */
    @RequestMapping(path = { "/ok", "/ok.htm" })
    public String ok() {
        return "OK";
    }

    /**
     * health页面
     *
     * @return 接口返回信息
     */
    @RequestMapping("/health")
    public String health() {
        return "OK";
    }
}
