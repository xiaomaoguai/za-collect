/*
 * Copyright (C) 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xiaomaoguai.fcp.pre.kepler.oss.endpoint;

import com.aliyun.oss.model.Bucket;
import com.xiaomaoguai.fcp.pre.kepler.oss.utils.OssClient;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.context.ApplicationContext;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Actuator {@link org.springframework.boot.actuate.endpoint.annotation.Endpoint} to expose OSS Meta Data
 *
 * @author <a href="mailto:fangjian0423@gmail.com">Jim</a>
 */
@Endpoint(id = "oss")
public class OssEndpoint {

	@Resource
	private ApplicationContext applicationContext;

	@ReadOperation
	public Map<String, Object> invoke() {
		Map<String, Object> result = new HashMap<>(16);

		Map<String, OssClient> ossClientMap = applicationContext.getBeansOfType(OssClient.class);
		int size = ossClientMap.size();
		List<Object> ossClientList = new ArrayList<>(size);
		ossClientMap.keySet().forEach(beanName -> {
			Map<String, Object> ossProperties = new HashMap<>();
			OssClient client = ossClientMap.get(beanName);
			ossProperties.put("beanName", beanName);
			ossProperties.put("clientConfiguration", client.getClientConfiguration());
			ossProperties.put("credentials", client.getCredentialsProvider().getCredentials());
			ossProperties.put("bucketList", client.listBuckets().stream().map(Bucket::getName).toArray());
			ossClientList.add(ossProperties);
		});
		result.put("size", size);
		result.put("info", ossClientList);
		return result;
	}

}
