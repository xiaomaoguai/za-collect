package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.dh.mailbox2.api.Actor;
import com.xiaomaoguai.fcp.pre.plouto.mailbox.actor.AsyncActor;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * mail box
 *
 * @fileName: MailBoxesEnums.java
 * @author: WeiHui
 * @date: 2018/8/20 15:10
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum MailBoxesEnums implements IMailBox {

	/**
	 * 通用异步任务
	 */
	ASYNC_CALL(AsyncActor.class, 512, 10),
	;

	/**
	 * actor class
	 */
	private final Class<? extends Actor> clazz;

	/**
	 * bufferSize
	 */
	private final int bufferSize;

	/**
	 * threadSize
	 */
	private final int threadSize;

}
