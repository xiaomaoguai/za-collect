/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper;

import com.xiaomaoguai.fcp.domain.UserAccount;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * The Interface UserAccountMapper.
 */
public interface UserAccountMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(UserAccount record);


	int updateByUnique(UserAccount record);

	/**
	 * Select by unique.
	 *
	 * @param productCode the product code
	 * @param thirdUserNo the third user no
	 * @return the user account
	 */
	UserAccount selectByUnique(@Param("productCode") String productCode, @Param("thirdUserNo") String thirdUserNo);

	/**
	 * Select by user id.
	 *
	 * @param userId the user id
	 * @return the user account
	 */
	UserAccount selectByUserId(Long userId);

	/**
	 * Select by cert no.
	 *
	 * @param productCode the product code
	 * @param certNo      the cert no
	 * @return the long
	 */
	Long selectByCertNo(@Param("productCode") String productCode, @Param("certNo") String certNo);

	UserAccount selectInfoByCertNo(@Param("productCode") String productCode, @Param("certNo") String certNo);

	List<UserAccount> findAllUserAccount();

	/**
	 * 根据thirdUserNo查询用户信息
	 *
	 * @param thirdUserNo 用户的唯一标识
	 * @return 用户信息
	 */
	UserAccount selectByThirdUserNo(String thirdUserNo);

	int deleteByUserId(Long userId);
}