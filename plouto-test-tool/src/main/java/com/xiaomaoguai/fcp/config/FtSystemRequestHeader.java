package com.xiaomaoguai.fcp.config;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @fileName: FtSystemRequestHeader.java
 * @author: WeiHui
 * @date: 2018/10/23 16:08
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class FtSystemRequestHeader implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 2701160232506356570L;

	private static Map<String, String> header = new HashMap<>(16);

	static {
		header.put("Accept", "*/*");
		header.put("Accept-Encoding", "gzip, deflate");
		header.put("Accept-Language", "zh-CN,zh;q=0.9,en;q=0.8");
		header.put("Connection", "keep-alive");
		header.put("X-Requested-With", "XMLHttpRequest");
		header.put("DNT", "1");
		header.put("Host", "ft-test.xiaomaoguaionline.com");
		header.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.67 Safari/537.36");
		header.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		header.put("Origin", "http://10.253.8.229:32360");
		header.put("Referer", "https://ft-test.xiaomaoguaionline.com/creditcore_clear.html");
		header.put("Cookie", "_za_sso_session_=vgTnR3uB4+m2Mep4VtBECP+U/K+1+ZT7VvUr6bVgfgQl+JJ0m4txjw1JN1SnDqjH2bfN/quuE49D2mbwcpsknPkelyHSFQscCTDS3SW62tig72vubxyAqa3jXZMNIJ3sZ13j+5GV7XKGpUi7YHyb5w/u5yToSVda26Il3IEHJlw0eQBKYgI6Gcb7tmX0UHADq2PEA+2zJgZtYYbfQ3rni5dAP2jnUfI1BJFJC+4plpogeGxbq+3P/aF4k3Aja7TZ");
	}

	public static Map<String, String> updateCookie(String cookie) {
		if (StringUtils.isNotBlank(cookie)) {
			header.put("Cookie", cookie);
		}
		return header;
	}

	public static Map<String, String> getHeader() {
		return header;
	}
}
