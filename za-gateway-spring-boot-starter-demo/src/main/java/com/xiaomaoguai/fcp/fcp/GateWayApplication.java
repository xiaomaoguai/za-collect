package com.xiaomaoguai.fcp.fcp;

import com.github.xiaomaoguai.xxr.autoconfigure.EnableZaClients;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableZaClients(basePackages = "com.xiaomaoguai.fcp")
public class GateWayApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GateWayApplication.class, args);
	}

	@Override
	public void run(String... args) {
		System.out.println("http://localhost:8080/document.html");
	}
}
