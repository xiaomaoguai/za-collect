package com.xiaomaoguai.fcp.pre.kepler.router.handler.enums;

public enum BranchType {
	ADD,//新增独立handler
	GOTO,//跳转
	GOTO_LAST,//跳转到尾handler
	ADD_PIPELINE,//新增独立分支
	TERMINATED,//终止
	EXITWITH;//退出前附加任务
}
