package com.xiaomaoguai.boot.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaomaoguai.boot.entity.CreditApply;
import com.xiaomaoguai.boot.mapper.CreditApplyMapper;
import com.xiaomaoguai.boot.service.CreditApplyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 授信申请表 服务实现类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-04
 */
@Service
public class CreditApplyServiceImpl extends ServiceImpl<CreditApplyMapper, CreditApply> implements CreditApplyService {

}
