package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class HttpRequestBulidException extends RuntimeException {


	private static final long serialVersionUID = -7330119926163603951L;
	
	public HttpRequestBulidException(Throwable cause) {
		super(cause);
	}

	public HttpRequestBulidException(String message) {
		super(message);
	}

	public HttpRequestBulidException(String message, Throwable cause) {
		super(message, cause);
	}

}
