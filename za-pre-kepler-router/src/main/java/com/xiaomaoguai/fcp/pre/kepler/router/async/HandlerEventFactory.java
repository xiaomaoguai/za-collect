package com.xiaomaoguai.fcp.pre.kepler.router.async;

import com.lmax.disruptor.EventFactory;

/**
 * AsyncWorkPool ringbuffer的事件工厂
 *
 * @param <O>
 * @author DH
 */
public class HandlerEventFactory<O> implements EventFactory<HandlerEvent<O>> {

	@Override
	public HandlerEvent<O> newInstance() {
		return new HandlerEvent<O>();
	}

}
