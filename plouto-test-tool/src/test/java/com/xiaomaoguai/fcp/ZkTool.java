package com.xiaomaoguai.fcp;

import org.junit.Test;

/**
 * @fileName: ZkTool.java
 * @author: WeiHui
 * @date: 2018/11/15 21:10
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ZkTool extends AbstractZkTool {

	private String path = "D:\\workspace\\git\\za-collect\\plouto-test-tool\\src\\test\\resources\\";

	@Override
	String setUpYourLocalPath() {
		return path;
	}

	@Test
	public void testCreateZkNode() {
		createZkNode("/plouto/convert/SZXXR.fql.loan.withholding.out");
	}

	@Test
	public void testLoanRiskInfo() {
		this.node = "/plouto/convert/SZXXR.default.extra.loan.riskInfo";
	}

	@Test
	public void testSJGJLoanRiskInfo() {
		this.node = "/plouto/convert/SJGJ.default.extra.loan.riskInfo";
	}

	@Test
	public void testFQLQueryRepaymentPlan() {
		this.node = "/plouto/convert/SZXXR.fql.loan.queryRepaymentPlan.in";
	}

	@Test
	public void testFQLWithholdingResultIn() {
		this.node = "/plouto/convert/SZXXR.fql.loan.withholding.in";
	}

	@Test
	public void testFQLWithholdingResultOut() {
		this.node = "/plouto/convert/SZXXR.fql.loan.withholding.out";
	}

	@Test
	public void testFQLQueryWithholdingResultIn() {
		this.node = "/plouto/convert/SZXXR.fql.loan.queryWithholdingResult.in";
	}

	@Test
	public void testFQLQueryWithholdingResultOut() {
		this.node = "/plouto/convert/SZXXR.fql.loan.queryWithholdingResult.out";
	}

	@Test
	public void downOne() {
		String node = "SZXXR.fql.loan.queryWithholdingResult.in";
		downOne(node);
	}

	@Test
	public void downAll() {
		downZKAll();
	}

}
