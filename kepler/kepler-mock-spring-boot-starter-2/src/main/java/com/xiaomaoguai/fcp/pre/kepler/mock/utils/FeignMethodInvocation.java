package com.xiaomaoguai.fcp.pre.kepler.mock.utils;

import com.xiaomaoguai.fcp.pre.kepler.mock.dc.FeignMethodParamInfo;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/15 20:41
 * @since JDK 1.8
 */
public class FeignMethodInvocation {

	private Class<?> invocationClass;

	private String className;

	private String methodName;

	private Class<?> returnType;

	private Class<?>[] parameterTypes;

	private FeignMethodParamInfo[] methodParamInfos;

	/**
	 * Getter for property 'invocationClass'.
	 *
	 * @return Value for property 'invocationClass'.
	 */
	public Class<?> getInvocationClass() {
		return invocationClass;
	}

	/**
	 * Setter for property 'invocationClass'.
	 *
	 * @param invocationClass Value to set for property 'invocationClass'.
	 */
	public void setInvocationClass(final Class<?> invocationClass) {
		this.invocationClass = invocationClass;
	}

	/**
	 * Getter for property 'className'.
	 *
	 * @return Value for property 'className'.
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Setter for property 'className'.
	 *
	 * @param className Value to set for property 'className'.
	 */
	public void setClassName(final String className) {
		this.className = className;
	}

	/**
	 * Getter for property 'methodName'.
	 *
	 * @return Value for property 'methodName'.
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * Setter for property 'methodName'.
	 *
	 * @param methodName Value to set for property 'methodName'.
	 */
	public void setMethodName(final String methodName) {
		this.methodName = methodName;
	}

	/**
	 * Getter for property 'returnType'.
	 *
	 * @return Value for property 'returnType'.
	 */
	public Class<?> getReturnType() {
		return returnType;
	}

	/**
	 * Setter for property 'returnType'.
	 *
	 * @param returnType Value to set for property 'returnType'.
	 */
	public void setReturnType(final Class<?> returnType) {
		this.returnType = returnType;
	}

	/**
	 * Getter for property 'parameterTypes'.
	 *
	 * @return Value for property 'parameterTypes'.
	 */
	public Class<?>[] getParameterTypes() {
		return parameterTypes;
	}

	/**
	 * Setter for property 'parameterTypes'.
	 *
	 * @param parameterTypes Value to set for property 'parameterTypes'.
	 */
	public void setParameterTypes(final Class<?>[] parameterTypes) {
		this.parameterTypes = parameterTypes;
	}

	/**
	 * Getter for property 'methodParamInfos'.
	 *
	 * @return Value for property 'methodParamInfos'.
	 */
	public FeignMethodParamInfo[] getMethodParamInfos() {
		return methodParamInfos;
	}

	/**
	 * Setter for property 'methodParamInfos'.
	 *
	 * @param methodParamInfos Value to set for property 'methodParamInfos'.
	 */
	public void setMethodParamInfos(final FeignMethodParamInfo[] methodParamInfos) {
		this.methodParamInfos = methodParamInfos;
	}

}
