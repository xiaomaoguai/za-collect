package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.impl;

import com.google.common.collect.Lists;
import com.xiaomaoguai.datasecure.common.enums.SensitiveRulesEnum;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.dto.SensitiveRulesVo;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.SensitiveRulesManageService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 14:12
 * @since JDK 1.8
 */
public class SensitiveRulesManageServiceImpl implements SensitiveRulesManageService {

	private static final List<SensitiveRulesVo> SENSITIVE_RULES = new ArrayList<>();

	/**
	 * 初始化脱敏规则
	 */
	@PostConstruct
	public void init() {
		SensitiveRulesEnum[] sensitiveRulesEnums = SensitiveRulesEnum.values();
		List<SensitiveRulesVo> sensitiveRulesVos = Lists.newArrayListWithCapacity(sensitiveRulesEnums.length);

		Arrays.stream(sensitiveRulesEnums).forEach(sre -> {
			SensitiveRulesVo sensitiveRulesVo = new SensitiveRulesVo();
			sensitiveRulesVo.setName(sre.name());
			sensitiveRulesVo.setDataField(sre.getDataField());
			sensitiveRulesVo.setDataType(sre.getDataType());
			sensitiveRulesVos.add(sensitiveRulesVo);
		});

		SENSITIVE_RULES.addAll(sensitiveRulesVos);
	}

	/**
	 * 获取所有脱敏规则
	 *
	 * @return 脱敏规则列表
	 */
	@Override
	public List<SensitiveRulesVo> getAllSensitiveRules() {
		return SENSITIVE_RULES;
	}

}
