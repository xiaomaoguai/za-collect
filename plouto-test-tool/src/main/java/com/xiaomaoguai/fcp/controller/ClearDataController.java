package com.xiaomaoguai.fcp.controller;

import com.xiaomaoguai.fcp.config.FtSystemRequestHeader;
import com.xiaomaoguai.fcp.dto.ResponseModel;
import com.xiaomaoguai.fcp.dto.req.ClearDataReq;
import com.xiaomaoguai.fcp.dto.resp.ClearDataResp;
import com.xiaomaoguai.fcp.service.ClearDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @fileName: EsController.java
 * @author: WeiHui
 * @date: 2018/10/22 19:38
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@RestController
@RequestMapping("/clear")
@Api(value = "清理数据")
public class ClearDataController {

	@Resource
	private ClearDataService clearDataService;

	/**
	 * type  1- 授信 2 -借款  3 -还款  4 - 全部数据
	 *
	 * @param clearDataReq 用户Id
	 * @return 删除条数
	 */
	@PostMapping("deleteByUserId")
	@ApiOperation(value = "根据【userId】删除数据")
	public ResponseModel<ClearDataResp> clearByUserIdAndType(@RequestBody ClearDataReq clearDataReq) {
		return new ResponseModel<>(clearDataService.clearByUserIdAndType(clearDataReq.getUserId(), clearDataReq.getType()));
	}

	/**
	 * @return 删除条数
	 */
	@PostMapping("updateCookie")
	@ApiOperation(value = "更新cookie数据")
	public ResponseModel<Object> updateCookie(@RequestBody String cookie) {
		return new ResponseModel<>(FtSystemRequestHeader.updateCookie(cookie));
	}

}
