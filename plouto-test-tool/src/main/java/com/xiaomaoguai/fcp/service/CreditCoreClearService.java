package com.xiaomaoguai.fcp.service;

import com.xiaomaoguai.fcp.dto.resp.CreditCoreClearDetail;

/**
 * @fileName: CreditCoreClearService.java
 * @author: WeiHui
 * @date: 2018/10/25 10:38
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface CreditCoreClearService {

	CreditCoreClearDetail clear(String productCode, Long userId, String type);

}
