package com.xiaomaoguai.fcp;

import com.google.common.collect.ImmutableMap;
import com.xiaomaoguai.fcp.config.FtSystemRequestHeader;
import com.xiaomaoguai.fcp.utils.HttpClientUtils;
import org.junit.Test;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * @fileName: DeleteCreditCore.java
 * @author: WeiHui
 * @date: 2018/10/26 10:09
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class DeleteCreditCore extends BaseTest {

	@Resource
	private Environment environment;

	@Test
	public void test() throws Exception {
		try {
			String url = environment.getProperty("test.login.url");
			String post = HttpClientUtils.doGet(url, FtSystemRequestHeader.getHeader(),
					ImmutableMap.of(
							"productCode", "SZXXR",
							"userId", "32683018",
							"type", "4"));
			log.info("====>{}", post);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
