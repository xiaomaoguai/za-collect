package com.xiaomoaguai.javassist;

import com.xiaomaoguai.javassist.kepler.KeplerJavassistApplication;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {KeplerJavassistApplication.class}, value = {"DEPLOY_ENV=test"})
public class BaseTest {

	/**
	 * logger
	 */
	protected final Logger log = LoggerFactory.getLogger(getClass());

}
