package com.xiaomaoguai.fcp.kepler.dto.req;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/10 20:12
 * @since JDK 1.8
 */
@Getter
@Setter
public class PublicRequestDto implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 4638082977003588342L;

	/**
	 * 开发者appKey
	 */
	private String appKey;

	/**
	 * API接口地址
	 */
	private String serviceName;

	/**
	 * 应用级请求参数json序列串，(详见各个接口文档)
	 */
	private String bizContent;

	/**
	 * 时间戳,格式为yyyyMMddHHmmssSSS
	 */
	private String timestamp;

	/**
	 * 响应格式,目前支持格式为json
	 */
	private String format;

	/**
	 * API请求的签名
	 */
	private String sign;

	/**
	 * 签名类型，目前仅支持RSA
	 */
	private String signType;

	/**
	 * 字符编码，目前只支持UTF-8
	 */
	private String charset;

	/**
	 * 版本号
	 */
	private String version;

}
