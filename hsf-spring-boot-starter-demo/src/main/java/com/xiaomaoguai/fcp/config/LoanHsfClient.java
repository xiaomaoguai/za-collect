package com.xiaomaoguai.fcp.config;

import com.alibaba.fastjson.JSONObject;
import com.xiaomaoguai.fcp.hsf.autoconfigure.HsfClient;
import com.xiaomaoguai.creditcore.cashloan.api.LoanApi;
import com.xiaomaoguai.creditcore.cashloan.req.LoanQueryReq;

/**
 * @fileName: LoanHsfClient.java
 * @author: WeiHui
 * @date: 2018/11/20 22:10
 * @version: v1.0.0
 * @since JDK 1.8
 */
@HsfClient(value = LoanApi.class, url = "${hsf.creditcore.cashloan.url}", version = "${hsf.creditcore.cashloan.version}")
public interface LoanHsfClient {

	/**
	 * 借款交易查询
	 *
	 * @param request 借款交易查询Request
	 * @return 借款交易查询Response
	 */
	JSONObject loanQuery(LoanQueryReq request);

	/**
	 * 借款交易查询
	 *
	 * @param request 借款交易查询Request
	 * @return 借款交易查询Response
	 */
	String loanQuery2(LoanQueryReq request);
}
