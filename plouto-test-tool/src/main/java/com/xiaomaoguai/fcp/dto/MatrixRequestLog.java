package com.xiaomaoguai.fcp.dto;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Getter
@Setter
@Document(indexName = "fcp_fe_szbiz_pluto", type = "matrix_request_log", shards = 2, refreshInterval = "-1")
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MatrixRequestLog {

	@Id
	private String id;

	/**
	 *
	 */
	private String reqNo;

	/**
	 * 业务接口名
	 */
	private String apiName;

	/**
	 * 业务接口方法名
	 */
	private String transName;

	/**
	 * 业务接口版本
	 */
	private String apiVersion;

	/**
	 * 合作方id
	 */
	private String productCode;

	/**
	 * 渠道名称
	 */
	private String channelName;

	/**
	 * 用户账号
	 */
	private String thirdUserNo;

	/**
	 * 用户请求入口
	 */
	private String client;

	/**
	 * 用户IP
	 */
	private String clientIp;

	/**
	 * 用户设备MAC地址
	 */
	private String clientMac;

	/**
	 * 请求信息
	 */
	private String reqContent;

	/**
	 * 请求状态 0-初始 1-成功 2-失败
	 */
	private String state;

	/**
	 * 响应码
	 */
	private String respCode;

	/**
	 * 响应信息
	 */
	private String respMessage;

	/**
	 * 响应信息
	 */
	private String respContent;

	/**
	 * 创建时间
	 */
	private String gmtCreated;

	/**
	 * 更新时间
	 */
	private String gmtModified;

	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}

}