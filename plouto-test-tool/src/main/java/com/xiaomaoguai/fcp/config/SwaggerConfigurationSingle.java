
package com.xiaomaoguai.fcp.config;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.mg.swagger.framework.configuration.EnableSwaggerMgUi;
import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 单一的接口文档，没有分组那些
 *
 * @author 暮光：城中城
 * @since 2018年8月19日
 */
@Configuration
@EnableSwagger2
@EnableSwaggerMgUi
public class SwaggerConfigurationSingle {

	// 划重点！！：读取配置文件中是否是线上，如果是线上则关闭文档功能，否则所有接口全部暴露了！
	//Predicate<String> paths = AppConfig.isOnline() ? PathSelectors.none() : PathSelectors.any();
	Predicate<String> paths = false ? PathSelectors.none() : PathSelectors.any();

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(Predicates.or(
						RequestHandlerSelectors.basePackage("com.xiaomaoguai.fcp"),
						RequestHandlerSelectors.withClassAnnotation(Api.class)))
				.paths(paths)
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("小杏仁接口文档")
				.description("欢迎使用")
				.termsOfServiceUrl("https://www.showdoc.cc/xxr?page_id=884133684425528")
				.contact(new Contact("DH", "https://www.showdoc.cc/xxr?page_id=884133684425528", "806783409@qq.com"))
				.version("1.0")
				.build();
	}

}
