package com.xiaomaoguai.javassist.kepler.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author huwenxuan
 * @date 2018/12/25
 */
@Data
@NoArgsConstructor
public class StandardBaseDTO implements Serializable {

    private static final long serialVersionUID = -8775820533392016826L;

    /**
     * 请求时间
     */
    private String reqDate;

    /**
     * 请求流水号
     */
    private String reqNo;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 第三方账号
     */
    private String thirdUserNo;

    /**
     * 众安用户ID
     */
    private Long userId;

    /**
     * 核心账号
     */
    private Long acctId;

    /**
     * api请求名称
     */
    private String apiName;

    /**
     * api请求版本号
     */
    private String apiVersion;

    /**
     * api请求路由名称
     */
    private String routerName;

	/**
	 * api请求报文参数
	 */
	private String reqMsg;

	/**
	 * 返回码
	 */
	private String respCode;

	/**
	 * 返回码描述
	 */
	private String respMsg;

    /**
     * 延迟对象业务类型
     */
    private String delayBiz;

    /**
     * 延迟处理的对象
     */
    private Object delayObject;

    /**
     * 延迟时间
     */
    private Long delaySeconds;

	/**
	 * 起始游标
	 */
    private Integer offset;

	/**
	 * 分批条数
	 */
    private Integer pageSize;
}
