package com.xiaomaoguai.fcp.pre.kepler.router.annotation;

import org.springframework.core.annotation.AliasFor;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

@Documented
@Inherited
@Component
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HandlerAnnotation {

    @AliasFor(annotation = Component.class)
    String value() default "";

    /**
     * 开发者
     */
    String author() default "请填充开发者名称";

    /**
     * handler信息描述
     */
    String desc() default "请填充描述";

    /**
     * 是否共享Handler
     *
     * @return
     */
    boolean share() default false;

}
