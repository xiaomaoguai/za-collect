package com.dh.convert.constants;

public class Constants {

	public final static String QUOTES ="\"";
	public final static String POINT =".";
	public final static String MARK ="/";
	public final static String LINE = "-";
	public final static String EQUAL ="=";
	public final static String YES ="Y";
	public final static String NO ="N";
	public final static String CURRENT_VALUE = "cv";
	public final static String CURRENT_OBJECT = "co";
	public final static String CURRENT_LIST_ENTRY = "cle";
	public final static String SRC_OBJECT = "so";
	public final static String LIST_NUM="NUM";
	public final static String BRACKETS_PREFIX ="<";
	public final static String BRACKETS_ENDINDEX =">";
	public final static String PLACE_PREFIX ="{";
	public final static String PLACE_ENDINDEX = "}";
	public final static char REPLACE = '~';
	public final static char LIST_FLAG ='*';
}
