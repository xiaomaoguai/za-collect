package com.xiaomaoguai.fcp.pre.kepler.mock.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/18 15:37
 * @since JDK 1.8
 */
@ConfigurationProperties(prefix = "feign.mock")
public class FeignMockProperties {

	private static final boolean DEFAULT_OBJECT_RETURN = true;

	/**
	 * 是否启用 feignMock
	 */
	private boolean enabled;

	/**
	 * 是否返回object类型
	 */
	private boolean objectReturn = DEFAULT_OBJECT_RETURN;

	/**
	 * 是否 debug
	 */
	private boolean debug;

	/**
	 * 生成的class路径
	 */
	private String debugClassPath;

	/**
	 * 生成的groovy路径
	 */
	private String groovyFilePath;

	/**
	 * Getter for property 'enabled'.
	 *
	 * @return Value for property 'enabled'.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Setter for property 'enabled'.
	 *
	 * @param enabled Value to set for property 'enabled'.
	 */
	public void setEnabled(final boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Getter for property 'debug'.
	 *
	 * @return Value for property 'debug'.
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * Setter for property 'debug'.
	 *
	 * @param debug Value to set for property 'debug'.
	 */
	public void setDebug(final boolean debug) {
		this.debug = debug;
	}

	/**
	 * Getter for property 'debugClassPath'.
	 *
	 * @return Value for property 'debugClassPath'.
	 */
	public String getDebugClassPath() {
		return debugClassPath;
	}

	/**
	 * Setter for property 'debugClassPath'.
	 *
	 * @param debugClassPath Value to set for property 'debugClassPath'.
	 */
	public void setDebugClassPath(final String debugClassPath) {
		this.debugClassPath = debugClassPath;
	}

	/**
	 * Getter for property 'groovyFilePath'.
	 *
	 * @return Value for property 'groovyFilePath'.
	 */
	public String getGroovyFilePath() {
		return groovyFilePath;
	}

	/**
	 * Setter for property 'groovyFilePath'.
	 *
	 * @param groovyFilePath Value to set for property 'groovyFilePath'.
	 */
	public void setGroovyFilePath(final String groovyFilePath) {
		this.groovyFilePath = groovyFilePath;
	}

	/**
	 * Getter for property 'objectReturn'.
	 *
	 * @return Value for property 'objectReturn'.
	 */
	public boolean isObjectReturn() {
		return objectReturn;
	}

	/**
	 * Setter for property 'objectReturn'.
	 *
	 * @param objectReturn Value to set for property 'objectReturn'.
	 */
	public void setObjectReturn(final boolean objectReturn) {
		this.objectReturn = objectReturn;
	}

}
