package com.xiaomaoguai.fcp.pre.kepler.tddl.exception;

/**
 * @author WeiHui
 * @date 2019/1/24
 */
public class TddlRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 7634892282599474024L;

	private String messageCode;

	public TddlRuntimeException() {
	}

	public TddlRuntimeException(String message) {
		super(message);
	}

	public TddlRuntimeException(Throwable cause) {
		super(cause);
	}

	public TddlRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}

	public TddlRuntimeException(String messageCode, String message, Throwable cause) {
		super(message, cause);
		this.messageCode = messageCode;
	}

	public String getMessageCode() {
		return this.messageCode;
	}
}
