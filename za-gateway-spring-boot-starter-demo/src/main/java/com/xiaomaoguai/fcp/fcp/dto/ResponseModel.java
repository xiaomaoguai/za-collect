package com.xiaomaoguai.fcp.fcp.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.github.xiaomaoguai.xxr.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;


/**
 * 统一返回  r360
 *
 * @fileName: ResultModel.java
 * @author: WeiHui
 * @date: 2018/8/14 16:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ResponseModel<T> extends BaseDto {

	/**
	 * 序列号
	 */
	private static final long serialVersionUID = 7255476495391169021L;

	/**
	 * 结果实体
	 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private T data;

	/**
	 * 结果码
	 */
	private int code;

	/**
	 * 结果描述
	 */
	private String msg;

	/**
	 * r360 扩展字段
	 */
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String reason;


	/**
	 * 构造方法
	 */
	public ResponseModel() {
		this.code = 200;
	}

	public ResponseModel(Object data) {
		this.setData((T) data);
		this.code = 200;
	}

}
