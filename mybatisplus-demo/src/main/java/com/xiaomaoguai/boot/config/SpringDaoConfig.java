//package com.xiaomaoguai.boot.config;
//
//import com.taobao.tddl.client.jdbc.TDataSource;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.transaction.PlatformTransactionManager;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//import org.springframework.transaction.support.TransactionTemplate;
//
//@Slf4j
//@Configuration
////@MapperScan(value = "com.xiaomaoguai.boot.mapper")
//@EnableTransactionManagement(proxyTargetClass = true)
//public class SpringDaoConfig {
//
//    /**
//     * T data source.
//     *
//     * @param env the env
//     * @return the t data source
//     */
////    @Bean(initMethod = "init")
////    public TDataSource tDataSource(Environment env) {
////        TDataSource tDataSource = new TDataSource();
////        log.info("appName:" + env.getProperty("tddl.appname"));
////        tDataSource.setAppName(env.getProperty("tddl.appname"));
////        tDataSource.setDynamicRule(true);
////        return tDataSource;
////    }
//
////    @Bean
////    public MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean(TDataSource tDataSource) throws Exception {
////        MybatisSqlSessionFactoryBean sqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
////        sqlSessionFactoryBean.setDataSource(tDataSource);
////        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
////        Resource[] resources = resolver.getResources("classpath*:/mapper/*.xml");
////        sqlSessionFactoryBean.setMapperLocations(resources);
//////        Resource configResources = resolver.getResource("classpath*:/mybatis/mybatis-config.xml");
//////        sqlSessionFactoryBean.setConfigLocation(configResources);
////        sqlSessionFactoryBean.setTypeAliasesPackage("com.xiaomaoguai.boot.entity");
////        sqlSessionFactoryBean.setPlugins(new Interceptor[]{paginationInterceptor()});
////        sqlSessionFactoryBean.setGlobalConfig(globalConfig());
////        return sqlSessionFactoryBean;
////    }
//
////    @Bean
////    public PaginationInterceptor paginationInterceptor() {
////        return new PaginationInterceptor();
////    }
//
////    @Bean
////    public GlobalConfig globalConfig() {
////        GlobalConfig globalConfig = new GlobalConfig();
////        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig();
////        dbConfig.setIdType(IdType.AUTO);
////        globalConfig.setDbConfig(dbConfig);
////        return globalConfig;
////    }
//
//    /**
//     * Transaction manager.
//     *
//     * @param tDataSource the t data source
//     * @return the platform transaction manager
//     */
//    @Bean
//    public PlatformTransactionManager transactionManager(TDataSource tDataSource) {
//        DataSourceTransactionManager dataSourceTransactionManager = new DataSourceTransactionManager();
//        dataSourceTransactionManager.setDataSource(tDataSource);
//        return dataSourceTransactionManager;
//    }
//
//    /**
//     * Transaction templates.
//     *
//     * @param transactionManager the transaction manager
//     * @return the transaction templates
//     */
//    @Bean
//    public TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
//        TransactionTemplate transactionTemplate = new TransactionTemplate();
//        transactionTemplate.setTransactionManager(transactionManager);
//        return transactionTemplate;
//    }
//
//}
