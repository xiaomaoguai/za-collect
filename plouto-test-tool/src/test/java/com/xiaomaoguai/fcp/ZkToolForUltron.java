package com.xiaomaoguai.fcp;

import org.junit.Test;

/**
 * @fileName: ZkTool.java
 * @author: WeiHui
 * @date: 2018/11/15 21:10
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ZkToolForUltron extends AbstractZkTool {

	private String path = "D:\\workspace\\git\\za-collect\\plouto-test-tool\\src\\test\\resources\\ultron\\1\\";

	private String path_handler = "D:\\workspace\\git\\za-collect\\plouto-test-tool\\src\\test\\resources\\ultron\\handler\\";

	private boolean handler = true;

	@Override
	public String setUpYourLocalPath() {
		return handler ? path_handler : path;
	}

	@Test
	public void createCreditApplyRouter() {
		node = "/asset/za-pre-ultron-insurance/router/creditApplyRouter";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void createCreditQueryRouter() {
		node = "/asset/za-pre-ultron-insurance/router/creditQueryRouter";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void createCreditApplyMockRouter() {
		node = "/asset/za-pre-ultron-insurance/router/creditApplyMockRouter";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void createCreditApplyOnsRouter() {
		node = "/asset/za-pre-ultron-insurance/router/WLDZXCreditApplyOnsRouter";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/completeProductConfHandler.WLDZX.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testOpenAccountConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/openAccountHandler.WLDZX.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testAuthConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/doAuthHandler.WLDZX.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreLockConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/lockApplyHandler.WLDZX.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreditApplyConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/creditApplyHandler.default.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreditApplyQueryConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/creditQueryHandler.default.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testInitUserIdCardImgHandlerConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/initUserIdCardImgHandler.default.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreditApplyUpdateHandlerConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_handler/creditApplyUpdateHandler.default.in";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreditApplyRouterConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_router/creditApplyRouter.default.out";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testCreditQueryRouterConvert() {
		node = "/asset/za-pre-ultron-insurance/convert_router/creditQueryRouter.default.out";
		createZkNode(node);
		writeData(node);
	}

	@Test
	public void testDelete() {
		client.delete("/za-fcp-pre-kepler/router/51YND/yndWriteOffRouter");
	}

}
