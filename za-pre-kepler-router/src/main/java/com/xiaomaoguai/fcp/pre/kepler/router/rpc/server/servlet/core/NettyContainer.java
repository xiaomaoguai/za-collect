package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core;

import com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception.NettyServerException;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.channelInitializer.NettyChannelInitializer;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.NettyServer;

import org.springframework.boot.web.server.WebServer;

/**
 * Netty servlet容器 处理请求，返回响应
 *
 * @author DH
 */

public class NettyContainer extends NettyServer implements WebServer {

	private final NettyContext servletContext;

	public NettyContainer(Integer port, NettyChannelInitializer channelInitializer, NettyContext servletContext) {
		super(port, channelInitializer);
		this.servletContext = servletContext;
	}

	@Override
	public void start()  {
		servletContext.setInitialised(false);
		try {
			super.start();
		} catch (InterruptedException e) {
			throw new NettyServerException(e);
		}
		servletContext.setInitialised(true);

	}

	/**
	 * 优雅地关闭各种资源
	 */
	@Override
	public void stop()  {
		super.close();
	}

}
