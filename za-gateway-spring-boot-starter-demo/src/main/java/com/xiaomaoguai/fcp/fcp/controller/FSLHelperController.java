package com.xiaomaoguai.fcp.fcp.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.xiaomaoguai.xxr.dto.resp.PublicRespDTO;
import com.github.xiaomaoguai.xxr.properties.ZaProperties;
import com.github.xiaomaoguai.xxr.utils.http.HttpClientUtil;
import com.github.xiaomaoguai.xxr.utils.json.JsonUtils;
import com.github.xiaomaoguai.xxr.utils.security.AES;
import com.xiaomaoguai.fcp.fcp.dto.ResponseModel;
import com.xiaomaoguai.fcp.fcp.utils.ReqUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * 房司令-问题-解决 controller
 *
 * @fileName: FSLHelperController.java
 * @author: WeiHui
 * @date: 2018/9/13 12:37
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@RestController
@RequestMapping("/fsl")
@Api(value = "房司令问题控制器")
public class FSLHelperController {

	private String aesKey = "BF58217B493783BB";

	@Resource
	private ZaProperties zaProperties;

	@PostMapping("/info")
	@ApiOperation(value = "房司令报文解密")
	public ResponseModel<Object> info(@RequestBody String content) throws Exception {
		if (StringUtils.startsWith(content, "\"")) {
			content = content.substring(1, content.length() - 1).replace("\\u003d\\", "=");
		}
		content = content.replaceAll("\\\\", "");
		log.info("\n===========房司令报文开始============\n{}", JsonUtils.formatJson(content));

		ObjectNode jsonNode = (ObjectNode) JsonUtils.getObjectMapper().readTree(content);
		String reqContent = jsonNode.get("reqContent").asText();
		String decrypt = AES.decryptFromBase64(reqContent, aesKey);
		log.info("\n===========房司令请求明文============\n{}", JsonUtils.formatJson(decrypt));

		//更新token,再次请求，获取结果
		ObjectNode bizJsonNode = (ObjectNode) JsonUtils.getObjectMapper().readTree(decrypt);
		String thirdUserNo = bizJsonNode.get("thirdUserNo").asText();
		String apiName = bizJsonNode.get("apiName").asText();
		String transName = bizJsonNode.get("transName").asText();

		String token = ReqUtils.buildToken(thirdUserNo, aesKey);
		bizJsonNode.put("token", token);

		log.info("\n===========再次请求小杏仁开始============\n{}", JsonUtils.formatJson(bizJsonNode.toString()));
		String reqParam = jsonNode.put("reqContent", AES.encryptToBase64(bizJsonNode.toString(), aesKey)).toString();
		String reqUrl = ReqUtils.buildReqUrl(zaProperties.getUrl(), apiName, transName);
		String result = HttpClientUtil.doPost(reqUrl, reqParam);
		String respJson = result.substring(1, result.length() - 1).replace("\\u003d\\", "=").replaceAll("\\\\", "");
		log.info("\n===========请求小杏仁结果============\n{}", JsonUtils.formatJson(respJson));
		ResponseModel<Object> responseModel = new ResponseModel<>(respJson);
		ObjectNode resultJsonNode = (ObjectNode) JsonUtils.getObjectMapper().readTree(respJson);
		if (ReqUtils.isSuccess(resultJsonNode.get("respCode").asText())) {
			PublicRespDTO respDTO = JsonUtils.toJavaObject(respJson, PublicRespDTO.class);
			String respContent = respDTO.getRespContent();
			String decryptContent = AES.decryptFromBase64(respContent, aesKey);
			log.info("众安返回报文：\n {}", JsonUtils.formatJson(decryptContent));
			responseModel.setData(decryptContent);
		}
		return responseModel;
	}

}
