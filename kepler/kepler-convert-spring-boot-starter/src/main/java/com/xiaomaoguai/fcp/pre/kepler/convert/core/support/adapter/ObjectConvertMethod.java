package com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter;


import com.xiaomaoguai.fcp.pre.kepler.convert.core.support.ConvertSupport;

import java.lang.reflect.Method;

/**
 * @fileName: ObjectConvertMethod.java
 * @author: WeiHui
 * @date: 2018/11/26 13:40
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ObjectConvertMethod extends AbstractConvertMethod {

    @Override
    public boolean canHandler(Method method) {
        return "convert".equals(method.getName()) && method.getParameterCount() == 3;
    }

    @Override
    public Object invoke(Method method, Object[] args) {
        return convert((ConvertSupport) args[0], (String) args[1], args[2]);
    }

}
