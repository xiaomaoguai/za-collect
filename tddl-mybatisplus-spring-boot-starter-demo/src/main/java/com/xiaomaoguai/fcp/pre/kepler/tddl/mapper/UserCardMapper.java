package com.xiaomaoguai.fcp.pre.kepler.tddl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaoguai.fcp.pre.kepler.tddl.entity.UserCard;

/**
 * @fileName: UserCardMapper.java
 * @author: WeiHui
 * @date: 2019/2/11 17:44
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface UserCardMapper extends BaseMapper<UserCard> {

}
