package com.xiaomaoguai.fcp.pre.kepler.trace.service;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 17:37
 * @since JDK 1.8
 */
public class NonDataDesensitization implements DataDesensitization {

	@Override
	public String dataDesensitization(final String source) {
		return source;
	}

}
