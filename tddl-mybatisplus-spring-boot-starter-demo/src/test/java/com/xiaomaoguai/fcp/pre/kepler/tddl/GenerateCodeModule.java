package com.xiaomaoguai.fcp.pre.kepler.tddl;

import lombok.Data;

/**
 * @fileName: GenerateCodeModule.java
 * @author: WeiHui
 * @date: 2019/3/12 17:50
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
public class GenerateCodeModule {

	private String columnName;

	private String jdbcType;

	private String fieldDis;

}
