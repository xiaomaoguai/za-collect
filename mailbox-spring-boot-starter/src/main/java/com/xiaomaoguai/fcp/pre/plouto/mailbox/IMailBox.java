package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.dh.mailbox2.api.Actor;

/**
 * @fileName: IMailBox.java
 * @author: WeiHui
 * @date: 2018/9/27 20:37
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface IMailBox {

	/**
	 * actor类
	 *
	 * @return actor
	 */
	Class<? extends Actor> getClazz();

	/**
	 * bufferSize
	 *
	 * @return buffer大小
	 */
	int getBufferSize();

	/**
	 * threadSize
	 *
	 * @return 线程数
	 */
	int getThreadSize();

}
