package com.xiaomaoguai.javassist.kepler.config;

/**
 * handler dynamic class 格转统一接口
 *
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/20 12:43
 * @since JDK 1.8
 */
public interface DCConvertClient<T> {

	/**
	 * 通用解析方法
	 *
	 * @param root 原始参数
	 * @return 目标对象
	 */
	T convert(Object root);

	/**
	 * 通用解析方法
	 *
	 * @param productCode 产品属性
	 * @param root        原始参数
	 * @return 目标对象
	 */
	T convert(String productCode, Object root);

}
