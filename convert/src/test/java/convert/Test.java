package convert;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.dh.convert.node.BeanNode;
import com.dh.convert.node.ObjectNode;
import com.dh.convert.node.TreeNode;
import com.dh.convert.node.ValueNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.Maps;

public class Test {
	
	public Integer getA() {
		return a;
	}
	public Integer getB() {
		return b;
	}
	public List<En> getEnList() {
		return enList;
	}
	public void setA(Integer a) {
		this.a = a;
	}
	public void setB(Integer b) {
		this.b = b;
	}
	public void setEnList(List<En> enList) {
		this.enList = enList;
	}
	public void setBig(BigDecimal big){
		this.big = big;
	}
	public BigDecimal getBig(){
		return big;
	}
	public TestEnum getTestEnum() {
		return testEnum;
	}
	public void setTestEnum(TestEnum testEnum) {
		this.testEnum = testEnum;
	}
	
	private BigDecimal big;
	
	
	private Integer a;
	private Integer b;
	private List<En> enList;
	private HashMap map;
	private En[] ens;
	private String[] ss;
	private Set set;
	private Date date;
	public String getDateString() {
		return dateString;
	}
	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	private String dateString;
	private TestEnum testEnum;
	

	public static void main(String[] args) throws NoSuchFieldException, SecurityException{
		System.out.println(BeanNode.object2Node(Test.class));
	}
}
