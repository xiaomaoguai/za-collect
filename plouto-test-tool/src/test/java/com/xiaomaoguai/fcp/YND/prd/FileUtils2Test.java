package com.xiaomaoguai.fcp.YND.prd;


import com.xiaomaoguai.fcp.utils.FileUtils2;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/3/27 15:18
 * @since JDK 1.8
 */
public class FileUtils2Test {

	//生产加密归集文件
	private static final String KEY2 = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBANGQBEiBy0ZzJApRLPDlno5Zd4kVgjD7iNVDMHTqfnFgozkP/qFoBxg+FM/Z69Y10qIXhiMImBFWoW624C8kuJrTwwT8BwW5335y7L8vP8R7O9bU4XeXrARvW5hubn+ZSzQTpbh06QOZ1R0Ni8KiMjLg5x2rAcQmvMlyLMErcZNPAgMBAAECgYBOG7qs98u3T3Uwbz8r7rnhXtFiA+VJsoc2zdtCyKAmTBmqTB7hNC/oxkLlkdkIN3lxtwjlB+/+DmcPKBR5Hdu8FK6ROMxdfHzXdcb3W0e5sjnsKQzdgtm1ZvB09pw6tw7F2lR9Q5liGe6qPG5fMBvEK8/wzbo0/qSsGCvGWCIMyQJBAPOMJ8lwFZJBmdPZrOuT/qcRDe3fn59j2+A611auEckp7JftCBcYYQfoCkAHr7b01k7wKFCop2jLglVEwZ/ax90CQQDcRwaJx105Sx71TRlN8NO8jHoqNL136FMk1CUde3ns1IXsW+7RLJ95nUvd08T2S/qC5JnimeSyBP5p0mSjaAsbAkAj2KcqgPLL2hf4FHvdGKVMlbgFIPrvjNUaBc8YFPrNLvyByAN+0rnFXiPUFM2KEjNQ3wwwAwLQ69M9FaeiDaOJAkA+oYtAXYkqIyvYP2W0fYcWkzKhEYHVuB7717391f/i0MKLY9BRdUiawZiyp+XWyd+em/UNMwiNfVIBMKsgsxKFAkB/6H++VPqZqqhlTcn+A0j8kscgj4oid6cbT99ssNKB/4EZLtHv2PNjzBdV7BOsDsR1VOKg+AWwOyI5I95GXzma";

	//生产解密归集文件
	private static final String KEY4 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDRkARIgctGcyQKUSzw5Z6OWXeJFYIw+4jVQzB06n5xYKM5D/6haAcYPhTP2evWNdKiF4YjCJgRVqFutuAvJLia08ME/AcFud9+cuy/Lz/EezvW1OF3l6wEb1uYbm5/mUs0E6W4dOkDmdUdDYvCojIy4OcdqwHEJrzJcizBK3GTTwIDAQAB";

	public static void main(String[] args) {
		//FileUtils2.encryptFile(KEY4, "D://testData/repay_20190723_201907242199_de.txt", "D://testData/repay_20190723_201907242188.txt");
		FileUtils2.decryptFile(KEY2, "D://repay_20200212_202002122109.txt", "D://repay_20200212_202002122109_des.txt");
	}

}
