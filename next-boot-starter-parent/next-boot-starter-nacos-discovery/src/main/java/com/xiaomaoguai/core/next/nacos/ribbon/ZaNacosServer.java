/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.nacos.ribbon;

import app.myoss.cloud.core.constants.DeployEnvEnum;
import com.alibaba.cloud.nacos.ribbon.NacosServer;
import com.alibaba.nacos.api.naming.pojo.Instance;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

import static com.xiaomaoguai.core.next.nacos.constants.NacosDiscoveryConstants.META_SERVICE_DOMAIN;

/**
 * Nacos服务实例信息
 *
 * @author chenyao
 * @since 2019年6月19日 下午6:08:56
 */
public class ZaNacosServer extends NacosServer {
    /**
     * 创建Nacos服务实例信息
     *
     * @param instance 服务实例的对象
     */
    public ZaNacosServer(Instance instance) {
        super(instance);
    }

    private String getServiceDomain() {
        Map<String, String> metadata = getMetadata();
        return StringUtils.defaultIfBlank(metadata.get(META_SERVICE_DOMAIN), null);
    }

    @Override
    public String getHost() {
        if (DeployEnvEnum.isDev()) {
            String serviceDomain = getServiceDomain();
            if (serviceDomain != null) {
                return serviceDomain;
            }
        }
        return super.getHost();
    }

    @Override
    public int getPort() {
        if (DeployEnvEnum.isDev()) {
            String serviceDomain = getServiceDomain();
            if (serviceDomain != null) {
                return 80;
            }
        }
        return super.getPort();
    }
}
