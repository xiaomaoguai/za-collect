package com.xiaomaoguai.javassist.kepler.config;

import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author August.zhang
 * @version v1.0.0
 * @date 2019/12/11 18:17
 * @since JDK 1.8
 */
@Documented
@Inherited
@Component
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HandlerAnnotation {

	/**
	 * 开发者
	 *
	 * @return 开发者
	 */
	String author() default "请填充开发者名称";

	/**
	 * handler信息描述
	 *
	 * @return Handler描述
	 */
	String desc() default "请填充描述";

	/**
	 * 是否共享Handler
	 *
	 * @return true -共享 false -非共享
	 */
	boolean share() default false;

}
