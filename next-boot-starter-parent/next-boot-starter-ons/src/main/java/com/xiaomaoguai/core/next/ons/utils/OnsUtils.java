/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.ons.utils;

import com.aliyun.openservices.ons.api.Message;

import app.myoss.cloud.core.constants.MyossConstants;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * ONS工具类
 *
 * @author chenyao
 * @since 2017年1月6日 下午6:05:33
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class OnsUtils {
    /**
     * 转换消息内容
     *
     * @param message 消息实体对象
     * @return 消息内容
     */
    public static String getMessageBody(Message message) {
        return convertMessageBody(message.getBody());
    }

    /**
     * 转换消息内容
     *
     * @param body 消息体
     * @return 消息内容
     */
    public static String convertMessageBody(byte[] body) {
        return new String(body, MyossConstants.DEFAULT_CHARSET);
    }
}
