/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.servlet.config;

import com.xiaomaoguai.core.next.web.servlet.constants.WebMvcConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Web 属性配置
 *
 * @author chenyao
 * @since 2019年6月27日 上午11:31:29
 */
@Data
@ConfigurationProperties(prefix = WebMvcConstants.CONFIG_PREFIX)
public class WebProperties {

}
