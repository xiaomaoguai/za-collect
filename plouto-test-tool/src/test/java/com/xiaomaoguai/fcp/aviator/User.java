package com.xiaomaoguai.fcp.aviator;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @fileName: User.java
 * @author: WeiHui
 * @date: 2018/11/23 10:23
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {

	private Long id;

	private int age;

	private String name;

}
