package com.xiaomaoguai.fcp.pre.plouto.mailbox.actor;

import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: AsyncData.java
 * @author: WeiHui
 * @date: 2018/10/16 17:44
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class AsyncData {

	private AsyncRunnable asyncRunnable;

	private Object param;

}
