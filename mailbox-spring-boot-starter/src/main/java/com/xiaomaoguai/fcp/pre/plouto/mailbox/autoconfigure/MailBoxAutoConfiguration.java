package com.xiaomaoguai.fcp.pre.plouto.mailbox.autoconfigure;

import com.dh.mailbox2.retrymailbox.RetryMailBox;
import com.xiaomaoguai.fcp.pre.plouto.mailbox.actor.AsyncActor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 自动注册配置
 *
 * @fileName: MailAutoConfiguration.java
 * @author: WeiHui
 * @date: 2018/11/19 20:32
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Configuration
public class MailBoxAutoConfiguration {

	@Bean
	public MailBoxes mailBoxes() {
		return new MailBoxes();
	}

	@Bean
	public RetryMailBox retryMailBox() {
		return new RetryMailBox();
	}

	@Bean
	public MailBoxTemplate mailBoxTemplate() {
		return new MailBoxTemplate();
	}

	@Bean
	public AsyncActor asyncActor() {
		return new AsyncActor();
	}

}
