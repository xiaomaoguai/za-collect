package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.channelInitializer;

import com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums.Protocol;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core.NettyContext;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.handlers.NettyServletChannelInitializer;

import static com.google.common.base.Preconditions.checkNotNull;

public class HandlerInitSelector {

	public static NettyChannelInitializer getHandlers(Protocol protocol, Integer nProcessors, Integer timeout,
													  NettyContext servletContext) {
		switch (protocol) {
			case SERVLET:
				return new NettyServletChannelInitializer(protocol, timeout, nProcessors, checkNotNull(servletContext));
			case REST:
				// return new NettyRestChannelInitializer(timeout,new
				// DefaultEventExecutorGroup(nProcessors));
			case RPC:
				// channelInitializer = new
				// NettyRpcChannelInitializer(timeout,Processor);
				// break;
			default:
				throw new IllegalStateException("Invalid protocol: " + protocol);
		}
	}

}
