package com.xiaomaoguai.fcp.pre.kepler.glue.exception;

/**
 * HmilyException.
 *
 * @author xiaoyu
 */
public class GlueException extends RuntimeException {

    private static final long serialVersionUID = -948934144333391208L;

    /**
     * Instantiates a new Tcc exception.
     */
    public GlueException() {
    }

    /**
     * Instantiates a new Tcc exception.
     *
     * @param message the message
     */
    public GlueException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Tcc exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public GlueException(final String message, final Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Tcc exception.
     *
     * @param cause the cause
     */
    public GlueException(final Throwable cause) {
        super(cause);
    }
}
