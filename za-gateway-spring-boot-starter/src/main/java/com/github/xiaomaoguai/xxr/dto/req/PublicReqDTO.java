package com.github.xiaomaoguai.xxr.dto.req;

import com.github.xiaomaoguai.xxr.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class PublicReqDTO extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 8439662391254050199L;

	@NotBlank(message = "请求流水号不能为空")
	private String reqNo = UUID.randomUUID().toString();

	@NotBlank(message = "业务接口版本不能为空")
	private String apiVersion = "1.0.0";

	/**
	 * 用户账号
	 */
	private String thirdUserNo;

	/**
	 * 产品编码
	 */
	@NotBlank(message = "产品编码不能为空")
	@Length(max = 15, message = "产品编码长度不能超过15个字符")
	private String productCode;

	/**
	 * 用户请求入口
	 */
	@NotBlank(message = "用户请求入口不能为空")
	@Length(max = 32, message = "用户请求入口不能超过32个字符")
	private String client;

	/**
	 * 渠道 两位组成 如"00"
	 * 第一位 0 -H5
	 * 第二位 1 -微信公众号XX
	 */
	@NotBlank(message = "渠道不能为空")
	private String channelID;

	/**
	 * 用户IP
	 */
	@Length(max = 15, message = "用户IP不能超过15个字符")
	private String clientIP;

	/**
	 * 用户设备MAC地址
	 */
	@Length(max = 20, message = "用户设备MAC地址长度不能超过20个字符")
	private String clientMAC;

	/**
	 * 密钥
	 */
	@NotBlank(message = "密钥不能为空")
	private String key;

	/**
	 * 接口名
	 */
	@NotBlank(message = "接口名不能为空")
	private String apiName;

	/**
	 * 交易名称
	 */
	@NotBlank(message = "交易名称不能为空")
	private String transName;

	/**
	 * 业务参数
	 */
	@NotBlank(message = "业务参数不能为空")
	private String reqContent;
}
