/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.hsf.excetion;

/**
 * HSF运行时异常
 *
 * @author chenyao
 * @since 2016年10月5日 上午9:12:48
 */
public class HsfRuntimeException extends RuntimeException {
    private static final long serialVersionUID = -5682790931263764684L;

    /**
     * HSF运行时异常
     */
    public HsfRuntimeException() {
        super();
    }

    /**
     * HSF运行时异常
     *
     * @param message 错误信息
     */
    public HsfRuntimeException(String message) {
        super(message);
    }

    /**
     * HSF运行时异常
     *
     * @param message 错误信息
     * @param cause 异常信息
     */
    public HsfRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * HSF运行时异常
     *
     * @param cause 异常信息
     */
    public HsfRuntimeException(Throwable cause) {
        super(cause);
    }
}
