package org.apache.skywalking.apm.plugin.kepler.define;

import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.matcher.ElementMatcher;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.ConstructorInterceptPoint;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.DeclaredInstanceMethodsInterceptPoint;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.InstanceMethodsInterceptPoint;
import org.apache.skywalking.apm.agent.core.plugin.interceptor.enhance.ClassInstanceMethodsEnhancePluginDefine;
import org.apache.skywalking.apm.agent.core.plugin.match.ClassMatch;

import static net.bytebuddy.matcher.ElementMatchers.isPublic;
import static net.bytebuddy.matcher.ElementMatchers.named;
import static org.apache.skywalking.apm.agent.core.plugin.match.NameMatch.byName;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/5/17 20:41
 * @since JDK 1.8
 */
public abstract class AbstractKeplerStandardHandlerPluginDefine extends ClassInstanceMethodsEnhancePluginDefine {

	/**
	 * 设置需要拦截的类的逻辑
	 *
	 * @return 拦截的类
	 */
	protected abstract String setInterceptClass();

	/**
	 * 设置待增强的class
	 *
	 * @return 待增强的class
	 */
	protected abstract String setEnhanceClass();

	@Override
	public ClassMatch enhanceClass() {
		return byName(setEnhanceClass());
	}

	@Override
	public ConstructorInterceptPoint[] getConstructorsInterceptPoints() {
		return new ConstructorInterceptPoint[0];
	}

	@Override
	public InstanceMethodsInterceptPoint[] getInstanceMethodsInterceptPoints() {

		return new InstanceMethodsInterceptPoint[]{

				new DeclaredInstanceMethodsInterceptPoint() {

					@Override
					public ElementMatcher<MethodDescription> getMethodsMatcher() {
						return isPublic().and(named("handle"));
					}

					@Override
					public String getMethodsInterceptor() {
						return setInterceptClass();
					}

					@Override
					public boolean isOverrideArgs() {
						return false;
					}
				}
		};
	}

}
