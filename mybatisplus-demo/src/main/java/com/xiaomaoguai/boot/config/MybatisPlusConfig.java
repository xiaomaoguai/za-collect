package com.xiaomaoguai.boot.config;

import com.baomidou.mybatisplus.extension.incrementer.H2KeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @fileName: MybatisPlusConfig.java
 * @author: WeiHui
 * @date: 2018/8/4 17:16
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Configuration
@MapperScan("com.xiaomaoguai.boot.mapper*")
public class MybatisPlusConfig {

    @Bean
    public H2KeyGenerator getH2KeyGenerator() {
        return new H2KeyGenerator();
    }

    /**
     * 性能分析拦截器，不建议生产使用
     */
    @Bean
    public PerformanceInterceptor performanceInterceptor(){
        return new PerformanceInterceptor();
    }

}
