package com.xiaomaoguai.fcp.pre.kepler.glue.service;

import com.xiaomaoguai.fcp.pre.kepler.glue.config.GlueConfig;

/**
 * 初始化方法
 *
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 19:15
 * @since JDK 1.8
 */
@FunctionalInterface
public interface GlueInitService {

	/**
	 * 初始化扩展点
	 *
	 * @param glueConfig {@linkplain com.xiaomaoguai.fcp.pre.kepler.glue.config.GlueConfig}
	 */
	void initialization(GlueConfig glueConfig);

}
