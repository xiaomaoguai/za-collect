package com.xiaomaoguai.fcp.pre.kepler.tddl.sequence;

import com.baomidou.mybatisplus.core.incrementer.IKeyGenerator;
import com.taobao.tddl.client.sequence.impl.GroupSequence;

/**
 * mybatisPlus 主键策略
 *
 * @fileName: TddlKeyGenerator.java
 * @author: WeiHui
 * @date: 2019/2/11 16:26
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class TddlKeyGenerator implements IKeyGenerator {

	private GroupSequence sequence;

	@Override
	public String executeSql(String incrementerName) {
		return null;
	}

}
