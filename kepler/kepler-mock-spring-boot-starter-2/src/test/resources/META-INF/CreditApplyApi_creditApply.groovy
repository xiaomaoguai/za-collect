package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class CreditApplyApi_creditApply implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        def resp = new BaseResp()
        resp.setResultCode(ResultCode.SUCCESS)
        resp.setResult(1)
        return resp
    }

}
