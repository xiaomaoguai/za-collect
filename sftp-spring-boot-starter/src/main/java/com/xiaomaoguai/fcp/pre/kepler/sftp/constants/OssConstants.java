package com.xiaomaoguai.fcp.pre.kepler.sftp.constants;

/**
 * @author WeiHui
 * @date 2019/1/30
 */
public class OssConstants {

	/**
	 * oss配置前缀，支持单个数据库
	 */
	public static final String CONFIG_PREFIX = "oss";

	/**
	 * oss配置前缀，支持多个数据库
	 */
	public static final String MULTI_CONFIG_PREFIX = "oss.multi";

	/**
	 * oss配置前缀，支持单个数据库
	 */
	public static final String BEAN_SUFFIX_NAME = "ossClient";

}
