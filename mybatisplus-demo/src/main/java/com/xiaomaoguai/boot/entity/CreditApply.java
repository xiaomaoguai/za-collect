package com.xiaomaoguai.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 授信申请表
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class CreditApply implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 请求流水号
     */
    private String reqNo;

    /**
     * 授信申请号
     */
    private String creditApplyNo;

    /**
     * 合作方用户账号
     */
    private String thirdUserNo;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 申请金额
     */
    private BigDecimal applyAmount;

    /**
     * 申请日期
     */
    private LocalDateTime applyDate;

    /**
     * 用户名称
     */
    private String userName;

    /**
     * 证件类型 1-身份证
     */
    private Integer certType;

    /**
     * 证件号码
     */
    private String certNo;

    /**
     * 手机号码
     */
    private String phoneNo;

    /**
     * 风控扩展信息JSON格式
     */
    private String riskInfo;

    /**
     * 众安渠道账户ID
     */
    private Long userId;

    /**
     * 授信申请状态:0-初始 1-处理中 2-成功 3-拒绝
     */
    private Integer applyStatus;

    /**
     * 标识用户的授信是否激活，1代表激活，2代表未激活，3代表授信成功后注销，默认激活
     */
    private String activeStatus;

    /**
     * 授信额度
     */
    private BigDecimal creditAmount;

    /**
     * 授信协议号
     */
    private String creditContractNo;

    /**
     * 授信额度过期时间
     */
    private LocalDateTime expireDate;

    /**
     * 风控决策返回码
     */
    private String themisCode;

    /**
     * 风控决策返回信息
     */
    private String themisMsg;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreated;

    /**
     * 更新人
     */
    private String modifier;

    /**
     * 修改时间
     */
    private LocalDateTime gmtModified;

    /**
     * 是否删除
     */
    private String isDeleted;

    /**
     * 资金编码
     */
    private String fundCode;

    /**
     * 申请调整的额度
     */
    private BigDecimal adjustAmount;

    /**
     * 调额申请状态:0-初始 1-处理中 2-成功 3-拒绝
     */
    private Integer adjustStatus;

    /**
     * 申请调额的时间
     */
    private LocalDateTime adjustDate;

    /**
     * 绑定银行卡信息
     */
    private String bindBankInfoJson;


}
