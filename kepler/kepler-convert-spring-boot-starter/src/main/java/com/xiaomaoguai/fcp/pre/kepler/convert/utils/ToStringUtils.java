package com.xiaomaoguai.fcp.pre.kepler.convert.utils;

import java.lang.reflect.Method;

/**
 * @fileName: ToStringUtils.java
 * @author: WeiHui
 * @date: 2019/2/19 14:03
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ToStringUtils {

	public static final String TO_STRING = "toString";

	/**
	 * 默认的一些toString方法，防止idea调试的 toString的各种错误信息
	 *
	 * @param method 方法
	 * @param args   参数
	 * @return toString 内容
	 */
	public static Object handlerToString(Object object, Method method, Object[] args) {
		String methodName = method.getName();
		return object.toString() + "@" + methodName;
	}

}
