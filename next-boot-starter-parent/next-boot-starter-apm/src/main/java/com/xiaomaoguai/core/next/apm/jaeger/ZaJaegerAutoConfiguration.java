/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.apm.jaeger;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 众安自定义 Jaeger 自动配置
 *
 * @author chenyao
 * @since 2019年11月14日 上午11:04:08
 */
@Configuration
@ConditionalOnClass({ io.jaegertracing.internal.JaegerTracer.class,
        io.opentracing.contrib.java.spring.jaeger.starter.TracerBuilderCustomizer.class })
@ConditionalOnProperty(value = "opentracing.jaeger.enabled", havingValue = "true", matchIfMissing = true)
@AutoConfigureBefore(io.opentracing.contrib.java.spring.jaeger.starter.JaegerAutoConfiguration.class)
public class ZaJaegerAutoConfiguration {
    /**
     * 众安自定义调用链辅助功能
     * <ul>
     * <li>将jaeger调用链的traceID等信息打印到logback里面，用于业务日志错误时，可以排查调用链信息，辅助排错
     * </ul>
     *
     * @return TracerBuilderCustomizer
     */
    @ConditionalOnMissingBean(name = "zaTracerBuilderCustomizer")
    @Bean(name = "zaTracerBuilderCustomizer")
    public io.opentracing.contrib.java.spring.jaeger.starter.TracerBuilderCustomizer zaTracerBuilderCustomizer() {
        return builder -> builder.withScopeManager(new WrappedThreadLocalScopeManager());
    }
}
