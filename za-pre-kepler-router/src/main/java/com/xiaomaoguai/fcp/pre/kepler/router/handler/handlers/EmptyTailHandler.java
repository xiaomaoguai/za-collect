package com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers;

import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlercontext.TailContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.router.RouterPipeline;
import org.springframework.stereotype.Component;

@Component
public class EmptyTailHandler extends TailHandler<Buf> {

	@Override
	public void beforeHandle(HandlerContext<Buf> hc) {
	}

	@Override
	public void genInnerParam(HandlerContext<Buf> hc) {

	}

	@Override
	public Buf genOutParam(TailContext<Buf> tailContext) {
		return tailContext.getBuf();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void exceptionCaught(RouterException cause) {
		HandlerContext<?> handlerContext = cause.getHandlerContext();
		RouterPipeline<Buf, Buf> router = (RouterPipeline<Buf, Buf>) handlerContext.getRouter();
		Buf respBuf = router.getBuf();
		respBuf.setExceptionHandlerName(handlerContext.name());
		respBuf.setException(cause.clear());
		log.error("{} router:{} Handler:{}执行异常", respBuf.getId(),router.getRouterName(),handlerContext.name(), cause.getCause());
		router.getLast().setOutParam(respBuf);
	}

}
