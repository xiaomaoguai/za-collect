package com.xiaomaoguai.fcp.pre.kepler.delay.task;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @fileName: DelayTaskMsg.java
 * @author: WeiHui
 * @date: 2019/1/4 16:54
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false)
public class DelayTaskMsg {

	/**
	 * 任务名称
	 */
	private String taskName;

	/**
	 * 具体消息内容
	 */
	private Object data;

	/**
	 * 延迟秒数
	 */
	private Long delaySeconds;

	/**
	 * 轮询次数
	 */
	private AtomicInteger robinTimes = new AtomicInteger(1);

	/**
	 * 消息创建时间
	 */
	private Date createTime = new Date();

	/**
	 * 构造方法
	 *
	 * @param taskName taskName
	 * @param data     消息体
	 */
	public DelayTaskMsg(String taskName, Object data) {
		this.taskName = taskName;
		this.data = data;
	}

}
