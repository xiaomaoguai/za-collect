package com.xiaomaoguai.fcp.pre.kepler.router.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/5 21:59
 * @since JDK 1.8
 */
@Documented
@Inherited
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface TraceCrossDisruptor {

}
