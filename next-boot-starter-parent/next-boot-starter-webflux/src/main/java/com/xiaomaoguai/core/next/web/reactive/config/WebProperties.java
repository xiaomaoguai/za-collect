/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.reactive.config;

import com.xiaomaoguai.core.next.web.reactive.constants.WebReactiveConstants;
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Web 属性配置
 *
 * @author chenyao
 * @since 2018年5月27日 下午10:28:37
 */
@Data
@ConfigurationProperties(prefix = WebReactiveConstants.CONFIG_PREFIX)
public class WebProperties {

}
