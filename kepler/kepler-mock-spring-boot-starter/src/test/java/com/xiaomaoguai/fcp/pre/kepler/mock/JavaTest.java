package com.xiaomaoguai.fcp.pre.kepler.mock;

import com.google.common.collect.Lists;
import com.xiaomaoguai.account.api.response.AccountOpenResponse;
import com.xiaomaoguai.account.common.beans.ResultBase;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/14 16:12
 * @since JDK 1.8
 */
public class JavaTest {

	public static void main(String[] args) {
		AccountOpenResponse accountOpenResponse = new AccountOpenResponse();
		accountOpenResponse.setCaId(0L);
		accountOpenResponse.setBaId(0L);
		accountOpenResponse.setPaId(0L);
		accountOpenResponse.setChannelAccountNo("");
		accountOpenResponse.setChannelAccountName("");
		accountOpenResponse.setChannelId(0L);
		accountOpenResponse.setSubChannelId(0L);
		accountOpenResponse.setBizCat("");
		accountOpenResponse.setEacctNo("");
		accountOpenResponse.setAcctValues(0);
		accountOpenResponse.setAuthorityList(Lists.newArrayList());

		ResultBase.success(accountOpenResponse);
	}

}
