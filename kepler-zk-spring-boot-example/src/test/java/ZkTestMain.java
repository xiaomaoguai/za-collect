import lombok.extern.slf4j.Slf4j;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.KeeperException;

import java.util.concurrent.TimeUnit;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/11 11:24
 * @since JDK 1.8
 */
@Slf4j
public class ZkTestMain {

	public static void main(String[] args) throws Exception {
		CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder()
				.connectString("10.139.34.112:2181")
				.retryPolicy(new ExponentialBackoffRetry(
						2000,
						5,
						5000))
				.namespace("asset");
		CuratorFramework curatorFramework = builder.build();
		curatorFramework.start();
		try {
			if (!curatorFramework.blockUntilConnected(5000 * 5, TimeUnit.MILLISECONDS)) {
				curatorFramework.close();
				throw new KeeperException.OperationTimeoutException();
			}
		} catch (Exception e) {
			log.error("zk exception", e);
		}

		curatorFramework.delete().forPath("/asset/za-fcp-pre-kepler/url/za-pre-kepler-ultron");

	}

}
