package com.xiaomaoguai.fcp.pre.kepler.delay.task.zk;

import org.apache.curator.framework.recipes.queue.QueueSerializer;

import java.nio.charset.StandardCharsets;

/**
 * 队列数据序列化与反序列化
 *
 * @fileName: StringQueueSerializer.java
 * @author: WeiHui
 * @date: 2019/2/14 11:01
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class StringQueueSerializer implements QueueSerializer<String> {

	@Override
	public byte[] serialize(String item) {
		return item.getBytes();
	}

	@Override
	public String deserialize(byte[] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}
}
