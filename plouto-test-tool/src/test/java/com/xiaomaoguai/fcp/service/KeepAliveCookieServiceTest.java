package com.xiaomaoguai.fcp.service;

import com.xiaomaoguai.fcp.utils.HttpClientUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @fileName: KeepAliveCookieServiceTest.java
 * @author: WeiHui
 * @date: 2018/11/1 17:29
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class KeepAliveCookieServiceTest {

	@Resource
	private com.xiaomaoguai.fcp.service.KeepCookieJobService keepCookieJobService;

	public static void main(String[] args) {
		try {
			String url = "http://nsso.xiaomaoguaionline.com/login?service=za-fcp-test-platform&error=-100&target=http%3A%2F%2Fft-test.xiaomaoguaionline.com%2Fcreditcore_clear.html";
			Map<String, String> login = new HashMap<>(3);
			login.put("username", "zhangweihui");
			login.put("password", "Zhang321");

			String get = HttpClientUtils.doGet(url, login);
			System.out.println(get);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
