package org.apache.skywalking.apm.plugin.kepler;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/6 20:22
 * @since JDK 1.8
 */
public class Constants {

	public static final String CONVERT_VALUE = "CONVERT_VALUE";

	public static final String REQUEST_KEY_IN_RUNTIME_CONTEXT = "SW_REQUEST";

	public static final String RESPONSE_KEY_IN_RUNTIME_CONTEXT = "SW_RESPONSE";

	public static final String FORWARD_REQUEST_FLAG = "SW_FORWARD_REQUEST_FLAG";

	public static final String WEBFLUX_REQUEST_KEY = "SW_WEBFLUX_REQUEST_KEY";

	public static final String CONTROLLER_METHOD_STACK_DEPTH  = "SW_CONTROLLER_METHOD_STACK_DEPTH";

}
