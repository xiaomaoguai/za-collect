package com.dh.convert.rule.function;

import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.function.FunctionUtils;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorString;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;


/**
 * 覆盖框架自己的  StringSubStringFunction
 *
 * @fileName: StringSubStringFunction.java
 * @author: WeiHui
 * @date: 2018/11/15 11:37
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class StringSubStringFunction extends AbstractFunction {

	@Override
	public String getName() {
		return "string.substring";
	}

	@Override
	public AviatorObject call(Map<String, Object> env, AviatorObject arg1, AviatorObject arg2, AviatorObject arg3) {
		String target = FunctionUtils.getStringValue(arg1, env);
		Number beginIndex = FunctionUtils.getNumberValue(arg2, env);
		Number endIndex = FunctionUtils.getNumberValue(arg3, env);
		return new AviatorString(StringUtils.substring(target, beginIndex.intValue(), endIndex.intValue()));
	}

	@Override
	public AviatorObject call(Map<String, Object> env, AviatorObject arg1, AviatorObject arg2) {
		String target = FunctionUtils.getStringValue(arg1, env);
		Number beginIndex = FunctionUtils.getNumberValue(arg2, env);
		return new AviatorString(StringUtils.substring(target, beginIndex.intValue()));
	}

}
