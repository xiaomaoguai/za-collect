package com.xiaomaoguai.fcp.r360.utils;

import com.xiaomaoguai.fcp.fcp.utils.r360.RsaEncrypt;

/**
 * @fileName: RsaEncryptTest.java
 * @author: WeiHui
 * @date: 2018/9/20 14:02
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class RsaEncryptTest {

	public static void main(String[] args) {
		RsaEncrypt rsaEncrypt = new RsaEncrypt();
		rsaEncrypt.genKeyPair();
		System.out.println(rsaEncrypt.getPrivateKey());
		System.out.println(rsaEncrypt.getPublicKey());
	}
}
