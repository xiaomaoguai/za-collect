package com.xiaomaoguai.fcp.pre.kepler.router.test;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.xiaomaoguai.fcp.pre.kepler.router.async.AsyncInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.Branch;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.RouterInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.BranchType;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.spi.HandlerManager;
import com.xiaomaoguai.fcp.pre.kepler.router.router.RouterConfig;
import com.xiaomaoguai.fcp.pre.kepler.router.router.RouterPipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMain {

	public static void main(String args[]) {

		HandlerManager handlerManager = new HandlerManager();
		handlerManager.addLocalHandler("CheckHandler", new CheckHandler());
		handlerManager.addLocalHandler("DBHandler", new DBHandler());
		handlerManager.addLocalHandler("RPCHandler", new RPCHandler());
		List<RouterInfo> RouterInfos = new ArrayList<>();
		RouterInfo b2oRouterInfo = new RouterInfo("b2ohandler");
		b2oRouterInfo.setName("TestB2OHandler");
		b2oRouterInfo.setOrder(1);
		b2oRouterInfo.setAsyncInfo(new AsyncInfo("testPool", 10));
		RouterInfos.add(b2oRouterInfo);
		RouterInfo RouterInfo1 = new RouterInfo("RouterInfo1");
		RouterInfo1.setName("CheckHandler");
		RouterInfo1.setOrder(2);
		RouterInfos.add(RouterInfo1);
		RouterInfo RouterInfo2 = new RouterInfo("RouterInfo2");
		RouterInfo2.setName("DBHandler");
		RouterInfo2.setOrder(3);
		RouterInfos.add(RouterInfo2);
		RouterInfo RouterInfo3 = new RouterInfo("RouterInfo3");
		RouterInfo3.setName("RPCHandler");
		RouterInfo3.setOrder(4);
		RouterInfos.add(RouterInfo3);
		Branch b = new Branch();
		b.setRule("test == 1");
		b.setAsyncInfo(new AsyncInfo("testPool", 10));
		b.setNextHandlerName("RPCHandler");
		b.setBranchType(BranchType.ADD_PIPELINE);
		RouterInfo1.setBranchList(Lists.newArrayList(b));
		for (RouterInfo routerInfo : RouterInfos) {
			routerInfo.init();
		}
		System.out.println(RouterInfos);

		RouterConfig r = new RouterConfig() {

			@Override
			public List<RouterInfo> getRouterInfos(String routerName) {
				List<RouterInfo> RouterInfos = new ArrayList<>();
				RouterInfo RouterInfo1 = new RouterInfo("RouterInfo11");
				RouterInfo1.setName("CheckHandler");
				RouterInfo1.setOrder(1);
				RouterInfos.add(RouterInfo1);
				RouterInfo RouterInfo2 = new RouterInfo("RouterInfo22");
				RouterInfo2.setName("DBHandler");
				RouterInfo2.setOrder(2);
				RouterInfos.add(RouterInfo2);
				RouterInfo RouterInfo3 = new RouterInfo("RouterInfo33");
				RouterInfo3.setName("RPCHandler");
				RouterInfo3.setOrder(3);
				RouterInfos.add(RouterInfo3);
				return RouterInfos;
			}

		};

		RouterPipeline<String, Buf> router = new RouterPipeline<String, Buf>(new FirstHandler(), new EndHandler(), r);

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("test", new Integer(1));
		router.init(RouterInfos).fireHandlers(JSON.toJSONString(data));
		System.out.println(router.getResp());
	}

}
