package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class NettyServerException extends RuntimeException {

	public NettyServerException(Throwable cause) {
		super(cause);
	}

	public NettyServerException(String message) {
		super(message);
	}

	public NettyServerException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1828844394955602634L;

}
