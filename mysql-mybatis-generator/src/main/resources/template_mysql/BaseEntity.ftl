package com.xiaomoaguai.generator.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @fileName: BaseEntity.java
 * @author: WeiHui
 * @date: 2019/3/5 17:36
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = -6921224927935522475L;

	/**
	 * Database Column Name: id
	 * <p>
	 * Database Column Remarks: 主键id
	 */
	private Long id;

	/**
	 * Database Column Name: creator
	 * <p>
	 * Database Column Remarks: 创建者
	 * </p>
	 */
	private String creator;

	/**
	 * Database Column Name: modifier
	 * <p>
	 * Database Column Remarks: 修改者
	 * </p>
	 */
	private String modifier;

	/**
	 * Database Column Name: gmt_created
	 * <p>
	 * Database Column Remarks: 创建时间
	 * </p>
	 */
	private Date gmtCreated;

	/**
	 * Database Column Name: gmt_modified
	 * <p>
	 * Database Column Remarks: 修改时间
	 * </p>
	 */
	private Date gmtModified;

	/**
	 * Database Column Name: is_deleted
	 * <p>
	 * Database Column Remarks: 是否删除
	 * </p>
	 */
	private String isDeleted;
}
