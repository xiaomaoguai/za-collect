package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};

import java.util.List;

/**
* <p>
    * ${entity} 服务类
    * </p>
*
* @author ${author}
* @since ${date}
*/
<#if kotlin>
    interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceImplName} {

    /**
    * 查询${entity}列表
    * @param model  参数
    * @return List  结果
    * @throws Exception 异常
    */
    List<${entity}> find${entity}List(${entity} model) throws Exception;

}
</#if>
