package com.xiaomaoguai.fcp.pre.kepler.redis.listener;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/4 15:27
 * @since JDK 1.8
 */
public interface RedisListenerKeyDefine {

	/**
	 * 监听key
	 *
	 * @return 前缀
	 */
	String getListenerTopic();

}
