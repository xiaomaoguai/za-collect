package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.impl;

import com.xiaomaoguai.tac.cloud.apm.logging.logback.SensitiveDataConverter;
import com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service.SensitiveService;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/17 17:08
 * @since JDK 1.8
 */
public class SensitiveServiceImpl implements SensitiveService {

	@Override
	public String filterMessage(final String source) {
		return SensitiveDataConverter.filterMessage(Boolean.TRUE.toString(), source);
	}

}
