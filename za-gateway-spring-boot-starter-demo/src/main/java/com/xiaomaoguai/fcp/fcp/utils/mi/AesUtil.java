package com.xiaomaoguai.fcp.fcp.utils.mi;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @fileName: AesUtil.java
 * @author: WeiHui
 * @date: 2018/10/8 16:19
 * @version: v1.0.0
 * @since JDK 1.8
 */
public final class AesUtil {

	public static String bytesToHexString(byte[] bytes) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			String strHex = Integer.toHexString(bytes[i]);
			if (strHex.length() > 3) {
				sb.append(strHex.substring(6));
			} else {
				if (strHex.length() < 2) {
					sb.append("0" + strHex);
				} else {
					sb.append(strHex);
				}
			}
		}
		return sb.toString();
	}

	public static String genRandomKey() {
		return RandomStringUtils.randomAlphanumeric(16);
	}

}
