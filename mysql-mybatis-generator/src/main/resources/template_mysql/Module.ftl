/*
 * 文件名：${module}.java
 * 版本信息:v1.0.0
 * 日期：${createdTime} 
 * Copyright 深圳市佰仟金融服务有限公司版权所有
 */ 
package  com.billionsfinance.crs.model.${moduleType};

import com.billionsfinance.crs.model.BaseModel;


/**
* @ FileName: ${module}.java
* @ Author: ${createdName}
* @ Date: ${createdTime}
* @ Version: v1.0.0
*/
public class ${module} extends BaseModel {
	
	private static final long serialVersionUID = 1L;

    <#list moduleInfoList as moduleInfo>
	   <#if moduleInfo.propertyName!="createdTime" && moduleInfo.propertyName!="uuid" && moduleInfo.propertyName!="createdBy" && moduleInfo.propertyName!="updatedBy" && moduleInfo.propertyName!="updatedTime" && moduleInfo.propertyName!="isDelete">
	/**
	* ${moduleInfo.fieldDis}
	*/
	private ${moduleInfo.fieldType} ${moduleInfo.propertyName};
		</#if>
    </#list>
    
    <#list moduleInfoList as moduleInfo>
	   <#if moduleInfo.propertyName!="createdTime" && moduleInfo.propertyName!="uuid" && moduleInfo.propertyName!="createdBy" && moduleInfo.propertyName!="updatedBy" && moduleInfo.propertyName!="updatedTime" && moduleInfo.propertyName!="isDelete">
	public void set${moduleInfo.capFieldName}(${moduleInfo.fieldType} ${moduleInfo.propertyName}) {
	     this.${moduleInfo.propertyName} = ${moduleInfo.propertyName};
	}

	public ${moduleInfo.fieldType} get${moduleInfo.capFieldName}() {
	     return ${moduleInfo.propertyName};
	}
	
		</#if>
    </#list>
}