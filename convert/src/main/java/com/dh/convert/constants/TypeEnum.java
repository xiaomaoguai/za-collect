package com.dh.convert.constants;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;


public enum TypeEnum {
	
	MAP("@",Map.class,LinkedHashMap.class),
	LIST("*",List.class,ArrayList.class),
	ARRAY("$",List.class,ArrayList.class),
	COMMON("",Object.class,null),
	CONTEXT("context",Map.class,HashMap.class);
	private String flag;
	private Class<?> interfaceCls;
	private Class<?> instanceClass;
 
	TypeEnum(String flag,Class<?> interfaceCls,Class<?> instanceClass){
		this.flag = flag;
		this.interfaceCls = interfaceCls;
		this.instanceClass = instanceClass;
	}
	public String getFlag() {
		return flag;
	}

	public Class<?> getInterfaceCls() {
		return interfaceCls;
	}

	public Class<?> getInstanceClass() {
		return instanceClass;
	}
	
	public static TypeEnum type(String path){
		
		if(StringUtils.isBlank(path))
			return null;
		if(path.startsWith(MAP.flag))
			return MAP;
		else if(path.equals(CONTEXT.flag))
			return CONTEXT;
		else if(path.startsWith(LIST.flag))
			return LIST;
		else if(path.startsWith(ARRAY.flag))
			return ARRAY;
		else
			return null;
	}

}
