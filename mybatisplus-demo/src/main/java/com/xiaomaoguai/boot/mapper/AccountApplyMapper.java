package com.xiaomaoguai.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaomaoguai.boot.entity.AccountApply;

/**
 * <p>
 * 调账申请表 Mapper 接口
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
public interface AccountApplyMapper extends BaseMapper<AccountApply> {

}
