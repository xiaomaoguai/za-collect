package com.xiaomaoguai.fcp;

import com.xiaomaoguai.fcp.config.CreditCoreProperties;
import com.xiaomaoguai.fcp.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.Resource;
import java.util.Optional;

@EnableScheduling
@SpringBootApplication
@EnableConfigurationProperties(CreditCoreProperties.class)
public class XxrToolApplication implements CommandLineRunner {

	private static final String DEPLOY_ENV = "DEPLOY_ENV";

	@Resource
	private CreditCoreProperties creditCoreProperties;

	@Override
	public void run(String... args) {
		System.out.println(JsonUtils.toFormatJsonString(creditCoreProperties));
		System.out.println("http://localhost:9090/document.html");
	}

	public static void main(String[] args) {
		System.setProperty(DEPLOY_ENV, Optional.ofNullable(StringUtils.defaultIfBlank(System.getProperty(DEPLOY_ENV), System.getenv(DEPLOY_ENV))).orElse("dev"));
		SpringApplication.run(XxrToolApplication.class, args);
	}

}
