package com.dh.convert.rule.function;

import java.util.Collection;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.dh.convert.utils.ClassUtils;
import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorBoolean;
import com.googlecode.aviator.runtime.type.AviatorObject;

public class IsEmptyFunction extends AbstractFunction {

	@Override
	public String getName() {
		return "isEmpty";
	}

	@Override
	public AviatorBoolean call(Map<String, Object> env, AviatorObject arg1) {
		Object o = arg1.getValue(env);
		if(o == null)
			return AviatorBoolean.TRUE;
		Class<?> cls = o.getClass(); 
		if (ClassUtils.isColltion(cls) && ((Collection)o).isEmpty())
			return  AviatorBoolean.TRUE;
		if (ClassUtils.isMap(cls) && MapUtils.isEmpty((Map)o))
			return  AviatorBoolean.TRUE; 
		return AviatorBoolean.FALSE;
	}

}
