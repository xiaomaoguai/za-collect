package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.Date;

@Getter
@Setter
public class SubRepayApply {

	private Long id;

	private String thirdUserNo;

	private String productCode;

	private Long userId;

	private String repayOuterNo;

	private Date applyDate;

	private String reqMsg;

	private BigDecimal repayAmount;

	private Integer repayStatus;

	private String creator;

	private Date gmtCreated;

	private String modifier;

	private Date gmtModified;

	private String isDeleted;

}