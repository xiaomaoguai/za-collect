package com.xiaomaoguai.javassist.kepler;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Optional;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/1/2 14:31
 * @since JDK 1.8
 */
@SpringBootApplication
public class KeplerJavassistApplication {

	private static final String DEPLOY_ENV = "DEPLOY_ENV";

	public static void main(String[] args) {
		System.setProperty(DEPLOY_ENV, Optional.ofNullable(StringUtils.defaultIfBlank(System.getProperty(DEPLOY_ENV), System.getenv(DEPLOY_ENV))).orElse("test"));
		SpringApplication.run(KeplerJavassistApplication.class, args);
	}

}
