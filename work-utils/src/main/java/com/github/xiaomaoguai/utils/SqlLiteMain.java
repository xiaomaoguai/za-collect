package com.github.xiaomaoguai.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/16 11:42
 * @since JDK 1.8
 */
public class SqlLiteMain {

	private static String DB_DRIVE_CLASS = "org.sqlite.JDBC";

	private static String DB_PATH = SqlLiteMain.class.getResource("zhou.db").getPath();

	public static void main(String[] args) {
		String dbFilePath = DB_PATH.substring(1);
		String dbUrl = "jdbc:sqlite:" + dbFilePath;
		try {
			// 加载驱动,连接sqlite的jdbc
			Class.forName(DB_DRIVE_CLASS);
			//连接数据库zhou.db,不存在则创建
			Connection connection = DriverManager.getConnection(dbUrl);
			//创建连接对象，是Java的一个操作数据库的重要接口
			Statement statement = connection.createStatement();
			String sql = "create table tables(name varchar(20),pwd varchar(20))";
			//判断是否有表tables的存在。有则删除
			statement.executeUpdate("drop table if exists tables");
			//创建数据库
			statement.executeUpdate(sql);
			//向数据库中插入数据
			statement.executeUpdate("insert into tables values('zhou','156546')");
			//搜索数据库，将搜索的放入数据集ResultSet中
			ResultSet rSet = statement.executeQuery("select* from tables");
			//遍历这个数据集
			while (rSet.next()) {
				//依次输出 也可以这样写 rSet.getString(“name”)
				System.out.println("姓名：" + rSet.getString(1));
				System.out.println("密码：" + rSet.getString("pwd"));
			}
			rSet.close();//关闭数据集
			connection.close();//关闭数据库连接
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}