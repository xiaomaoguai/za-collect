package com.xiaomaoguai.fcp.pre.kepler.glue.repository;

import com.xiaomaoguai.fcp.pre.kepler.glue.config.GlueConfig;
import com.xiaomaoguai.fcp.pre.kepler.glue.annotation.GlueSPI;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 19:20
 * @since JDK 1.8
 */
@GlueSPI
public interface GlueRepository {

	/**
	 * init. 初始化，比如连接zk
	 *
	 * @param glueConfig {@linkplain com.xiaomaoguai.fcp.pre.kepler.glue.config.GlueConfig}
	 */
	void init(GlueConfig glueConfig) throws Exception;

}
