/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.servlet.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Web项目自动配置
 *
 * @author chenyao
 * @since 2019年6月27日 上午11:31:29
 */
@EnableConfigurationProperties(WebProperties.class)
@ComponentScan(basePackages = "com.xiaomaoguai.core.next.web.servlet.config")
@Configuration
public class WebAutoConfiguration {
    /**
     * 初始化Web项目自动配置
     */
    public WebAutoConfiguration() {
    }
}
