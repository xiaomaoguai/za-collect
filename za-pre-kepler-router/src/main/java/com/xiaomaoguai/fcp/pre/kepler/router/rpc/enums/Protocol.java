package com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums;

public enum Protocol {
	RPC, REST, SERVLET
}
