/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.loan;

import com.xiaomaoguai.fcp.domain.UserLoan;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * The Interface UserLoanMapper.
 */
public interface UserLoanMapper {

	/**
	 * 新写入数据库记录.
	 *
	 * @param record
	 *            the record
	 * @return the int
	 */
	int insert(UserLoan record);

	/**
	 * 动态字段,写入数据库记录.
	 *
	 * @param record
	 *            the record
	 * @return the int
	 */
	int insertSelective(UserLoan record);

	/**
	 * Select by user id.
	 *
	 * @param userId
	 *            the user id
	 * @param loanOuterNo
	 *            the loan outer no
	 * @param status
	 *            the status
	 * @return the list
	 */
	List<UserLoan> selectByUserId(@Param("userId") Long userId, @Param("loanOuterNo") String loanOuterNo,
			@Param("status") Integer status);

	/**
	 * Select by user id.
	 *
	 * @param userId
	 *            the user id
	 * @param status
	 *            the status
	 * @return the list
	 */
	List<UserLoan> selectByUserIdAndSettleStatus(@Param("userId") Long userId, @Param("status") Integer status);

	/**
	 * Update status by loan outer no.
	 *
	 * @param userLoan
	 *            the user loan
	 * @param status
	 *            the status
	 * @return the int
	 */
	int updateStatusByLoanOuterNo(@Param("userLoan") UserLoan userLoan, @Param("status") Integer status);

	/**
	 * Select by loan date.
	 *
	 * @param userId
	 *            the user id
	 * @param beginDate
	 *            the begin date
	 * @param endDate
	 *            the end date
	 * @param status
	 *            the status
	 * @return the list
	 */
	List<UserLoan> selectByLoanDate(@Param("userId") Long userId, @Param("beginDate") String beginDate,
			@Param("endDate") String endDate, @Param("status") Byte status);

	/**
	 * Select by loan inner no.
	 *
	 * @param userId
	 *            the user id
	 * @param loanOuterNo
	 *            the loan outer no
	 * @param loanInnerNo
	 *            the loan inner no
	 * @return the user loan
	 */
	UserLoan selectByLoanInnerNo(@Param("userId") Long userId, @Param("loanOuterNo") String loanOuterNo,
			@Param("loanInnerNo") String loanInnerNo);

	/**
	 * 根据userId分页查询借款记录
	 * 
	 * @param userId
	 *            the user_id
	 * @param offset
	 *            查询个数
	 * @param limit
	 *            页数
	 * @return 借款记录
	 */
	List<UserLoan> selectPagedByUserId(@Param("userId") Long userId, @Param("loanInnerNo") String loanInnerNo,
									   @Param("offset") Integer offset, @Param("pageSize") Integer limit);

	int deleteByUserId(Long userId);
}
