package com.dh.convert.deserializer;

import java.util.Map;

import com.dh.convert.BeanConvertor;
import com.dh.convert.constants.Constants;
import com.dh.convert.jackson.JSON;
import com.dh.convert.node.TreeNode;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Maps;

public class String2BeanConvertor implements BeanConvertor<String>{

	public Object convert(String srcContent, TreeNode targetNode) throws Exception {
		JsonNode jn = JSON.readTree(srcContent);
		Map<String,Object> context = Maps.newHashMap();
		Object o = targetNode.getRootObject();
		context.put(Constants.CURRENT_OBJECT, o );
		context.put(Constants.SRC_OBJECT, srcContent );
		return targetNode.genObject(jn,o ,null,context);
	}

}
