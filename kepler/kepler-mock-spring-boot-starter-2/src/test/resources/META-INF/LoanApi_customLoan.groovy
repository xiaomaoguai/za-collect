package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock

import com.xiaomaoguai.creditcore.cashloan.dto.LoanResultDTO
import com.xiaomaoguai.creditcore.cashloan.enums.LoanStatusEnum
import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler

/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class LoanApi_customLoan implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        BaseResp<LoanResultDTO> baseResp = new BaseResp()

        LoanResultDTO loanResultDTO = new LoanResultDTO()
        loanResultDTO.setLoanInnerNo("大辉郎")
        loanResultDTO.setLoanStatus(LoanStatusEnum.LEADING)

        baseResp.setResultCode(ResultCode.SUCCESS)
        baseResp.setResult(loanResultDTO)

        return baseResp
    }

}
