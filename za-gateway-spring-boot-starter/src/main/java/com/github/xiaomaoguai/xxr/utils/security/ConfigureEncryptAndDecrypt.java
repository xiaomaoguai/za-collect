package com.github.xiaomaoguai.xxr.utils.security;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ConfigureEncryptAndDecrypt {

	public static final String CHAR_ENCODING = "UTF-8";

	public static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";

	public static final String RSA_ALGORITHM = "RSA/ECB/PKCS1Padding";

}
