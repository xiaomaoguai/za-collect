package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class SubCreditApply {
    private Long id;

    private String thirdUserNo;

    private String productCode;

    private String channelId;
    
    private Long userId;

    private String creditApplyNo;

    private Date applyDate;

    private Integer applyStatus;

    private String creator;

    private Date gmtCreated;

    private String modifier;

    private Date gmtModified;

    private String isDeleted;

}