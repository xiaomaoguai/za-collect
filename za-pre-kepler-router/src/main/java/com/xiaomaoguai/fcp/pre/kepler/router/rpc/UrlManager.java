package com.xiaomaoguai.fcp.pre.kepler.router.rpc;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author huwenxuan
 * @date 2019/1/23
 */
public class UrlManager {

    private static Map<String, String> urlMapper = Maps.newHashMap();

    /**
     * 根据application获取url信息
     *
     * @param application
     * @return
     */
    public static String getUrl(String application) {
        return urlMapper.get(application);
    }

    public static void putUrl(String application, String url) {
        if (StringUtils.isBlank(application)) {
            return;
        }
        urlMapper.put(application, url);
    }

    public static String remove(String application) {
        return urlMapper.remove(application);
    }
}
