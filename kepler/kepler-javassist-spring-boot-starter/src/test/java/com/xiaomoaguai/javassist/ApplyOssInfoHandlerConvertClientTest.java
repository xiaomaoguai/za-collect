package com.xiaomoaguai.javassist;

import com.google.common.collect.Maps;
import com.xiaomaoguai.javassist.kepler.config.DCConvertClient;
import com.xiaomaoguai.javassist.kepler.dto.StandardBaseDTO;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/20 14:13
 * @since JDK 1.8
 */
public class ApplyOssInfoHandlerConvertClientTest extends BaseTest {

	@Resource
	private DCConvertClient<StandardBaseDTO> applyOssInfoHandlerConvertClient;

	@Before
	public void setUp() {
		try {
			TimeUnit.SECONDS.sleep(30);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test() {
		try {
			Map<String, Object> root = Maps.newHashMapWithExpectedSize(16);
			root.put("productCode", "51YND");
			root.put("thirdUserNo", "123456");
			root.put("userId", 10059L);
			root.put("reqNo", "987456");
			StandardBaseDTO standardBaseDTO = applyOssInfoHandlerConvertClient.convert("51YND", root);
			System.out.println(standardBaseDTO);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
