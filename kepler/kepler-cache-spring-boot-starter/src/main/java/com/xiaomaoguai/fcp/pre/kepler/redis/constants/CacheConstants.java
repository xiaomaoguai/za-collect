/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.fcp.pre.kepler.redis.constants;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 类CacheConstants的实现描述：缓存常量
 *
 * @author chenyao
 * @since 2018年5月22日 下午5:35:39
 */
public class CacheConstants {

	/**
	 * 缓存配置前缀
	 */
	public static final String CACHE_PREFIX = "tac-cloud.cache";

	/**
	 * Redis 配置前缀
	 */
	public static final String REDIS_PREFIX = CACHE_PREFIX + ".redis";

	/**
	 * 环境信息
	 */
	public static final String DEPLOY_ENV = "DEPLOY_ENV";

	/**
	 * 编码
	 */
	public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;

}
