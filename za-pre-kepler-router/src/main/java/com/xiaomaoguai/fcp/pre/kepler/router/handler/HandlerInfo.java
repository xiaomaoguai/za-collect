package com.xiaomaoguai.fcp.pre.kepler.router.handler;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HandlerInfo implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 5479410779927678264L;

	/**
	 * 属于哪一个应用
	 */
	private String application;

	/**
	 * handler 名称
	 */
	private String handlerName;

	/**
	 * handler 类全限定名
	 */
	private String handlerClass;

	/**
	 * handler 描述
	 */
	private String desc;

	/**
	 * 作者
	 */
	private String author;

	/**
	 * handler 版本
	 */
	private String version;

	/**
	 * 注册日期
	 */
	private String regDate;

	/**
	 * 格转组字段
	 */
	private String convertStr;

}
