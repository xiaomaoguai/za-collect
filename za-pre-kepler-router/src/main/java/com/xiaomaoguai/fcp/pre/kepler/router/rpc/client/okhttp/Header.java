package com.xiaomaoguai.fcp.pre.kepler.router.rpc.client.okhttp;

import java.io.Serializable;
import java.util.Objects;

/**
 * http header
 * @author DH
 *
 */
public class Header implements Serializable {

	    private static final long serialVersionUID = -6525353427059094141L;

	    private  String name;
	    private  String value;

	    public Header(String key, String value) {
	        this.name = Objects.requireNonNull(key);
	        this.value = Objects.requireNonNull(value);
	    }

	    public static  Header of(String key, String value) {
	        return new Header(key, value);
	    }


	    public String name() {
	        return name;
	    }

	    public String value() {
	        return value;
	    }


	    @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;

	        Header header = (Header) o;

	        if (!name.equals(header.name())) return false;
	        return value.equals(header.value());
	    }

	    @Override
	    public int hashCode() {
	        int result = name.hashCode();
	        result = 31 * result + value.hashCode();
	        return result;
	    }

	    @Override
	    public String toString() {
	        return "(" + name + " = " + value + ")";
	    }


}
