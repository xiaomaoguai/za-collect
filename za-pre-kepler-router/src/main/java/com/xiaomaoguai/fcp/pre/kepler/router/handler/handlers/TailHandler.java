package com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers;


import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlercontext.TailContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;

public abstract class TailHandler<O> extends AbstractHandler<Buf> {

	@Override
	public void beforeHandle(HandlerContext<Buf> hc) {

	}

	@Override
	public void genInnerParam(HandlerContext<Buf> hc) {

	}


	@SuppressWarnings("unchecked")
	@Override
	public void handle(HandlerContext<Buf> hc) {
		TailContext<O> tailContext = (TailContext<O>) hc;
		O outParam = genOutParam(tailContext);
		tailContext.setOutParam(outParam);
	}

	/**
	 * 生成返回报文
	 *
	 * @param tailContext
	 * @return
	 */
	public abstract O genOutParam(TailContext<O> tailContext);

	@Override
	public abstract  void exceptionCaught(RouterException cause);
	    
}
