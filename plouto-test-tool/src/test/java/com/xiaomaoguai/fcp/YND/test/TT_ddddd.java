package com.xiaomaoguai.fcp.YND.test;

import com.xiaomaoguai.fcp.kepler.utils.FileTool;
import com.xiaomaoguai.fcp.utils.money.MoneyTool;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/21 2:10
 * @since JDK 1.8
 */
public class TT_ddddd {

	public static void main(String[] args) throws Exception {
		List<RepayApplyReq> repayApplyReqs = FileTool.readTxtFile("D:\\testData\\test\\repay_20190724_201907242173.txt", "UTF-8", RepayApplyReq.class);
		BigDecimal repayAmountTotal = BigDecimal.ZERO;
		for (RepayApplyReq repayApplyReq : repayApplyReqs) {
			Integer isEarlySettle = repayApplyReq.getIsEarlySettle();
			List<RepayDetailReq> repayWriteOffList = repayApplyReq.getRepayWriteOffList();
			System.out.println(repayApplyReq.getThirdUserNo());
			System.out.println(repayApplyReq.getRepayOuterNo());
			System.out.println("isEarlySettle :" + isEarlySettle + "        loanOuterNO: " + repayApplyReq.getLoanOuterNo() + "   ,installmentNo: " + repayWriteOffList.get(0).getInstallmentNo());
			BigDecimal repayAmount = repayApplyReq.getRepayAmount();
			repayAmountTotal = MoneyTool.add(repayAmountTotal, repayAmount);
		}
		System.out.println(repayAmountTotal);
	}

}
