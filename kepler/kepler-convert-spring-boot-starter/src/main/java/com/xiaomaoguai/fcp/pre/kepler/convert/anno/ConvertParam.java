package com.xiaomaoguai.fcp.pre.kepler.convert.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @fileName: ConvertParam.java
 * @author: WeiHui
 * @date: 2018/11/24 22:00
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConvertParam {

	String value() default "";

}
