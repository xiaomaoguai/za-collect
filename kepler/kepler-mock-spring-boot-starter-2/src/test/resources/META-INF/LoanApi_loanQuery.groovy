package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock


import com.xiaomaoguai.creditcore.cashloan.dto.LoanTransDTO
import com.xiaomaoguai.creditcore.common.dto.BaseResp
import com.xiaomaoguai.creditcore.common.enums.ResultCode
import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler
/**
 *
 * @author August.Zhang* @version v1.0.0* @date 2020/2/14 17:41
 * @since JDK 1.8
 */
class LoanApi_loanQuery implements GlueHandler {

    @Override
    Object handle(final Map<String, Object> params) {
        BaseResp<List<LoanTransDTO>> baseResp = new BaseResp()

        baseResp.setResultCode(ResultCode.SUCCESS)
        baseResp.setResult(null)

        return baseResp
    }

}
