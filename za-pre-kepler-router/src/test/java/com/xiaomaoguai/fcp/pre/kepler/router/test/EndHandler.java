package com.xiaomaoguai.fcp.pre.kepler.router.test;

import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlercontext.TailContext;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.TailHandler;

public class EndHandler extends TailHandler<Buf> {

	@Override
	public void beforeHandle(HandlerContext<Buf> hc) {

	}


	@Override
	public void genInnerParam(HandlerContext<Buf> hc) {

	}

	@Override
	public Buf genOutParam(TailContext<Buf> tailContext) {
		System.out.println("tailContext" + tailContext.getBuf());
		return tailContext.getBuf();
	}

	@Override
	public void exceptionCaught(RouterException cause) {

	}


}
