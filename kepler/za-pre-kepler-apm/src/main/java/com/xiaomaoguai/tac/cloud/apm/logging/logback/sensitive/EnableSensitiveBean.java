package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive;

import org.springframework.context.annotation.Import;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/5/25 16:01
 * @since JDK 1.8
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Import(SensitiveBean.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface EnableSensitiveBean {

}
