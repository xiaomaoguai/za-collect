package com.github.xiaomaoguai.xxr.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author: WeiHui
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@ConfigurationProperties(prefix = "za")
public class ZaProperties {

	private String url;

	private String channelId;

	private String publicKey;

	private String client;

}
