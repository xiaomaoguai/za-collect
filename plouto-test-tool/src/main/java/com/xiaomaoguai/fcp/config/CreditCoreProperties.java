package com.xiaomaoguai.fcp.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @fileName: CreditCoreProperties.java
 * @author: WeiHui
 * @date: 2018/10/26 14:19
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
@ConfigurationProperties(prefix = "creditcore")
public class CreditCoreProperties {

	/**
	 * queryUrl
	 */
	private String queryUrl;

	/**
	 * queryUrl
	 */
	private String clearUrl;

	/**
	 * queryUrl
	 */
	private String cookie;

}
