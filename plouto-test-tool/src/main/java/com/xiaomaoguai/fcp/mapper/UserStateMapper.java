/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper;

import com.xiaomaoguai.fcp.domain.UserAccount;
import org.apache.ibatis.annotations.Param;

/**
 * The Interface UserStateMapper.
 */
public interface UserStateMapper {

	/**
	 * Insert selective.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insertSelective(UserAccount record);

	/**
	 * Select by user id.
	 *
	 * @param userId the user id
	 * @return the user state
	 */
	UserAccount selectByUserId(Long userId);

	/**
	 * Update by use id.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByUseId(UserAccount record);

	/**
	 * Update access step.
	 *
	 * @param userId        the user id
	 * @param accessStep    the access step
	 * @param preAccessStep the pre access step
	 * @return the int
	 */
	int updateAccessStep(@Param("userId") Long userId, @Param("accessStep") Integer accessStep, @Param("preAccessStep") Integer preAccessStep);

	int deleteByUserId(Long userId);
}