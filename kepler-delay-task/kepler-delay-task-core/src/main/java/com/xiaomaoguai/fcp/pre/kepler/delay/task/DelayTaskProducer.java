package com.xiaomaoguai.fcp.pre.kepler.delay.task;

/**
 * 延迟任务-生产者
 *
 * @fileName: DelayTaskProducer.java
 * @author: WeiHui
 * @date: 2019/2/13 13:48
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface DelayTaskProducer {

	/**
	 * 创建延迟任务
	 *
	 * @param taskName     任务名称
	 * @param taskData     任务数据
	 * @param delaySeconds 延迟多少秒
	 */
	void createDelayTask(String taskName, Object taskData, Long delaySeconds) throws Exception;

}
