package com.xiaomaoguai.fcp.pre.kepler.oss.configuration;

import com.xiaomaoguai.fcp.pre.kepler.oss.constants.OssConstants;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author WeiHui
 * @date 2019/1/30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ConfigurationProperties(prefix = OssConstants.CONFIG_PREFIX)
public class OssProperties {

	/**
	 * oss-accessKeyId
	 */
	private String accessKeyId;

	/**
	 * oss-accessKeySecret
	 */
	private String accessKeySecret;

	/**
	 * 公网URL
	 */
	private String publicEndpoint;

	/**
	 * 内网URL
	 */
	private String privateEndpoint;

	/**
	 * oss-bucketName
	 */
	private String bucketName;

	/**
	 * oss根目录
	 */
	private String object;

	/**
	 * 生成sts信息需要
	 */
	private String arn;

}
