package com.xiaomaoguai.fcp.dao;

import com.xiaomaoguai.fcp.dto.MatrixRequestLog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @fileName: MatrixRequestLogRepository.java
 * @author: WeiHui
 * @date: 2018/10/22 19:44
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface MatrixRequestLogRepository extends ElasticsearchRepository<MatrixRequestLog, String> {

}
