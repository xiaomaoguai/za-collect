package com.xiaomaoguai.fcp.pre.kepler.convert.core;

import com.xiaomaoguai.fcp.pre.kepler.convert.core.support.ConvertSupport;

/**
 * @fileName: ConvertTemplate.java
 * @author: WeiHui
 * @date: 2018/11/24 22:01
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface ConvertTemplate {

	/**
	 * 根据 aviator 表达式 解析
	 *
	 * @param el      aviator 表达式
	 * @param envName aviator env
	 * @param root    元数据
	 * @param clazz   返回class， 主要用来 消除 Object返回类型的警告
	 * @param <T>     返回class
	 * @return 各转返回值
	 */
	<T> T parserAvExpression(String el, String envName, Object root, Class<T> clazz);

	/**
	 * 根据 aviator 表达式 各转
	 *
	 * @param el      aviator 表达式
	 * @param envName aviator env
	 * @param root    元数据
	 * @param clazz   返回class， 主要用来 消除 Object返回类型的警告
	 * @param <T>     返回class
	 * @return 各转返回值
	 */
	<T> T buildDataByEl(String el, String envName, Object root, Class<T> clazz);

	/**
	 * 根据 各转node 表达式 各转
	 *
	 * @param nodeName zk各转节点名称
	 * @param root     元数据
	 * @param clazz    返回class， 主要用来 消除 Object返回类型的警告
	 * @param <T>      返回class
	 * @return 各转返回值
	 */
	<T> T buildDataByNodeName(String nodeName, Object root, Class<T> clazz);

	/**
	 * 根据 各转node 表达式 各转
	 *
	 * @param converterSupport zk各转节点名称
	 * @param nodeName         zk各转节点名称
	 * @param root             元数据
	 * @return 各转返回值
	 */
	Object convert(ConvertSupport converterSupport, String nodeName, Object root);
}
