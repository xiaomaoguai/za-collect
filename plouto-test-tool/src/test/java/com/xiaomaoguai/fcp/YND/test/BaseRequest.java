package com.xiaomaoguai.fcp.YND.test;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/23 15:04
 * @since JDK 1.8
 */
@Getter
@Setter
public class BaseRequest extends BaseDto implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 3210636791468368205L;

	/**
	 * 业务接口版本，目前为1.0.0
	 */
	@NotBlank(message = "业务接口版本不能为空")
	private String apiVersion;

	/**
	 * 合作方用户账号.
	 */
	@NotBlank(message = "合作方用户账号不能为空")
	private String thirdUserNo;

	/**
	 * 请求时间.
	 */
	@NotBlank(message = "请求时间不能为空")
	private String reqDate;

	/**
	 * 请求流水号.
	 */
	@NotBlank(message = "请求流水号不能为空")
	private String reqNo;

	/**
	 * 接口名.
	 */
	@NotBlank(message = "接口名不能为空")
	private String apiName;

	/**
	 * 交易名.
	 */
	@NotBlank(message = "交易名不能为空")
	private String transName;

	/**
	 * 产品编码.
	 */
	@NotBlank(message = "产品编码不能为空")
	private String productCode;

}

