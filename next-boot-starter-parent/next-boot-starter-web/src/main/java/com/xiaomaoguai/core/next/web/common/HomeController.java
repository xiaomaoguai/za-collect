/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.common;

import com.xiaomaoguai.core.next.web.constants.WebConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Root Reset API 接口
 *
 * @author chenyao
 * @since 2018年5月31日 上午2:55:40
 */
@Setter
@Getter
@AllArgsConstructor
@ConditionalOnProperty(prefix = WebConstants.CONFIG_PREFIX + ".page.home", name = "enabled", matchIfMissing = true)
@RestController
public class HomeController {
    private String homeInfo;

    /**
     * 根目录
     *
     * @return 根目录展示的信息
     */
    @RequestMapping("/")
    public String home() {
        return this.getHomeInfo();
    }
}
