package com.xiaomaoguai.fcp.service;

import com.google.common.collect.ImmutableMap;
import com.xiaomaoguai.fcp.config.CreditCoreProperties;
import com.xiaomaoguai.fcp.config.FtSystemRequestHeader;
import com.xiaomaoguai.fcp.utils.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 测试环境，定时任务维持cookie
 *
 * @fileName: JobService.java
 * @author: WeiHui
 * @date: 2018/8/7 11:30
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Service
public class KeepCookieJobService {

	@Resource
	private CreditCoreProperties creditCoreProperties;

	@Scheduled(cron = "0/10 * * * * ? ")
	public void keepCookieAlive() throws Exception {
		String url = creditCoreProperties.getQueryUrl();
		try {
			String post = HttpClientUtils.doGet(url, FtSystemRequestHeader.getHeader(), ImmutableMap.of("type", "1", "serialNo", "Loan"));
			log.debug("====>请求产品平台,result: {}", post);
		} catch (HttpException e) {
			log.debug("====>请求产品平台失败", e);
		}
	}

}
