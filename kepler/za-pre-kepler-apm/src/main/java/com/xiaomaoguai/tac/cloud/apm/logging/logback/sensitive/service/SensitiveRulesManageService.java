package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service;

import com.xiaomaoguai.tac.cloud.apm.logging.logback.dto.SensitiveRulesVo;

import java.util.List;

/**
 * 脱敏规则管理
 *
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 14:02
 * @since JDK 1.8
 */
public interface SensitiveRulesManageService {

	/**
	 * 获取所有脱敏规则
	 *
	 * @return 脱敏规则列表
	 */
	List<SensitiveRulesVo> getAllSensitiveRules();

}
