package com.xiaomaoguai.fcp.fcp.controller;

import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: User.java
 * @author: WeiHui
 * @date: 2018/8/7 09:40
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class User {

	private String userName;

	private int age;
}
