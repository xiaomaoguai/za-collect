package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.xiaomaoguai.fcp.pre.plouto.mailbox.autoconfigure.AbstractMailBoxRegister;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @fileName: MyMailBoxRegister.java
 * @author: WeiHui
 * @date: 2018/11/19 21:29
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Component
public class MyMailBoxRegister {

	@Bean(initMethod = "init")
	public AbstractMailBoxRegister mailBoxRegister() {
		return new AbstractMailBoxRegister() {

			@Override
			public IMailBox[] register() {
				return TestMailEnum.values();
			}
		};
	}
}
