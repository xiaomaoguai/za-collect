/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserAccountDtl {

	private Long id;

	private String channelId;

	private String productCode;

	private String thirdUserNo;

	private Long userId;

	private Long acctId;

	private String certOrderNo;

	private String faceOrderNo;

	private Integer certType;

	private String certNo;

	private String userMobile;

	private String userName;

	private String certAddress;

	private String certExpireDate;

	private String addressJson;

	private Integer monthSalary;

	private String openId;

	private String creator;

	private Date gmtCreated;

	private String modifier;

	private Date gmtModified;

	private String isDeleted;

	private String linkInfoJson;

	private String addInfo;
}