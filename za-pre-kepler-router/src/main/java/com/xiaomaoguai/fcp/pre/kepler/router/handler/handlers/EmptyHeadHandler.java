package com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers;

import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.api.HandlerContext;
import com.xiaomaoguai.fcp.pre.kepler.router.router.RouterPipeline;

import org.springframework.stereotype.Component;

@Component
public class EmptyHeadHandler extends AbstractHandler<Buf> {

	@Override
	public void beforeHandle(HandlerContext<Buf> hc) {
		Buf buf = hc.getInnerParam();
		if (buf != null) {
			RouterPipeline<?, ?>  routerPipe = hc.getRouter();
			routerPipe.setBuf(buf);
		}
	}

	@Override
	public void genInnerParam(HandlerContext<Buf> hc) {

	}

	@Override
	public void exceptionCaught(RouterException cause) {
	}

	@Override
	public void handle(HandlerContext<Buf> hc) {
	}

}
