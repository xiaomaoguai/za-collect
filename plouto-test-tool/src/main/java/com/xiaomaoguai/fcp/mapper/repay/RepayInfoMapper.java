/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper.repay;

import com.xiaomaoguai.fcp.domain.RepayInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * The Interface RepayInfoMapper.
 */
public interface RepayInfoMapper {

	/**
	 * Insert.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(RepayInfo record);

	/**
	 * Count by loan outer no.
	 *
	 * @param loanOuterNo the loan outer no
	 * @param userId      the user id
	 * @return the int
	 */
	int countByLoanOuterNo(@Param("loanOuterNo") String loanOuterNo, @Param("userId") Long userId);

	int countByUserId(@Param("userId") Long userId, @Param("actualRepayDate") String actualRepayDate);

	/**
	 * Update by repay outer no.
	 *
	 * @param repayInfo the repay info
	 * @param status    the status
	 * @return the int
	 */
	int updateByRepayOuterNo(@Param("repayInfo") RepayInfo repayInfo, @Param("status") Integer status);

	/**
	 * Select repay info page.
	 *
	 * @param userId       the user id
	 * @param repayOuterNo the repay outer no
	 * @param loanOuterNo  the loan outer no
	 * @param repayStatus  the repay status
	 * @param pageSize     the page size
	 * @param offset       the offset
	 * @return the list
	 */
	List<RepayInfo> selectRepayInfoPage(@Param("userId") Long userId, @Param("repayOuterNo") String repayOuterNo, @Param("loanOuterNo") String loanOuterNo, @Param("repayStatus") Integer repayStatus, @Param("pageSize") Integer pageSize, @Param("offset") Integer offset);

	Integer selectCountRepayInfo(@Param("userId") Long userId, @Param("repayOuterNo") String repayOuterNo, @Param("loanOuterNo") String loanOuterNo, @Param("repayStatus") Integer repayStatus);

	RepayInfo selectRepayInfoByOuterNo(@Param("userId") Long userId, @Param("repayOuterNo") String repayOuterNo);


	int deleteByUserId(Long userId);
}