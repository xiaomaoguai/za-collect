package com.xiaomaoguai.fcp.zk;

import com.xiaomaoguai.fcp.pre.kepler.zk.utils.ZookeeperContext;
import org.apache.curator.framework.CuratorFramework;
import org.junit.Before;
import org.junit.Test;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/6/11 11:10
 * @since JDK 1.8
 */
public class ZkEnumTest {

	@Before
	public void beforeTest() {
		CuratorFramework curatorFramework = ZkEnum.TEST.getCuratorFramework();
		ZookeeperContext.setClient(curatorFramework);
	}

	@Test
	public void test() {
		try {
			ZookeeperContext.createOrUpdateData("/application/ons", "{\n" +
					"  \"CREDIT\": {\n" +
					"    \"PPDZR\": \"5518-fcp-za-pre-kepler-batch-347094-7976bbddd5-lzrqr\",\n" +
					"    \"HFQHJ\": \"6601-fcp-za-pre-kepler-batch-xdfj9-6df57cd788-c6gv5\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-8598c8d574-k68kv\",\n" +
					"    \"WBXYX\": \"hexiong\",\n" +
					"    \"JDZEDXYX\": \"fcp-za-pre-kepler-batch-sdppr-558d48f946-6hlhk\",\n" +
					"    \"FQLZX\": \"zhang-pc\"\n" +
					"  },\n" +
					"  \"WITHHOLD_PUBLIC\": {\n" +
					"    \"51YND\": \"fcp-za-pre-kepler-batch-347094-55b798c7f4-n6b9w,zhang-pc\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\"\n" +
					"  },\n" +
					"  \"SETTLE_PRIVATE\": {\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-347094-67d69f8bdc-49pjs\"\n" +
					"  },\n" +
					"  \"ULTRON_REPORT\": {},\n" +
					"  \"BATCH_COMPENSATORY\": {\n" +
					"    \"MMTT1\": \"5518-fcp-za-pre-kepler-batch-355981-857cbc6645-tbfzr\"\n" +
					"  },\n" +
					"  \"REPORT\": {\n" +
					"    \"SYSXD\": \"5858-fcp-za-pre-kepler-batch-sxvb6-5cc55f7c97-brvhx\",\n" +
					"    \"HFQHJ\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-664cb99f5-6k6xl\",\n" +
					"    \"JDZEDXYX\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-664cb99f5-6k6xl\",\n" +
					"    \"HBZX\": \"huwenxuan\"\n" +
					"  },\n" +
					"  \"WITHHOLD_PRIVATE\": {\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-347094-7976bbddd5-lzrqr\"\n" +
					"  },\n" +
					"  \"RECEIPT_SINGLETASK\": {\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\"\n" +
					"  },\n" +
					"  \"PRE_REPORT\": {\n" +
					"    \"HFQHJ\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-664cb99f5-6k6xl\",\n" +
					"    \"JDZEDXYX\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-664cb99f5-6k6xl\",\n" +
					"    \"HBZX\": \"huwenxuan\"\n" +
					"  },\n" +
					"  \"BATCH_REPAY\": {\n" +
					"    \"FQL2\": \"6830-fcp-za-pre-kepler-batch-7c8ch-7d8554cd4b-mxvkl\",\n" +
					"    \"PPDZR\": \"5518-fcp-za-pre-kepler-batch-355981-76c8ccd49b-8pmr4\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\",\n" +
					"    \"ZEDBZ\": \"yanghui001,5275-fcp-za-pre-kepler-batch-351524-585bbcdb88-7tbqt\",\n" +
					"    \"JDZEDXYX\": \"fcp-za-pre-kepler-batch-j6qbm-7d75788ff-999rb\"\n" +
					"  },\n" +
					"  \"REPAY\": {\n" +
					"    \"FQL2\": \"6830-fcp-za-pre-kepler-batch-7c8ch-7d8554cd4b-mxvkl\",\n" +
					"    \"PPDZR\": \"5518-fcp-za-pre-kepler-batch-355981-6db8dd7c68-c8bzr\",\n" +
					"    \"HFQHJ\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-55b9d57964-hx8bp\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\",\n" +
					"    \"ZEDBZ\": \"yanghui001,5275-fcp-za-pre-kepler-batch-351524-585bbcdb88-7tbqt\",\n" +
					"    \"JDZEDXYX\": \"luohuaxingdeMacBook-Pro.local,6601-fcp-za-pre-kepler-batch-xdfj9-7cf7f58898-bz6xw\"\n" +
					"  },\n" +
					"  \"IN_LOAN_RISK\": {},\n" +
					"  \"RECEIPT_BATCHTASK\": {\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\"\n" +
					"  },\n" +
					"  \"LOAN\": {\n" +
					"    \"PPDZR\": \"5518-fcp-za-pre-kepler-batch-355981-6db8dd7c68-jx6g6\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\",\n" +
					"    \"WBXYX\": \"hexiong\"\n" +
					"  },\n" +
					"  \"BATCH_DETAIL\": {\n" +
					"    \"PPDZR\": \"huwenxuan,5518-fcp-za-pre-kepler-batch-355981-76c8ccd49b-8pmr4\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\",\n" +
					"    \"ZEDBZ\": \"yanghui001,5275-fcp-za-pre-kepler-batch-351524-585bbcdb88-7tbqt\"\n" +
					"  },\n" +
					"  \"SURRENDER\": {},\n" +
					"  \"ULTRON_INSURANCE_REPAY\": {},\n" +
					"  \"SETTLE_PUBLIC\": {\n" +
					"    \"PPDZR\": \"huwenxuan,5518-fcp-za-pre-kepler-batch-355981-76c8ccd49b-8pmr4\",\n" +
					"    \"HBZR\": \"mazehong,6761-fcp-za-pre-kepler-batch-355981-5749844fc9-dmhqb\"\n" +
					"  },\n" +
					"  \"UNDERWRITE\": {\n" +
					"    \"HFQHJ\": \"6601-fcp-za-pre-kepler-batch-xdfj9-6df57cd788-c6gv5\",\n" +
					"    \"JDZEDXYX\": \"fcp-za-pre-kepler-batch-sdppr-558d48f946-6hlhk,luohuaxingdeMacBook-Pro.local\",\n" +
					"    \"FQLZX\": \"zhang-pc\"\n" +
					"  }\n" +
					"}");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testSensitive() {
		try {
			ZookeeperContext.createOrUpdateData("/za-fcp-pre-kepler/sensitive", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void test22() {
		try {
			ZookeeperContext.createOrUpdateData("/za-fcp-pre-kepler/convert_router", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDelete() {
		try {
			ZookeeperContext.delete("/product/hex");
			ZookeeperContext.delete("/product/fpjk");
			ZookeeperContext.delete("/product/testNew");
			ZookeeperContext.delete("/product/ZH");
			ZookeeperContext.delete("/product/chen3");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testUAT() {
		try {
			CuratorFramework curatorFramework = ZkEnum.UAT.getCuratorFramework("kepler");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testGlue() {
		try {
			ZookeeperContext.createOrUpdateData("/application/glue/za-pre-kepler-ops/OverdueFileHandler", "package com.xiaomaoguai.fcp.pre.kepler.ops.web.controller.biz\n" +
					"\n" +
					"import app.myoss.cloud.core.spring.context.SpringContextHolder\n" +
					"import com.alibaba.excel.EasyExcel\n" +
					"import com.alibaba.excel.context.AnalysisContext\n" +
					"import com.alibaba.excel.event.AnalysisEventListener\n" +
					"import com.alibaba.fastjson.JSON\n" +
					"import com.google.common.collect.Maps\n" +
					"import com.xiaomaoguai.fcp.pre.kepler.glue.handler.GlueHandler\n" +
					"import com.xiaomaoguai.fcp.pre.kepler.ops.feign.KeplerBaseResponse\n" +
					"import com.xiaomaoguai.fcp.pre.kepler.ops.feign.KeplerPostLoanFeign\n" +
					"import com.xiaomaoguai.fcp.pre.kepler.role.utils.ErrCode\n" +
					"import com.xiaomaoguai.fcp.pre.kepler.role.utils.ReturnUtil\n" +
					"import org.apache.commons.io.FileUtils\n" +
					"import org.apache.commons.lang3.time.DateFormatUtils\n" +
					"import org.slf4j.Logger\n" +
					"import org.slf4j.LoggerFactory\n" +
					"import org.springframework.web.multipart.MultipartFile\n" +
					"\n" +
					"/**\n" +
					" *\n" +
					" * @author WeiHui-Z\n" +
					" * @version v1.0.0* @date 2019/9/25 10:38\n" +
					" * @since JDK 1.8\n" +
					" */\n" +
					"class OverdueFileHandler implements GlueHandler {\n" +
					"\n" +
					"    /**\n" +
					"     * logger\n" +
					"     */\n" +
					"    private static final Logger log = LoggerFactory.getLogger(OverdueFileHandler.class)\n" +
					"\n" +
					"    @Override\n" +
					"    Object handle(Map<String, Object> params) {\n" +
					"        MultipartFile file = (MultipartFile) params.get(\"file\")\n" +
					"        if (file.isEmpty()) {\n" +
					"            return ReturnUtil.rtn(ErrCode.PARAM_ERROR, \"文件为空\")\n" +
					"        }\n" +
					"        // 获取文件名\n" +
					"        String fileName = file.getOriginalFilename()\n" +
					"        log.info(\"上传的文件名为：\" + fileName)\n" +
					"        // 获取文件的后缀名\n" +
					"        String suffixName = fileName.substring(fileName.lastIndexOf(\".\"))\n" +
					"        log.info(\"上传的后缀名为：\" + suffixName)\n" +
					"        file.transferTo(new File(fileName))\n" +
					"        File dest = new File(File.TempDirectory.location(), fileName)\n" +
					"        //读取excel，插入逾期信息表\n" +
					"        EasyExcel.read(FileUtils.openInputStream(dest), UploadData.class, new UploadDataListener()).sheet().doRead()\n" +
					"\n" +
					"        return null\n" +
					"    }\n" +
					"\n" +
					"    class UploadData {\n" +
					"\n" +
					"        private String productCode\n" +
					"\n" +
					"        private String thirdUserNo\n" +
					"\n" +
					"        private String repayOuterNo\n" +
					"\n" +
					"        private String loanOuterNo\n" +
					"\n" +
					"        private String installmentNo\n" +
					"\n" +
					"        private String actualRepayDate\n" +
					"\n" +
					"        private BigDecimal principal\n" +
					"\n" +
					"        private BigDecimal interest\n" +
					"\n" +
					"        String getProductCode() {\n" +
					"            return productCode\n" +
					"        }\n" +
					"\n" +
					"        void setProductCode(String productCode) {\n" +
					"            this.productCode = productCode\n" +
					"        }\n" +
					"\n" +
					"        String getThirdUserNo() {\n" +
					"            return thirdUserNo\n" +
					"        }\n" +
					"\n" +
					"        void setThirdUserNo(String thirdUserNo) {\n" +
					"            this.thirdUserNo = thirdUserNo\n" +
					"        }\n" +
					"\n" +
					"        String getRepayOuterNo() {\n" +
					"            return repayOuterNo\n" +
					"        }\n" +
					"\n" +
					"        void setRepayOuterNo(String repayOuterNo) {\n" +
					"            this.repayOuterNo = repayOuterNo\n" +
					"        }\n" +
					"\n" +
					"        String getLoanOuterNo() {\n" +
					"            return loanOuterNo\n" +
					"        }\n" +
					"\n" +
					"        void setLoanOuterNo(String loanOuterNo) {\n" +
					"            this.loanOuterNo = loanOuterNo\n" +
					"        }\n" +
					"\n" +
					"        String getInstallmentNo() {\n" +
					"            return installmentNo\n" +
					"        }\n" +
					"\n" +
					"        void setInstallmentNo(String installmentNo) {\n" +
					"            this.installmentNo = installmentNo\n" +
					"        }\n" +
					"\n" +
					"        String getActualRepayDate() {\n" +
					"            return actualRepayDate\n" +
					"        }\n" +
					"\n" +
					"        void setActualRepayDate(String actualRepayDate) {\n" +
					"            this.actualRepayDate = actualRepayDate\n" +
					"        }\n" +
					"\n" +
					"        BigDecimal getPrincipal() {\n" +
					"            return principal\n" +
					"        }\n" +
					"\n" +
					"        void setPrincipal(BigDecimal principal) {\n" +
					"            this.principal = principal\n" +
					"        }\n" +
					"\n" +
					"        BigDecimal getInterest() {\n" +
					"            return interest\n" +
					"        }\n" +
					"\n" +
					"        void setInterest(BigDecimal interest) {\n" +
					"            this.interest = interest\n" +
					"        }\n" +
					"    }\n" +
					"\n" +
					"    class UploadDataListener extends AnalysisEventListener<UploadData> {\n" +
					"\n" +
					"        private static final Logger log = LoggerFactory.getLogger(UploadDataListener.class)\n" +
					"\n" +
					"        /**\n" +
					"         * 每隔5条存储数据库，实际使用中可以200条，然后清理list ，方便内存回收\n" +
					"         */\n" +
					"        private static final int BATCH_COUNT = 5\n" +
					"\n" +
					"        private List<UploadData> list = new ArrayList<UploadData>()\n" +
					"\n" +
					"        @Override\n" +
					"        void invoke(UploadData uploadData, AnalysisContext analysisContext) {\n" +
					"            list.add(uploadData)\n" +
					"            if (list.size() >= BATCH_COUNT) {\n" +
					"                saveData()\n" +
					"                list.clear()\n" +
					"            }\n" +
					"        }\n" +
					"\n" +
					"        def saveData() {\n" +
					"            log.info(\"upload data size:{},begin call kepler-post-loan\", list.size())\n" +
					"            for (UploadData uploadData : list) {\n" +
					"                Map<String, Object> param = Maps.newHashMapWithExpectedSize(16)\n" +
					"                param.put(\"transName\", \"batchRepay\")\n" +
					"                param.put(\"isEarlySettle\", 0)\n" +
					"                param.put(\"apiName\", \"repayPreApi\")\n" +
					"                param.put(\"apiVersion\", \"1.0.0\")\n" +
					"                String nowDate = DateFormatUtils.format(new Date(), \"yyyy-MM-dd HH:mm:ss\")\n" +
					"                param.put(\"reqNo\", nowDate)\n" +
					"                param.put(\"reqDate\", nowDate)\n" +
					"\n" +
					"                param.put(\"productCode\", uploadData.getProductCode())\n" +
					"                param.put(\"thirdUserNo\", uploadData.getThirdUserNo())\n" +
					"                param.put(\"repayOuterNo\", uploadData.getRepayOuterNo())\n" +
					"                param.put(\"loanOuterNo\", uploadData.getLoanOuterNo())\n" +
					"                param.put(\"installmentNo\", uploadData.getInstallmentNo())\n" +
					"                param.put(\"repayAmount\", uploadData.getPrincipal().add(uploadData.getInterest()))\n" +
					"                param.put(\"principal\", uploadData.getPrincipal())\n" +
					"                param.put(\"interest\", uploadData.getInterest())\n" +
					"                param.put(\"repayType\", \"OVERDUE_REPAY\")\n" +
					"                param.put(\"repayMode\", \"PARTNER_REPAY\")\n" +
					"\n" +
					"                List<Map<String, Object>> repayWriteOffList = new ArrayList<>()\n" +
					"                Map<String, Object> repayWriteOffMap = Maps.newHashMapWithExpectedSize(1)\n" +
					"                repayWriteOffMap.put(\"paidInterest\", uploadData.getInterest())\n" +
					"                repayWriteOffMap.put(\"paidPrincipal\", uploadData.getPrincipal())\n" +
					"                repayWriteOffMap.put(\"actualRepayDate\", uploadData.getActualRepayDate())\n" +
					"                repayWriteOffMap.put(\"currentFlag\", 0)\n" +
					"                repayWriteOffMap.put(\"paidFee\", 0.00)\n" +
					"                repayWriteOffMap.put(\"installmentNo\", uploadData.getInstallmentNo())\n" +
					"                repayWriteOffMap.put(\"repayType\", 2)\n" +
					"\n" +
					"                repayWriteOffList.add(repayWriteOffMap)\n" +
					"                param.put(\"repayWriteOffList\", repayWriteOffList)\n" +
					"                param.put(\"reqMsg\", JSON.toJSONString(param))\n" +
					"                String requestParam = JSON.toJSONString(param)\n" +
					"\n" +
					"                log.info(\"requestParam ====> {}\", requestParam)\n" +
					"                KeplerPostLoanFeign keplerPostLoanFeign = SpringContextHolder.getBean(KeplerPostLoanFeign.class)\n" +
					"                KeplerBaseResponse yndSyncRealRepayInfoRouterResult = keplerPostLoanFeign.invoke(uploadData.getProductCode(), \"yndUploadRealRepayInfoRouter\", requestParam)\n" +
					"                log.info(\"yndSyncRealRepayInfoRouterResult ====> {}\", JSON.toJSONString(yndSyncRealRepayInfoRouterResult))\n" +
					"                if (yndSyncRealRepayInfoRouterResult.isFail()) {\n" +
					"                    log.error(\"导入逾期真实还款失败,productCode:{},loanOuterNo:{},repayOuterNo:{}\", uploadData.getProductCode(), uploadData.getLoanOuterNo(), uploadData.getRepayOuterNo())\n" +
					"                }\n" +
					"            }\n" +
					"            log.info(\"call kepler-post-loan done\")\n" +
					"        }\n" +
					"\n" +
					"        @Override\n" +
					"        void doAfterAllAnalysed(AnalysisContext analysisContext) {\n" +
					"            saveData()\n" +
					"            log.info(\"所有数据解析完成！\")\n" +
					"        }\n" +
					"    }\n" +
					"\n" +
					"}\n");
		} catch (Exception e) {
		}
	}

}
