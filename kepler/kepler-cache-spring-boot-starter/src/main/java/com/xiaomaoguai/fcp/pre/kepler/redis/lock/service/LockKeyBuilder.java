package com.xiaomaoguai.fcp.pre.kepler.redis.lock.service;

import java.lang.reflect.Method;

/**
 * 锁构造器
 *
 * @fileName: LockKeyBuilder.java
 * @author: WeiHui
 * @date: 2018/12/31 20:28
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface LockKeyBuilder {

	/**
	 * 构建锁key
	 *
	 * @param definitionKeys  表达式
	 * @param method          方法名
	 * @param parameterValues 入参
	 * @return 锁key
	 */
	String builder(String[] definitionKeys, Method method, Object... parameterValues);

}
