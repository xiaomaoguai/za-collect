package com.xiaomaoguai.javassist.kepler.config;

/**
 * @author WeiHui
 * @version v1.0.0
 * @date 2019/4/20 14:58
 * @since JDK 1.8
 */
public interface HandlerEnhancePostProcessor {

	/**
	 * 后置处理
	 */
	void postProcessor();

}
