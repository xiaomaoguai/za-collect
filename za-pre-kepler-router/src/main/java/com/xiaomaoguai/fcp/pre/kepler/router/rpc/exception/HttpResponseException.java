package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class HttpResponseException extends RuntimeException {

	private static final long serialVersionUID = 2049535499191863583L;

	public HttpResponseException(Throwable cause) {
		super(cause);
	}

	public HttpResponseException(String message) {
		super(message);
	}

	public HttpResponseException(String message, Throwable cause) {
		super(message, cause);
	}
}
