package com.xiaomaoguai.tac.cloud.apm.logging.logback.sensitive.service;

import com.xiaomaoguai.tac.cloud.apm.logging.logback.SensitiveDataRule;

import java.util.Set;

/**
 * 脱敏字段管理
 *
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/7/27 14:23
 * @since JDK 1.8
 */
public interface SensitiveDataRuleService {

	/**
	 * 初始化加载一些默认的 脱敏规则 ，注意可以被动态 脱敏规则 覆盖
	 *
	 * @param sensitiveDataRules 默认脱敏规则
	 */
	void initLoadSensitiveDataRule(Set<SensitiveDataRule> sensitiveDataRules);

	/**
	 * 增加一个脱敏规则
	 *
	 * @param sensitiveDataRule 脱敏规则
	 * @return true-增加成功
	 */
	boolean addSensitiveDataRule(SensitiveDataRule sensitiveDataRule);

	/**
	 * 删除一个脱敏规则
	 *
	 * @param fieldName 脱敏字段
	 * @return true-删除成功
	 */
	boolean deleteSensitiveDataRule(String fieldName);

	/**
	 *   清除所有脱敏规则
	 */
	void clearAll( );

	/**
	 * 查询现在生效的所有默认规则
	 *
	 * @return 所有脱敏规则
	 */
	Set<SensitiveDataRule> getAllSensitiveDataRule();

}
