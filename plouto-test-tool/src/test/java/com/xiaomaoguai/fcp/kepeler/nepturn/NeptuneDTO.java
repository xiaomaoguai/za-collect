package com.xiaomaoguai.fcp.kepeler.nepturn;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/23 16:45
 * @since JDK 1.8
 */
@Getter
@Setter
public class NeptuneDTO implements Serializable {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 4411645168490477109L;

	/**
	 * test uat prd
	 */
	private String profile;

	/**
	 * 应用名称
	 */
	private String name;

	/**
	 * 标签
	 */
	private String label;

}
