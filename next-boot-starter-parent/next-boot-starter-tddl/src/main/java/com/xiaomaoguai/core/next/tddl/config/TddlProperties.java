/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.tddl.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import com.xiaomaoguai.core.next.tddl.constants.TddlConstants;

import lombok.Data;

/**
 * TDDL 属性配置，支持单个数据库
 *
 * @author chenyao
 * @since 2018年5月17日 下午1:38:19
 */
@Data
@ConfigurationProperties(prefix = TddlConstants.CONFIG_PREFIX)
public class TddlProperties {
    /**
     * 是否启用 TDDL 自动配置
     */
    private Boolean                enabled;
    /**
     * TDDL appName
     */
    private String                 appName;
    /**
     * TDDL groupName, 如果需要设置多个, 请使用英文逗号隔开
     */
    private String                 groupName;

    /**
     * TDDL Sequence 属性配置
     */
    @NestedConfigurationProperty
    private TddlSequenceProperties sequenceConfig = new TddlSequenceProperties();
}
