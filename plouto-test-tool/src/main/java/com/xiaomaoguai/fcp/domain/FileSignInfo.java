package com.xiaomaoguai.fcp.domain;

import java.util.Date;

public class FileSignInfo {
    /**
     *  pk
     */
    private Long id;

    /**
     *  渠道子账户id
     */
    private Long userId;

    /**
     *  第三方渠道编号
     */
    private String thirdUserNo;

    /**
     *  产品code
     */
    private String productCode;

    /**
     *  文件类型 (1=借款合同 2=服务协议 3...)
     */
    private Integer fileType;

    /**
     *  内部协议号
     */
    private String loanInnerNo;

    /**
     *  签章流水号
     */
    private String loanOuterNo;

    /**
     *  状态(1=签章中,2=受理成功,3=受理失败,4=签章成功,5=签章失败)
     */
    private Integer signStatus;

    /**
     *  签章返回code
     */
    private String signRespCode;

    /**
     *  签章返回结果
     */
    private String signRespMsg;

    /**
     *  签章成功下载地址
     */
    private String downUrl;

    /**
     *  是否已通知
     */
    private Boolean isNotify;

    /**
     *  创建时间
     */
    private Date gmtCreated;

    /**
     *  
     */
    private String creator;

    /**
     *  更新时间
     */
    private Date gmtModified;

    /**
     *  
     */
    private String modifier;

    /**
     *  
     */
    private String isDeleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getThirdUserNo() {
        return thirdUserNo;
    }

    public void setThirdUserNo(String thirdUserNo) {
        this.thirdUserNo = thirdUserNo == null ? null : thirdUserNo.trim();
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }

    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }

    public String getLoanInnerNo() {
        return loanInnerNo;
    }

    public void setLoanInnerNo(String loanInnerNo) {
        this.loanInnerNo = loanInnerNo == null ? null : loanInnerNo.trim();
    }

    public String getLoanOuterNo() {
        return loanOuterNo;
    }

    public void setLoanOuterNo(String loanOuterNo) {
        this.loanOuterNo = loanOuterNo == null ? null : loanOuterNo.trim();
    }

    public Integer getSignStatus() {
        return signStatus;
    }

    public void setSignStatus(Integer signStatus) {
        this.signStatus = signStatus;
    }

    public String getSignRespCode() {
        return signRespCode;
    }

    public void setSignRespCode(String signRespCode) {
        this.signRespCode = signRespCode == null ? null : signRespCode.trim();
    }

    public String getSignRespMsg() {
        return signRespMsg;
    }

    public void setSignRespMsg(String signRespMsg) {
        this.signRespMsg = signRespMsg == null ? null : signRespMsg.trim();
    }

    public String getDownUrl() {
        return downUrl;
    }

    public void setDownUrl(String downUrl) {
        this.downUrl = downUrl == null ? null : downUrl.trim();
    }

    public Boolean getIsNotify() {
        return isNotify;
    }

    public void setIsNotify(Boolean isNotify) {
        this.isNotify = isNotify;
    }

    public Date getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator == null ? null : creator.trim();
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier == null ? null : modifier.trim();
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted == null ? null : isDeleted.trim();
    }
}