package com.xiaomaoguai.fcp.mapper.credit;

import com.xiaomaoguai.fcp.domain.UserImageTransfer;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * The Interface UserImageTransferMapper.
 */
public interface UserImageTransferMapper {

	/**
	 * 新写入数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int insert(UserImageTransfer record);

	/**
	 * 根据主键来更新符合条件的数据库记录.
	 *
	 * @param record the record
	 * @return the int
	 */
	int updateByCredit(UserImageTransfer record);

	/**
	 * 查询需要上送影像的数据.
	 *
	 * @param state   最大状态
	 * @param endTime 截止时间
	 * @return the list
	 */
	List<UserImageTransfer> selectNeedTransferInfo(
			@Param("state") Integer state, @Param("endTime") Date endTime);

	/**
	 * 获取历史上送身份证ORC信息不为空的记录.
	 *
	 * @param productCode 产品编码
	 * @param userId      用户ID
	 * @param state       the state
	 * @return the user image transfer
	 */
	UserImageTransfer selectFirstHistoryOrcInfo(@Param("productCode") String productCode
			, @Param("userId") Long userId
			, @Param("state") Integer state);

	/**
	 * 查询指定授信申请号的数据.
	 *
	 * @param creditNo 授信申请号
	 * @return the int
	 */
	int countByCreditNo(String creditNo);

	UserImageTransfer selectByCreditNo(@Param("creditNo") String creditNo);

}