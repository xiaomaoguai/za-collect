package com.xiaomaoguai.fcp.pre.plouto.mailbox;

import com.dh.mailbox2.api.Actor;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @fileName: TestMailEnum.java
 * @author: WeiHui
 * @date: 2018/11/6 11:07
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum TestMailEnum implements IMailBox {

	/**
	 * 测试actor
	 */
	TestActor(TestActor.class, 32, 5),
	;

	/**
	 * actor class
	 */
	private final Class<? extends Actor> clazz;

	/**
	 * bufferSize
	 */
	private final int bufferSize;

	/**
	 * threadSize
	 */
	private final int threadSize;

}
