package com.dh.convert.rule.validator;


import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

@Validator("Name")
public class NameValidator extends AbstractValidator<String>{

	private static final Pattern NAME_REG = Pattern.compile("^[\u4e00-\u9fa5]+(·[\u4e00-\u9fa5]+)*$");
    private String regexp;
    private int len = 25;
	@Override
	public void initialize() {
		
	}
    @Override
    public boolean isValid(String value) {
        if (StringUtils.isBlank(value))
            return false;
        if(value.length()>len)
        	return false;
        if(StringUtils.isBlank(regexp))
        	return NAME_REG.matcher(value).matches();
        else{
        	return Pattern.compile(regexp).matcher(value).matches();
        }
    }

}
