package com.xiaomaoguai.fcp.pre.kepler.glue;

import com.xiaomaoguai.fcp.pre.kepler.glue.autoconfiguration.GlueBootstrap;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 20:25
 * @since JDK 1.8
 */
@RestController
@RequestMapping
@AllArgsConstructor
public class RunController {

	private final GlueBootstrap glueBootstrap;

	@RequestMapping(path = "/run/{name}", method = {RequestMethod.POST, RequestMethod.GET})
	public Object index(@PathVariable(value = "name") String name, Map<String, Object> params) throws Exception {
		return glueBootstrap.run(name, params);
	}

	@RequestMapping(path = "/getAllGlueClass", method = {RequestMethod.POST, RequestMethod.GET})
	public Object getAllGlueClass() {
		return glueBootstrap.getAllGlueClass();
	}

}
