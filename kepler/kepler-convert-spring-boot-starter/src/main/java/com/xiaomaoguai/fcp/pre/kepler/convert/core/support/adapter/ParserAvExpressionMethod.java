package com.xiaomaoguai.fcp.pre.kepler.convert.core.support.adapter;

import java.lang.reflect.Method;

/**
 * @fileName: ParserAvExpressionMethod.java
 * @author: WeiHui
 * @date: 2018/11/26 10:20
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class ParserAvExpressionMethod extends AbstractConvertMethod {

    @Override
    public boolean canHandler(Method method) {
        return "parserAvExpression".equals(method.getName());
    }

    @Override
    public Object invoke(Method method, Object[] args) {
        return parserAvExpression((String) args[0], (String) args[1], args[2], method.getReturnType());
    }

}
