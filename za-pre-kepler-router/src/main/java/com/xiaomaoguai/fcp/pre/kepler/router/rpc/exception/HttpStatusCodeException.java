package com.xiaomaoguai.fcp.pre.kepler.router.rpc.exception;

public class HttpStatusCodeException extends RuntimeException {

	private static final long serialVersionUID = -776496430831424872L;
	private final String url;
	private final int statusCode;
	private final String statusMessage;

	public HttpStatusCodeException(String url, int statusCode, String statusMessage) {
		super("Request url[=" + url + "] failed, status code is " + statusCode + ",status message is " + statusMessage);
		this.url = url;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
	}

	/**
	 * 请求失败时的HTTP Status Code
	 *
	 * @return Http错误码
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * 请求失败时错误消息
	 *
	 * @return 失败消息
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * 返回请求地址
	 *
	 * @return 请求地址
	 */
	public String getUrl() {
		return url;
	}
}
