package com.xiaomaoguai.fcp.pre.kepler.redis.cache;

import java.util.Arrays;
import java.util.Objects;

/**
 * @fileName: RedisCacheKeyBuilder.java
 * @author: WeiHui
 * @date: 2019/1/4 19:34
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface RedisCacheKeyBuilder {

	/**
	 * 缓存前缀
	 *
	 * @return 前缀
	 */
	String getPrefix();

	/**
	 * redis key 拼接， 类似 db:table 形式
	 *
	 * @param keys 多层key值
	 * @return redis key
	 */
	default String key(Object... keys) {
		StringBuilder builder = new StringBuilder();
		builder.append(this.getPrefix());
		if (keys != null) {
			Arrays.stream(keys).forEach(k -> {
				if (Objects.nonNull(k)) {
					builder.append(":");
					builder.append(k.toString());
				}
			});
		}
		return builder.toString();
	}

}
