package com.dh.convert.exception;

public class ValidateException extends IllegalArgumentException {

	private static final long serialVersionUID = 251919502366896411L;

    public ValidateException(String message) {
        super(message);
    }
}
