package com.xiaomaoguai.fcp.dto.resp;

import com.xiaomaoguai.fcp.dto.BaseDto;
import lombok.Getter;
import lombok.Setter;

/**
 * @fileName: ClearDataResp.java
 * @author: WeiHui
 * @date: 2018/10/26 13:52
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@Setter
public class ClearDataResp extends BaseDto {

	/**
	 * uid
	 */
	private static final long serialVersionUID = 595469504612520721L;

	private int applyClearNum;

	private int loanClearNum;

	private int repayClearNum;

	private int allDataClearNum;
}
