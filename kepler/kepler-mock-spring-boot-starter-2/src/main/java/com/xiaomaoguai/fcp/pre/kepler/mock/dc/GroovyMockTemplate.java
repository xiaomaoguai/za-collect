package com.xiaomaoguai.fcp.pre.kepler.mock.dc;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.FastDateFormat;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2020/2/16 13:23
 * @since JDK 1.8
 */
public class GroovyMockTemplate {

	public static void createTemplate(String path, String groovyName) {
		StringBuilder sb = new StringBuilder("package com.xiaomaoguai.fcp.pre.kepler.mock.feignmock\n" +
				"\n" +
				"import com.xiaomaoguai.fcp.pre.kepler.common.config.listener.glue.GlueHandler\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("/**\n" +
				" *\n" +
				" * @author August.Zhang* @version v1.0.0* @date " + FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(new Date()) +
				" * @since JDK 1.8\n" +
				" */\n");

		sb.append("class ").append(groovyName).append(" implements GlueHandler {\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("\n");
		sb.append("    @Override\n" +
				"    Object handle(final Map<String, Object> params) {\n");
		sb.append("  BaseResp<LoanResultDTO> baseResp = new BaseResp()\n");
		sb.append("  baseResp.setResultCode(ResultCode.SUCCESS)\n");
		sb.append("  return baseResp\n");
		sb.append("\n");
		sb.append("}\n");

		String filePath = path + groovyName + ".groovy";
		try {
			FileUtils.writeStringToFile(new File(filePath), sb.toString(), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

