/*
 * Copyright 2019 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */

package com.xiaomaoguai.core.next.web.constants;

import com.xiaomaoguai.core.next.core.constants.CoreConstants;

/**
 * web常量
 *
 * @author chenyao
 * @since 2018年5月17日 上午11:31:29
 */
public class WebConstants {
    /**
     * web配置前缀
     */
    public static final String CONFIG_PREFIX = CoreConstants.BOOT_CONFIG_PREFIX + ".web";
}
