package com.xiaomaoguai.fcp.YND.test;

import org.junit.Test;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/9/23 17:14
 * @since JDK 1.8
 */
public class DesUtilsTest {

	private static final String key = "664b874ed42a9615f5ed265ddfd6ce01";

	@Test
	public void test() {
		DesFileUtils.decryptFile(key,"D:/underwrite_detail_20190923.csv","D:/underwrite_detail_20190923_des.csv");
	}

}
