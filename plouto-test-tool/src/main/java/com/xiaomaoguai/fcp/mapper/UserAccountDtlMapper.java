/*
 *  * Copyright 2017 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com
 * anything without Bugs
 *
 **/
package com.xiaomaoguai.fcp.mapper;

import com.xiaomaoguai.fcp.domain.UserAccount;

/**
 * The Interface UserAccountDtlMapper.
 */
public interface UserAccountDtlMapper {

    /**
     * Insert selective.
     *
     * @param record the record
     * @return the int
     */
    int insertSelective(UserAccount record);

    /**
     * Query user info by user id.
     *
     * @param userId the user id
     * @return the user account dtl
     */
    UserAccount queryUserInfoByUserId(Long userId);


    /**
     * Query user info by user id.
     *
     * @param userId the user id
     * @return the user account dtl
     */
    UserAccount queryUserDtlInfoByUserId(Long userId);

    /**
     * Update by user id.
     *
     * @param record the record
     * @return the int
     */
    int updateByUserId(UserAccount record);

    /**
     * Gets the open id.
     *
     * @param userId the user id
     * @return the open id
     */
    UserAccount getOpenId(Long userId);

	int deleteByUserId(Long userId);
}