package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.request;

import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core.NettyAsyncContext;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core.NettyContext;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.servlet.core.NettyRequestDispatcher;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;
import org.apache.commons.javaflow.api.Continuation;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.security.Principal;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author DH
 */
public class NettyHttpServletRequest implements HttpServletRequest {

    public static final String DISPATCHER_TYPE = NettyRequestDispatcher.class.getName() + ".DISPATCHER_TYPE";

    private static final String DEFAULT_CHARSET = "UTF-8";

    private final ChannelHandlerContext ctx;

    private final NettyContext servletContext;

    private final FullHttpRequest request;

    private final NettyServletInputStream inputStream;

    /*====== Request Attributes 相关方法 开始 ======*/
    private final Map<String, Object> attributes;

    public Continuation c;

    private boolean asyncSupported = true;

    private NettyAsyncContext asyncContext;

    /*====== Header 相关方法 开始 ======*/
    private HttpHeaders headers;

    /*====== 各种路径 相关方法 开始 ======*/
    private String servletPath;

    private String queryString;

    private String pathInfo;

    private String requestUri;

    private transient boolean isPathsParsed = false;

    /*====== Session 考虑到基本是分布式系统，取消本地session缓存 相关方法 开始 ======*/
    private boolean isCookieSession;

    private boolean isURLSession;

    private transient boolean isParameterParsed = false; //请求参数是否已经解析

    private Map<String, String[]> paramMap = new HashMap<>(); //存储请求参数
    /*====== Header 相关方法 结束 ======*/

    private InetSocketAddress socketAddress; //请求的服务地址

    private transient boolean isServerParsed = false; //请求服务地址是否已经解析过

    private String characterEncoding;

    public NettyHttpServletRequest(ChannelHandlerContext ctx, NettyContext servletContext, FullHttpRequest request) {
        this.ctx = ctx;
        this.servletContext = servletContext;
        this.request = request;
        this.inputStream = new NettyServletInputStream(request);
        this.attributes = new ConcurrentHashMap<>();
        this.headers = request.headers();
    }

    public Continuation getC() {
        return c;
    }

    public void setC(Continuation c) {
        this.c = c;
    }

    public ChannelHandlerContext getCtx() {
        return ctx;
    }

    HttpRequest getNettyRequest() {
        return request;
    }

    @Override
    public Object getAttribute(String name) {
        return attributes.get(name);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(attributes.keySet());
    }

    @Override
    public String getCharacterEncoding() {
        if (characterEncoding == null) {
            characterEncoding = parseCharacterEncoding();
        }
        return characterEncoding;
    }

    @Override
    public void setCharacterEncoding(String env) throws UnsupportedEncodingException {
        characterEncoding = env;
    }

    @Override
    public int getContentLength() {
        return HttpUtil.getContentLength(request, -1);
    }

    @Override
    public long getContentLengthLong() {
        return (long) getContentLength();
    }
    /*====== 各种路径 相关方法 结束 ======*/

    @Override
    public String getContentType() {
        return headers.get("content-type");
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        return inputStream;
    }

    @Override
    public String getParameter(String name) {
        checkParameterParsed();
        String[] values = paramMap.get(name);
        if (values == null || values.length == 0) {
            return null;
        }
        return values[0];
    }

    /**
     * 检查请求参数是否已经解析。
     * 如果还没有则解析之。
     */
    private void checkParameterParsed() {
        if (!isParameterParsed) {
            parseParameter();
        }
    }

    /**
     * 解析请求参数
     */
    private void parseParameter() {
        if (isParameterParsed) {
            return;
        }

        QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.uri());
        Map<String, List<String>> params = queryStringDecoder.parameters();
        for (Map.Entry<String, List<String>> entry : params.entrySet()) {
            List<String> valueList = entry.getValue();
            String[] valueArray = new String[valueList.size()];
            paramMap.put(entry.getKey(), valueList.toArray(valueArray));
        }

        this.isParameterParsed = true;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        checkParameterParsed();
        return Collections.enumeration(paramMap.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {
        checkParameterParsed();
        return paramMap.get(name);
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        checkParameterParsed();
        return paramMap;
    }

    /*====== 请求协议、地址、端口 相关方法 开始 ======*/
    @Override
    public String getProtocol() {
        return request.protocolVersion().toString();
    }

    @Override
    public String getScheme() {
        return request.protocolVersion().protocolName();
    }
    /*====== Session 相关方法 结束 ======*/



    /*====== Request Parameters 相关方法 开始 ======*/

    @Override
    public String getServerName() {
        checkAndParseServer();
        return socketAddress.getHostName();
    }

    private void checkAndParseServer() {
        if (isServerParsed) {
            return;
        }
        String hostHeader = headers.get("Host");
        if (hostHeader != null) {
            String[] parsed = hostHeader.split(":");
            if (parsed.length > 1) {
                socketAddress = new InetSocketAddress(parsed[0], Integer.parseInt(parsed[1]));
            } else {
                socketAddress = new InetSocketAddress(parsed[0], 80);
            }
        }
        isServerParsed = true;
    }

    @Override
    public int getServerPort() {
        checkAndParseServer();
        return socketAddress.getPort();
    }

    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(inputStream, getCharacterEncoding()));
    }

    @Override
    public String getRemoteAddr() {
        return ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress();
    }

    @Override
    public String getRemoteHost() {
        return ((InetSocketAddress) ctx.channel().remoteAddress()).getHostName();
    }

    @Override
    public void setAttribute(String name, Object o) {
        if (o != null)
            attributes.put(name, o);
    }

    @Override
    public void removeAttribute(String name) {
        attributes.remove(name);
    }

    /*====== Request Parameters 相关方法 结束 ======*/

    @Override
    public Locale getLocale() {
        return null;
    }

    @Override
    public Enumeration<Locale> getLocales() {
        return null;
    }

    @Override
    public boolean isSecure() {
        return getScheme().equalsIgnoreCase("HTTPS");
    }

    @Override
    public RequestDispatcher getRequestDispatcher(String path) {
        return null;
    }

    @Override
    @Deprecated
    public String getRealPath(String path) {
        return null;
    }

    @Override
    public int getRemotePort() {
        return ((InetSocketAddress) ctx.channel().remoteAddress()).getPort();
    }

    @Override
    public String getLocalName() {
        return "localhost";
    }

    @Override
    public String getLocalAddr() {
        return "127.0.0.1";
    }

    @Override
    public int getLocalPort() {
        return 0;
    }

    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public AsyncContext startAsync() {
        return startAsync(this, null);
    }

    @Override
    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) {
        return ((NettyAsyncContext) getAsyncContext()).startAsync(servletRequest, servletResponse);
    }

    @Override
    public boolean isAsyncStarted() {
        return null != asyncContext && asyncContext.isAsyncStarted();
    }

    /*====== 请求协议、地址、端口 相关方法 结束 ======*/

    @Override
    public boolean isAsyncSupported() {
        return asyncSupported;
    }

    void setAsyncSupported(boolean asyncSupported) {
        this.asyncSupported = asyncSupported;
    }

    @Override
    public AsyncContext getAsyncContext() {
        if (null == asyncContext) {
            asyncContext = new NettyAsyncContext(this, ctx);
        }
        return asyncContext;
    }

    @Override
    public DispatcherType getDispatcherType() {
        return attributes.containsKey(DISPATCHER_TYPE) ? (DispatcherType) attributes.get(DISPATCHER_TYPE) : DispatcherType.REQUEST;
    }

    private String parseCharacterEncoding() {
        String contentType = getContentType();
        if (contentType == null) {
            return DEFAULT_CHARSET;
        }
        int start = contentType.indexOf("charset=");
        if (start < 0) {
            return DEFAULT_CHARSET;
        }
        String encoding = contentType.substring(start + 8);
        int end = encoding.indexOf(';');
        if (end >= 0) {
            encoding = encoding.substring(0, end);
        }
        encoding = encoding.trim();
        if ((encoding.length() > 2) && (encoding.startsWith("\""))
                && (encoding.endsWith("\""))) {
            encoding = encoding.substring(1, encoding.length() - 1);
        }
        return encoding.trim();
    }

    @Override
    public String getAuthType() {
        return null;
    }

    /*====== Request Attributes 相关方法 结束 ======*/



    /*====== 异步 相关方法 开始 ======*/

    @Override
    public Cookie[] getCookies() {
        return null;
    }

    @Override
    public long getDateHeader(String name) {
        return Optional.ofNullable(this.headers.getTimeMillis(name)).orElse(-1L);
    }

    @Override
    public String getHeader(String name) {
        return this.headers.get(name);
    }

    @Override
    public Enumeration<String> getHeaders(String name) {
        return Collections.enumeration(this.headers.getAll(name));
    }

    @Override
    public Enumeration<String> getHeaderNames() {
        return Collections.enumeration(this.headers.names());
    }

    @Override
    public int getIntHeader(String name) {
        String headerStringValue = this.headers.get(name);
        if (headerStringValue == null) {
            return -1;
        }
        return Integer.parseInt(headerStringValue);
    }

    /*====== 异步 相关方法 结束 ======*/

    @Override
    public String getMethod() {
        return request.method().name();
    }

    //TODO ServletPath和PathInfo应该是互补的，根据URL-Pattern匹配的路径不同而不同
    // 现在把PathInfo恒为null，ServletPath恒为uri-contextPath
    // 可以满足SpringBoot的需求，但不满足ServletPath和PathInfo的语义
    // 需要在RequestUrlPatternMapper匹配的时候设置,new NettyRequestDispatcher的时候传入MapperData
    @Override
    public String getPathInfo() {
        checkAndParsePaths();
        return this.pathInfo;
    }

    /*====== multipart/form-data 相关方法 结束 ======*/

    private void checkAndParsePaths() {
        if (isPathsParsed) {
            return;
        }

        String servletPath = request.uri().replace(servletContext.getContextPath(), "");
        if (!servletPath.startsWith("/")) {
            servletPath = "/" + servletPath;
        }
        int queryInx = servletPath.indexOf('?');
        if (queryInx > -1) {
            this.queryString = servletPath.substring(queryInx + 1, servletPath.length());
            servletPath = servletPath.substring(0, queryInx);
        }
        this.servletPath = servletPath;
        this.requestUri = this.servletContext.getContextPath() + servletPath; //TODO 加上pathInfo
        this.pathInfo = null;

        isPathsParsed = true;
    }

    @Override
    public String getPathTranslated() {
        return null;
    }

    @Override
    public String getContextPath() {
        return servletContext.getContextPath();
    }

    @Override
    public String getQueryString() {
        checkAndParsePaths();
        return this.queryString;
    }

    @Override
    public String getRemoteUser() {
        return null;
    }

    @Override
    public boolean isUserInRole(String role) {
        return false;
    }

    @Override
    public Principal getUserPrincipal() {
        return null;
    }

    @Override
    public String getRequestedSessionId() {
        return null;
    }

    @Override
    public String getRequestURI() {
        checkAndParsePaths();
        return this.requestUri;
    }

    @Override
    public StringBuffer getRequestURL() {
        checkAndParsePaths();
        StringBuffer requestBuffer = new StringBuffer();
        requestBuffer.append(request.headers().get(HttpHeaderNames.HOST.toString()));
        requestBuffer.append(requestUri);
        return requestBuffer;
    }

    @Override
    public String getServletPath() {
        checkAndParsePaths();
        return this.servletPath;
    }

    @Override
    public HttpSession getSession(boolean create) {
        return null;
    }


    /*====== 以下是暂不处理的接口方法 ======*/

    @Override
    public HttpSession getSession() {
        return getSession(true);
    }

    @Override
    public String changeSessionId() {
        return null;
    }

    @Override
    public boolean isRequestedSessionIdValid() {
        return false;
    }

    @Override
    public boolean isRequestedSessionIdFromCookie() {
        return isCookieSession;
    }

    @Override
    public boolean isRequestedSessionIdFromURL() {
        return isURLSession;
    }

    @Override
    @Deprecated
    public boolean isRequestedSessionIdFromUrl() {
        return isRequestedSessionIdFromURL();
    }

    @Override
    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        return false;
    }

    @Override
    public void login(String username, String password) throws ServletException {
    }

    @Override
    public void logout() throws ServletException {
    }

    /*====== multipart/form-data 相关方法 开始 ======*/
    @Override
    public Collection<Part> getParts() throws IOException, IllegalStateException, ServletException {
        //TODO
        return null;
    }

    @Override
    public Part getPart(String name) throws IOException, IllegalStateException, ServletException {
        //TODO
        return null;
    }

    @Override
    public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
        return null;
    }
}
