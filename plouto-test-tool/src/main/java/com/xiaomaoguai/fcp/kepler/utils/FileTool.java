package com.xiaomaoguai.fcp.kepler.utils;

import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * @功能 文件相关常量
 *
 * @author zhangfangqing
 * @date 2016年7月19日
 * @time 上午10:35:12
 */
public class FileTool {
	
	private static final Logger logger = LoggerFactory.getLogger(FileTool.class);

	/**
	 * @功能 创建文件
	 *
	 * @author zhangfangqing
	 * @date 2016年7月21日
	 * @time 下午6:56:35
	 */
	public static boolean createFile(File file) throws Exception {
		boolean flag = false;
		if (!file.exists()) {
			file.createNewFile();
			flag = true;
		}
		return flag;
	}

	/**
	 * @功能 往文件添加内容
	 *
	 * @author zhangfangqing
	 * @date 2016年7月21日
	 * @time 下午6:58:00
	 */
	public static boolean writeTxtFile(String content, File file) throws Exception {
		RandomAccessFile mm = null;
		boolean flag = false;
		FileOutputStream o = null;
		try {
			o = new FileOutputStream(file);
			o.write(content.getBytes("UTF-8"));
			o.close();
			flag = true;
		} catch (Exception e) {
			logger.error("====writeTxtFile异常：", e);
		} finally {
			if (mm != null) {
				mm.close();
			}
		}
		return flag;
	}


	/**
	 * @功能   读取文件内容转换成对象集合
	 * change by DH
	 * @author zhangfangqing 
	 * @param <T>
	 * @throws java.io.IOException
	 * @date 2016年7月21日 
	 * @time 下午8:57:15
	 */
	public static <T> List<T> readTxtFile(String filePath , String encoding ,Class<T> clazz) throws IOException {
		String lineTxt = null ;
		InputStreamReader read = null ;
		BufferedReader bufferedReader = null ;
		List<T> list = new ArrayList<T>();
		try {
			File file = new File(filePath);
			if (file.isFile() && file.exists()) { 
				read = new InputStreamReader(new FileInputStream(file),encoding);
			    bufferedReader = new BufferedReader(read);
			    
				while ((lineTxt = bufferedReader.readLine()) != null) {
				    if(clazz !=null)
				        list.add(JSON.parseObject(lineTxt,clazz));
				}
				read.close();
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			logger.error("=========================文件不存在或者读取文件内容异常：", e);
		}finally {
			if(read != null){
				read.close();
			}
			if(bufferedReader != null){
				bufferedReader.close();
			}
		}
		return list;
	}
	

	/**
	 * @功能       删除文件
	 *
	 * @author zhangfangqing 
	 * @date 2016年8月1日 
	 * @time 上午10:30:02
	 */
	public static void removeFile(String filePath, String suffix) throws IOException {
		File file = new File(filePath);
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			File tmp = files[i];
			if (tmp.toString().endsWith(suffix)) {
				tmp.delete();
			}
		}
	}
	
	
	/**
	 * @功能       删除指定文件
	 *
	 * @author zhangfangqing 
	 * @date 2016年8月1日 
	 * @time 上午10:30:02
	 */
	public static void removeFileByName(String filePath, String fileName) throws IOException {
		File file = new File(filePath);
		File[] files = file.listFiles();
		for (int i = 0; i < files.length; i++) {
			File tmp = files[i];
			if (tmp.getName().equals(fileName)) {
				tmp.delete();
			}
		}
	}
	

	/**
	 * @功能   下载远程文件并保存到本地  
	 *
	 * @author zhangfangqing 
	 * @date 2016年8月15日 
	 * @time 下午2:19:16
	 */
    public static void downloadFile(String remoteFilePath, String fileName) throws IOException
	{
		URL urlfile = null;
		HttpURLConnection httpUrl = null;
		BufferedInputStream bis = null;
		BufferedOutputStream bos = null;
		File f = new File(fileName);
		try {
			urlfile = new URL(remoteFilePath);
			httpUrl = (HttpURLConnection) urlfile.openConnection();
			httpUrl.connect();
			bis = new BufferedInputStream(httpUrl.getInputStream());
			bos = new BufferedOutputStream(new FileOutputStream(f));
			int len = 2048;
			byte[] b = new byte[len];
			while ((len = bis.read(b)) != -1) {
				bos.write(b, 0, len);
			}
			bos.flush();
			bis.close();
			httpUrl.disconnect();
		} catch (Exception e) {
			logger.error("====下载远程文件并保存到本地异常：" , e);
		} finally {
			try {
				if (bis != null) {
					bis.close();
				}
				if (bos != null) {
					bos.close();
				}
			} catch (IOException e) {
				logger.error("====下载远程文件并保存到本地，关闭异常：" , e);
			}
		}
	}

}
