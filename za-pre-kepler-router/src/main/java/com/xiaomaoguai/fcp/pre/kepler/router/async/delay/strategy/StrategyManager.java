package com.xiaomaoguai.fcp.pre.kepler.router.async.delay.strategy;

public class StrategyManager {

	public static RetryStrategy create(String strategy, Long interval, Long maxRetryTimes) {
		switch (Strategy.valueOf(strategy)) {
			case defaultStrategy:
				return new DefaultStrategy();
			case fibStrategy:
				return new FibStrategy(interval, maxRetryTimes);
			case fixedTimeStrategy:
				return new FixedTimeStrategy(interval);
			default:
				break;
		}
		return null;
	}

	enum Strategy {
		defaultStrategy,
		fibStrategy,
		fixedTimeStrategy;
	}

}
