package com.xiaomaoguai.fcp.pre.kepler.zk.autoconfiguration;

import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.xiaomaoguai.fcp.pre.kepler.zk.properties.ZookeeperProperties;
import com.xiaomaoguai.fcp.pre.kepler.zk.utils.ZookeeperContext;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.ACLProvider;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.data.ACL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

/**
 * @fileName: ZookeeperAutoConfiguration.java
 * @author: WeiHui
 * @date: 2019/1/11 17:37
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Configuration
@EnableConfigurationProperties(ZookeeperProperties.class)
public class ZookeeperAutoConfiguration {

	/**
	 * logger
	 */
	private final Logger logger = LoggerFactory.getLogger(ZookeeperAutoConfiguration.class);

	/**
	 * 线程工厂
	 */
	private ThreadFactory threadFactory = new ThreadFactoryBuilder()
			.setDaemon(true)
			.setNameFormat("PathNodeListener-%s")
			.build();

	@Resource
	private ZookeeperProperties zookeeperProperties;

	@Bean
	public CuratorFramework curatorFramework() {
		ZookeeperProperties zkConfig = zookeeperProperties;
		logger.debug("zookeeper registry center init, server lists is: {}.", zkConfig.getServerLists());
		CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder()
				.threadFactory(threadFactory)
				.connectString(zkConfig.getServerLists())
				.retryPolicy(new ExponentialBackoffRetry(
						zkConfig.getBaseSleepTimeMilliseconds(),
						zkConfig.getMaxRetries(),
						zkConfig.getMaxSleepTimeMilliseconds()))
				.namespace(zkConfig.getNamespace());
		if (0 != zkConfig.getSessionTimeoutMilliseconds()) {
			builder.sessionTimeoutMs(zkConfig.getSessionTimeoutMilliseconds());
		}
		if (0 != zkConfig.getConnectionTimeoutMilliseconds()) {
			builder.connectionTimeoutMs(zkConfig.getConnectionTimeoutMilliseconds());
		}
		if (!Strings.isNullOrEmpty(zkConfig.getDigest())) {
			builder.authorization("digest", zkConfig.getDigest().getBytes(Charsets.UTF_8))
					.aclProvider(new ACLProvider() {

						@Override
						public List<ACL> getDefaultAcl() {
							return ZooDefs.Ids.CREATOR_ALL_ACL;
						}

						@Override
						public List<ACL> getAclForPath(final String path) {
							return ZooDefs.Ids.CREATOR_ALL_ACL;
						}
					});
		}
		CuratorFramework curatorFramework = builder.build();
		curatorFramework.start();
		try {
			if (!curatorFramework.blockUntilConnected(zkConfig.getMaxSleepTimeMilliseconds() * zkConfig.getMaxRetries(), TimeUnit.MILLISECONDS)) {
				curatorFramework.close();
				throw new KeeperException.OperationTimeoutException();
			}
		} catch (Exception e) {
			logger.error("zk exception", e);
		}
		return curatorFramework;
	}

	@Bean
	public ZookeeperContext zookeeperContext() {
		return new ZookeeperContext();
	}

}
