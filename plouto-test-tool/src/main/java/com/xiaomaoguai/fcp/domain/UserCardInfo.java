/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Map;
/**
 * 用户的卡信息
 * @author THINK
 *
 */
@Getter
@Setter
public class UserCardInfo extends User {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5456087617615894478L;

	/**
	 * 订单编号
	 */
	private String orderNo;

	/**
	 * 绑卡申请orderNo
	 */
	private String applyBindOrderNo;

	/**
	 * 发卡行名称
	 */
	private String bankName;

	/**
	 * 银行编码
	 */
	private String bankCode;

	/**
	 * 银行开户姓名
	 */
//	@JsonIgnore
//	private String userName;

	/**
	 * 银行开户人身份证号
	 */
//	@JsonIgnore
//	private String certNo;

	/**
	 * 银行账号
	 */

	private String bankCardNo;

	/**
	 * 银行预留手机号
	 */

	private String cardPhone;

	/**
	 * 是否默认卡 0-否 1-是
	 */
	@NotNull(message = "默认卡标识不能为空")
	@Range(min = 0, max = 1, message = "默认卡标识，必须0-1")
	private Integer isDefault;

	/**
	 * 卡类型 1-借记 2-贷记
	 */
	private Integer cardType;

	//返回参数

	/**
	 * 0 - 未鉴权OTP、未绑卡，验证码必传 1 - 已绑卡
	 */
	private Integer cardExist;

	//请求参数

	private String fundCode;

	/**
	 * 验证码 账户系统未绑卡必传
	 */
	private String mobileMessage;
	
	/**
	 * r360-流程阶段标志
	 */
	private Integer progressStage;

	/**
	 * 创建者
	 */
	private String creator;
	
	/**
	 * 创建时间
	 */
	private Date gmtCreated;

	/**
	 * 修改人
	 */
	private String modifier;

	/**
	 * 修改时间
	 */
	private Date gmtModified;

	/**
	 * 是否删除
	 */
	private String isDeleted;

	@Override
	public String toString() {
		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> map = Maps.newHashMapWithExpectedSize(3);
		map.put("bankCardNo", this.bankCardNo);
		map.put("bankName", this.bankName);
		map.put("cardType", this.cardType);
		String string = null;
		try {
			string = objectMapper.writeValueAsString(map);
		} catch (JsonProcessingException e) {
			//ignore
		}
		return string;
	}
}