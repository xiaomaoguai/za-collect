package com.xiaomaoguai.fcp.pre.kepler.redis.lock.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 加锁的Handler
 *
 * @fileName: Lock.java
 * @author: WeiHui
 * @date: 2018/12/31 19:43
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Inherited
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Lock {

	/**
	 * el 表达式组成的lockKey，适配console里面的加锁情景
	 *
	 * @return lockKey
	 */
	String value() default "";

    /**
     * el 表达式组成的lockKey
     *
     * @return lockKey
     */
    String[] lockKeyOfEl() default "";

    /**
     * av 表达式组成的 lockKey
     *
     * @return lockKey
     */
    String[] lockKeyOfAv() default "";

}
