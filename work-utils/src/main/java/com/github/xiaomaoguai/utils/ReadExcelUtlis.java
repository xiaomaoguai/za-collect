package com.github.xiaomaoguai.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/10 12:19
 * @since JDK 1.8
 */
@Slf4j
public class ReadExcelUtlis {

	private static final String CURL_TEMPLATE = "curl --header \"Content-Type:application/json\" -d {0}  http://localhost:8080/invoke";

	public static void main(String[] args) throws IOException {
		File files = new File("D:/51YND/DATA_TEST.xlsx");
		//读取excel
		UploadDataListener readListener = new UploadDataListener();
		EasyExcel.read(FileUtils.openInputStream(files), UploadData.class, readListener).sheet().doRead();

		//逾期数据
		List<UploadData> uploadData = readListener.getList();
		log.info("====>Excel文件数据条数：{}", uploadData.size());

		//组装请求逾期router的请求报文体
		List<String> routerParam = generatorRouterParam(uploadData);

		//生成文件
		generatorFile(routerParam);
	}

	private static void generatorFile(List<String> routerParam) throws IOException {
		List<String> lines = Lists.newArrayList();
		int counter = 1;
		//生成shell脚本请求命令体
		for (String s : routerParam) {
			String curlCmd = MessageFormat.format(CURL_TEMPLATE, "'" + s + "'");
			lines.add(curlCmd);
			if (lines.size() >= 100000) {
				String fileName = "D:/51YND/51ynd_data_" + counter + ".sh";
				File shFile = new File(fileName);
				FileUtils.touch(shFile);
				FileUtils.writeLines(shFile, "UTF-8", lines);
				lines.clear();
				counter++;
			}
		}
		String fileName = "D:/51YND/51ynd_data_" + counter + ".sh";
		File shFile = new File(fileName);
		FileUtils.touch(shFile);
		FileUtils.writeLines(shFile, "UTF-8", lines);
		lines.clear();
	}

	/**
	 * 生成odps查询的Sql语句
	 *
	 * @param uploadData 原始数据
	 */
	private static void generatorSql(List<UploadData> uploadData) {
		//select third_user_no,user_id from finance_odps_prd.za_data_fcp_fe_biz_kepler_user_account  where product_code='51YND' and  third_user_no in () and pt='20191009';
		String sql = "select third_user_no,user_id from finance_odps_prd.za_data_fcp_fe_biz_kepler_user_account  where product_code=`51YND` and  third_user_no in ({0}) and pt=`20191009`;";
		StringBuilder sb = new StringBuilder();
		//组合es返回数据以及excel数据
		for (UploadData item : uploadData) {
			sb.append("'");
			sb.append(item.getThirdUserNo());
			sb.append("'");
			sb.append(",");
		}
		String sqlParam = sb.toString();
		String select = MessageFormat.format(sql, sqlParam.substring(0, sqlParam.length() - 1));
		System.out.println(select);
	}

	private static List<String> generatorRouterParam(List<UploadData> list) {
		List<String> routerParam = Lists.newArrayList();
		for (UploadData uploadData : list) {
			Map<String, Object> param = Maps.newHashMapWithExpectedSize(16);
			param.put("routerName", "yndUploadRealRepayInfoRouter");
			param.put("transName", "batchRepay");
			param.put("isEarlySettle", 0);
			param.put("apiName", "repayPreApi");
			param.put("apiVersion", "1.0.0");
			String nowDate = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
			param.put("reqNo", nowDate);
			param.put("reqDate", nowDate);

			param.put("productCode", "51YND");
			param.put("thirdUserNo", uploadData.getThirdUserNo());
			param.put("repayOuterNo", "51YND" + UUID.randomUUID().toString().replaceAll("-", ""));
			param.put("loanOuterNo", uploadData.getLoanOuterNo());
			param.put("installmentNo", uploadData.getCurNo());
			param.put("repayAmount", uploadData.getPrincipal().add(uploadData.getInterest()));
			param.put("principal", uploadData.getPrincipal());
			param.put("interest", uploadData.getInterest());
			param.put("repayType", "OVERDUE_REPAY");
			param.put("repayMode", "PARTNER_REPAY");

			List<Map<String, Object>> repayWriteOffList = new ArrayList<>();
			Map<String, Object> repayWriteOffMap = Maps.newHashMapWithExpectedSize(1);
			repayWriteOffMap.put("paidInterest", uploadData.getInterest());
			repayWriteOffMap.put("paidPrincipal", uploadData.getPrincipal());
			repayWriteOffMap.put("actualRepayDate", uploadData.getActualRepayDate());
			repayWriteOffMap.put("currentFlag", 1);
			repayWriteOffMap.put("paidFee", 0.00);
			repayWriteOffMap.put("installmentNo", uploadData.getCurNo());
			repayWriteOffMap.put("repayType", 2);

			repayWriteOffList.add(repayWriteOffMap);
			param.put("repayWriteOffList", repayWriteOffList);
			param.put("reqMsg", JSON.toJSONString(param));
			String requestParam = JSON.toJSONString(param);

			//log.info("requestParam ====> {}", requestParam);
			routerParam.add(requestParam);
		}
		return routerParam;
	}

}
