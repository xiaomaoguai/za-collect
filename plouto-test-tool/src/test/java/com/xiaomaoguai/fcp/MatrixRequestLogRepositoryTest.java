package com.xiaomaoguai.fcp;

import com.xiaomaoguai.fcp.dao.MatrixRequestLogRepository;
import com.xiaomaoguai.fcp.dto.MatrixRequestLog;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.junit.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import javax.annotation.Resource;

import java.util.Iterator;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

/**
 * @fileName: MatrixRequestLogRepositoryTest.java
 * @author: WeiHui
 * @date: 2018/10/22 19:46
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class MatrixRequestLogRepositoryTest extends BaseTest {

	@Resource
	private MatrixRequestLogRepository matrixRequestLogRepository;

	@Test
	public void test() throws Exception {
		try {
			SearchQuery searchQuery = new NativeSearchQueryBuilder()
					.withQuery(matchAllQuery())
					.withIndices("fcp_fe_szbiz_pluto")
					.withTypes("matrix_request_log")
					.withPageable(PageRequest.of(0, 10))
					.build();

			Page<MatrixRequestLog> search = matrixRequestLogRepository.search(searchQuery);

			search.forEach(m -> log.info("====>{}", m));

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testDdd() throws Exception {
		try {
			QueryStringQueryBuilder builder = new QueryStringQueryBuilder("256237621965095");
			Iterable<MatrixRequestLog> searchResult = matrixRequestLogRepository.search(builder);
			Iterator<MatrixRequestLog> iterator = searchResult.iterator();
			int size = 0;
			while (iterator.hasNext()) {
				size++;
				MatrixRequestLog next = iterator.next();
			}
			System.out.println("size = " + size);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
