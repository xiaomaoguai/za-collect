/*
 * Copyright 2018 xiaomaoguai.com All right reserved. This software is the
 * confidential and proprietary information of xiaomaoguai.com ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with xiaomaoguai.com.
 */
package com.xiaomaoguai.fcp.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UserState {

	private Long id;

	private String productCode;

	private String thirdUserNo;

	private Long userId;

	private Long acctId;

	private Integer accessStep;

	private Integer creditStatus;

	private Integer isoverdue;

	private Integer isloaning;

	private Date creditApplyTime;

	private String creator;

	private Date gmtCreated;

	private String modifier;

	private Date gmtModified;

	private String isDeleted;

	private String addInfo;

	private Integer preCreditStatus;

	private Date preCreditApplyTime;

}