package com.xiaomaoguai.fcp.pre.plouto.mailbox.actor;

import com.dh.mailbox2.retrymailbox.strategy.DefaultStrategy;
import com.dh.mailbox2.wrapper.AbstractActor;
import com.dh.mailbox2.wrapper.MSG;
import lombok.extern.slf4j.Slf4j;

/**
 * 共用异步actor ，只要是在异步实现 ，都用这个actor
 *
 * @fileName: AsyncActor.java
 * @author: WeiHui
 * @date: 2018/10/16 17:40
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
public class AsyncActor extends AbstractActor {

	@Override
	@SuppressWarnings("unchecked")
	public void onReceive(MSG msg) {
		Object data = msg.getData();
		try {
			if (data instanceof AsyncData) {
				AsyncData asyncData = (AsyncData) data;
				AsyncRunnable asyncRunnable = asyncData.getAsyncRunnable();
				if (asyncRunnable == null) {
					throw new IllegalStateException("异步调用方法体暂未设置");
				} else {
					Object param = asyncData.getParam();
					asyncRunnable.run(param);
				}
			}
		} catch (Exception e) {
			log.error("异步任务运行异常,准备重试", e);
			callRetry(msg, new DefaultStrategy(), 5L);
		}
	}

}
