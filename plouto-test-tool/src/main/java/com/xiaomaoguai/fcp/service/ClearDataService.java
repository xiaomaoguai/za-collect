package com.xiaomaoguai.fcp.service;

import com.xiaomaoguai.fcp.dto.ResponseModel;
import com.xiaomaoguai.fcp.dto.resp.ClearDataResp;

/**
 * @fileName: ClearDataService.java
 * @author: WeiHui
 * @date: 2018/10/25 10:22
 * @version: v1.0.0
 * @since JDK 1.8
 */
public interface ClearDataService {

	/**
	 * 清理授信数据
	 *
	 * @param userId 用户id
	 * @return 清除条数
	 */
	int clearCreditDataByUserId(Long userId);

	/**
	 * 清理借款数据
	 *
	 * @param userId 用户id
	 * @return 清除条数
	 */
	int clearLoanDataByUserId(Long userId);

	/**
	 * 清理还款数据
	 *
	 * @param userId 用户id
	 * @return 清除条数
	 */
	int clearRepayDataByUserId(Long userId);

	/**
	 * 根据userId来删除数据
	 *
	 * @param userId userId
	 * @return
	 */
	ClearDataResp clearByUserIdAndType(Long userId, String type);

	/**
	 * 根据userId来删除数据
	 *
	 * @param thirdUserNo thirdUserNo
	 * @param channelId   channelId
	 * @return
	 */
	ResponseModel<Object> clearByThirdUserNo(String thirdUserNo, String channelId);

	/**
	 * 根据userId来删除数据
	 *
	 * @param userId    userId
	 * @param channelId 渠道id
	 * @return
	 */
	ResponseModel<Object> clearByUserMobile(Long userId, String channelId);

}
