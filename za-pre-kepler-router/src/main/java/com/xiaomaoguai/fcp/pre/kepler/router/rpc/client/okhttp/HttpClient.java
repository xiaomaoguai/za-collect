package com.xiaomaoguai.fcp.pre.kepler.router.rpc.client.okhttp;

import java.util.concurrent.TimeUnit;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.constants.HttpConstants;
import com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums.HttpMethod;

import lombok.extern.slf4j.Slf4j;
import okhttp3.ConnectionPool;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.logging.HttpLoggingInterceptor.Level;

@Slf4j
public class HttpClient {

	private static class SingletonOkHttpClient {
		private static final OkHttpClient okHttp = new OkHttpClient.Builder()
				.readTimeout(15 * 1000, TimeUnit.MILLISECONDS)
				.addNetworkInterceptor(new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
					@Override
					public void log(String message) {
						 log.debug("okHttpLogInfo:{}", message);
					}
				}).setLevel(Level.BODY))
				.connectionPool(new ConnectionPool(10, 5, TimeUnit.MINUTES)).build();
	}

	public static OkHttpClient getOkHttp() {
		return SingletonOkHttpClient.okHttp;
	}

	/**
	 * Start a GET request
	 */
	public static RequestBuilder get(String url) {
		return newRequest(HttpMethod.GET, url,getOkHttp());
	}

	/**
	 * Start a POST request
	 */
	public static RequestBuilder post(String url) {
		return newRequest(HttpMethod.POST, url,getOkHttp());
	}

	/**
	 * Start a PUT request
	 */
	public static RequestBuilder put(String url) {
		return newRequest(HttpMethod.PUT, url,getOkHttp());
	}

	/**
	 * Start a DELETE request
	 */
	public static RequestBuilder delete(String url) {
		return newRequest(HttpMethod.DELETE, url,getOkHttp());
	}

	/**
	 * Start a HEAD request
	 */
	public static RequestBuilder head(String url) {
		return newRequest(HttpMethod.HEAD, url,getOkHttp());
	}

	/**
	 * Start a PATCH request
	 */
	public static RequestBuilder patch(String url) {
		return newRequest(HttpMethod.PATCH, url,getOkHttp());
	}
	

	/**
	 * Start a GET request
	 */
	public static RequestBuilder get(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.GET, url ,okHttpClient);
	}

	/**
	 * Start a POST request
	 */
	public static RequestBuilder post(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.POST, url ,okHttpClient);
	}

	/**
	 * Start a PUT request
	 */
	public static RequestBuilder put(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.PUT, url ,okHttpClient);
	}

	/**
	 * Start a DELETE request
	 */
	public static RequestBuilder delete(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.DELETE, url,okHttpClient);
	}

	/**
	 * Start a HEAD request
	 */
	public static RequestBuilder head(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.HEAD, url,okHttpClient);
	}

	/**
	 * Start a PATCH request
	 */
	public static RequestBuilder patch(String url,OkHttpClient okHttpClient) {
		return newRequest(HttpMethod.PATCH, url,okHttpClient);
	}
	
	/**
	 * 创建一个okhttpRequest (默认okHttpclient)
	 */
	public static RequestBuilder newRequest(HttpMethod method, String url) {
		return new RequestBuilder().okHttpClient(getOkHttp()).method(method).url(url);
	}

	/**
	 * 创建一个okhttpRequest
	 */
	public static RequestBuilder newRequest(HttpMethod method, String url,OkHttpClient okHttpClient) {
		return new RequestBuilder().okHttpClient(okHttpClient).method(method).url(url);
	}

	public static void main(String[] args){
		MediaType type = MediaType.parse(HttpConstants.HEADER_CONTENT_TYPE_JSON);
		System.out.println(type.type() +"  " +type.subtype() +"  "+type.charset());
	}
}
