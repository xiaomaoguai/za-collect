package com.dh.convert.mapping;

import com.dh.convert.node.BeanNode;
import com.dh.convert.node.TreeNode;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class BeanMapping {

	protected volatile Class<?> targetClassObject;
	protected volatile TreeNode targetNode;

	public void init(Class<?> targetClassObject, List<BeanMappingObject> beanMappingObejcts) {
		TreeNode newTargetNode = BeanNode.object2Node(targetClassObject);
		for (BeanMappingObject beanMappingObject : beanMappingObejcts) {
			beanMappingObject.buildValidator();
			beanMappingObject.compile();
			beanMappingObject.parseSrcField();
			if (StringUtils.isBlank(beanMappingObject.getSrcField()) && beanMappingObject.getExpression() != null) {
				beanMappingObject.setFirstEl(Boolean.TRUE);
			} else {
				beanMappingObject.setFirstEl(Boolean.FALSE);
			}
			newTargetNode.bulidMappingNode(beanMappingObject.getTargetField(), beanMappingObject);
		}
		this.targetClassObject = targetClassObject;
		this.targetNode = newTargetNode;
	}

	public Class<?> getTargetClassObject() {
		return targetClassObject;
	}

	public TreeNode getTargetNode() {
		return targetNode;
	}

	@Override
	public String toString() {
		return "BeanMapping [ targetClassObject=" + targetClassObject + ", targetNode=" + targetNode + "]";
	}
}
