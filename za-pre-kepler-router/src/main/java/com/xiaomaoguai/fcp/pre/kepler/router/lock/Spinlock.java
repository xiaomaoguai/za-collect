package com.xiaomaoguai.fcp.pre.kepler.router.lock;

import java.util.concurrent.atomic.AtomicBoolean;

public class Spinlock {

	private AtomicBoolean lock = new AtomicBoolean(false);

	public void lock() {
		while (true) {
			if (lock.compareAndSet(false, true)) {
				break;
			}
		}
	}

	public void wait(boolean isLock) {
		while (true) {
			if (lock.compareAndSet(isLock, isLock)) {
				break;
			}
		}
	}

	public boolean isLock() {
		return lock.get();
	}

	public void unlock() {
		lock.set(false);
	}
}
