package com.xiaomaoguai.boot.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 调账申请表
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AccountApply implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 产品编码
     */
    private String productCode;

    /**
     * 内部借款流水号
     */
    private String loanInnerNo;

    /**
     * 还款流水号
     */
    private String repayOuterNo;

    /**
     * 流水号，服务端用`acct_id`,`biz_sys`,`out_txn_no`做唯一键
     */
    private String outTxnNo;

    /**
     * 账务调账类型1:调减保证金账户  2:调减服务费  3:调增保证金 4:调增服务费 5:调增储备金 6调减储备金
     */
    private Integer accountType;

    /**
     * 账务调整状态 1:处理中 2:失败 3:成功
     */
    private Integer accountStatus;

    /**
     * 账务调账的请求
     */
    private String accountReqMapJson;

    /**
     * 备注，存储失败的原因
     */
    private String remark;

    /**
     * 创建时间
     */
    private LocalDateTime gmtCreated;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 更新时间
     */
    private LocalDateTime gmtModified;

    /**
     * 修改者
     */
    private String modifier;

    /**
     * 是否删除
     */
    private String isDeleted;


}
