package com.dh.convert.rule.validator;

import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

@Validator("CertNo")
public class CertNoValidator extends AbstractValidator<String> {
	private static final Pattern CERTNO_REG = Pattern.compile(
			"(^[1-9]\\d{5}(18|19|([23]\\d))\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$)|(^[1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{2}[0-9Xx]$)");
	private String regexp;

	public void initialize() {
	}

	public boolean isValid(String value) {
		if (StringUtils.isBlank(value))
			return true;
		if (StringUtils.isBlank(regexp))
			return CERTNO_REG.matcher(value).matches();
		else
			return Pattern.compile(regexp).matcher(value).matches();
	}
}
