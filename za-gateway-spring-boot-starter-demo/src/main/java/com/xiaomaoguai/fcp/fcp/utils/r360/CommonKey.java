package com.xiaomaoguai.fcp.fcp.utils.r360;

import com.github.xiaomaoguai.xxr.utils.http.HttpClientUtil;
import com.github.xiaomaoguai.xxr.utils.json.JsonUtils;
import org.apache.http.HttpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @fileName: CommonKey.java
 * @author: WeiHui
 * @date: 2018/9/20 16:20
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class CommonKey {

	private static final Logger log = LoggerFactory.getLogger(CommonKey.class);

	public static final String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAMDjAi0VDSmopCqJdfWRA3PXxiSt4pH1ZvCrVWKKqiCgxTQpkRH8JgQpYx5wgTcCYIk74/P9Zq9V9YU9XkcfFcfBkfQLFHoTCY+AL7QtcFSeOxTV2XPM7pIhv01hjJp4V7BDRjOV4aE4GQpqWBoXcvgvQ5u1IBtSUgsOJ0Y1KN4LAgMBAAECgYEAn3/YP9FFlio2vln26f5lHXjTB2emkE2eNutm6tqBPgPTroOky3t3MD/ND9G2d/eGPlUA9bSYIerx4KDTgfpJdfqrTm8KmCosdfiIk/NK7cnk+YsmNkIJcxcIql8DPx9hxBPD0HQksuvi0FN16JJ6wrYwa+DZiKOeaqaU/wkl0iECQQDgll2QekMSEXwwRwYLcsU4EKtwFt6g6WUfdemkvtWUtosMOyiT1mu2WtM1nDr2kI6YfCpxFuTzBnptkBOR9gubAkEA292P/wMRwKaZAIbQUx1LbadzPJj5GWjbqW1nAgih5/KWVHpefi+f+tOBTGPMwqCyrnRUoOLcn3pbs4LmtJ22UQJAJSFW+LLt8cfIQ2cKncvsxTckUNTHG81adZgV97HBN+PyGySQsdhqWGytrJhWuzrtCFLG5YmGcpNonEKMqb4G7wJAPpBwvldIQPLnTPAP7ebDJQPmVdMN5z4ga7j2++wq7k7omkdsBipVMtHm1C7+AFAsKFpwDowfzb3KxG02ayUJ0QJBAKxiaRRH7QFanULNnWB/n0nZ84ISEg0QPDnPl/GQDrPKCP/0tcPC0Z4bS6GEHNZuqzNNyALfx8JD00/Uq2/1er0=";

	public static final String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDA4wItFQ0pqKQqiXX1kQNz18YkreKR9Wbwq1ViiqogoMU0KZER/CYEKWMecIE3AmCJO+Pz/WavVfWFPV5HHxXHwZH0CxR6EwmPgC+0LXBUnjsU1dlzzO6SIb9NYYyaeFewQ0YzleGhOBkKalgaF3L4L0ObtSAbUlILDidGNSjeCwIDAQAB";

	public static final String matrix = "http://10.253.144.160:9999/thirdParty/r360/user/check";


	public static void main(String[] args) throws Exception {
		Map<String, Object> bizParam = new HashMap<>();
		bizParam.put("md5", "123456");
		bizParam.put("user_name", "测试人");
		bizParam.put("product_id", 19644);

		String value = JsonUtils.toJsonString(bizParam);
		String sign = Base64Utils.encode(RSAUtils.generateSHA1withRSASigature(value, privateKey));

		Map<String, String> params = new HashMap<>(2);
		params.put("biz_data", value);
		params.put("sign", sign);

		String reqJson = JsonUtils.toJsonString(params);
		log.info("====>请求数据:  {}", JsonUtils.formatJson(reqJson));


		ExecutorService executorService = new ThreadPoolExecutor(100, 100,
				0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>());

		for (int i = 0; i < 100_000; i++) {
			executorService.submit(() -> {
				try {
					long start = System.currentTimeMillis();
					String post = HttpClientUtil.doPost(matrix, reqJson);
					long end = System.currentTimeMillis();
					log.info("====>请求结果:  {}, {} ms", post, (end - start));
				} catch (HttpException e) {
					e.printStackTrace();
				}
			});
		}
	}

}
