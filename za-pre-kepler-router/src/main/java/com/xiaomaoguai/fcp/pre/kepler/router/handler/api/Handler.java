package com.xiaomaoguai.fcp.pre.kepler.router.handler.api;

import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.HandlerInfo;

public interface Handler<I> {

	/**
	 * 前置处理
	 * 如内部报文校验、处理
	 *
	 * @param hc
	 */
	void beforeHandle(HandlerContext<I> hc);

	/**
	 * 具体业务处理
	 *
	 * @param hc
	 */
	void handle(HandlerContext<I> hc);

	/**
	 * 创建handler内部域
	 * 多用于buf ----》innerParam
	 *
	 * @return
	 */
	void genInnerParam(HandlerContext<I> hc);

	/**
	 * 异常处理
	 *
	 * @param cause
	 */
	void exceptionCaught(RouterException cause);

	/**
	 * 后置处理
	 *
	 * @param hc
	 */
	void handleCompleted(HandlerContext<I> hc);
	
	/**
	 * 获取handler信息
	 * @return
	 */
	HandlerInfo getHandlerInfo();

	/**
	 * 填充handler信息
	 * @param handlerInfo
	 */
	void setHandlerInfo(HandlerInfo handlerInfo);
	
	/**
	 * 是否批量转联机
	 *
	 * @return boolean
	 */
	boolean isB2O();
}
