package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import com.xiaomaoguai.fcp.pre.kepler.ons.anno.EnableOnsSender;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.annotation.Role;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.lang.Nullable;

/**
 * @fileName: ProxyOnsConfiguration.java
 * @author: WeiHui
 * @date: 2019/2/28 11:00
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Configuration
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ProxyOnsConfiguration implements ImportAware {

	@Nullable
	private AnnotationAttributes enableOnsSender;

	@Override
	public void setImportMetadata(AnnotationMetadata importMetadata) {
		this.enableOnsSender = AnnotationAttributes.fromMap(
				importMetadata.getAnnotationAttributes(EnableOnsSender.class.getName(), false));
		if (this.enableOnsSender == null) {
			throw new IllegalArgumentException(
					"@EnableOnsSender is not present on importing class " + importMetadata.getClassName());
		}
	}

}
