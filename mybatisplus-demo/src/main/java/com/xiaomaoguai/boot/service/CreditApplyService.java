package com.xiaomaoguai.boot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiaomaoguai.boot.entity.CreditApply;

/**
 * <p>
 * 授信申请表 服务类
 * </p>
 *
 * @author WeiHui
 * @since 2018-08-04
 */
public interface CreditApplyService extends IService<CreditApply> {

}
