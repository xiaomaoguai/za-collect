package com.xiaomaoguai.fcp.pre.kepler.ons.configuration;

import com.xiaomaoguai.fcp.pre.kepler.ons.anno.EnableOnsSender;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.AdviceModeImportSelector;
import org.springframework.context.annotation.AutoProxyRegistrar;
import org.springframework.lang.Nullable;

/**
 * @fileName: OnsConfigurationSelector.java
 * @author: WeiHui
 * @date: 2019/2/28 10:57
 * @version: v1.0.0
 * @since JDK 1.8
 */
public class OnsConfigurationSelector extends AdviceModeImportSelector<EnableOnsSender> {

	/**
	 * Returns {@link ProxyOnsConfiguration} or {@code AspectJAsyncConfiguration}
	 * for {@code PROXY} and {@code ASPECTJ} values of {@link com.xiaomaoguai.fcp.pre.kepler.ons.anno.EnableOnsSender#mode()},
	 * respectively.
	 */
	@Override
	@Nullable
	protected String[] selectImports(AdviceMode adviceMode) {
		switch (adviceMode) {
			case PROXY:
				return new String[]{AutoProxyRegistrar.class.getName(),
						ProxyOnsConfiguration.class.getName()};
			case ASPECTJ:
				return new String[]{};
			default:
				return null;
		}
	}

}
