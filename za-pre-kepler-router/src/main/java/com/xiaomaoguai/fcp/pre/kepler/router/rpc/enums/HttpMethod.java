package com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public enum HttpMethod {

	GET, POST, PUT, HEAD, DELETE, PATCH, TRACE, OPTIONS, CONNECT;

	private static final Map<String, HttpMethod> mappings = new HashMap<>(16);

	static {
		for (HttpMethod httpMethod : values()) {
			mappings.put(httpMethod.name(), httpMethod);
		}
	}


	@Nullable
	public static HttpMethod resolve(@Nullable String method) {
		return (method != null ? mappings.get(method) : null);
	}


	public boolean matches(String method) {
		return (this == resolve(method));
	}
}
