package com.xiaomaoguai.fcp.pre.kepler.router.rpc.server.channelInitializer;


import com.xiaomaoguai.fcp.pre.kepler.router.rpc.enums.Protocol;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.util.concurrent.DefaultEventExecutorGroup;

public abstract class NettyChannelInitializer extends ChannelInitializer<SocketChannel> {

	protected Integer timeout;

	protected Integer nProcessors;

	protected DefaultEventExecutorGroup defaultEventExecutorGroup;

	protected Protocol protocol;

	protected NettyChannelInitializer(Protocol protocol, Integer timeout, Integer nProcessors) {
		this.protocol = protocol;
		this.timeout = timeout;
		this.nProcessors = nProcessors;
		this.defaultEventExecutorGroup = new DefaultEventExecutorGroup(nProcessors);
	}

	public Integer getTimeOut() {
		return timeout;
	}

	public Integer getnProcessors() {
		return nProcessors;
	}

	public Protocol getProtocol() {
		return protocol;
	}
}
