package com.xiaomaoguai.fcp.dto.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @fileName: ClearTypeEnum.java
 * @author: WeiHui
 * @date: 2018/10/26 14:01
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Getter
@AllArgsConstructor
public enum ClearTypeEnum {

	/**
	 *
	 */
	CREDIT("1"),

	/**
	 *
	 */
	LOAN("2"),

	/**
	 *
	 */
	REPAY("3"),

	/**
	 *
	 */
	ALL("4"),

	;

	/**
	 * 描述
	 */
	private final String code;

	/**
	 * cache
	 */
	private static Map<String, ClearTypeEnum> CACHE_HOLDER = new ConcurrentHashMap<>();

	/*
	 * init cache
	 */
	static {
		EnumSet.allOf(ClearTypeEnum.class).forEach(v -> CACHE_HOLDER.put(v.code, v));
	}

	/**
	 * getValue
	 *
	 * @param code code
	 * @return enum
	 */
	public static ClearTypeEnum getValue(String code) {
		if (StringUtils.isBlank(code)) {
			throw new IllegalArgumentException("清理数据类型不对");
		}
		return CACHE_HOLDER.get(code);
	}

}
