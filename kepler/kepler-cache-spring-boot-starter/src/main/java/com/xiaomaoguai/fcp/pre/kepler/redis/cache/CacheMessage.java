package com.xiaomaoguai.fcp.pre.kepler.redis.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * redis消息发布/订阅，传输的消息类
 *
 * @fileName: CacheMessage.java
 * @author: WeiHui
 * @date: 2018/4/28 10:22
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CacheMessage implements Serializable {

    /**
     * 序列化ID
     */
    private static final long serialVersionUID = 1815517713037923114L;

    /**
     * 缓存名称
     */
    private String cacheName;

    /**
     * 缓存key
     */
    private Object key;

}
