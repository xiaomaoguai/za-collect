package com.xiaomaoguai.fcp.pre.kepler.glue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 *
 * @author zhangweihui
 * @since 2018年12月17日 下午4:54:45
 */
@SpringBootApplication
public class GlueExampleApplication {

	/**
	 * 项目启动类
	 *
	 * @param args 启动参数
	 */
	public static void main(String[] args) {
		SpringApplication.run(GlueExampleApplication.class, args);
	}

}
