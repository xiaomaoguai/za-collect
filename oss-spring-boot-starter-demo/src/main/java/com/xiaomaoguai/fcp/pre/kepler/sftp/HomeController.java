package com.xiaomaoguai.fcp.pre.kepler.sftp;

import com.aliyuncs.auth.sts.AssumeRoleResponse;
import com.xiaomaoguai.fcp.pre.kepler.sftp.utils.OssClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author WeiHui
 * @date 2019/3/20 20:13
 * @since JDK 1.8
 */
@RestController
public class HomeController {

	@Resource
	private OssClient ossClient;

	@RequestMapping("/sts")
	public AssumeRoleResponse getSTS() {
		return ossClient.getOssInfo();
	}

}
