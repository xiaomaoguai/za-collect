package com.xiaomaoguai.fcp.pre.kepler.router.handler.api;

import java.util.List;

import com.xiaomaoguai.fcp.pre.kepler.router.buf.Buf;
import com.xiaomaoguai.fcp.pre.kepler.router.exception.RouterException;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.HandleResult;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.Instruction;
import com.xiaomaoguai.fcp.pre.kepler.router.handler.RouterInfo;
import com.xiaomaoguai.fcp.pre.kepler.router.router.RouterPipeline;

public interface HandlerContext<I> {

	/**
	 * 获取handler内部域
	 *
	 * @return
	 */
	I getInnerParam();

	/**
	 * 设置handler内部域
	 *
	 * @param innerParam
	 */
	void setInnerParam(I innerParam);

	/**
	 * 获取下一个handlerContext
	 *
	 * @return
	 */
	HandlerContext<?> getNext();

	/**
	 * 设置下一个handlerContext
	 *
	 * @param nextHandlercontext
	 * @return
	 */
	HandlerContext<?> setNext(HandlerContext<?> nextHandlercontext);

	/**
	 * 责任链调用
	 * @return 
	 */
	Buf invokeHandler();
	
	/**
	 * 异常调用处理
	 * @param cause
	 */
	void exceptionHandler(Throwable cause);

	/**
	 * 当前handler调用 执行 @see com.dh.ultron.handler.api.HandlerAnnotation 所有方法
	 */
	Instruction invokeCurrentHandler();

	/**
	 * 当前handler同步调用
	 * @return
	 */
	Instruction syncHandler();
	/**
	 * 获取handlerName
	 *
	 * @return
	 */
	String name();
	
	/**
	 * 获取handler order
	 *
	 * @return
	 */
	Integer order();

	/**
	 * 获取下一个handlerName
	 *
	 * @return
	 */
	String getNextHandlerName();

	/**
	 * 获取当前handler执行结果
	 *
	 * @return
	 * @see com.xiaomaoguai.fcp.pre.kepler.router.handler.enums.HandleResult
	 */
	HandleResult getHandlerResult();

	/**
	 * 设置当前handler处理结果
	 *
	 * @param handleResult
	 */
	void setHandlerResult(HandleResult handleResult);

	/**
	 * 获取当前handlerContext的handler
	 *
	 * @return
	 */
	Handler<I> getHandler();

	/**
	 * 设置当前handlerContext的handler
	 *
	 * @param handler
	 * @return
	 */
	HandlerContext<I> setHandler(Handler<I> handler);

	/**
	 * 根据handlerName获取handlerContext
	 *
	 * @param handlerName
	 * @return
	 */
	HandlerContext<?> getHandlerContext(String handlerName,Integer order);

	/**
	 * 获取handler信息
	 *
	 * @return
	 * @see RouterInfo
	 */
	RouterInfo getRouterInfo();

	/**
	 * 设置handler信息
	 *
	 * @param RouterInfo
	 * @return
	 */
	HandlerContext<I> setRouterInfo(RouterInfo RouterInfo);

	/**
	 * 获取最后一个handlerContext
	 *
	 * @return
	 */
	HandlerContext<?> getLast();

	/**
	 * 触发异常处理责任链
	 *
	 * @param cause
	 */
	void fireExceptionCaught(RouterException cause);

	/**
	 * 链式调用Rpchandler 
	 * 保证同一个app的多个handler连续调用，减少交互
	 */
	Instruction fireRpcHandlerContexts();

	/**
	 * 获取剩余handlercontext的routerInfo
	 * @param routerInfos
	 * @return
	 */
	List<RouterInfo>  getNextRouterInfos(List<RouterInfo> routerInfos);
	/**
	 * 获取责任链的buffer
	 *
	 * @return
	 */
	Buf getBuf();

	/**
	 * 替换buf
	 *
	 * @return
	 */
	Buf setNewBuf(Buf buf);

	/**
	 * 填充buffer键值对
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	Buf putBuf(String key, Object value);

	/**
	 * 获取url
	 * 
	 * @return
	 */
	String getUrl();

	/**
	 * 获取当前责任链
	 *
	 * @return
	 */
	RouterPipeline<?, ?> getRouter();

	boolean isRpc();
	
}
