package com.xiaomaoguai.fcp.pre.kepler.redis.lock.function;

/**
 * 使用锁执行方法的时候，使用的回调函数
 *
 * @author Jerry.Chen
 * @since 2018年5月9日 下午4:55:33
 */
public interface LockFunction extends BaseLockFunction {
	/**
	 * 获取锁成功的时候执行
	 */
	void onLockSuccess();

	/**
	 * 获取锁失败的时候执行
	 */
	void onLockFailed();
}
