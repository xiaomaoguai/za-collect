package com.xiaomaoguai.fcp.pre.kepler.oss.configuration;

import com.xiaomaoguai.fcp.pre.kepler.oss.constants.OssConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * @author WeiHui
 * @date 2019/1/30
 */
@Configuration
@EnableConfigurationProperties(MultiOssProperties.class)
@ConditionalOnProperty(prefix = OssConstants.MULTI_CONFIG_PREFIX, value = "enabled")
public class MultiOssAutoConfiguration {

	/**
	 * 初始化多个 ossClient 自动配置
	 *
	 * @param environment 环境变量属性
	 * @return OssClient 自动扫描注册器
	 */
	@Bean
	public MultiOssScannerConfigurer multiOssScannerConfigurer(Environment environment) {
		Binder binder = Binder.get(environment);
		MultiOssProperties properties = binder.bind(OssConstants.MULTI_CONFIG_PREFIX, MultiOssProperties.class).get();
		MultiOssScannerConfigurer multiOssScannerConfigurer = new MultiOssScannerConfigurer();
		multiOssScannerConfigurer.setMultiOssProperties(properties);
		return multiOssScannerConfigurer;
	}

}
