package com.xiaomaoguai.fcp.pre.kepler.redis.controller;

import com.xiaomaoguai.fcp.pre.kepler.redis.TestService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author August.Zhang
 * @version v1.0.0
 * @date 2019/11/15 11:02
 * @since JDK 1.8
 */
@RestController
@AllArgsConstructor
public class TestController {

	private final TestService testService;

	@GetMapping("/test")
	public String test(String name) {
		return testService.testCache(name);
	}

}
