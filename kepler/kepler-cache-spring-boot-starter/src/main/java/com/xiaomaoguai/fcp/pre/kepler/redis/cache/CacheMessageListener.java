package com.xiaomaoguai.fcp.pre.kepler.redis.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @fileName: CacheMessageListener.java
 * @author: WeiHui
 * @date: 2018/4/28 10:28
 * @version: v1.0.0
 * @since JDK 1.8
 */
@Slf4j
public class CacheMessageListener implements MessageListener {

    private RedisTemplate<Object, Object> redisTemplate;

    private RedisCaffeineCacheManager redisCaffeineCacheManager;

    public CacheMessageListener(RedisTemplate<Object, Object> redisTemplate, RedisCaffeineCacheManager redisCaffeineCacheManager) {
        this.redisTemplate = redisTemplate;
        this.redisCaffeineCacheManager = redisCaffeineCacheManager;
    }

    @Override
    public void onMessage(Message message, byte[] pattern) {
        CacheMessage cacheMessage = (CacheMessage) redisTemplate.getValueSerializer().deserialize(message.getBody());
        String cacheName = cacheMessage.getCacheName();
        Object key = cacheMessage.getKey();
        log.debug("receive a redis topic message, clear local cache, the cacheName is {}, the key is {}", cacheName, key);
        redisCaffeineCacheManager.clearLocal(cacheName, key);
    }

}
