package com.xiaomaoguai.fcp.fcp.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @fileName: TestController.java
 * @author: WeiHui
 * @date: 2018/8/6 12:31
 * @version: v1.0.0
 * @since JDK 1.8
 */
@RestController
@RequestMapping("test")
public class TestController {

	@GetMapping("/user")
	public User getUser(User user) {
		System.out.println(user);
		return user;
	}

}
