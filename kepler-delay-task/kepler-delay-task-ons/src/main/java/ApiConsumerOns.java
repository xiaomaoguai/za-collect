//import com.aliyun.openservices.ons.api.PropertyKeyConst;
//import com.xiaomaoguai.msg.api.bootstrap.MqClient;
//import com.xiaomaoguai.msg.api.common.SerializerTypeEnum;
//import com.xiaomaoguai.msg.api.consumer.AckAction;
//import com.xiaomaoguai.msg.api.consumer.MqMessageListener;
//import com.xiaomaoguai.msg.api.consumer.ReceiveRecord;
//import com.xiaomaoguai.msg.api.consumer.binding.ConsumerConfig;
//import com.xiaomaoguai.msg.sample.TestMessageBean;
//import lombok.extern.slf4j.Slf4j;
//
///**
// * @fileName: ApiConsumerOns.java
// * @author: WeiHui
// * @date: 2019/2/26 16:00
// * @version: v1.0.0
// * @since JDK 1.8
// */
//@Slf4j
//public class ApiConsumerOns {
//
//	public void testConsumerOrder() {
//		ConsumerConfig consumerConfig = new ConsumerConfig("ons|http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal"
//				,"D-TEST-BALANCE-180202", new MqMessageListener<TestMessageBean>(){
//
//			// 注意"<TestMessageBean>"必须与发送时的对象一致, 可以用byte[]自已处理序列化
//			@Override
//			public AckAction onMessage(ReceiveRecord<TestMessageBean> record) {
//				log.info("#### testConsumerMedium onMessage:" + record);
//				return AckAction.Commit;
//			}
//
//			@Override
//			public boolean isRedeliver(ReceiveRecord<TestMessageBean> record) {
//				return false;
//			}
//		});
//		consumerConfig.put(PropertyKeyConst.ConsumerId, "CID-D-TEST-BALANCE-180202");
//		consumerConfig.put(PropertyKeyConst.AccessKey, "LTAITPZlbVtAsEPr");
//		consumerConfig.put(PropertyKeyConst.SecretKey, "ef7khpMK8wVNbwTmmrndfd9fIgwvbE");
//		consumerConfig.setSerializer(SerializerTypeEnum.JSON);
//		consumerConfig.setTag("*");
//		MqClient.buildConsumer(consumerConfig);
//	}
//
//	public void testConsumerConcurrent() {
//		ConsumerConfig consumerConfig = new ConsumerConfig("ons|http://jbponsaddr-internal.aliyun.com:8080/rocketmq/nsaddr4client-internal"
//				,"D-TEST-KEPLER-TEST-KEPLER-190226", new MqMessageListener<TestMessageBean>(){
//			@Override
//			public AckAction onMessage(ReceiveRecord<TestMessageBean> record) {
//				log.info("#### testConsumerMedium onMessage:" + record);
//				return AckAction.Commit;
//			}
//
//			@Override
//			public boolean isRedeliver(ReceiveRecord<TestMessageBean> record) {
//				return false;
//			}
//		});
//		consumerConfig.put(PropertyKeyConst.ConsumerId, "CID-D-TEST-KEPLER-190226");
//		consumerConfig.put(PropertyKeyConst.AccessKey, "LTAIpGP7VrW41aZo");
//		consumerConfig.put(PropertyKeyConst.SecretKey, "0tsiVNTlzHSTT2z0nx3fAOLbULmRLY");
//		consumerConfig.setSerializer(SerializerTypeEnum.JSON);
//		consumerConfig.setNumThreads(10);
//		MqClient.buildConsumer(consumerConfig);
//	}
//
//	public static void main(String args[]) throws Exception {
//		ApiConsumerOns consumer = new ApiConsumerOns();
//		consumer.testConsumerConcurrent();
//		//consumer.testConsumerOrder();
//		System.in.read();
//
//	}
//}
