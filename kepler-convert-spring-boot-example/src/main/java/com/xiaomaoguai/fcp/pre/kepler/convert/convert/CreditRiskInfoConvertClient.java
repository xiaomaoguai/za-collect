package com.xiaomaoguai.fcp.pre.kepler.convert.convert;


import com.xiaomaoguai.fcp.pre.kepler.convert.anno.ConvertClient;
import com.xiaomaoguai.fcp.pre.kepler.convert.anno.ConvertNode;
import com.xiaomaoguai.fcp.pre.kepler.convert.anno.ConvertParam;
import com.xiaomaoguai.fcp.pre.kepler.convert.model.CreditApply;

/**
 * @author WeiHui
 * @date 2019/1/25
 */
@ConvertClient
public interface CreditRiskInfoConvertClient {

	/**
	 * 构建风控字段
	 *
	 * @param creditApply 授信参数
	 * @return 风控信息
	 */
	@ConvertNode("creditApply.productCode + '.default.extra.credit.riskInfo'")
	String buildRiskInfo(@ConvertParam("creditApply") CreditApply creditApply);

}
