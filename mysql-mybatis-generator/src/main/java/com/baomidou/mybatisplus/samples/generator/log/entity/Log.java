package com.baomidou.mybatisplus.samples.generator.log.entity;

import java.time.LocalDateTime;
import com.xiaomoaguai.generator.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangweihui
 * @since 2019-03-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Log extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 创建日期
     */
    private LocalDateTime createTime;

    /**
     * 描述
     */
    private String description;

    /**
     * 异常详情
     */
    private String exceptionDetail;

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 方法名称
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 请求ip
     */
    private String requestIp;

    /**
     * 耗时
     */
    private String time;

    /**
     * 操作用户
     */
    private String username;


}
