package com.xiaomaoguai.fcp.kepler.autoconfigure;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 众安开放api
 *
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/10/10 19:52
 * @since JDK 1.8
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ZaOpenAPi {

	String value();

}
