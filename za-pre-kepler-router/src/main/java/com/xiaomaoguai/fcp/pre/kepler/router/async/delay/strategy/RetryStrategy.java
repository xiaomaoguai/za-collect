package com.xiaomaoguai.fcp.pre.kepler.router.async.delay.strategy;

/**
 * 重试策略
 * 类DelayStrategy.java的实现描述： 类实现描述
 * @author DH 2017年5月6日 下午8:21:01
 */
public interface RetryStrategy {
    /**
     * 获取
     * @return
     */
    public Long interval(int retrytimes);
        

}
