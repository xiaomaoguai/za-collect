package com.xiaomaoguai.fcp.pre.kepler.config;

import com.alibaba.fastjson.JSONObject;
import com.xiaomaoguai.fcp.pre.kepler.zk.AfterUpdate;
import com.xiaomaoguai.fcp.pre.kepler.zk.ZkEventType;
import com.xiaomaoguai.fcp.pre.kepler.zk.integration.listener.AbstractPathNodeDataConsumer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author WeiHui
 * @date 2019/1/21
 */
@Slf4j
@Component
public class ProductConfigConsumer2 extends AbstractPathNodeDataConsumer {

	@AfterUpdate(ZkEventType.ADDED)
	public void init(JSONObject jsonObject) {
		log.info("====>ADDED:{}", jsonObject);
	}

	@AfterUpdate(ZkEventType.UPDATED)
	public void update(JSONObject jsonObject) {
		log.info("====>UPDATED: {}", jsonObject);
	}

	@AfterUpdate(ZkEventType.REMOVED)
	public void delete(JSONObject jsonObject) {
		log.info("====>REMOVED{}", jsonObject);
	}

}
