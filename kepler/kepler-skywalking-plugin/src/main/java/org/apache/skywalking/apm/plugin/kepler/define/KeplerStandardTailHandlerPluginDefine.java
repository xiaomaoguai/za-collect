package org.apache.skywalking.apm.plugin.kepler.define;

/**
 * @author WeiHui-Z
 * @version v1.0.0
 * @date 2019/5/17 20:41
 * @since JDK 1.8
 */
public class KeplerStandardTailHandlerPluginDefine extends AbstractKeplerStandardHandlerPluginDefine {

	/**
	 * Intercept class.
	 */
	private static final String INTERCEPT_CLASS = "org.apache.skywalking.apm.plugin.kepler.KeplerStandardTailHandlerInterceptor";

	/**
	 * enhance class.
	 */
	private static final String ENHANCE_CLASS = "com.xiaomaoguai.fcp.pre.kepler.router.handler.handlers.TailHandler";

	@Override
	protected String setInterceptClass() {
		return INTERCEPT_CLASS;
	}

	@Override
	protected String setEnhanceClass() {
		return ENHANCE_CLASS;
	}

}
