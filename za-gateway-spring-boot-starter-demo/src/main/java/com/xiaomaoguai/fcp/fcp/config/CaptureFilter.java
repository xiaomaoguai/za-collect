package com.xiaomaoguai.fcp.fcp.config;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@WebFilter(urlPatterns = { "/*" })
public class CaptureFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		String requestURI = httpServletRequest.getRequestURI();
		if (requestURI.endsWith("/swagger-resources") || requestURI.endsWith("/v2/api-docs")) {
			httpServletResponse.addHeader("Access-Control-Allow-Origin", "*");
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
	}

}